from php:5.5.33-fpm



RUN apt-get update && apt-get install -y \
        libmcrypt-dev git\ 
    && docker-php-ext-install -j$(nproc) mcrypt \ 
    && docker-php-ext-install   mysql mysqli pdo pdo_mysql


RUN apt-get install -y nginx 


RUN apt-get install -y cron


COPY nginx/template.conf /etc/nginx/conf.d/default.conf

RUN  apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
    
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

EXPOSE 80 
EXPOSE 443


