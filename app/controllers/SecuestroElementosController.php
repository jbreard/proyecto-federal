<?php

use Oficios\Repositories\PedidoSecuestroElementosRepo;

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\SecuestroElementosManager;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Managers\DocumentosManager;


class SecuestroElementosController extends \BaseController {

    protected $elementosRepo;
    protected $oficiosRepo;
    protected $personasRepo;
    protected $documentosRepo;


    public function __construct(PedidoSecuestroElementosRepo $elementosRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->elementosRepo        = $elementosRepo;
        $this->oficiosRepo          = $oficiosRepo;
        $this->personasRepo         = $personasRepo;
        $this->oficiosVistoManager  = $oficiosVistoManager;
        $this->documentosRepo       = $documentosRepo;
    }




    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $form_data = array('id'=>'form','route' => 'secuestro_elementos.store', 'method' => 'POST','enctype' => 'multipart/form-data');

        return View::make('formularios.secuestro_elementos',compact("form_data"));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function getPdf($id){



        $datos = $this->elementosRepo->getSecuestroElementos($id);

        $html = View::make('solo_lectura.pdf.secuestro_elementos',compact("datos"))->render();



        $this->oficiosRepo->getPdf($html);

    }


    public function store()
    {

        $data = Input::all();
        $oficios           = $this->oficiosRepo->newOficio(8);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);


        $titular            = $this->personasRepo->newPersona($oficios->id,5);
        $titularManager     = new PersonaManager($titular,$data);
        $titularManager->save();

        $secuestroElementos      = $this->elementosRepo->newSecuestroElementos($titular->id);
        $secuestroElementosManager = new SecuestroElementosManager($secuestroElementos,$data);
        $secuestroElementosManager ->save();

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$titular['id']);
        }

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $datos = $this->elementosRepo->getSecuestroElementos($id);

        return View::make('solo_lectura.secuestro_elementos',compact("datos"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {


        $form_data = array('route' => ['secuestro_elementos.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');

        $secuestro_elementos = new PedidoSecuestroElementosRepo();

        extract($secuestro_elementos->getSecuestroElementos($id));

       // print_r($secuestro_elementos);

        return View::make('formularios.secuestro_elementos',compact("form_data","oficios","titular","archivos","secuestro_elementos","documentos"));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();

        extract($this->elementosRepo->getSecuestroElementos($id));

        $oficioManager      = new OficiosManager($oficios,$data);

        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);



        $titularManager     = new PersonaManager($titular,$data);

        $titularManager->save();


        $secuestroElementosManager = new SecuestroElementosManager($secuestro_elementos,$data);

        $secuestroElementosManager ->save();

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$titular['id']);

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }

}
