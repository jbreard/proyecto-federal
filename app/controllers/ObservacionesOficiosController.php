<?php


use Oficios\Clases\ArchivosManager;
use Oficios\Managers\ArchivosObservacionesManager;
use Oficios\Managers\ObservacionesOficiosManager;
use Oficios\Repositories\ArchivosObservacionesRepo;
use Oficios\Repositories\ObservacionesRepo;

class ObservacionesOficiosController extends BaseController{
    /**
     * @var Oficios\Repositories\ObservacionesRepo
     */
    private $observacionesRepo;


    /**
     * @var Oficios\Repositories\OficiosRepo
     */

    function __construct(ObservacionesRepo $observacionesRepo)
    {


        $this->observacionesRepo = $observacionesRepo;
    }


    public function store($id){
        $data = Input::all();
        if(!empty($data['observaciones']) or (!empty($data['adjunto']) )){
        $archivos = Input::file('adjunto');
//        dd($archivos);
        $oficioObservaciones = $this->observacionesRepo->newObservacion($id);
        $obsManager = new ObservacionesOficiosManager($oficioObservaciones,$data);
        $archivosObsManager  = new ArchivosObservacionesManager();
        $archivosObsRepo     = new ArchivosObservacionesRepo();
            if($obsManager->save()) {
                $id = $oficioObservaciones->id;
                $archivosManager = new ArchivosManager($id);
                $archivosManager->setBaseManager($archivosObsManager);
                $archivosManager->setBaseRepo($archivosObsRepo);
                $archivosManager->subir('adjunto');
                return Redirect::back()->with('mensa_ok','Observaciones agregadas correctamente');
            }
        } else{
            return Redirect::back()->with('mensa_ok','No se puede agregar Observaciones vacias');
        }
    }











}