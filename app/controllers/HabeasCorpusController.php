<?php


use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\OficiosVistoManager;

use Oficios\Managers\DocumentosManager;

class HabeasCorpusController extends \BaseController {

    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->titulo_oficio           = "Habeas Corpus";
        $this->documentosRepo          = $documentosRepo;
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data = array('route' => 'habeas_corpus.store','id'=>'form', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = 'Habeas Corpus / Informes';
        Session::push("plazo_hr",true);
        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio"));

    }


    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        $data               = Input::all();

        unset($data['id']);

        $oficios            = $this->oficiosRepo->newOficio(7);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $responsable        = $this->personasRepo->newPersona($oficios->id,2);
        $responsableManager = new PersonaManager($responsable,$data);
        $responsableManager->save();
        $responsableManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}

    public function  getPdf($id){
        $oficios        = $this->oficiosRepo->find($id);

        $responsable    = $this->personasRepo->getPersona($id,2);

        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;

        Session::push("plazo_hr",true);

        $html = View::make('solo_lectura.pdf.pedidos_captura',compact("titulo_oficio","datos"))->render();


        return $this->oficiosRepo->getPdf($html);

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $oficios        = $this->oficiosRepo->find($id);


        $responsable    = $this->personasRepo->getPersona($id,2);

        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;

        Session::push("plazo_hr",true);

        return View::make('solo_lectura.pedidos_captura',compact("titulo_oficio","datos"));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $form_data = array('route' => ['habeas_corpus.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
        $titulo_oficio = $this->titulo_oficio;
        $oficios = $this->oficiosRepo->find($id);
        $this->oficiosVistoManager->save($id);
        $persona        = new PersonasRepo();
        $responsable    = $persona->getPersona($id,2);
        $documentos     = $responsable->getDocumentos;

        Session::push("plazo_hr",true);
//          $oficios = new PersonasRepo();
//
//          $oficios = $oficios->find('21');

        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio","oficios","documentos"));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();
        $oOficios = new OficiosRepo();
        $oficios = $oOficios->find($id);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $oPersona = new PersonasRepo();
        $persona  = $oPersona->getPersona($oficios->id,2);
        $personaManager      = new PersonaManager($persona,$data);
        $personaManager->save();

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }
        $this->guardarDocumentos($dataDocumentos,$persona['id']);

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");


    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }


}
