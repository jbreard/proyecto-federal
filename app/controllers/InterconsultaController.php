<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Interconsultas\Clients\Persona;
use Interconsultas\Clients\Vehiculos;
use Interconsultas\Clients\Armas;
use Interconsultas\Clients\Coordenadas;
use Interconsultas\Repositories\InterconsultasRepo;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Psr7\Request;

class InterconsultaController extends \BaseController {

    public function __construct()
    {
        if(! \Auth::user()->permiso_interconsultas)
            App::abort(404, 'No tienes autorización para estar aquí');
    }

    public function index() {

        $form_data = array('route' => 'interconsultas.buscar', "id"=>"formInterconsulta",'method' => 'GET','enctype' => 'multipart/form-data');

        return View::make('interconsultas.busqueda',compact("form_data"));
    }
    
    public function Buscar(){

        $options = Input::get('options');
        $dato[1]  = Input::get('dato');
        $dato[4]  = Input::get('dispositivo');
        if(Input::get('latitud')==""||Input::get('longitud')==""){
            $error = "<div class='alert alert-warning'>Debe habilitar la ubicación</div>";
           
           
            return  $error;
        }

        
        $ip_array = $this->getRealIP();//ip de Lan 
        $partes = explode(",", $ip_array);
        $dato[5] = $partes[0];
        $dato[2] = Input::get('latitud');
        $dato[3] = Input::get('longitud');
        
        
         // dd($dato);
        switch ($options) {
            case 'personas':
                $captchaInput = Input::get('captcha'); // Obtener el valor ingresado por el usuario

                    $captchaDataPath = storage_path('/captcha_code.json');

                    $captchaData = json_decode(file_get_contents($captchaDataPath), true);
                    if (isset($captchaData[\Auth::user()->id])) {
                        $storedCaptcha = $captchaData[\Auth::user()->id];
                    }else{
                        $error = "<div class='alert alert-warning'>Refresque el Captcha</div>";
                    return $error;
                    }
                 if ($captchaInput == $storedCaptcha) {

                        $servicios =  $this->personas($dato);
                        $this->deleteKeyCaptcha();
                } else {
                    $error = "<div class='alert alert-warning'>Captcha Incorrecto</div>";

                    return $error;
                }

                break;
            case 'armas':
                $servicios =  $this->armas($dato);
                break;
            case 'vehiculos':
                $captchaInput = Input::get('captcha'); // Obtener el valor ingresado por el usuario

                    $captchaDataPath = storage_path('/captcha_code.json');

                    $captchaData = json_decode(file_get_contents($captchaDataPath), true);
                    if (isset($captchaData[\Auth::user()->id])) {
                        $storedCaptcha = $captchaData[\Auth::user()->id];
                    }else{
                        $error = "<div class='alert alert-warning'>Refresque el Captcha</div>";
                    return $error;
                    }
                 if ($captchaInput == $storedCaptcha) {

                         $servicios =  $this->vehiculos($dato);
                        $this->deleteKeyCaptcha();
                } else {
                    $error = "<div class='alert alert-warning'>Captcha Incorrecto</div>";

                    return $error;
                }
               
                break;
            default:
                $servicios['error'][0] =  'Selecciones una opcion' ;
        }



        $indices = array_keys($servicios);
//var_dump($servicios);
        if(key_exists('status_code', $servicios)){
            echo "<div class='alert alert-warning'>"
                ."Sistema Saturado intente en unos minutos</div>";
            die;
        }
        //var_dump($servicios);die;
        /*if (array_key_exists('renaper', $servicios)) {
            $servicios['renaper'] =  $this->normalizarRenaper($servicios['renaper']);
        }*/
       
       return View::make('interconsultas.resultados',compact("servicios","indices","options"));
    }

    public function personas($datos){

        if($this->validar_dni($datos[1]))
        {
            $data = array(
                'err' => false,
                'err_code'=>'',
                'err_msg'=>'',
                'status_code' => Illuminate\Http\Response::HTTP_OK,
                'data' => []);

    	    try {
                $client = new Persona($datos);
                $repo = new InterconsultasRepo($client);
                $data['data'] = $repo->get("personas",['documento'=>$datos[1]]);
                //echo "aCA";
                  //              dd($data);

    	    } catch (Exception $e){
                //echo "dsad";
    	        $data['err'] = true;
    	        $data['err_code'] = $e->getCode();
    	        $data['err_msg'] = $e->getMessage();
    	        $data['status_code'] =  Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR;
                //dd($data);
                Log::info($data);
                Log::info($e->getTraceAsString());
                return  $data;
            }
            
           
                

        if($data['data'])
            $datas = $this->process($data['data']);
        //dd($datas);
            return $datas;
        $error['error'] = ['No se encontro resultados' ];
        return  $error;
        }
            $error['error'] = ['DNI no valido' ];
            return  $error;
    }

    public function armas($datos){
        $data = array(
            'err' => false,
            'err_code'=>'',
            'err_msg'=>'',
            'status_code' => Illuminate\Http\Response::HTTP_OK,
            'data' => []);

        try {

            $client = new Armas($datos);
            $repo = new InterconsultasRepo($client);
            $data['data'] = $repo->get("armas",['matricula'=>$datos[1]]);
        } catch (Exception $e){

            $data['err'] = true;
            $data['err_code'] = $e->getCode();
            $data['err_msg'] = $e->getMessage();
            $data['status_code'] =  Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR;
//dd($data);
           Log::info($data);
           Log::info($e->getTraceAsString());

            return  $data;
        }
        if($data['data'])
            $datas = $this->process($data['data']);
            return $datas;
        $error['error'] = ['No se encontro resultados' ];
        return  $error;
    }

    public function vehiculos($datos){
        $data = array(
            'err' => false,
            'err_code'=>'',
            'err_msg'=>'',
            'status_code' => Illuminate\Http\Response::HTTP_OK,
            'data' => []);

        try {
            $client = new Vehiculos($datos);
            $repo = new InterconsultasRepo($client);
            $data['data'] = $repo->get("vehiculos",['dominio'=>$datos[1]]);

        } catch (Exception $e){

            $data['err'] = true;
            $data['err_code'] = $e->getCode();
            $data['err_msg'] = $e->getMessage();
            $data['status_code'] =  Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR;
            //dd($data);
            Log::info($data);

            Log::info($e->getTraceAsString());
            return $data;
        }

        $datas = $this->process($data['data'],2);
        
        return  $datas;
        

        if($data['data'])
        if(empty($data['data']['sifcop_vehiculos'])){
            unset($data['data']['sifcop_vehiculos']);
        }
            return $data['data'];
        $error['error'] = ['No se encontro resultados' ];
        return  $error;
    }

    public function renaper()
    {
        /* parte del captcha agregar en un metodo aparte*/
        $captchaInput = Input::get('captcha'); // Obtener el valor ingresado por el usuario

        $captchaDataPath = storage_path('/captcha_code.json');

        $captchaData = json_decode(file_get_contents($captchaDataPath), true);
        if (isset($captchaData[\Auth::user()->id])) {
                        $storedCaptcha = $captchaData[\Auth::user()->id];
        }else{
                     $error = "<div class='alert alert-warning'>Refresque el Captcha</div>";
                    return $error;
            }
        if ($captchaInput == $storedCaptcha) {
                
                    $options = Input::get('options');
                    $dato[1]  = Input::get('dato');
                    $dato[4]  = Input::get('dispositivo');

                    if(Input::get('latitud')==""||Input::get('longitud')==""){
                        $error = "<div class='alert alert-warning'>Debe habilitar la ubicación</div>";
                        return  $error;
                    }

                    $ip_array = $this->getRealIP();//ip de Lan 
                    $partes = explode(",", $ip_array);
                    $dato[5] = $partes[0];
                    $dato[2] = Input::get('latitud');
                    $dato[3] = Input::get('longitud');
                    $dato[6] = "renaper";
            //dd($dato);
                    if($options && $dato)
                    {
                        $personas = $this->personas($dato);
                        $servicios['renaper'] =  $personas['renaper'];
                        $indices = array_keys($servicios);
                        return View::make('interconsultas.resultados',compact("servicios","indices","options"));
                    }
             $this->deleteKeyCaptcha();
        } else {
            $error = "<div class='alert alert-warning'>Captcha Incorrecto</div>";

            return $error;
        }    
    }
    public function anses()
    {
        $options = Input::get('options');
        $dato = Input::get('dato');
        if($options && $dato)
        {
            $personas = $this->personas($dato);
            if (array_key_exists('ansesrelaciones', $personas))
                $servicios['ansesrelaciones'] =  $personas['ansesrelaciones'];
            else
                $servicios = $personas;
        }else {
                $servicios['error'][0] = 'Complete los campos a buscar';
            }

        $indices = array_keys($servicios);
        return View::make('interconsultas.resultados',compact("servicios","indices","options"));
    }
    public function drnpa()
    {
        $options = Input::get('options');
        $dato = Input::get('dato');
        if($options && $dato)
        {
            $result = $this->vehiculos($dato);
            if (array_key_exists('dnrpa', $result))
                $servicios['dnrpa'] =  $result['dnrpa'];
            else
                $servicios = $result;
        }else{
                $servicios['error'][0] = 'Complete los campos a buscar';
            }

        $indices = array_keys($servicios);
        return View::make('interconsultas.resultados',compact("servicios","indices","options"));
    }

    public function migraciones()
    {
        $options = Input::get('options');
        $dato = Input::get('dato');
        if($options && $dato)
        {
            $personas = $this->personas($dato);
            if (array_key_exists('migraciones', $personas)) {
                $servicios['migraciones'] =  $personas['migraciones'];
            }
            elseif (array_key_exists('migracionesmovimientos', $personas)) {
                $servicios['migracionesmovimientos'] =  $personas['migracionesmovimientos'];
            }else {
                $servicios['error'][0] = 'No se encontro resultados';
            }
        }else{
                $servicios['error'][0] = 'Complete los campos a buscar';
        }
        $indices = array_keys($servicios);
        return View::make('interconsultas.resultados',compact("servicios","indices","options"));
    }
    
    public function validar_dni($dni){
        return true;
    //     if (strlen($dni) != 9) {
    //     return false;
    // }
    // /* Ajustamos las letras especiales "x", "y" y "z" */
    // switch(strtolower(substr($dni, 0, 1))) {
    //     case 'x':
    //         $dni = '0' + substr($dni, 1);
    //         break;
    //     case 'y':
    //         $dni = '1' + substr($dni, 1);
    //         break;
    //     case 'z':
    //         $dni = '2' + substr($dni, 1);
    //         break;
    //     }
    // $numero = intval(substr($dni, 0, strlen($dni) - 1)) % 23;
    // $letra = substr($dni, strlen($dni) - 1);
    // return $letra == substr('TRWAGMYFPDXBNJZSQVHLCKET', $numero, 1);
    }
    public function getRealIP() {
 
        if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            return $_SERVER["HTTP_CLIENT_IP"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
            return $_SERVER["HTTP_X_FORWARDED"];
        } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
            return $_SERVER["HTTP_FORWARDED"];
        } else {
            return $_SERVER["REMOTE_ADDR"];
        }
    }

    public function process($data,$tipo=null){
      // dd($data);
        $dato = array();
        foreach ($data as $index => $value){
            
            foreach($value as $ind => $val){
                if(!empty($val['data'])){
                    
                    switch ($ind) {
                    case "X-PFA":
                        $ind = "pfa";
                        break;
                    case "X-SIFCOPFILTRADO1-4":
                        $ind = "sifcop";
                        break;
                    case "X-RENAPER":
                        $ind = "renaper";
                        break;
                    case "X-Reincidencia":
                        $ind = "reincidencia";
                        break;
                    case "X-PNA":
                        $ind = "pna";
                        break;
                    case "X-REINCIDENCIA":
                        $ind = "reincidencia";
                        break;
                    case "X-SIMES":
                        $ind = "simes";
                        break;
                    case "X-SIFCOP":
                        if($tipo==2){
                            $ind="sifcop_vehiculos";
                        }else{
                            $ind = "sifcop";
                        }
                        
                        break;
                    case "X-SIFCOPTOTAL":
                        $ind = "sifcop";
                        break;    
                    case "X-GNA":
                        $ind = "gna";
                        break;
                    case "X-DNRPA":
                        $ind = "dnrpa";
                        break;
                    }

                    $dato[$ind]=$val['data'];
          
                }
            } 
        }
        //dd($dato);
        return $dato;
        
        
    }
        public function deleteKeyCaptcha(){
           //$captchaData[\Auth::user()->id] = "";
            $captchaDataPath = storage_path('/captcha_code.json');
            $captchaData = json_decode(file_get_contents($captchaDataPath), true);

                unset($captchaData[\Auth::user()->id]);
            file_put_contents($captchaDataPath, json_encode($captchaData));    
            
            return;
        }
                    }
