<?php
use Combos\Repositories\Organismos;
use Correos\Repositories\CorreoRepo;
use Illuminate\Support\Facades\Log;
use Oficios\Managers\ArchivosAdjuntosManager;
use Oficios\Managers\DestinatariosMensajesManager;
use Oficios\Managers\MensajesManager;
use Oficios\Managers\RespuestasMensajesManager;
use Oficios\Repositories\ArchivosMensajesRepo;
use Oficios\Repositories\DestinatariosMensajesRepo;
use Oficios\Repositories\MensajesRepo;
use Oficios\Repositories\MensajesVistosRepo;
use Oficios\Repositories\RespuestasMensajesRepo;
use Oficios\Repositories\GruposOrganismosRepo;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class EmailController extends BaseController {

    private $emails;
    private $mensajesRepo;
    private $destinatariosMensajesRepo;
    private $respuestasMensajesRepo;
    private $mensajesVistosRepo;
    private $opcionesMensajes;
    private $remitente;
    private $archivosMensajesRepo;
    private $adjuntos = [];
    private $data;
    private $organismosRepo;

    const PATH_ARCHIVOS_ADJUNTOS = 'archivos/archivos_adjuntos/';

    function __construct(CorreoRepo $emails,
                         MensajesRepo $mensajesRepo,
                         DestinatariosMensajesRepo $destinatariosMensajesRepo,
                         RespuestasMensajesRepo $respuestasMensajesRepo,
                         MensajesVistosRepo $mensajesVistosRepo,
                         ArchivosMensajesRepo $archivosMensajesRepo,
                         Organismos $organismosRepo,
                         GruposOrganismosRepo $gruposOrganismosRepo
    )
    {

        $this->emails = $emails;

        $this->mensajesRepo = $mensajesRepo;

        $this->destinatariosMensajesRepo = $destinatariosMensajesRepo;

        $this->respuestasMensajesRepo    = $respuestasMensajesRepo;

        $this->mensajesVistosRepo = $mensajesVistosRepo;

        $this->opcionesMensajes     = ["1"=>"Todos","2"=>"Leidos","3"=>"No Leido"];

        $this->archivosMensajesRepo = $archivosMensajesRepo;

        $this->data = null;

        $this->organismosRepo = $organismosRepo;
        $this->gruposOrganismosRepo = $gruposOrganismosRepo;
    }


    public function index(){

        $q                 = Input::get('q',null);
        $leidos            = Input::get('leidos',1);
        $mails             = $this->mensajesRepo->getRecibidosPaginate($leidos,15,$q);
        $form_data         = $this->formDataCreate();
        $mailsDirecciones  = $this->organismosRepo->getOrganismos(); //cambia el listado del mail por el listado de organismos 2015-06-10
        $opcionesMensajes  = $this->opcionesMensajes;
        $grupos            = $this->gruposOrganismosRepo->getGruposWithMiembros();

        $tagLeidos = true;
        $labelDestino = "Remitentes";
        return View::make('mails.index',compact("form_data","mails","mailsDirecciones","opcionesMensajes","leidos","tagLeidos","q","labelDestino","grupos"));

    }

    public function getEnviadosPaginate(){
        $page = Input::get('page');
        $form_data = $this->formDataCreate();
        $mails = $this->mensajesRepo->getEnvidos();
        $mailsDirecciones     = $this->organismosRepo->getOrganismos();
        $opcionesMensajes = false;
        $tagLeidos = false;
        $grupos            = $this->gruposOrganismosRepo->getGruposWithMiembros();
        $labelDestino = "Destinos";
        return View::make('mails.index',compact("form_data","mails","mailsDirecciones","opcionesMensajes","tagLeidos","labelDestino","grupos"));
    }

    public function getMailsPaginate(){
        $page      = Input::get('page');
        $leidos    = Input::get('leidos',1);
        $recibidos = Input::get('recibidos',true);
        $data      = Input::get('data',null);
        if($recibidos) {
            $mails = $this->mensajesRepo->getRecibidosPaginate($leidos,15,$data,$page);
            $tagLeidos = true;
            if (count($mails) != 0) return $this->renderListadoMails(compact("mails", "page", "tagLeidos"));
        }



        $mails = $this->mensajesRepo->getEnvidos(15,$data);

        $tagLeidos = false;
        if (count($mails) != 0) return $this->renderListadoMails(compact("mails", "page", "tagLeidos"));


    }

    public function renderListadoMails($datos){
        return View::make('mails.listadoMails',$datos);
    }


    public function actualizarMails(){
        $id = Input::get('id_ultimo',false);
        $tagLeidos = true;
        if($id) {
            $mails = $this->mensajesRepo->actualizarRecibidos(1,$id);
            return  $this->renderListadoMails(compact("mails","tagLeidos"));
        }
    }

    public function formMail(){
        $form_data = array('route' =>'mensajes.send','id'=>'formMail','method' => 'POST','id'=>'form_respuesta','enctype' => 'multipart/form-data');
        return View::make('mails.formulario',compact("form_data"));
    }

    public function listaMails(){
        $respuesta = array();
        $term = Input::get('term');
        $mails = $this->emails->getCorreoByTerm($term);
        foreach ($mails as $mail){
            $respuesta[] = $mail->mail;
        }
        return Response::json($respuesta);
    }


    public function sendMail()
    {

        $data = Input::all();

        $respuesta = ["success"=>false];
        if($this->saveMail($data)){
          if(Config::get('app.modulos.mensajes'))
            {
                $this->pushEmailsInQueue($data);
            }
            $respuesta["success"] = true;

        }

        return Response::json($respuesta);

    }


    public function saveMail($data)
    {
        $mensajesRepo = $this->mensajesRepo->newMensaje();
        $mensajesManager = new MensajesManager($mensajesRepo,$data);

        if($mensajesManager->save())
        {
            $idMensajeRepo = $mensajesRepo->id;
            $idDestinatarios  = $data['mails'];
            $this->saveDestinatariosMensajes($idDestinatarios, $idMensajeRepo);
            $this->processArchivosAdjuntos($idMensajeRepo);
            $this->remitente = "Remitente:"
                .$mensajesRepo->emisor->getJurisdiccion->descripcion." "
                .$mensajesRepo->emisor->nombre." "
                .$mensajesRepo->emisor->apellido;

            return true;
        }

        return false;
    }

    public function show($id){
        $form_data = array('route' => ['mensajes.respuesta',$id], 'id' => 'form_mail', 'method' => 'POST', 'enctype' => 'multipart/form-data');
        $mail  = $this->mensajesRepo->getWithRespuestas($id);
        $this->guardarMensajeVisto($id);
        $mailsDirecciones     = $this->emails->all();
        $labelDestino= null;
        $grupos            = $this->gruposOrganismosRepo->getGruposWithMiembros();

        return View::make('mails.respuestas',compact('mail','form_data','mailsDirecciones',"labelDestino","grupos"));
    }

    public function pdf($id){
        $mail  = $this->mensajesRepo->getWithRespuestas($id);
        $this->guardarMensajeVisto($id);
        $html = \View::make('mails.respuestas_pdf',compact('mail'))->render();

        $pdf = \View::make('solo_lectura.pdf.pdf_general', array("html" => $html))->render();
        //return $pdf;
        return \PDF::load($pdf, 'A4', 'portrait')->show();
    }

    public function formDataCreate()
    {
        $form_data = array('route' => 'mensajes.send', 'id' => 'form_mail', 'method' => 'POST', 'enctype' => 'multipart/form-data');
        return $form_data;
    }

    public function saveRespuesta($id){

        $data      = Input::all();

        $respuesta = $this->respuestasMensajesRepo->newRespuesta($id);

        $mail      = $this->mensajesRepo->find($id);

        $mail->updated_at = \Carbon::now();

        $respuestaManager = new RespuestasMensajesManager($respuesta,$data);

        if($respuestaManager->save())
        {
            $mesajesManager = new MensajesManager($mail,$data);
            $mesajesManager->save();
            if(Config::get('app.modulos.mensajes')){


            Mail::send('mails.cuerpo_respuestas',
                [
                    "mensaje_anterior"=>$mail->contenido,
                    "remitente"=>Auth::user(),
                    "contenido"=>$data["respuesta"]
                ],
                function($message) use($data,$mail){
                    $message->getheaders()->addTextHeader("Disposition-Notification-To",Auth::user()->getMail->mail);
                    $message->getheaders()->addTextHeader("X-Confirm-Reading-To",Auth::user()->getMail->mail);
                    $message->to($mail->emisor->getMail->mail)->subject($mail->asunto);
                }
            );
            }

            return Redirect::back()->with('mensa_ok','Mensaje Enviado Correctamente');
        };
    }

    public function  marcarLeido($id){
        if($this->guardarMensajeVisto($id)) return ["success"=>True];
        return ["success"=>false];
    }

    public function mensajesVistos($id){
        $mensajesVistos = $this->mensajesVistosRepo->where('id_mensaje',$id);
        return View::make('mails.mensajesUsuariosVistos',compact("mensajesVistos"));
    }

    public function guardarMensajeVisto($id)
    {

        $datos = ['id'=>$id,"id_organismo"=>\Auth::user()->id_jurisdiccion,'id_usuario'=>\Auth::user()->id];

        $destinatariosRepo  = $this->destinatariosMensajesRepo->getByMensajeOrg($datos);

        if(!empty($destinatariosRepo)) $this->setVisto($destinatariosRepo);

        return \Queue::push('Oficios\Clases\Mensajes@saveVisto',$datos);

    }

    public function setVisto(&$destinatariosRepo){
        $destinatariosRepo->leido  = 1;
        $dM  = new DestinatariosMensajesManager($destinatariosRepo,[]);
        $dM->save();
    }


    public function cantNoLeidos(){
        $idOrganismo = \Auth::user()->id_jurisdiccion;
        return $this->destinatariosMensajesRepo->getCantidadSinLeer($idOrganismo);
    }

    public function saveArchivosAdjuntos($idMensajesRepo)
    {
        try
        {
            if(!empty($this->adjuntos) && is_array($this->adjuntos))
            {
                foreach ($this->adjuntos as $adjunto) {
                    $archivosMensajesRepo = $this->archivosMensajesRepo->newArchivo($idMensajesRepo,$adjunto);
                    $archivosAdjuntosManager = new ArchivosAdjuntosManager($archivosMensajesRepo,[]);
                    $archivosAdjuntosManager->save();
                }
            } else {
                throw new Exception('El atributo \'$this->adjuntos\' puede estar vacio o no ser un array');
            }

        } catch (Exception $e) {
            Log::info('Excepción capturada: ' .  $e->getMessage() . "\n");
        }
    }

    private function pushEmailsInQueue($data)
    {
        $idDestinatarios  = $data['mails'];
        $queueData = [
            "data" => $data,
            "mails" => $this->emails->getMailByIdOrganismos($idDestinatarios),
            "remitente" => $this->remitente,
            "adjuntos" => $this->adjuntos
        ];

        return \Queue::push('Oficios\Clases\Mensajes@sendEmail', $queueData);
    }

    public function processArchivosAdjuntos($idMensajesRepo)
    {
        if (Input::hasFile('adjuntos'))
        {
            $adjuntos = Input::file('adjuntos');
            foreach ($adjuntos as $archivo) $this->moveArchivoAdjuntoToLocalPath($idMensajesRepo, $archivo);
        }

        $this->saveArchivosAdjuntos($idMensajesRepo);

        return $this;
    }

    public function mensajesBusqueda(){
        $this->data = Input::all();
        $mails = $this->mensajesRepo->buscar($this->data);
        $tagLeidos = false;
        if (count($mails) != 0) return $this->renderListadoMails(compact("mails", "page", "tagLeidos"));
    }

    public function saveDestinatariosMensajes($idDestinatarios, $idMensajeRepo)
    {
        foreach ($idDestinatarios as $idDestinatario) $this->saveDestinatario($idMensajeRepo, $idDestinatario);
    }

    private function saveDestinatario($idMensajeRepo, $idDestinatario)
    {
        $destinatariosRepo = $this->destinatariosMensajesRepo->newDestMen($idMensajeRepo, $idDestinatario);
        $destinatariosManager = new DestinatariosMensajesManager($destinatariosRepo, []);
        $destinatariosManager->save();
    }

    private function moveArchivoAdjuntoToLocalPath($idMensajesRepo, $archivo)
    {
        $fileName = md5(Carbon\Carbon::now() . $archivo->getClientOriginalName()) . "." . $archivo->getClientOriginalExtension();
        $destinationPath = self::PATH_ARCHIVOS_ADJUNTOS . $idMensajesRepo;
        $this->adjuntos[] = $destinationPath . "/" . $fileName;

        try {
            $archivo->move($destinationPath, $fileName);
        } catch (FileException $e) {
            Log::info('Excepcion capturada: ' . $e->getMessage() . "\n");
        }

    }
}
