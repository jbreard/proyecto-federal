<?php


use Combos\Repositories\PartidoRepo;
//use Combos\Repositories\LocalidadRepo;


class CombosController  extends BaseController{

  //  private $localidadRepo;
    private $partidoRepo;

//LocalidadRepo $localidadRepo
    public function __construct(PartidoRepo $partidoRepo){

        //$this->localidadRepo = $localidadRepo;
        $this->partidoRepo   = $partidoRepo;


    }
/*
    public function getLocalidad(){

       $data = Input::all();

       return $this->localidadRepo->getLocalidad($data["id_partido"]);

    }
*/
    public function getPartido(){

        $data = Input::all();

        return $this->partidoRepo->getPartido($data["id_provincia"]);
    }


} 