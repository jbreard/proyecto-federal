<?php

use Combos\Repositories\Organismos as OrganismosCombos;
use Organismos\Repositories\OrganismosRepo as OrganismosRepo;
use Organismos\Managers\OrganismosManager;
use Organismos\Entities\Organismos;

class OrganismoController extends \BaseController{

    protected $OrganismosCombos;

public function __construct(OrganismosRepo $organismoRepo, OrganismosCombos $OrganismosCombos){


    $this->organismoRepo    = $organismoRepo;
    $this->OrganismosCombos = $OrganismosCombos;
}   
    /**
*/
public function index()
{
    $org = Organismos::all();

    //dd($org);

    return View::make("organismo.listado", array('Organismo' => $org));




}


/**
* Show the form for creating a new resource.
*
* @return Response
*/
public function create()
{			
			$combosOrganismos = $this->OrganismosCombos->getCombo();
      	    $form_data  = array('route' => 'organismo.store', 'method' => 'POST','enctype' => 'multipart/form-data');
    		return View::make("organismo.create",compact("form_data","combosOrganismos"));

}


/**
* Store a newly created resource in storage.
*
* @return Response
*/
public function store()
{
	$data    = Input::all();

        $organismo           = $this->organismoRepo->newOrganismo();
        $OrganismoManager    = new OrganismosManager($organismo,$data);
        $OrganismoManager->save();

        return Redirect::route('panel.index');

}

/**
* Display the specified resource.
*
* @param  int  $id
* @return Response
*/
public function show($id)
{
   $organismo = $this->organismoRepo->find($id);
        if (is_null($organismo)) {
            App::abort(404);
        }
        $combosOrganismos = $this->OrganismosCombos->getCombo();
        $form_data  = array('route' => ['organismo.update',$id], 'method' => 'PATCH','enctype' => 'multipart/form-data');
        return View::make("organismo.create", compact("organismo","form_data","combosOrganismos"));

}


/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return Response
*/
public function edit($id)
{
        
}


/**
* Update the specified resource in storage.
*
* @param  int  $id
* @return Response
*/
public function update($id)
{
        $organismo = $this->organismoRepo->find($id);
        // Obtenemos la data enviada por el usuario
        $data = Input::all();


        $OrganismoManager    = new OrganismosManager($organismo,$data);
        $OrganismoManager->save();

        if($OrganismoManager->save()) return Redirect::route('organismo.index');

        return Redirect::back()->withInput()->withErrors($OrganismoManager->getErrors());
}


/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return Response
*/
public function destroy($id)
{
//
}


}
