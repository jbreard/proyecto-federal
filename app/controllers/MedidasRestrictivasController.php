<?php

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\MedidasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Managers\DocumentosManager;



class MedidasRestrictivasController extends \BaseController {

    protected  $oficiosRepo;
    protected  $personasRepo;
    protected  $medidasRepo;
    protected  $oficiosVistoManager;
    protected $documentosRepo;



    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,MedidasRepo $medidasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->oficiosRepo         = $oficiosRepo;
        $this->personasRepo        = $personasRepo;
        $this->medidasRepo         = $medidasRepo;
        $this->oficiosVistoManager = $oficiosVistoManager;
        $this->documentosRepo      = $documentosRepo;
    }




	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$form_data = array('id'=>'form','route' => 'medidas_restrictivas.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        Session::push("plazo_fecha",true);
        return View::make('formularios.medidas_restrictivas',compact("form_data"));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data    = Input::all();
//        dp($data);

        $oficios           = $this->oficiosRepo->newOficio(5);
        $oficioManager     = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);


        $victima           = $this->personasRepo->newPersona($oficios->id,1);
        $victimaManager    = new PersonaManager($victima,$data["datos_persona"]["victima"]);
        $victimaManager->save();
        $victimaManager->CargarFotos($oficios->id,$data['fotoVictima'],$victima->id,'fotoVictima');



        $responsable        = $this->personasRepo->newPersona($oficios->id,2);
        $responsableManager = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);
        $responsableManager->save();
        $victimaManager->CargarFotos($oficios->id,$data['fotoResponsable'],$responsable->id,'fotoResponsable');

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");



	}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $datos = $this->medidasRepo->getMedidas($id);

        $titulo_oficio = "Medidas Restrictivas";

        $form_data = array('route' => ['medidas_restrictivas.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
        Session::push("plazo_fecha",true);
        Session::push("edit",true);
        return View::make('solo_lectura.medidas_restrictivas',compact("titulo_oficio","datos","form_data"));

    }
    public function  getPdf($id){

        $datos = $this->medidasRepo->getMedidas($id);
        $this->oficiosVistoManager->save($id);
        $titulo_oficio = "Medidas Restrictivas";
        Session::push("plazo_hr",true);
        $html = View::make('solo_lectura.pdf.medidas_restrictivas',compact("titulo_oficio","datos"))->render();


        return $this->oficiosRepo->getPdf($html);

    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->oficiosVistoManager->save($id);
        $form_data = array('route' => ['medidas_restrictivas.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
        Session::push("plazo_fecha",true);
        return View::make('formularios.medidas_restrictivas',compact("form_data")+$this->medidasRepo->getMedidas($id));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

//        $oficios        = new OficiosRepo();
//        $persona        = new PersonasRepo();
//
//        $oficios        = $oficios->find($id);
//        $victima        = $persona->getPersona($id,1);
//        $responsable    = $persona->getPersona($id,2);
          $data  = Input::all();

          extract($this->medidasRepo->getMedidas($id));

           $oficioManager      = new OficiosManager($oficios,$data);
         $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

           $buscadoManager     = new PersonaManager($victima,$data["datos_persona"]["victima"]);
           $buscadoManager->save();


           $solicitanteManager = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);
           $solicitanteManager->save();

           if(!isset($data["datos_documento"])) {
                $dataDocumentos = array();
           }
           else{
                $dataDocumentos      = $data["datos_documento"];
           }

           $this->guardarDocumentos($dataDocumentos,$responsable['id']);


           return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");




	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }



}
