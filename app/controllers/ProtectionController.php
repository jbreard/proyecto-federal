<?php
use Usuarios\Entities\User;
class ProtectionController extends BaseController
{
    public function generateProtection()
    {
        // Generar un código CAPTCHA aleatorio
        $captchaCode = substr(md5(rand()), 0, 3);

        // Almacenar el código CAPTCHA en archivo json

        
        $user = User::find(Auth::user()->id);
        $captchaDataPath = storage_path('/captcha_code.json');
        $captchaData = [];
        if (file_exists($captchaDataPath)) {
            $captchaData = json_decode(file_get_contents($captchaDataPath), true);
        }

            // Agregar la nueva entrada al array
            $captchaData[$user->id] = $captchaCode;

            // Escribir el contenido actualizado en el archivo JSON
            file_put_contents($captchaDataPath, json_encode($captchaData));
        
        

        // Crear una imagen CAPTCHA
        $captchaImage = imagecreatetruecolor(100, 30);

        // Colores
        $bgColor = imagecolorallocate($captchaImage, 255, 255, 255); // Blanco
        $textColor = imagecolorallocate($captchaImage, 0, 0, 0); // Negro

        // Rellenar el fondo
        imagefill($captchaImage, 0, 0, $bgColor);

        // Agregar el texto del CAPTCHA a la imagen
        imagestring($captchaImage, 5, 20, 8, $captchaCode, $textColor);

        // Establecer el tipo de contenido de la imagen
        header('Content-Type: image/png');

        // Mostrar la imagen CAPTCHA
        imagepng($captchaImage);

        // Liberar la memoria
        imagedestroy($captchaImage);
    }
}

