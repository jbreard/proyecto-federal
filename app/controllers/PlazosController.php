<?php

use Oficios\Clases\ArchivosManager;
use Oficios\Managers\ArchivosPlazosManager;
use Oficios\Managers\PlazoManager;
use Oficios\Repositories\ArchivosPlazosRepo;
use Oficios\Repositories\PlazosRepo;
use Oficios\Repositories\OficiosRepo;



class PlazosController extends \BaseController {

    private $plazosRepo;
    protected $oficiosRepo;

    function __construct(PlazosRepo $plazosRepo, OficiosRepo $oficiosRepo)
    {
        $this->plazosRepo = $plazosRepo;
        $this->oficiosRepo = $oficiosRepo;
    }


    public function store()
    {
        $data = Input::all();
        $plazoRepo = $this->plazosRepo->newPlazo($data['id_oficio']);
        $plazoManager = new PlazoManager($plazoRepo,$data);
        $archivosPlazoManager  = new ArchivosPlazosManager();
        $archivosPlazoRepo     = new ArchivosPlazosRepo();

        if($plazoManager->save())
        {
            $id = $plazoRepo->id;
            $archivosManager = new ArchivosManager($id);
            $archivosManager->setBaseManager($archivosPlazoManager);
            $archivosManager->setBaseRepo($archivosPlazoRepo);
            $archivosManager->subir('escaneo_plazo');
            $oficios = $this->oficiosRepo->find($data['id_oficio']);
            $oficios->plazo_autorizado=1;
            $oficios->save();
            if(isset($data['link'])){
                        $path = $oficios->tipo->ruta.'/'.$data['id_oficio'];
                        return Redirect::to('/oficios/plazo/'.$data['id_oficio'])->with('message',' Los Cambios se realizaron Correctamente')->with('path',$path);
            }
                return Redirect::back()->with('mensa_ok','Plazo agregado correctamente');
        }
    }

}
