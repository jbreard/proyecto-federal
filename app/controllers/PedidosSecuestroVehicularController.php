<?php

use Oficios\Clases\ArchivosManager;
use Oficios\Managers\FotosVehicularManager;
use Oficios\Repositories\FotosVehicularRepo;
use Oficios\Repositories\SecuestroVehicularRepo;

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;
use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;
use Oficios\Repositories\TiposOficiosRepo;
use Oficios\Managers\PersonaManager;

use Oficios\Managers\SecuestroVehicularManager;
use Oficios\Managers\DocumentosManager;


class PedidosSecuestroVehicularController extends \BaseController {
    
    protected $secuestroVehicularRepo;
    protected $oficiosRepo;
    protected $personasRepo;  
    protected $oficiosVistoManager;
    protected $documentosRepo;
    protected $tiposOficiosRepo;
    public function __construct(SecuestroVehicularRepo $secuestroVehicularRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,\Oficios\Managers\OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo, TiposOficiosRepo $tiposOficiosRepo){

        $this->secuestroVehicularRepo  = $secuestroVehicularRepo;
        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->documentosRepo          = $documentosRepo;
        $this->tiposOficiosRepo        = $tiposOficiosRepo;
    }




    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data  = array('id'=>'form','route' => 'secuestro_vehicular.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $tipoOficio = $this->tiposOficiosRepo->find(2);
        return View::make('formularios.secuestro_vehicular',compact("form_data","tipoOficio"));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data = Input::all();

        unset($data['id']);

//        dd($data);
        $oficios            = $this->oficiosRepo->newOficio(2);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $titular            = $this->personasRepo->newPersona($oficios->id,5);
        $titularManager     = new PersonaManager($titular,$data["datos_persona"]["titular"]);
        $titularManager->save();

        $responsable           = $this->personasRepo->newPersona($oficios->id,6);
        $responsableManager     = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);
        $responsableManager->save();

        $secuestroVehicular       = $this->secuestroVehicularRepo->newSecuestro($titular->id,$oficios->id);

        $data['id_oficio'] = $oficios->id;
        $secuestroVehicularManager = new SecuestroVehicularManager($secuestroVehicular,$data);

        $secuestroVehicularManager->save();


        $this->saveFotos($oficios->id);


        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }


        //return View::make('header');
        //die($oficios->tipo->ruta);
        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $datos    = $this->secuestroVehicularRepo->getSecuestroVehicular($id);
        $datos[]  ="Secuestro Vehicular";
	
        return View::make('solo_lectura.secuestro_vehicular',compact('datos'));
	}
    public function getPdf($id)
	{
        $datos    = $this->secuestroVehicularRepo->getSecuestroVehicular($id);
        $datos[]  ="Secuestro Vehicular";
        $html     = View::make('solo_lectura.pdf.secuestro_vehicular',compact('datos'))->render();
        $this->oficiosRepo->getPdf($html);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->oficiosVistoManager->save($id);

        $form_data = array('route' => ['secuestro_vehicular.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');

        $secuestro_vehicular = $this->secuestroVehicularRepo->getSecuestroVehicular($id);

        $tipoOficio = $this->tiposOficiosRepo->find(2);

        return View::make('formularios.secuestro_vehicular',compact("form_data","tipoOficio")+$secuestro_vehicular);




    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();
	
        extract($this->secuestroVehicularRepo->getSecuestroVehicular($id));

        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

	$titularManager     = new PersonaManager($titular,$data['datos_persona']['titular']);
        $titularManager->save();

        $responsableManager     = new PersonaManager($responsable,$data['datos_persona']['responsable']);
        $responsableManager->save();


        $data['id_oficio'] = $oficios->id;

        $secuestroVehicularManager = new SecuestroVehicularManager($secuestro_vehicular,$data);

        $secuestroVehicularManager->save();


        $this->saveFotos($oficios->id);

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$responsable['id']);


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

    }

    public function tienePatente(){
        $dominio = Input::get('dominio');
        $response = [];
        $secuestroVehicular  = $this->secuestroVehicularRepo->getPatente(compact("dominio"));
        if($secuestroVehicular->count()>0){
            $queryString = ['buscar_por'=>'elementos','tipo_elemento' => 'vehiculo','vehiculo[dominio]'=>$dominio];
            $response = ["respuesta"=>"true","url"=>route('oficios.index',$queryString)];
            return Response::json($response);
            $response = ["respuesta"=>"false"];
        }

        return Response::json($response);
    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function saveFotos($idOficio)
    {
        $fotosVehicular = new ArchivosManager($idOficio);
        $fotosVehicular->setBaseRepo(new FotosVehicularRepo());
        $fotosVehicular->setBaseManager(new FotosVehicularManager());
        $fotosVehicular->subir('fotos_vehicular');
    }


    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }


}
