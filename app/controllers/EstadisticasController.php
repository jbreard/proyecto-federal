<?php 
use Oficios\Repositories\OficiosRepo;
use Oficios\Repositories\TiposOficiosRepo;


class EstadisticasController extends BaseController {

    protected $oficiosRepo;
    private $tiposOficiosRepo;


    function __construct(OficiosRepo $oficiosRepo, TiposOficiosRepo $tiposOficiosRepo) {

        $this->oficiosRepo  = $oficiosRepo;

        $this->tiposOficiosRepo = $tiposOficiosRepo;
    }

    public function requerimientos() {
//        dd("hola");
        //return $oficios;
        return View::make("estadisticas/requerimientos",compact("oficios"));
    }

    public function getReqMes(){
        $anio = Input::get('anio',null);
        return $oficios = $this->oficiosRepo->reqPorMes($anio);
    }

    public function getReqTipo(){
        $anio = Input::get('anio',null);
        return $oficios = $this->oficiosRepo->reqTipo($anio);
    }

    public function getTiposOficios(){
        $anio = Input::get('anio',null);
        return $this->tiposOficiosRepo->all($anio);
    }

    public function getReqPorOrganismos(){
        $anio = Input::get('anio',null);
        return $this->oficiosRepo->reqPorOrganismo($anio);
    }

    public function getOrganismos(){
        return $this->oficiosRepo->getOrganismos();
    }

}