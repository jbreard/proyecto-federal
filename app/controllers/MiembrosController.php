<?php

use Administracion\Helpers\HelperOperadores;
use Administracion\Helpers\HelperResponsable;
use Administracion\Manager\ManagerMiembro;
use Administracion\Repositories\RepoMiembro;
use Administracion\Repositories\RepoMiembroxOperador;
use Administracion\Repositories\RepoMiembroxResponsable;
use Combos\Entities\CombosOrganismos;

/**
 * Created by PhpStorm.
 * User: damian
 * Date: 10/08/15
 * Time: 16:00
 */
class MiembrosController extends \BaseController
{

    private $repoMiembro;
    protected $ruta;
    protected $view;
    private $helperOperadores;
    private $helperResponsable;
    private $repoMiembroxOperador;
    private $repoMiembroxResponsable;

    function __construct(RepoMiembro $repoMiembro,
                         HelperOperadores $helperOperadores,
                         HelperResponsable $helperResponsable,
                         RepoMiembroxOperador $repoMiembroxOperador,
                         RepoMiembroxResponsable $repoMiembroxResponsable
                        )
    {
        $this->repoMiembro = $repoMiembro;
        $this->ruta    = "miembros.";
        $this->view    = "miembros/";
        $this->helperOperadores = $helperOperadores;
        $this->helperResponsable = $helperResponsable;
        $this->repoMiembroxOperador = $repoMiembroxOperador;
        $this->repoMiembroxResponsable = $repoMiembroxResponsable;
    }

    public function index()
    {
        $miembros = $this->repoMiembro->all();
        return View::make($this->view."listado",compact('miembros'));
    }

    public function create()
    {
        $operadores = array();
        $responsables = array();
        Session::put('operadores',$operadores);
        Session::put('responsables',$responsables);


        $model = $this->repoMiembro->getModel();
//        dd($model);
        $form_data = array('id'=>'form','route' => $this->ruta.'store', 'method' => 'POST', 'action' => 'return false');
        $action = "Agregar";
        $combosOrganismos = CombosOrganismos::remember(60)->lists('descripcion','id');
        $responsables = "";
        $operadores = "";
        return View::make($this->view.'form',compact("model","form_data","action","combosOrganismos","responsables","operadores"));
    }

    public function agregarOperador()
    {
        return $this->helperOperadores->agregarOperador();
    }

    public function eliminarOperador()
    {
        return $this->helperOperadores->eliminarOperador();
    }

    public function agregarResponsable()
    {
        return $this->helperResponsable->agregarResponsable();
    }

    public function eliminarResponsable()
    {
        return $this->helperResponsable->eliminarResponsable();
    }

    public function validarDatos()
    {
        $data      = Input::all();
        $model     = null;
        $valid     = true;
        $idMiembro = Input::get('id',false);
        
        $model = $this->repoMiembro->firstOrNew(["id"=>$idMiembro]);
        
        $manager = new ManagerMiembro($model,$data);
        
        $validacion  = $manager->isValid();
        
        if($validacion["valid"])
        {
            if(count(\Session::get("responsables")) == 0){
               $validacion["valid"] = false;
               $validacion["mensaje"] = "Debe ingresar al menos un responsable";  
            } 

            if(count(\Session::get("operadores")) == 0){
                $validacion["valid"] = false;
                $validacion["mensaje"] = "Debe ingresar al menos un operador";;  
            } 
        }

        return $validacion;

    }

    public function store(){
        $model   = $this->repoMiembro->nuevoMiembro();
        $data    = Input::all();
        $manager = new ManagerMiembro($model,$data);
        $manager->save();

        $lista_responsables = \Session::get("responsables");
        foreach($lista_responsables as $responsable)
            $this->repoMiembroxResponsable->agregarResponsable($manager->getEntity()->id,$responsable);

        $lista_operadores = \Session::get("operadores");
        foreach($lista_operadores as $operador)
            $this->repoMiembroxOperador->agregarOperador($manager->getEntity()->id,$operador);

        return Redirect::route($this->ruta."index");
//        dd($_POST);
    }

    public function edit($id)
    {
        $lista_responsables = $this->repoMiembroxResponsable->darResponsablesXMiembro($id);

        $lista_operadores = $this->repoMiembroxOperador->darOperadoresXMiembro($id);

//        $espacios = array();
        Session::put('responsables',$lista_responsables);
        Session::put('operadores',$lista_operadores);
        $responsables = $this->helperResponsable->listadoResponsables($lista_responsables);

        $operadores = "";
        $cancelar = route($this->ruta.'index') ;

        $combosOrganismos = CombosOrganismos::remember(60)->lists('descripcion','id');
        $model = $this->repoMiembro->find($id);
        $action = "Editar";
        $form_data = ['id'=>'form','route' =>[$this->ruta.'actualizar',$id], 'method' => 'POST', 'action' => 'return false'];
        return View::make($this->view."form",compact("model","action","form_data","combosOrganismos","responsables","operadores","cancelar"))
            ->with("ruta",$this->ruta);

    }

    public function listadoResponsables()
    {
        $lista = Session::get("responsables");
        return $this->helperResponsable->listadoResponsables($lista);
    }

    public function listadoOperadores()
    {
        $lista = Session::get("operadores");
        return $this->helperOperadores->listadoOperadores($lista);
    }

    public function actualizar($id)
    {
        $model = $this->repoMiembro->find($id);
        $data    = Input::all();
        $manager = new ManagerMiembro($model,$data);
        $manager->save();

//        dd($manager);
        $this->repoMiembroxResponsable->eliminarResponsableXMiembro($id);
        $lista_responsables = \Session::get("responsables");
        foreach($lista_responsables as $responsable)
            $this->repoMiembroxResponsable->agregarResponsable($manager->getEntity()->id,$responsable);

        $this->repoMiembroxOperador->eliminarOperadorXMiembro($id);
        $lista_operadores = \Session::get("operadores");
        foreach($lista_operadores as $operador)
            $this->repoMiembroxOperador->agregarOperador($manager->getEntity()->id,$operador);

        return Redirect::route($this->ruta."index");
    }


    public function show($id){
        $model = $this->repoMiembro->find($id);
        return View::make('miembros.show')->with('miembro',$model);
    }

}