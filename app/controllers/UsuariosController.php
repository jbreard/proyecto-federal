<?php

use Combos\Entities\CombosOrganismos;
use Usuarios\Entities\User;
use Usuarios\Entities\Perfiles;
use Combos\Repositories\ProvinciaRepo;
use SubOrganismos\Repositories\SubOrganismosRepo as SubOrganismosRepo;


class UsuariosController extends BaseController {

    protected $provinciaRepo;
    protected $subOrganismoRepo;


    public function __construct(ProvinciaRepo $provinciaRepo,SubOrganismosRepo $subOrganismoRepo){

        $this->provinciaRepo    = $provinciaRepo;
        $this->subOrganismoRepo = $subOrganismoRepo;

    }

	function index() {

		//echo "kilombo";
		
		//$user = DB::table("usuarios")->join("perfiles","perfiles.id","=","usuarios.id_perfil")->select("usuarios.id","usuarios.id_perfil","nombre","dni","perfiles.perfil as perfil","apellido","email")->get();

        $perfiles = Perfiles::remember(60)->lists('perfil', 'id');

        $combosOrganismos = CombosOrganismos::remember(60)->lists('descripcion','id');

        $userClass    = new User();

        $data    = Input::all();

        $user    = $userClass->buscar($data);

		return View::make("usuarios.listados", array('users' => $user,"perfiles"=>$perfiles,"busqueda"=>$data,"combosOrganismos"=>$combosOrganismos));
	}

	function create() {

		$user = new User;

		$perfiles = Perfiles::all() -> lists('perfil', 'id');



		return View::make("usuarios.create", array('user' => $user,  'perfiles' => $perfiles));
	}

	public function show($id) {
		$user = User::find($id);
		if (is_null($user)) {
			App::abort(404);
		}
                $user_creator = "";
                if($user->id_user_creator <> 0){
                    $aux_user_creator = User::find($user->id_user_creator);
                    $user_creator['nombre'] = $aux_user_creator->nombre;
                    $user_creator['apellido'] = $aux_user_creator->apellido;
                    $user_creator['username'] = $aux_user_creator->username;
                }
                
                $perfiles = Perfiles::all() -> lists('perfil', 'id');
                $provincias   = $this->provinciaRepo->getCombo();

                $sub_organismo = $this->subOrganismoRepo->comboSubOrganismoByIdOrg($user->id_jurisdiccion);

		return View::make("usuarios.create", array('user' => $user,  'perfiles' => $perfiles,"provincias"=>$provincias,"sub_organismo"=>$sub_organismo,"user_creator"=>$user_creator));

	}

	public function update($id) {
		$user = User::find($id);
		if (is_null($user)) {
			App::abort(404);
		}

		// Obtenemos la data enviada por el usuario
		$data = Input::all();

		// Revisamos si la data es válido
		if ($user -> isValid($data)) {
			// Si la data es valida se la asignamos al usuario
			$user -> fill($data);
			// Guardamos el usuario
			$user -> save();
			// Y Devolvemos una redirección a la acción show para mostrar el usuario
			return Redirect::route('usuarios.show', array($user -> id));
		} else {
			// En caso de error regresa a la acción edit con los datos y los errores encontrados
			return Redirect::route('usuarios.show', $user -> id) -> withInput() -> withErrors($user -> errors);
		}

	}

	public function validarDni() {
		if (Request::ajax()) {// it's an ajax request

			$input = Input::all();
			$usuarios = DB::table('usuarios') -> where('dni', '=', $input['dni']) -> get();

			if (count($usuarios)) {
				$response['respuesta'] = true;
			} else {
				$response['respuesta'] = false;
			}
			return json_encode((object)$response);
		}
	}
	function store() {

		$user = new User;

		$data = Input::all();

		// Revisamos si la data es válido
		if ($user -> isValid($data)) {
                        //$pass_text = $this->generatePassword(10);
                        $pass_text = 'Sifcopargentina';
                        $data['password'] =  Hash::make($pass_text);
			//$data['password'] = Hash::make('Sifcopargentina');
			// Si la data es valida se la asignamos al usuario
			$user -> fill($data);
			// Guardamos el usuario
                        $user->pass_created = date('Y-m-d');
                        $user['id_user_creator'] = \Auth::user()->id;
			$user -> save();
			// Y Devolvemos una redirección a la acción show para mostrar el usuario
                        //$this->enviar_mail($user->email,$user->username,$pass_text,$user->nombre,$user->apellido,"nuevo");
                        //enviar_mail($email,$username,$password,$name,$lastname)
                        return Redirect::route('usuarios.index');
			//return Redirect::route('usuarios.index')->with('mensaje',"Usuario <b>$user->username</b> creado Correctamente<br>Se envió email con el password a: <b>$user->email</b>");
		} else {
			// En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('usuarios.create') -> with('user', $user) -> withInput() -> withErrors($user -> errors);
		}

	}


	function comprobarUsuario() {
		if (Request::ajax()) {// it's an ajax request
			echo Auth::user() -> testigo;
		}
	}
        
        
        function cambiarClave($id) {
            echo"cambiarClave.$id";
		$user = new User;
                $user = $user->find($id);
                $pass_text = $this->generatePassword(10);
                $user->password =  Hash::make($pass_text);
                $user->pass_created = date('Y-m-d');
                $user->cambio_clave = 0;
		$user->save();
                //$this->enviar_mail("jbreard@gmail.com",$user->username,$pass_text,$user->nombre,$user->apellido,"change");
                //$this->enviar_mail($user->email,$user->username,$pass_text,$user->nombre,$user->apellido,"change");
		return Redirect::route('usuarios.index')->with('mensaje',"Password cambiado correctamente se envió mail a:<b>$user->email</b>");
	}

	function getCambiarClave() {
        $email = Auth::user()->email;
        if(!empty($email))
            {$noemail = false;   }
        else{$noemail = true;}
        //dd($email);
		return View::Make("usuarios.cambiar_clave",compact("email")) -> with("clave", null);
	}

	function postCambiarClave() {
		$user = new User;
		$data = Input::all();
		// Revisamos si la data es válido
		if ($user -> isValidClave($data)) {
			$user = $user -> find(Auth::user() -> id);
			$clave_old = $data['clave_old'];
			$clave_nueva = $data['clave_nueva'];
			if (Hash::check($clave_old, $user -> getAuthPassword())) {
			    $user -> email = $data['email'];
				$user -> password      = Hash::make($clave_nueva);
                $user -> cambio_clave  = 1;
                $user->pass_created = date('Y-m-d');
				if ($user -> save()) {
					return Redirect::route('logout') -> with('mensaje_correcto', 'Su Clave Se Cambio Correctamente');
				}
			} else {

				return Redirect::back() -> with("clave", null) -> with("mensa_error", "La Clave No Es Correcta");
			}
		} else {
			// En caso de error regresa a la acción create con los datos y los errores encontrados

			return Redirect::to('usuarios/clave') -> with('user', $user) -> withInput() -> withErrors($user -> errors);
		}

	}


    function RestablecerClave(){
        
        $data = Input::all();
        
        $user = new User();
        


        $usuario = $user->find($data["id"]);




        $usuario->password     = Hash::make($data["clave"]);
        $usuario->cambio_clave = 0;
        $usuario->pass_created = date('Y-m-d');

        //$usuario->fill($usuario);
        if($usuario->save()){
           
            return Redirect::back()->with('mensaje','Clave Cambiada Correctamente');

        }





    }
    public function aceptar(){
        
        $id =  Auth::user()->id;
        $user = new User();
        $usuario = $user->find($id);
        $usuario->term_cond = date('Y-m-d');
        if($usuario->save()){
           return 1;     
        }
        return 0;
       
    }
    
    public function enviar_mail($email,$username,$password,$name,$lastname,$plantilla){
            
        $fullname = $name.' '.$lastname;
       
        Mail::send('emails.'.$plantilla, array('email' => $email,'username' => $username,'pass' => $password, 'fullname' => $fullname), function($message)use ($email,$fullname)
        {
            $message->to($email , $fullname)->subject('Sifcop!');
        });
        return;    
            
    }
    public function generatePassword($length)
    {
        $key = "";
        $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_*";
        $max = strlen($pattern)-1;
        for($i = 0; $i < $length; $i++){
            $key .= substr($pattern, mt_rand(0,$max), 1);
        }
        return $key;
    }

	

}
?>