<?php

use Administracion\Repositories\RepoMensajeAnio;
use Oficios\Entities\Personas;
use Oficios\Managers\DocumentosManager;
use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;
use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;
use Oficios\Managers\OficiosVistoManager;

class PedidosCapturaController extends \BaseController {

    protected $oficiosRepo;
    protected $personasRepo;
    protected $documentosRepo;
    protected $oficiosVistoManager;
    private $repoMensajeAnio;

    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo, RepoMensajeAnio $repoMensajeAnio){

        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->documentosRepo          = $documentosRepo;
        $this->titulo_oficio           = 'Pedido de Captura / Detención';
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->repoMensajeAnio = $repoMensajeAnio;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data = array('id'=>'form','route' => 'pedidos_captura.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = $this->titulo_oficio;
        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio"));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data               = Input::all();

        unset($data['id']);

//        dd($data);
        $oficios            = $this->oficiosRepo->newOficio(4);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $responsable        = $this->personasRepo->newPersona($oficios->id,2);
        $responsableManager = new PersonaManager($responsable,$data);
        $responsableManager->save();
        $responsableManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $oficios        = $this->oficiosRepo->find($id);
        $responsable    = $this->personasRepo->getPersona($id,2);

        $this->oficiosVistoManager->save($id);

        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;


        return View::make('solo_lectura.pedidos_captura',compact("titulo_oficio","datos"));

    }
    public function getPdf($id)
    {
        $oficios        = $this->oficiosRepo->find($id);
        $responsable    = $this->personasRepo->getPersona($id,2);



        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;

        $html = View::make('solo_lectura.pdf.pedidos_captura',compact("titulo_oficio","datos"))->render();

        $this->oficiosRepo->getPdf($html);

    }

    public function getPdfReporteNota($id)
    {

        $dirigido = Input::get('dirigido');
        $sexo = Input::get('sexo');
        $nombre_dirigido = Input::get('nombre_dirigido');
        $nombre_juez_fiscal = Input::get('nombre_juez_fiscal');

        $destinatario = $this->darDestinatario($dirigido,$sexo);
        $asucargo      = $this->darASuCargo($dirigido);
        $nombre_fiscalia_juzgado = $this->darFiscaliaJuzgado($dirigido,$sexo,$nombre_dirigido);
        $dar_senor_senora = $this->darSenorSenora($sexo);

        $oficio        = $this->oficiosRepo->find($id);
//        dd($this->oficiosRepo->getTipoPersona($id));
        $responsable   = $this->personasRepo->getPersona($id,$this->oficiosRepo->getTipoPersona($id));
        if($responsable == null)
        {
            $responsable = new Personas();
            $responsable->nombre = "";
            $responsable->apellido = "";
            $responsable->nro_documento = "";
        }
//        dd($responsable);
//        dd($oficio->cesado);

        //$mensaje_anio = $this->repoMensajeAnio->darMensajeActual();


        $fecha_oficio = explode("/", $oficio->fecha_oficio);
        $fecha_oficio[1] = $this->darMes($fecha_oficio[1]);

        $created_at = substr($oficio->created_at, 0, 10);
        $fecha = explode("/", $created_at);
        $fecha[1] = $this->darMes($fecha[1]);

        $html = View::make('solo_lectura.pdf.reporte_nota_index',compact("oficio","fecha","destinatario","asucargo","responsable","nombre_fiscalia_juzgado","dar_senor_senora","nombre_juez_fiscal","fecha_oficio"))->render();
        $this->oficiosRepo->getPdfReporteNota($html);

    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->oficiosVistoManager->save($id);
        $form_data = array('route' => ['pedidos_captura.update',$id], 'method' => 'PATCH','enctype' => 'multipart/form-data','id'=>"form");
        $oficios        = new OficiosRepo();
        $persona        = new PersonasRepo();
        $oficios        = $oficios->find($id);
        $responsable    = $persona->getPersona($id,2);
        $documentos     = $responsable->getDocumentos;
        $titulo_oficio  = $this->titulo_oficio;
        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio","oficios","victima","responsable","documentos"));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();
        $oficios        = new OficiosRepo();
        $persona        = new PersonasRepo();

        $oficios        = $oficios->find($id);
        $responsable    = $persona->getPersona($id,2);
        $personaManager = new PersonaManager($responsable,$data);
        $oficiosManager = new OficiosManager($oficios,$data);

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$responsable['id']);

        $oficiosManager ->save(true,['archivo1','archivo2','archivo3','archivo4']);

	$personaManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);
                 dd("stop");
        $personaManager->save();
        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


    private function darDestinatario($dirigido, $sexo)
    {
        $mensaje = "";
        if($dirigido == "J")
        {
            if($sexo == "F")
                $mensaje .= "Señora Jueza";
            else
                $mensaje .= "Señor Juez";
        }
        else
        {
            if($sexo == "F")
                $mensaje .= "Señora Fiscal";
            else
                $mensaje .= "Señor Fiscal";
        }

        return $mensaje;

    }

    private function darASuCargo($dirigido)
    {
        $mensaje = "";

        if($dirigido == "J")
            $mensaje .= "el Juzgado";
        else
            $mensaje .= "la Fiscalía";

        return $mensaje;

    }


    private function darMes($mes)
    {
        if($mes == "01")
            return "Enero";
        if($mes == "02")
            return "Febrero";
        if($mes == "03")
            return "Marzo";
        if($mes == "04")
            return "Abril";
        if($mes == "05")
            return "Mayo";
        if($mes == "06")
            return "Junio";
        if($mes == "07")
            return "Julio";
        if($mes == "08")
            return "Agosto";
        if($mes == "09")
            return "Septiembre";
        if($mes == "10")
            return "Octubre";
        if($mes == "11")
            return "Noviembre";
        if($mes == "12")
            return "Diciembre";



    }
    function agregarDocumento() {
        if (Request::ajax()){
            $datos["i"] = $_POST['i'];
            return View::make("formularios.documentos",$datos);
        }
    }


    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }

    private function darFiscaliaJuzgado($dirigido, $sexo, $nombre_dirigido)
    {
        $mensaje = "";
        if($dirigido == "J")
            if($sexo == "M")
                $mensaje .= "JUEZ A/C DEL JUZGADO";
            else
                $mensaje .= "JUEZA A/C DEL JUZGADO";
        else
            $mensaje .= "FISCAL A/C DE LA FISCALIA";

        $mensaje .= " $nombre_dirigido";
        return $mensaje;
    }

    private function darSenorSenora($sexo)
    {
        if($sexo == "M")
            return "AL SEÑOR";
        else
            return "A LA SEÑORA";
    }


}
