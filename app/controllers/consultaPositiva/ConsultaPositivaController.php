<?php

use Illuminate\Routing\Controller;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasDestinatariosRepo;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasOficiosRepo;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasRepo;


class ConsultaPositivaController extends Controller
{
	private $destinatariosRepo;
	private $consultasPositivasOficiosRepo;
	private $consultasPositivasRepo;
	
	public function __construct(ConsultasPositivasDestinatariosRepo $destinatariosRepo,ConsultasPositivasOficiosRepo $consultasPositivasOficiosRepo,ConsultasPositivasRepo $consultasPositivasRepo)
	{
		$this->destinatariosRepo = $destinatariosRepo;
		$this->consultasPositivasOficiosRepo = $consultasPositivasOficiosRepo;
		$this->consultasPositivasRepo = $consultasPositivasRepo;
	}


	public function index(){
		$idOrganismo = \Auth::user()->id_jurisdiccion;
		$consultasPositivas = $this->destinatariosRepo->getByIdOrganismo($idOrganismo);
		return View::make('consultas_positivas.index',compact("consultasPositivas"));

	}


	public function show($id){
		$oficios  = $this->consultasPositivasOficiosRepo->getByIdConsulta($id)->paginate(15);
		$detalles = $this->consultasPositivasRepo->find($id);

		return View::make('consultas_positivas.detail',compact("oficios","consultaPositiva","detalles"));
	}

	

	
	
}