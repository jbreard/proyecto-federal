<?php

use Oficios\Repositories\SecuestroArmasRepo;

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\SecuestroArmasManager;

use Oficios\Managers\OficiosVistoManager;

use Oficios\Managers\DocumentosManager;


class SecuestroArmasController extends \BaseController {

    protected $armaRepo;
    protected $oficiosRepo;
    protected $personasRepo;
    protected $documentosRepo;


    public function __construct(SecuestroArmasRepo $armaRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->armaRepo             = $armaRepo;
        $this->oficiosRepo          = $oficiosRepo;
        $this->personasRepo         = $personasRepo;
        $this->oficiosVistoManager  = $oficiosVistoManager;
        $this->documentosRepo       = $documentosRepo;
    }




    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $form_data = array('route' => 'secuestro_armas.store', "id"=>"form",'method' => 'POST','enctype' => 'multipart/form-data');

        return View::make('formularios.secuestro_armas',compact("form_data"));
    }


    public function getPdf($id){


        $this->oficiosVistoManager->save($id);

        $datos = $this->armaRepo->getSecuestroArmas($id);

        $html = View::make('solo_lectura.pdf.secuestro_armas',compact("datos"))->render();

        $this->oficiosRepo->getPdf($html);

    }


    public function store()
    {

        $data = Input::all();
        $oficios           = $this->oficiosRepo->newOficio(10);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);




        $titular            = $this->personasRepo->newPersona($oficios->id,5);
        $titularManager     = new PersonaManager($titular,$data);
        $titularManager->save();



        $secuestroArmas      = $this->armaRepo->newSecuestroArmas($titular->id);
        $SecuestroArmasManager = new SecuestroArmasManager($secuestroArmas,$data);
        $SecuestroArmasManager->save();

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$titular['id']);
        }

       return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $this->oficiosVistoManager->save($id);
        $datos = $this->armaRepo->getSecuestroArmas($id);

        return View::make('solo_lectura.secuestro_armas',compact("datos"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $this->oficiosVistoManager->save($id);

        $form_data = array('route' => ['secuestro_armas.update',$id], "id"=>"form", 'method' => 'PATCH','enctype' => 'multipart/form-data');

        $secuestro_armas = new SecuestroArmasRepo();

        extract($secuestro_armas->getSecuestroArmas($id));

        // print_r($secuestro_elementos);
        //dd();

        return View::make('formularios.secuestro_armas',compact("form_data","oficios","titular","archivos","secuestro_armas","documentos"));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();

        extract($this->armaRepo->getSecuestroArmas($id));

        $oficioManager      = new OficiosManager($oficios,$data);

        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);



        $titularManager     = new PersonaManager($titular,$data);

        $titularManager->save();


        $secuestroArmasManager = new SecuestroArmasManager($secuestro_armas,$data);

        $secuestroArmasManager ->save();

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$titular['id']);

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");



    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }


}