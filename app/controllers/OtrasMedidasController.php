<?php

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\MedidasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\OficiosVistoManager;

use Oficios\Managers\DocumentosManager;


class OtrasMedidasController extends \BaseController {

    protected  $oficiosRepo;
    protected  $personasRepo;
    protected  $medidasRepo;
    protected  $oficiosVistoManager;
    protected  $documentosRepo;



    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,MedidasRepo $medidasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->oficiosRepo         = $oficiosRepo;
        $this->personasRepo        = $personasRepo;
        $this->medidasRepo         = $medidasRepo;
        $this->oficiosVistoManager = $oficiosVistoManager;
        $this->documentosRepo      = $documentosRepo;
    }




    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $form_data = array('id'=>'form','route' => 'otras_medidas.store', 'method' => 'POST','enctype' => 'multipart/form-data');

        return View::make('formularios.otras_medidas',compact("form_data"));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data    = Input::all();

        $oficios           = $this->oficiosRepo->newOficio(9);
        $oficioManager     = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);


        $responsable        = $this->personasRepo->newPersona($oficios->id,2);
        $responsableManager = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);
        $responsableManager->save();


        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }

       return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");



    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $datos = $this->medidasRepo->getMedidas($id);
        $titulo_oficio = "Otras Medidas";

        return View::make('solo_lectura.otras_medidas',compact("titulo_oficio","datos"));

    }
    public function  getPdf($id){

        $datos = $this->medidasRepo->getMedidas($id);
        $titulo_oficio = "Otras Medidas";

        $html = View::make('solo_lectura.pdf.otras_medidas',compact("titulo_oficio","datos"))->render();


        return $this->oficiosRepo->getPdf($html);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->oficiosVistoManager->save($id);
        $form_data = array('route' => ['otras_medidas.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
        return View::make('formularios.otras_medidas',compact("form_data")+$this->medidasRepo->getMedidas($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {


        $data  = Input::all();

        extract($this->medidasRepo->getMedidas($id));
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $responsable    = $persona->getPersona($id,2);
        $personaManager = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        $personaManager->save();

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");




    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }

}