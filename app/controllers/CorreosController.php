<?php

use Correos\Managers\CorreoManager;
use Correos\Entities\Correo;
use Combos\Repositories\ProvinciaRepo;
use Correos\Repositories\CorreoRepo;

class CorreosController extends BaseController {

    protected $correoRepo;
    protected $provinciaRepo;

    public function __construct(CorreoRepo $correoRepo,ProvinciaRepo $provinciaRepo){
        $this->correoRepo    = $correoRepo; 
        $this->provinciaRepo = $provinciaRepo;
    }
    /**
     * Display a listing of the resource. 
     *
     * @return Response
     */
    public function index()
    {
        $correos = Correo::paginate(10);

        return View::make("mails.listadoM", array('correos' => $correos));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $accion     = "Carga de";
        $provincias = $this->provinciaRepo->getCombo();
        $form_data  = array('route' => 'mails.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        return View::make("mails.create",compact("form_data","accion","provincias"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data    = Input::all();

        $correo           = $this->correoRepo->newCorreo();
        $correoManager    = new CorreoManager($correo,$data);
        $correoManager->save();

        return Redirect::route('mails.index');


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $correo = $this->correoRepo->find($id);
        if (is_null($correo)) {
            App::abort(404);
        }

        $accion="Editar";
        $provincias = $this->provinciaRepo->getCombo();
        $form_data  = array('route' => ['mails.update',$id], 'method' => 'PATCH','enctype' => 'multipart/form-data');
        return View::make("mails.create", compact("correo","form_data","accion","provincias"));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $correo = $this->correoRepo->find($id);

        // Obtenemos la data enviada por el usuario
        $data = Input::all();


        $correoManager = new CorreoManager($correo,$data);

        if($correoManager->save()) return Redirect::route('mails.index');

        return Redirect::back()->withInput()->withErrors($correoManager->getErrors());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
//
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
//
    }


}
