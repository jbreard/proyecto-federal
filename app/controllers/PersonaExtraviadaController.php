<?php

use Illuminate\Support\Facades\Auth;
use Oficios\Entities\Archivos;
use Oficios\Interfaces\PersonaExtraviada\IRepoPEPersonaBuscada;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasDestinatariosRepo;
use Oficios\Repositories\OficiosRepo;
use Oficios\Repositories\PersonasExtraviadas\RepoInterfacePersonaExtraviada;
use Oficios\Repositories\PersonasExtraviadas\RepoPEAutoridadJudicial;
use Oficios\Repositories\PersonasExtraviadas\RepoPEAutorizaciones;
use Oficios\Repositories\PersonasExtraviadas\RepoPEFotos;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaBuscada;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaDatosBusqueda;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaDatosDenunciante;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaDatosLaborales;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaDatosRelacionadoAusencia;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaDescripcionFisica;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaUltimoDomicilio;
use Oficios\Repositories\PersonasRepo;

/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/12/15
 * Time: 13:46
 */
class PersonaExtraviadaController extends \BaseController {

    protected $oficiosRepo;
    private $personasRepo;

    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo)
    {
        $this->oficiosRepo   = $oficiosRepo;
        $this->personasRepo  = $personasRepo;
        $this->titulo_oficio = 'Búsqueda de personas extraviadas';
    }


    /**
     * Display a listing of the resource.
     * no lo voy a usar en este caso
     *
     * @return Response
     */




    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $oficio = $this->oficiosRepo->getModel();
//        dd($oficio->fecha_oficio);
        $form_data     = array('id'=>'form','route' => 'solicitud_paradero.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        Session::push("plazo_hr",true);
        return View::make('formularios.solicitud_paradero.formulario',compact("form_data","titulo_oficio",'oficio'));
    }

    public function guardarPEAutoridadJudicial()
    {
        $data    = Input::all();
        $persona_buscada = new RepoPEAutoridadJudicial($data);
        return $this->guardar($persona_buscada);
    }

    public function guardarPEPersonaBuscada()
    {
        $data    = Input::all();
        $persona_buscada = new RepoPEPersonaBuscada($data);
        return $this->guardar($persona_buscada);
    }

    public function guardarPEUltimoDomicilio()
    {
        $data    = Input::all();
        $persona_ultimo_domicilio = new RepoPEPersonaUltimoDomicilio($data);
        return $this->guardar($persona_ultimo_domicilio);
    }

    public function guardarPEDescripcionFisica()
    {
        $data    = Input::all();
        $persona_descripcion_fisica = new RepoPEPersonaDescripcionFisica($data);
        return $this->guardar($persona_descripcion_fisica);
    }

    public function guardarPEDatosLaborales()
    {
        $data    = Input::all();
        $persona_descripcion_fisica = new RepoPEPersonaDatosLaborales($data);
        return $this->guardar($persona_descripcion_fisica);
    }

    public function guardarPEDatosRelacionadoAusencia()
    {
        $data    = Input::all();
        $persona_descripcion_fisica = new RepoPEPersonaDatosRelacionadoAusencia($data);
        return $this->guardar($persona_descripcion_fisica);
    }

    public function guardarPEDatosDenunciante()
    {
        $data    = Input::all();
        $persona_denunciante = new RepoPEPersonaDatosDenunciante($data);
        return $this->guardar($persona_denunciante);
    }

    public function guardarPEDatosBusqueda()
    {
        $data    = Input::all();
        $persona_busqueda = new RepoPEPersonaDatosBusqueda($data);
        return $this->guardar($persona_busqueda);
    }

    public function guardarPEFotos()
    {
        $data    = Input::all();
        $persona_autorizaciones = new RepoPEAutorizaciones($data);

        return $this->guardar($persona_autorizaciones);
    }

    public function getPdf($id)
    {

        $oficios        = $this->oficiosRepo->find($id);
        $responsable    = $this->personasRepo->getPersona($id,2);
        $datos = compact('oficios','persona','responsable');
        $titulo_oficio = $this->titulo_oficio;
        $permiso_pe = Auth::user()->permiso_pe;
        $html = View::make('solo_lectura.pdf.personas_extraviadas',compact("titulo_oficio","datos",'permiso_pe'))->render();
        $this->oficiosRepo->getPdf($html);
    }

    public function edit($id)
    {
        $oficio = $this->oficiosRepo->getPersonaPorId($id);
        $oficio->NrosCelulares = json_encode($oficio->NrosCelulares);
        $oficio->PersonaDescripcionFisica = $this->setSenias($oficio->PersonaDescripcionFisica);
        $oficios = $oficio;

        $permiso_pe = Auth::user()->permiso_pe;

        if(is_object($oficio->persona_buscada))
            $oficio->persona_buscada->id_persona = $this->personasRepo->getPersona($oficio->id,3)->id;

        $persona_denunciante = $this->personasRepo->getPersona($oficio->id,4);
//dd($oficio);
        if(is_object($persona_denunciante))
            $oficio->PersonaDatosDenunciante->id_persona_denunciante = $persona_denunciante->id;

        $form_data     = array('id'=>'form','route' => 'solicitud_paradero.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        Session::push("plazo_hr",true);
        return View::make('formularios.solicitud_paradero.formulario',compact("form_data","titulo_oficio","pe_persona_buscada","oficio","oficios","permiso_pe"));
    }

    public function setSenias($personaDescripcionFisica)
    {
        if($personaDescripcionFisica['id_senias_particulares_original'] != "")
        {
            $senias = explode(",",$personaDescripcionFisica['id_senias_particulares_original']);
            foreach($senias as $id_senia)
            {
                $variable = 'chk_'.$id_senia;
                $personaDescripcionFisica->$variable = true;
            }
        }

        return $personaDescripcionFisica;
    }

    public function guardar(RepoInterfacePersonaExtraviada $persona_extraviada)
    {
        $validacion = $persona_extraviada->validarDatos();
        if($validacion === true)
        {
            if(empty($persona_extraviada->data['id_oficio']))
            {
                $id = $persona_extraviada->guardar();

                $persona_extraviada->guardarArchivo($persona_extraviada->data['archivo1'],$id);
                $persona_extraviada->guardarArchivo($persona_extraviada->data['archivo2'],$id);
                return route('solicitud_paradero.edit',$id);
            }
            else
            {
                $oficio = $this->oficiosRepo->find($persona_extraviada->data['id_oficio']);
                $oficio->touch();
                return $persona_extraviada->guardar();
            }
        }
        else
            return $validacion;
    }



    public function show($file)
    {
        $ruta = public_path()."$file";

        return Response::download($ruta);
    }

    public function showComplete($id)
    {
        $datos["oficios"] = $this->oficiosRepo->getPersonaPorId($id);


        return View::make('solo_lectura.personas_extraviadas_completo',compact('datos'));
    }

    public function showReporte($id){
        
        $datos["oficios"] = $this->oficiosRepo->getPersonaPorId($id);
        $permiso_pe = Auth::user()->permiso_pe;
        $radioButtons = [
            "" => "",
            0 => "",
            1 => "X",
        ];

        return View::make('solo_lectura.personas_extraviadas',compact('datos','permiso_pe', 'radioButtons'));
    }

    public function obtenerHistorico()
    {
        $data    = Input::all();
        $resultado = $this->oficiosRepo->darDatosViejosPersonasExtraviadas($data['id_persona']);

        if(count($resultado) > 0)
        {
            $resultado = $resultado[0];
            $resultado->primera_desaparece = $this->darSiNoSD($resultado->primera_desaparece);
            $resultado->enfermedad = $this->darSiNoSD($resultado->enfermedad);
            $resultado->drogadiccion = $this->darSiNoSD($resultado->drogadiccion);
            $resultado->internado_institucion = $this->darSiNoSD($resultado->internado_institucion);



            return View::make('formularios.solicitud_paradero.datos_viejos',compact('resultado'));
        }
        else
            return 'No se encontraron datos históricos';
    }

    private function darSiNoSD($id)
    {
        if($id == 1)
            return "Si";
        else if($id == 2)
            return "No";
        else
            return "S/D";

    }

    public function estadisticas()
    {
        return View::make("estadisticas.personas_extraviadas");
    }

    public function filtroEstadisticas()
    {
        $data    = Input::all();
        $repo_persona_extraviada = new RepoPEPersonaBuscada();

        return $repo_persona_extraviada->getEstadisticas($data);
    }
}