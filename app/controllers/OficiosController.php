<?php

use Oficios\Avisos\Emails;
use Oficios\Clases\SinEfecto;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Repositories\OficiosRepo;
use Oficios\Entities\Oficios;
use \Oficios\Managers\OficiosManager;
use Oficios\Repositories\OficiosCesadosRepo;
use Oficios\Managers\OficiosCeseManager;
use Oficios\Repositories\PersonasRepo;
use Oficios\Repositories\TiposOficiosRepo;
use Oficios\Repositories\LogRepo;
use Oficios\Repositories\OficiosVistosRepo;
use Oficios\Repositories\ModificacionesRepo;
use Oficios\Repositories\SecuestroVehicularRepo;

use Oficios\Entities\OficiosCesados;




class OficiosController extends \BaseController {

    protected $oficiosRepo;
    protected $cesadosRepo;
    protected $personasRepo;
    protected $oficiosVistosRepo;
    protected $modificacionesVistasRepo;
    protected $tiposOficiosRepo;

    private $sinEfecto;

    public function __construct(
        ModificacionesRepo $modificacionesVistasRepo,
        OficiosVistosRepo $oficiosVistosRepo,
        OficiosRepo $oficiosRepo,
        OficiosCesadosRepo $cesadosRepo,PersonasRepo $personasRepo,
        TiposOficiosRepo $tiposOficiosRepo,
        SinEfecto $sinEfecto
    ){

        $this->oficiosRepo               = $oficiosRepo;
        $this->cesadosRepo               = $cesadosRepo;
        $this->personasRepo              = $personasRepo;
        $this->oficiosVistosRepo         = $oficiosVistosRepo;
        $this->modificacionesVistasRepo  = $modificacionesVistasRepo;
        $this->tiposOficiosRepo          = $tiposOficiosRepo;
        $this->sinEfecto = $sinEfecto;
    }

	public function index()
	{
//        dd("hola");
        $ofi = 0;
        $term = Auth::user()->term_cond;

        $busqueda = Input::except('page');

        if(!empty($busqueda)) {
            $ofi=1;

            $oficios = $this->oficiosRepo->Buscar($busqueda);

            $oficios = $oficios->paginate(15);

            $datos['parametros_busqueda'] = $busqueda;

            $datos['id_usario'] = \Auth::user()->id;

            $datos['resultados'] = json_encode($oficios);

            $emptyPage = Input::get('page', false);

            //$hayOficios  =(count($oficios)>0);

            //$hayBusqueda =(!empty($busqueda));

            //$consultaPositiva = ($hayBusqueda and $hayOficios and !$emptyPage);

//        dd($consultaPositiva);
            //if($consultaPositiva) \Queue::push('Oficios\Colas\OficiosCola@consultaPositiva',$datos);
            return View::make('listados.oficios',compact("oficios","busqueda","ofi","term"));
        }
       
        return View::make('listados.oficios',compact("ofi","term"));
	}

/*    public function filtrar(){
        $data    = Input::all();
        $oficios = $this->oficiosRepo->Buscar($data);


        return View::make('listados.oficios',compact("oficios","data"));
    }*/



    public function indexJson(){

        /*$busqueda = Input::except('page');
        $oficios   = $this->oficiosRepo->Buscar($busqueda);
        return $oficios->get();*/

        $personas = new PersonasRepo();
        return $personas->getPersonasPedidoCaptura();


    }


    public function getSecuestrosVehicular(){
        $vehiculos = new SecuestroVehicularRepo();
        return $vehiculos->getAll();
    }


    public function getVehiculos(){
      $vehiculos = new SecuestroVehicularRepo();
    }


    public function ConfirmarOficio($id){

        $oficios = $this->oficiosRepo->find($id);
        $oficios->timestamps = false;

        $data['confirmado'] = 2;
        $oficiosManager  = new OficiosManager($oficios,$data);
        $rutaArchivo = null;
        if($oficiosManager->save()) {


            return Redirect::route($oficios->tipo->ruta . ".show", $oficios->id)
                ->with("agregar_otro", "true")
                ->with("id", route($oficios->tipo->ruta . ".show", $oficios->id));
        }
    }

    public function LlenarCampos(){

        $data =  Input::all();

        $id   = $data["id"];


        Session::forget('oficio_id');


        return $this->oficiosRepo->find($id)->toJson();


    }

    public function cargarOtro($id){

        Session::put("oficio_id",$id);

        $oficios =  $this->oficiosRepo->find($id);
        
        //dd($oficios);


        $ruta   = $oficios->tipo->ruta;

        $ruta.=".create";
//dd($ruta);
        return Redirect::route($ruta);


    }


    public function cantMod(){

        return;
        //eliminar los count
        //return $this->modificacionesVistasRepo->modNoVistas();


    }


    public function tieneOficio(){
        $nro_causa = Input::get('nro_causa');
        $dni = Input::get('dni');

        $tipo_oficio = Input::get('tipo_oficio');

        $response = ["respuesta"=>"false"];

        $persona  = $this->personasRepo->getPersonaDni(compact("dni","nro_causa","tipo_oficio"));
        if($persona->count()>0){
            $queryString = ['buscar_por'=>'personas','personas[nro_documento]'=>$dni];
            $response = ["respuesta"=>"true","url"=>route('oficios.index',$queryString)];
            return Response::json($response);
            $response = ["respuesta"=>"false"];
        }
            return Response::json($response);
    }


    public function cesar(){
        $data = Input::all();

        if(isset($data['link'])){
            $oficios = $this->oficiosRepo->find($data['id_oficio']);

            $this->sinEfecto->dejarSinEfecto($data);
            $path = $oficios->tipo->ruta.'/'.$data['id_oficio'];
            return Redirect::to('/oficios/cease/'.$data['id_oficio'])->with('message',' Los Cambios se realizaron Correctamente')->with('path',$path);
        }
        return $this->sinEfecto->dejarSinEfecto($data);
    }

    public function vigente(){
        $data = Input::all();
        $id = $data['id_oficio2'];

        $oficio = Oficios::find($id);
        $oficio->estado = '1';
        $valor = OficiosRepo::delOficioCesado($id);

            $oficio->save();



        return $valor;
    }

    public function cesar_link($id){

        if($id) {
            $id_oficio = $id;
            return View::make('listados.cesar', compact("id_oficio"));
        }
        return Redirect::to('/oficios')->with('message',' Oficio Invalido!');
    }

    public function plazo_link($id){
        //dd($id);
        //if(is_int($id)) {
            $id_oficio = $id;
            return View::make('listados.plazo', compact("id_oficio"));
        //}
        //return Redirect::to('/oficios')->with('message',' Oficio Invalido!');
    }

    public function oficiosNoLeidos(){
        $idUsuario      =  Auth::user()->id;
        $oficiosNoLeido =  $this->oficiosRepo->getOficiosNoLeidos($idUsuario);
        foreach($oficiosNoLeido as $oficio){
            $oficioVistoManager = new OficiosVistoManager();
            $oficioVistoManager->saveMarcado($oficio->id);
        }
        return Redirect::back()->withInput()->with(['msg'=>'Se marcaron todos los oficios como leidos']);

    }

    public function cantModDetalle(){

        return;
        //eliminar los count
        //return $this->modificacionesVistasRepo->cantOficiosDetalle();

    }



	public function destroy($id)
	{
        $this->oficiosRepo->delete($id);
        return Redirect::back()->withInput();
	}

    public function inicio(){

        
        $term = Auth::user()->term_cond;
        if(Auth::user()->id_perfil == 2){
            return Redirect::route('oficios.index');
        }
        return View::make('inicio')->with('term',$term);
    }


    public function getCantidadOficiosNoVistos(){
        if (Request::ajax()) {
            return ;
            //return $this->oficiosRepo->getCantidadOficiosNoVistos();
        }
    }
    public function getCantidadOficiosNoVistosDetalle(){
        if (Request::ajax()) {
            return ;
            //return $this->oficiosRepo->getCantidadOficiosDetalle();
        }
    }

    public function getLog($id){
            $oficio = $this->oficiosRepo->find($id);

            return View::make('log.log')->with('oficio',$oficio);


    }


    public function confirmarVencimiento($id){

        $oficios = $this->oficiosRepo->find($id);
        $data = Input::all();
        $oficios->vencimiento_confirmado = 1;
        $oficiosManager = new OficiosManager($oficios,$data);
        $oficiosManager->save();

    }

    public function getAutoridad($idOficio){

        $oficios = $this->oficiosRepo->find($idOficio);
        $form_data     = array('id'=>'frmAutoridad','route' => ['oficios.autoridad',$idOficio], 'method' => 'POST','enctype' => 'multipart/form-data');
        return View::make('formularios.autoridad_judicial_edit',compact("oficios","form_data"));

    }

    public function setAutoridad($idOficio){

        $oficios = $this->oficiosRepo->find($idOficio);
        $data    =  Input::all();
        $oficiosManager  = new OficiosManager($oficios,$data);
        $oficiosManager->save();
        return Redirect::back()->with(['mensa_ok'=>"Autoridad Judicial Editada Correctamente"]);
    }
    public function getOficiosByAnio(){
        return  $this->oficiosRepo->getOficiosByAnio();
    }

    public function adminOficiosByAnio(){
        $oficios  = $this->oficiosRepo->getOficiosByAnio();


        return View::make('admin.oficios_vencidos',compact("oficios"));
    }





}
