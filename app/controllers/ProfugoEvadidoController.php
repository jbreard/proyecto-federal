<?php

use Administracion\Repositories\RepoMensajeAnio;
use Oficios\Managers\DocumentosManager;
use Oficios\Managers\OficiosManager;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Managers\PersonaManager;
use Oficios\Managers\ProfugoEvadidoManager;
use Oficios\Repositories\DocumentosRepo;
use Oficios\Repositories\OficiosRepo;
use Oficios\Repositories\PersonasRepo;

class ProfugoEvadidoController extends \BaseController {

    protected $oficiosRepo;
    protected $personasRepo;
    protected $documentosRepo;
    protected $oficiosVistoManager;
    private $repoMensajeAnio;

    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo, RepoMensajeAnio $repoMensajeAnio){

        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->documentosRepo          = $documentosRepo;
        $this->titulo_oficio           = "Prófugo / Evadido";
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->repoMensajeAnio = $repoMensajeAnio;
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data = array('id'=>'frmProfugoEvadido','route' => 'profugo_evadido.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = $this->titulo_oficio;
        $profugo_evadido = true;
        Session::put('alerta_temprana',1);

        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio","profugo_evadido"));

    }


    public function validarProfugoEvadido()
    {
        $data = Input::all();

//        dd($data);

        $oficios            = $this->oficiosRepo->newOficio(13);

        $responsable           = $this->personasRepo->newPersona($oficios->id,6);
//
        $profugoEvadidoManager = new ProfugoEvadidoManager($responsable,$data);
        if(!$profugoEvadidoManager->isValid())
            return $profugoEvadidoManager->mostrarErrores($profugoEvadidoManager->isValid());

        return "Ok";




//
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data               = Input::all();

        unset($data['id']);


        $oficios            = $this->oficiosRepo->newOficio(12);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $responsable        = $this->personasRepo->newPersona($oficios->id,3);
        $responsableManager = new PersonaManager($responsable,$data);
        $responsableManager->save();
        $responsableManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }

//        dd($oficios->tipo);


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $oficios        = $this->oficiosRepo->find($id);
        $responsable    = $this->personasRepo->getPersona($id,3);

        $this->oficiosVistoManager->save($id);

        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;

        $profugo_evadido = true;

        return View::make('solo_lectura.pedidos_captura',compact("titulo_oficio","datos","profugo_evadido"));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->oficiosVistoManager->save($id);
        $form_data = array('route' => ['profugo_evadido.update',$id], 'post' => 'patch','enctype' => 'multipart/form-data','id'=>"frmProfugoEvadido");
        $oficios        = new OficiosRepo();
        $persona        = new PersonasRepo();
        $oficios        = $oficios->find($id);
        $responsable    = $persona->getPersona($id,3);
        $documentos     = $responsable->getDocumentos;
        $titulo_oficio  = $this->titulo_oficio;
        $profugo_evadido = true;
        return View::make('formularios.pedidos_captura',compact("form_data","titulo_oficio","oficios","victima","responsable","documentos","profugo_evadido"));

    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
        $data = Input::all();
        $oficios        = new OficiosRepo();
        $persona        = new PersonasRepo();

        $oficios        = $oficios->find($data['id']);
        $responsable    = $persona->getPersona($data['id'],3);
        $personaManager = new PersonaManager($responsable,$data);
        $oficiosManager = new OficiosManager($oficios,$data);

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$responsable['id']);

        $oficiosManager ->save(true,['archivo1','archivo2','archivo3','archivo4']);
        $personaManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);
        $personaManager->save();
        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }

    public function getPdf($id)
    {
        $oficios        = $this->oficiosRepo->find($id);
        $responsable    = $this->personasRepo->getPersona($id,3);



        $datos = compact('oficios','persona','responsable');

        $titulo_oficio = $this->titulo_oficio;

        $profugo_evadido = true;

        $html = View::make('solo_lectura.pdf.pedidos_captura',compact("titulo_oficio","datos","profugo_evadido"))->render();

        $this->oficiosRepo->getPdf($html);

    }



}
