<?php

use Combos\Repositories\Organismos as OrganismosCombos;
use SubOrganismos\Repositories\SubOrganismosRepo as SubOrganismosRepo;
use SubOrganismos\Managers\SubOrganismosManager;
use SubOrganismos\Entities\SubOrganismos;

class SubOrganismoController extends \BaseController{

    protected $organismosCombos;
    protected $subOrganismoRepo;

public function __construct(SubOrganismosRepo $subOrganismoRepo, OrganismosCombos $organismosCombos){


    $this->subOrganismoRepo    = $subOrganismoRepo;
    $this->organismosCombos    = $organismosCombos;
}
    /**
*/
public function index()
{
    $org = $this->subOrganismoRepo->all();

    //dd($org);

    return View::make("suborganismo.listado", array('Organismo' => $org));




}


/**
* Show the form for creating a new resource.
*
* @return Response
*/
public function create()
{
			$combosOrganismos = $this->organismosCombos->getCombo();
      	    $form_data  = array('route' => 'subOrganismos.store', 'method' => 'POST','enctype' => 'multipart/form-data');
    		return View::make("suborganismo.create",compact("form_data","combosOrganismos"));

}


public function getSubOrganismoByIdOrg($idOrg){
    return $this->subOrganismoRepo->getSubOrganismoByIdOrg($idOrg);
}


/**
* Store a newly created resource in storage.
*
* @return Response
*/
public function store()
{
	$data    = Input::all();

        $organismo           = $this->subOrganismoRepo->newOrganismo();
        $OrganismoManager    = new SubOrganismosManager($organismo,$data);
        $OrganismoManager->save();

        return Redirect::route('panel.index');

}

/**
* Display the specified resource.
*
* @param  int  $id
* @return Response
*/
public function show($id)
{
    $subOrganismo = $this->subOrganismoRepo->find($id);

    if (is_null($subOrganismo)) App::abort(404);

    $combosOrganismos = $this->organismosCombos->getCombo();

    $form_data  = array('route' => ['subOrganismos.update',$id], 'method' => 'PATCH','enctype' => 'multipart/form-data');

    return View::make("suborganismo.create", compact("subOrganismo","form_data","combosOrganismos"));

}


/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return Response
*/
public function edit($id)
{

}


/**
* Update the specified resource in storage.
*
* @param  int  $id
* @return Response
*/
public function update($id)
{
        $organismo = $this->subOrganismoRepo->find($id);
        // Obtenemos la data enviada por el usuario
        $data = Input::all();


        $OrganismoManager    = new SubOrganismosManager($organismo,$data);
        $OrganismoManager->save();

        if($OrganismoManager->save()) return Redirect::route('subOrganismos.index');

        return Redirect::back()->withInput()->withErrors($OrganismoManager->getErrors());
}


/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return Response
*/
public function destroy($id)
{
//
}


}
