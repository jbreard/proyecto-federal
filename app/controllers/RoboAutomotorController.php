<?php

use Oficios\Clases\ArchivosManager;
use Oficios\Managers\DocumentosManager;
use Oficios\Managers\FotosVehicularManager;
use Oficios\Managers\OficiosManager;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Managers\PersonaManager;
use Oficios\Managers\RoboAutomotorManager;
use Oficios\Managers\RoboAutomotorResponsableManager;
use Oficios\Managers\SecuestroVehicularManager;
use Oficios\Repositories\DocumentosRepo;
use Oficios\Repositories\FotosVehicularRepo;
use Oficios\Repositories\OficiosRepo;
use Oficios\Repositories\PersonasRepo;
use Oficios\Repositories\SecuestroVehicularRepo;
use Oficios\Repositories\TiposOficiosRepo;

class RoboAutomotorController extends \BaseController {

    protected $secuestroVehicularRepo;
    protected $oficiosRepo;
    protected $personasRepo;
    protected $oficiosVistoManager;
    protected $documentosRepo;
    protected $tiposOficiosRepo;
    public function __construct(SecuestroVehicularRepo $secuestroVehicularRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo, OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo, TiposOficiosRepo $tiposOficiosRepo){

        $this->secuestroVehicularRepo  = $secuestroVehicularRepo;
        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->documentosRepo          = $documentosRepo;
        $this->tiposOficiosRepo        = $tiposOficiosRepo;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data  = array('id'=>'frmRoboAutomotor','route' => 'robo_automotor.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $tipoOficio = $this->tiposOficiosRepo->find(13);
        $robo_automotor = true;
        Session::put('alerta_temprana',1);
        return View::make('formularios.secuestro_vehicular',compact("form_data","tipoOficio","robo_automotor"));
	}


    public function validarRoboAutomotor()
    {
        $data = Input::all();
//
//        dd($data);
        $oficios            = $this->oficiosRepo->newOficio(13);

        $responsable           = $this->personasRepo->newPersona($oficios->id,6);
//
        $secuestroVehicular       = $this->secuestroVehicularRepo->newSecuestro($responsable->id,$oficios->id);

        $secuestroVehicularManager = new RoboAutomotorManager($secuestroVehicular,$data);
        if(!$secuestroVehicularManager->isValid())
            return $secuestroVehicularManager->mostrarErrores($secuestroVehicularManager->isValid());


//        if($data['marca_vehiculo'] == 0 && $data['modelo_vehiculo'] == 0 && $data['color'] == '')
//            return "Debe completarla marca, o el modelo, o el color";

        $roboAutomotorResponsableManager = new RoboAutomotorResponsableManager($responsable,$data['datos_persona']['responsable']);
        if(!$roboAutomotorResponsableManager->isValid())
            return $roboAutomotorResponsableManager->mostrarErrores($roboAutomotorResponsableManager->isValid());


//        dd($data);

        return "Ok";




//
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data = Input::all();

        unset($data['id']);


        $oficios            = $this->oficiosRepo->newOficio(13);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

//        $titular            = $this->personasRepo->newPersona($oficios->id,5);
//        $titularManager     = new PersonaManager($titular,$data["datos_persona"]["titular"]);
//        $titularManager->save();

        $responsable           = $this->personasRepo->newPersona($oficios->id,6);
        $responsableManager     = new PersonaManager($responsable,$data["datos_persona"]["responsable"]);
        $responsableManager->save();
//
        $secuestroVehicular       = $this->secuestroVehicularRepo->newSecuestro($responsable->id,$oficios->id);

        $data['id_oficio'] = $oficios->id;
        $secuestroVehicularManager = new RoboAutomotorManager($secuestroVehicular,$data);
//
        if(!$secuestroVehicularManager->isValid())
            return $secuestroVehicularManager->mostrarErrores($secuestroVehicularManager->isValid());

        $secuestroVehicularManager->save();

        $this->saveFotos($oficios->id);
//
//
        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }
//
//
//        dd($oficios);
        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");


    }

    public function saveFotos($idOficio)
    {
        $fotosVehicular = new ArchivosManager($idOficio);
        $fotosVehicular->setBaseRepo(new FotosVehicularRepo());
        $fotosVehicular->setBaseManager(new FotosVehicularManager());
        $fotosVehicular->subir('fotos_vehicular');
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $datos    = $this->secuestroVehicularRepo->getRoboAutomotor($id);
        $datos[]  ="Robo vehicular";
        $robo_automotor = true;
        Session::put('alerta_temprana',1);
//        dd($secuestro_vehicular);
        return View::make('solo_lectura.secuestro_vehicular',compact('datos','robo_automotor'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->oficiosVistoManager->save($id);

        $form_data = array('route' => ['robo_automotor.update',$id],'id'=>'frmRoboAutomotor','post' => 'PATCH','enctype' => 'multipart/form-data');

        $secuestro_vehicular = $this->secuestroVehicularRepo->getRoboAutomotor($id);

        $tipoOficio = $this->tiposOficiosRepo->find(13);

        $robo_automotor = true;

        Session::put('alerta_temprana',1);

        return View::make('formularios.secuestro_vehicular',compact("form_data","tipoOficio","robo_automotor")+$secuestro_vehicular);





    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
        $data = Input::all();

//        dd($data['id']);
        extract($this->secuestroVehicularRepo->getRoboAutomotor($data['id']));

        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

//        $titularManager     = new PersonaManager($titular,$data['datos_persona']['titular']);
//        $titularManager->save();

        $responsableManager     = new PersonaManager($responsable,$data['datos_persona']['responsable']);
        $responsableManager->save();


        $data['id_oficio'] = $oficios->id;
        $secuestroVehicularManager = new RoboAutomotorManager($secuestro_vehicular,$data);


        if(!$secuestroVehicularManager->isValid())
            return $secuestroVehicularManager->mostrarErrores($secuestroVehicularManager->isValid());

        $secuestroVehicularManager->save();


        $this->saveFotos($oficios->id);

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$responsable['id']);


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }

    public function getPdf($id)
    {
        $datos    = $this->secuestroVehicularRepo->getRoboAutomotor($id);
        $datos[]  ="";
        $robo_automotor = true;
        $html     = View::make('solo_lectura.pdf.secuestro_vehicular',compact('datos','robo_automotor'))->render();
        $this->oficiosRepo->getPdf($html);
    }


}
