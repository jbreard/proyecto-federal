<?php

use Oficios\Repositories\SolicitudParaderoRepo;

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\SolicitudParaderoManager;

use Oficios\Managers\OficiosVistoManager;



class SolicitudParaderoReqJudController extends \BaseController {

    protected $paraderoRepo;
    protected $oficiosRepo;
    protected $personasRepo;
    protected $oficiosVistoManager;

    public function __construct(SolicitudParaderoRepo $paraderoRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager){

        $this->paraderoRepo        = $paraderoRepo;
        $this->oficiosRepo         = $oficiosRepo;
        $this->personasRepo        = $personasRepo;
        $this->oficiosVistoManager = $oficiosVistoManager;
    }


    /**
     * Display a listing of the resource.
     * no lo voy a usar en este caso
     *
     * @return Response
     */




    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $form_data     = array('id'=>'form','route' => 'solicitud_paradero2.store', 'method' => 'POST','enctype' => 'multipart/form-data');

        $titulo_oficio = "Solicitud de Paradero por Comparendo";

        return View::make('formularios.solicitud_paradero2',compact("form_data","titulo_oficio"));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data    = Input::all();

        //dp($data);
        //dd(Input::hasFile('foto.buscado.0'));
        //dd(Input::hasFile("foto"));


        $oficios            = $this->oficiosRepo->newOficio(6);//tipo de oficio
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);



        $buscado           = $this->personasRepo->newPersona($oficios->id,3);
        $buscadoManager     = new PersonaManager($buscado,$data["datos_persona"]["desaparecido"]);
        $buscadoManager->save();


        $buscadoManager->CargarFotos($oficios-> id,$data['foto'],$buscado->id);

        $solicitudParadero = $this->paraderoRepo->newSolicitud($buscado->id);
        $solicitudParaderoManager = new SolicitudParaderoManager($solicitudParadero,$data["datos_persona"]);
        $solicitudParaderoManager->save();


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");


        return Redirect::back()->withInput()->withErrors($oficioManager->getErrors());
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */



    public function show($id)
    {
        $datos = $this->paraderoRepo->getSolicitud($id);


        $titulo_oficio = "Solicitud de Paradero por Comparendo";
        Session::push("plazo_hr",true);
        return View::make('solo_lectura.solicitud_paradero2',compact("titulo_oficio","datos"));

    }
    public function getPdf($id)
    {
        $datos = $this->paraderoRepo->getSolicitud($id);



        $titulo_oficio = "Solicitud de Paradero por Comparendo";

        $html = View::make('solo_lectura.pdf.solicitud_paradero2',compact("titulo_oficio","datos"));

        $this->oficiosRepo->getPdf($html);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $datos = $this->paraderoRepo->getSolicitud($id);



        $form_data     = array('route' => ['solicitud_paradero2.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');

        $titulo_oficio = "Solicitud de Paradero por Comparendo";


        return View::make('formularios.solicitud_paradero2',compact("form_data","titulo_oficio")+$datos);


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();
//        dd($data["datos_persona"]);
        extract($this->paraderoRepo->getSolicitud($id));

        $oficioManager      = new OficiosManager($oficios,$data);

        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $buscadoManager     = new PersonaManager($buscado,$data["datos_persona"]["desaparecido"]);
        $buscadoManager->CargarFotos($oficios-> id,$data['foto'],$buscado->id);
        $buscadoManager->save();
//        dd($buscadoManager);

        $solicitudParaderoManager = new SolicitudParaderoManager($solicitud_paradero,$data["datos_persona"]);
        $solicitudParaderoManager->save();


        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
