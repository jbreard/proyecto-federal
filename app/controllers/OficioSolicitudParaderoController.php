<?php

use Oficios\Repositories\SolicitudParaderoRepo;

use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\DocumentosRepo;

use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;

use Oficios\Managers\SolicitudParaderoManager;

use Oficios\Managers\OficiosVistoManager;

use Oficios\Managers\DocumentosManager;


class OficioSolicitudParaderoController extends \BaseController {

    protected $paraderoRepo;
    protected $oficiosRepo;
    protected $personasRepo;
    protected $oficiosVistoManager;
    protected $documentosRepo;

    public function __construct(SolicitudParaderoRepo $paraderoRepo,OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,OficiosVistoManager $oficiosVistoManager, DocumentosRepo $documentosRepo){

        $this->paraderoRepo        = $paraderoRepo;
        $this->oficiosRepo         = $oficiosRepo;
        $this->personasRepo        = $personasRepo;
        $this->oficiosVistoManager = $oficiosVistoManager;
        $this->documentosRepo      = $documentosRepo;
    }


	/**
	 * Display a listing of the resource.
     * no lo voy a usar en este caso
	 *
	 * @return Response
	 */




	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
//        dd("hola");
		$form_data     = array('id'=>'form','route' => 'solicitud_paradero.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        Session::push("plazo_hr",true);
		return View::make('formularios.solicitud_paradero.formulario',compact("form_data","titulo_oficio"));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data    = Input::all();
        //dd(Input::hasFile('foto.buscado.0'));
        //dd(Input::hasFile("foto"));


        $oficios            = $this->oficiosRepo->newOficio(1);//tipo de oficio
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);



        $buscado           = $this->personasRepo->newPersona($oficios->id,3);
        $buscadoManager     = new PersonaManager($buscado,$data["datos_persona"]["desaparecido"]);
        $buscadoManager->save();


        $buscadoManager->CargarFotos($oficios->id,$data['foto'],$buscado->id);


        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$buscado['id']);
        }


        $solicitante       = $this->personasRepo->newPersona($oficios->id,4);
        $solicitanteManager = new PersonaManager($solicitante,$data["datos_persona"]["denunciante"]);
        $solicitanteManager->save();

        $solicitudParadero = $this->paraderoRepo->newSolicitud($buscado->id);
        $solicitudParaderoManager = new SolicitudParaderoManager($solicitudParadero,$data["datos_persona"]["solicitud_paradero"]);
        $solicitudParaderoManager->save();




        //return Redirect::route("oficios.index")->with('id',$oficios->id);

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

        //return Redirect::back()->withInput()->withErrors($oficioManager->getErrors());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */



    public function show($id)
    {
        $datos = $this->paraderoRepo->getSolicitud($id);
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        Session::push("plazo_hr",true);
        return View::make('solo_lectura.solicitud_paradero',compact("titulo_oficio","datos"));

    }
    public function getPdf($id)
    {
        $datos = $this->paraderoRepo->getSolicitud($id);
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        $html = View::make('solo_lectura.pdf.solicitud_paradero',compact("titulo_oficio","datos"));
        Session::push("plazo_hr",true);
        $this->oficiosRepo->getPdf($html);

    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $datos = $this->paraderoRepo->getSolicitud($id);
        $form_data     = array('route' => ['solicitud_paradero.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
        Session::push("plazo_hr",true);
        $titulo_oficio = "Busqueda de Personas Extraviadas";
        return View::make('formularios.solicitud_paradero',compact("form_data","titulo_oficio")+$datos);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();

        extract($this->paraderoRepo->getSolicitud($id));

        $oficioManager      = new OficiosManager($oficios,$data);

        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);


        $buscadoManager     = new PersonaManager($buscado,$data["datos_persona"]["desaparecido"]);

        $buscadoManager->CargarFotos($oficios-> id,$data['foto'],$buscado->id);

        $buscadoManager->save();


        $solicitanteManager = new PersonaManager($denunciante,$data["datos_persona"]["denunciante"]);
        $solicitanteManager->save();


        $solicitudParaderoManager = new SolicitudParaderoManager($solicitud_paradero,$data["datos_persona"]["solicitud_paradero"]);
        $solicitudParaderoManager->save();

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$buscado['id']);

        //return Redirect::route("oficios.index");
        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }


}
