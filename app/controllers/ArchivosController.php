<?php
use Oficios\Repositories\ArchivosRepo;

/**
 * Created by PhpStorm.
 * User: juan
 * Date: 04/11/14
 * Time: 17:01
 */

class ArchivosController extends BaseController {


    /**
     * @var ArchivosRepo
     */
    private $archivosRepo;

    function __construct(ArchivosRepo $archivosRepo)
    {
        $this->archivosRepo = $archivosRepo;
    }

    public function delete($idArchivo,$idOficio){
//        dd("hola");
        if($this->archivosRepo->countFiles($idOficio) > 1)
        {
            $archivo = $this->archivosRepo->getArchivo($idArchivo,$idOficio);
            $respuesta = ["success"=>true];
            if($archivo->delete()){
                //unlink($archivo->path);
                return Response::json($respuesta);
            }
        }
        else
        {
            $respuesta = ["success"=>false];
            return Response::json($respuesta);
        }
    }

} 