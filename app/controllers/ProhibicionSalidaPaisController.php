<?php


use Oficios\Repositories\OficiosRepo;

use Oficios\Repositories\PersonasRepo;

use Oficios\Repositories\ProhibSalidaRepo;
use Oficios\Repositories\DocumentosRepo;


use Oficios\Managers\OficiosManager;

use Oficios\Managers\PersonaManager;
use Oficios\Managers\OficiosVistoManager;
use Oficios\Managers\DocumentosManager;


class ProhibicionSalidaPaisController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


    protected $oficiosRepo;
    protected $personasRepo;
    protected $prohibSalidaRepo;
    protected $oficiosVistoManager;
    protected $documentosRepo;
    private   $titulo;

    public function __construct(OficiosRepo $oficiosRepo,PersonasRepo $personasRepo,ProhibSalidaRepo $prohibSalidaRepo,OficiosVistoManager $oficiosVistoManager,DocumentosRepo $documentosRepo){

        $this->oficiosRepo             = $oficiosRepo;
        $this->personasRepo            = $personasRepo;
        $this->prohibSalidaRepo        = $prohibSalidaRepo;
        $this->oficiosVistoManager     = $oficiosVistoManager;
        $this->titulo                  = "Prohibición de Salida del País / Provincia";
        $this->documentosRepo          = $documentosRepo;
    }

    public function index()
	{
		//
	}

    public function  getPdf($id){


        $datos = $this->prohibSalidaRepo->getProhibSalida($id);
        $titulo_oficio = $this->titulo;

        $html = View::make('solo_lectura.pdf.prohibicion_salida',compact("titulo_oficio","datos"))->render();

        return $this->oficiosRepo->getPdf($html);

    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $form_data = array('id'=>'form','route' => 'prohibicion_salida.store', 'method' => 'POST','enctype' => 'multipart/form-data');
        Session::push("plazo_fecha",true);
        return View::make('formularios.prohibicion_salida',compact("form_data"));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data = Input::all();

        $oficios           = $this->oficiosRepo->newOficio(3);
        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);
        $responsable            = $this->personasRepo->newPersona($oficios->id,2);
        $responsableManager     = new PersonaManager($responsable,$data);
        $responsableManager->save();
        $responsableManager->CargarFotos($oficios->id,$data['foto'],$responsable->id);

        if (isset($data["datos_documento"])) {
            $dataDocumentos     = $data["datos_documento"];
            $this->guardarDocumentos($dataDocumentos,$responsable['id']);
        }

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");
	}



    public function show($id)
    {

        $datos = $this->prohibSalidaRepo->getProhibSalida($id);
        $titulo_oficio = $this->titulo;
        $form_data = $this->setFormDataEdit($id);
        Session::push("plazo_fecha",true);
        Session::push("edit",true);

        return View::make('solo_lectura.prohibicion_salida',compact("titulo_oficio","datos","form_data"));

    }


    private  function setFormDataEdit($id){
        return array('route' => ['prohibicion_salida.update',$id],'id'=>'form','method' => 'PATCH','enctype' => 'multipart/form-data');
    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $form_data = $this->setFormDataEdit($id);
        Session::push("plazo_fecha",true);
        return View::make('formularios.prohibicion_salida',compact("form_data")+$this->prohibSalidaRepo->getProhibSalida($id));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();

        extract($this->prohibSalidaRepo->getProhibSalida($id));

        $oficioManager      = new OficiosManager($oficios,$data);
        $oficioManager->save(true,['archivo1','archivo2','archivo3','archivo4']);

        $responsableManager     = new PersonaManager($buscado,$data);
        $responsableManager->save();

        if(!isset($data["datos_documento"])) {
            $dataDocumentos = array();
        }
        else{
            $dataDocumentos      = $data["datos_documento"];
        }

        $this->guardarDocumentos($dataDocumentos,$buscado['id']);

        return Redirect::route($oficios->tipo->ruta.".show",$oficios->id)->with('carga',"true");

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function procesoAutorizador()
    {
        $repo = new Oficios\Repositories\OficiosAutorizadosRepo();
        $proceso = new Oficios\Scheduled\OficiosPlazosAutorizados\OficiosPlazosAutorizados($repo);

        $proceso->actualizarOficiosAutorizados();
    }

    function guardarDocumentos($dataDocumentos,$idPersona) {
        foreach ($dataDocumentos as $key => $value) {
            $documento = $this->documentosRepo->getModel();
            if (isset($value["id_documento"])) {
                $documento = $this->documentosRepo->find($value["id_documento"]);
            }
            $managerDocumento = new DocumentosManager($documento, $value);
            $managerDocumento->saveDocumento($idPersona);
        }
    }


}
