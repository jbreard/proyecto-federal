<?php

use Combos\Repositories\Organismos as OrganismosRepo;
use Oficios\Repositories\GruposOrganismosMiembrosRepo;
use Oficios\Repositories\GruposOrganismosRepo;
use Oficios\Managers\GruposOrganismosManager;
use Oficios\Managers\GruposOrganismosMiembrosManager;

class GruposCorreosController extends \BaseController {

protected $organismosRepo,$gruposOrganismosMiembrosRepo,$gruposOrganismosRepo;

function __construct(OrganismosRepo $organismosRepo,GruposOrganismosMiembrosRepo $gruposOrganismosMiembrosRepo,GruposOrganismosRepo $gruposOrganismosRepo) {
	$this->organismosRepo = $organismosRepo;
	$this->gruposOrganismosMiembrosRepo = $gruposOrganismosMiembrosRepo;
	$this->gruposOrganismosRepo = $gruposOrganismosRepo;
}


public function index()
{

	$organismos =  $this->organismosRepo->getWitoutSd();

	return View::make('admin.listados_grupos_correos',compact('organismos'));	
}

public function getGrupos(){
	return $this->gruposOrganismosRepo->all();
}

public function createGrupo(){
	$data  = Input::all();
	$model = $this->gruposOrganismosRepo->newModel($data['descripcion']);
	$manager = new GruposOrganismosManager($model,[]);
	$manager->save();
	return $model;
}



public function createMiembroOrganismo(){
	$data = Input::all();
	$model = $this->gruposOrganismosMiembrosRepo->newModel($data);
	$manager = new GruposOrganismosMiembrosManager($model,[]);
	$manager->save();
}

public function removeMiembroOrganismo(){
	$data = Input::all();
	$model = $this->gruposOrganismosMiembrosRepo->getMiembro($data);
	$manager = new GruposOrganismosMiembrosManager($model,[]);
	$manager->delete();
}

public function getOrganismos(){
	return $this->organismosRepo->getWitoutSd();
}

public function getMiembrosByGrupo($idGrupo){
	
	return $this->gruposOrganismosMiembrosRepo->getMiembrosByIdGrupo($idGrupo);

}


}
