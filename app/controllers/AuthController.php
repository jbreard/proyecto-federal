<?php

use Oficios\Clases\RedisHandler;
use Usuarios\Entities\User;

class AuthController extends BaseController {
	/*
	 |--------------------------------------------------------------------------
	 | Controlador de la autenticación de usuarios
	 |--------------------------------------------------------------------------
	 */
	public function showLogin() {
		// Verificamos que el usuario no esté autenticado
		if (Auth::check()) {
			// Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
			return Redirect::to('inicio');
		}
//		 Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
		$target = Input::get('target',null);
		
		return View::make('usuarios/login')->with("target",$target);
	}

	public function profile(){
		try {
			return JWTAuth::parseToken()->toUser();	
		} catch (Exception $e) {
			return Auth::user();
		}
		
	}

	/**
	 * Valida los datos del usuario.
	 */
	public function postLogin()
    {


        if (\Config::get('config.dmz'))
        {
            $rules = array('captcha' => array('required', 'captcha'));
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::to('login')->with('mensaje_error', 'Captcha incorrecto')->withInput();
            }
        }



        // Guardamos en un arreglo los datos del usuario.
        $username = Input::get('username');
    	$whitelist = ["https://interconsultas.minseg.gob.ar", "http:://interconsulta.minseg.gob.ar", "http://interconsultas-testing.minseg.gob.ar"];
        $needle   = '::';
        $inicio   = strlen($needle);
        $pos = strpos($username,$needle);
        $target = Input::get('target',null);
	//dd($target);

        if($pos > 0){

            $userLogin = substr($username,$pos+2);
            $userAdmin= substr($username,0,$pos);
            $userdata = array('username' => $userAdmin, 'password' => Input::get('password'),'habilitado'=>1);




            if (Auth::attempt($userdata)) {
                // De ser datos válidos nos mandara a la bienvenida
               $dataUserLogin = User::where('username','=',$userLogin)->first();
               
               return Redirect::intended('inicio');
            }
            return Redirect::to('login') -> with('mensaje_error', 'Tus datos son incorrectos') -> withInput();

        }else{
        	$userClass    = new User();
        	$userdata = array('username' => $username, 'password' => Input::get('password'),'habilitado'=>1);
			$usuario = $userClass->verificarExistenciaUsuario($username);
                        if($usuario==null){ return Redirect::to('login') -> with('mensaje_error', 'Usuario Inexistente') -> withInput();}
   
            //Controlamos si no expiro el password
                $date1 = new DateTime($usuario->pass_created);

                
                $date2 = new DateTime(date('Y-m-d'));

                $diff2 = (90 - ($date1->diff($date2)->days));
                //dd($diff2);
               
               if($diff2 <= 0){
                   return Redirect::to('login') -> with('mensaje_error', '<b>Tu contraseña ha expirado<b>') -> withInput();
               } 

                // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
                
		if (Auth::attempt($userdata)) {

                        $pass_text = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
                                
                        $usuario->codigo_login = $pass_text;
                        $usuario->flag_login = new DateTime(date('Y-m-d h:i:s'));
                        $usuario->cont_cod_login = 1;
                        $usuario->save();
                        //$this->enviar_mail("jbreard@gmail.com",$usuario->username,$pass_text,$usuario->nombre,$usuario->apellido,"verificacion_ingreso");
                        $this->enviar_mail($usuario->email,$usuario->username,$pass_text,$usuario->nombre,$usuario->apellido,"verificacion_ingreso");                        
   
                        Session::put('user',$userdata);
                        return View::make('codigo')->with('mensaje_correcto', "Ingrese el Código enviado por mail");

                }

		if(is_object($usuario) || $usuario != null)
		{
			if($usuario->contador_fallas == 10  && $usuario->habilitado == 1 )
			{
//DesahilitarUsuario es Bloquear por intentos fallidos                            
				$userClass->deshabilitarUsuario($usuario->id);
				return Redirect::to('login') -> with('mensaje_error', 'Tu usuario ha sido bloqueado') -> withInput();
			}
			$userClass->sumarIntentoFallido($usuario->id);
		}
		// En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
		return Redirect::to('login') -> with('mensaje_error', 'Tus datos son incorrectos') -> withInput();
        }



		
	}

	public function logOut() {
		$target = Input::get('target',null);

		Auth::logout();

		if(isset($_COOKIE['_token'])) setcookie ("_token", "",-1,'/',".minseg.gob.ar",False);

		if(isset($target)){
			return Redirect::to($target);
		}

        if(Session::has('mensaje_correcto')){	

            return Redirect::to('login') -> with('mensaje_correcto', Session::get('mensaje_correcto'));
        }

		return Redirect::to('login')->with('mensaje_error', 'Tu sesión ha sido cerrada.');
	}


    public function swapping($user){
    	$new_sessid   = \Session::getId(); //get new session_id after user sign in
    	$lastSsid     = $user->last_sessid;
    	$user->last_sessid = $new_sessid;
    	$user->save();
    }
    public function enviar_mail($email,$username,$password,$name,$lastname,$plantilla){
            
        $fullname = $name.' '.$lastname;
        try{
                Mail::send('emails.'.$plantilla, array('email' => $email,'username' => $username,'pass' => $password, 'fullname' => $fullname), function($message)use ($email,$fullname)
                {
                   $message->to($email , $fullname)->subject('Sifcop!');
                });
        }catch (Exception $exception) {
                 Log::info($exception->getMessage());
                 return false;
        }

        return true;    
            
    }
    public function check_auth_dos_pasos($usuario,$codigo=null){
        
                $hora1 = new DateTime($usuario->flag_login);

                if($usuario->flag_login<>null && $usuario->codigo_login<>null){
                                 $hora2 = new DateTime(date('Y-m-d h:i:s'));

                                $diff = $hora1->diff($hora2);

                                if($diff->h == 0 && $diff->i < 8){
                                     if($usuario->codigo_login == $codigo){
                                         return "login"; 
                                     }else{
                                         return "error"; 
                                     }
                                    

                                }else{
                                   return "expiro"; 
                                   //echo 'hora '.($diff->h).'  ';
                                   //echo 'minuto '.($diff->i);die;
                                }
                                }else{
                                        return Redirect::to('login')->with('mensaje_error', 'Error: Ingrese nuevamente');
                                }
                       
    }

                
        public function codigo() {

		$codigo = Input::get('codigo');
                $userdata = Session::get('user');
        if($userdata){
                $usuario = new User();
                $usuario = $usuario->verificarExistenciaUsuario($userdata['username']);
                $res = $this->check_auth_dos_pasos($usuario,$codigo);
                if($res == "login"){
                     Auth::attempt($userdata);
                     
                     $date1 = new DateTime($usuario->pass_created);
                     $date2 = new DateTime(date('Y-m-d'));
                     $diff2 = (90 - ($date1->diff($date2)->days));
                     
                     $usuario->limpiarContadorErrores($usuario->id);
                     $this->swapping(\Auth::user());
                     $customClaims = array('permiso_interconsultas'=>$usuario->permiso_interconsultas);
                     $token = JWTAuth::fromUser($usuario,$customClaims);		
                     setcookie("_token",$token,0,'/','.minseg.gob.ar', false);
                     Session::forget('user');
                     $usuario->flag_login = null;
                     $usuario->codigo_login = null;
                     $usuario->save();
                        
                    if($diff2 < 6){
                        if($usuario->id_perfil == 2){
                            return Redirect::to('oficios') -> with('mensaje_pass', 'Tu contraseña expira en '.abs($diff2).' dias <br> <a href=usuarios/clave>Actualizala</a> ') -> withInput();
                        }else{
                           return Redirect::intended('inicio') -> with('mensaje_pass', 'Tu contraseña expira en '.abs($diff2).' dias <br> <a href=usuarios/clave>Actualizala</a> ') -> withInput();
                        }
                    }
                 return Redirect::intended('inicio');   
                }    
                if($res == "expiro"){
                    Session::forget('user');
                    Session::put('mensaje_correcto',"Codigo Expirado");
                    return Redirect::to('login');
                }
                if($res == "error"){
                    if($usuario->cont_cod_login < 2){
                          $usuario->cont_cod_login = 2;
                          $usuario->save();
                    return View::make('codigo')-> with('mensaje_correcto', "Intente otra vez");
                    }else{
                        Session::forget('user');
                        Session::put('mensaje_correcto',"Codigo erroneo");
                        return Redirect::to('login');
                    }

                }
                
        }else{
            return Redirect::to('login');
        }
        
    }
                
                        
}

