<?php
use Combos\Repositories\ModeloVehiculoRepo;

/**
 * Created by PhpStorm.
 * User: juan
 * Date: 17/12/14
 * Time: 13:35
 */

class VehiculosController extends BaseController {

    private $modeloVehiculoRepo;

    function __construct(ModeloVehiculoRepo $modeloVehiculoRepo)
    {
        $this->modeloVehiculoRepo = $modeloVehiculoRepo;
    }

    public function modelosByMarca($id)
    {
        $campos  = ['id','modelo as descripcion'];
        return $modelos = $this->modeloVehiculoRepo->getByMarca($id,$campos);
    }
}
