<?php
namespace Oficios\Scheduled\OficiosPlazosAutorizados;

use Carbon\Carbon;
use Oficios\Repositories\OficiosAutorizadosRepo;
use Oficios\Repositories\PersonasExtraviadas\RepoPEPersonaBuscada;
use Oficios\Repositories\PersonasRepo;
use Oficios\Repositories\PlazosRepo;
use Oficios\Repositories\OficiosRepo;

class OficiosPlazosAutorizados {

    protected $oficiosAutorizadosRepo, $oficiosRepo;

    private $plazosRepo,$fechaActual;

    function __construct(OficiosAutorizadosRepo $oficiosAutorizadosRepo, PlazosRepo $plazosRepo, Oficiosrepo $oficiosRepo)
    {
        $this->oficiosAutorizadosRepo = $oficiosAutorizadosRepo;
        $this->plazosRepo = $plazosRepo;
        $this->oficiosRepo = $oficiosRepo;
        $this->fechaActual = Carbon::now();
    }

    public function actualizarOficiosAutorizados()
    {
/*
        $oficios = $this->oficiosAutorizadosRepo->getOficiosEstado2();

        $this->walkOficios($oficios);

        $this->updatePersonasFromPersonasExtraviadas();

        $this->igualarPersonasExtraviadasAPersonas();
*/
    }


    private function walkOficios(&$oficios){
      foreach($oficios as $oficio)
      {
          $this->walkPlazos($oficio);
      }
    }


    private function walkPlazos(&$oficio){
      /*foreach($oficio->plazos as $plazo)
      {

          //transformamos las fechas por que vienen mutadas por los mutators de laravel.
          $fechaInicio = Carbon::createFromFormat('d/m/Y H:i:s',$plazo->fecha_inicio . " 00:00:00");
          $fechaFinal  = Carbon::createFromFormat('d/m/Y H:i:s',$plazo->fecha_final . " 00:00:00");

          $estaDentroDelRango = $this->fechaActual->gte($fechaInicio) && $this->fechaActual->lt($fechaFinal);

          echo "estaDentroDelRango: "+$estaDentroDelRango;

          //echo "idOficio: ".$oficio->id."rango: ".$estaDentroDelRango;

          $oficio->plazo_autorizado =  ($estaDentroDelRango) ? 1 : 2;
          $oficio->save();

      }*/
      }


    public function eliminarPlazos(){

        $plazos      = $this->plazosRepo->getPlazosVencidos();
        foreach($plazos as $plazo){
            if($plazo->delete())
            {
                $oficios = $this->oficiosRepo->find($plazo->id_oficio);
                $oficios->plazo_autorizado=0;
                $oficios->save();
            }
        }

    }

    private function getRepoPersonas()
    {
        return new PersonasRepo();
    }

    private function getDatosPersonaBuscada($id_oficio)
    {
        $repo = new RepoPEPersonaBuscada();
        return $repo->getModel()->where('id_oficio',$id_oficio)->first();
    }

    private function updatePersonas($id,$id_nacionalidad,$id_sexo,$id_tipo_documento)
    {
//        dd($id,$id_nacionalidad,$id_sexo,$id_tipo_documento);
        $this->getRepoPersonas()->getModel()
            ->withTrashed()
            ->where('id',$id)
            ->update([
                'id_nacionalidad' => $id_nacionalidad,
                'id_sexo' => $id_sexo,
                'id_tipo_documento' => $id_tipo_documento
            ]);
    }

    private function getDataPersonas()
    {
        return $this->getRepoPersonas()->getModel()
            ->join('oficios','oficios.id','=','personas.id_oficio')
            ->where('tipo_oficio',1)
            ->where('id_sexo',0)
            ->orWhere('id_nacionalidad',0)
            ->orWhere('id_tipo_documento',0)
            ->select(['personas.id as id','oficios.id as id_oficio'])
            ->get();

    }

    private function getDataDiferenciaEntreTablas()
    {
        return $this->getRepoPersonas()->getModel()
            ->join('pe_personas_buscadas','pe_personas_buscadas.id_oficio','=','personas.id_oficio')
            ->withTrashed()
            ->whereRaw('sexo <> id_sexo')
            ->orWhereRaw('pe_personas_buscadas.id_nacionalidad <> personas.id_nacionalidad')
            ->orWhereRaw('pe_personas_buscadas.id_tipo_documento <> personas.id_tipo_documento')
            ->select([
                'personas.id as id',
                'personas.id_oficio as id_oficio',
                'personas.id_sexo as id_sexo',
                'pe_personas_buscadas.sexo as sexo',
                'personas.id_nacionalidad as id_nacionalidad',
                'pe_personas_buscadas.id_nacionalidad as nacionalidad_id',
                'personas.id_tipo_documento as id_tipo_documento',
                'pe_personas_buscadas.id_tipo_documento as tipo_documento_id',
            ])
            ->get();
    }

    private function updatePersonasFromPersonasExtraviadas()
    {
        $personas = $this->getDataPersonas();

        foreach($personas as $persona)
        {
            if(is_object($persona))
            {
                $datosPersonaExtraviada = $this->getDatosPersonaBuscada($persona->id_oficio);
                if(is_object($datosPersonaExtraviada))
                {
                    $this->updatePersonas($persona->id,$datosPersonaExtraviada->id_nacionalidad,$datosPersonaExtraviada->sexo,$datosPersonaExtraviada->id_tipo_documento);
                }
            }
        }

    }

    private function igualarPersonasExtraviadasAPersonas()
    {
        $personas = $this->getDataDiferenciaEntreTablas();
//        dd($personas[0]);

//        dd($personas[0]);
        foreach($personas as $persona)
        {
//            echo $persona->id."/".$persona->id_sexo."/".$persona->sexo."\n";
            $this->updatePersonas($persona->id,$persona->nacionalidad_id,$persona->sexo,$persona->tipo_documento_id);

        }

    }


}
