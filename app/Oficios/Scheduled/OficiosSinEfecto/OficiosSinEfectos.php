<?php namespace Oficios\Scheduled\OficiosSinEfecto;
use Illuminate\Support\Facades\Log;
use Oficios\Clases\SinEfecto;
use Oficios\Managers\OficiosManager;
use Oficios\Repositories\OficiosRepo;

/**
 * Created by PhpStorm.
 * User: juan
 * Date: 06/11/14
 * Time: 10:22
 */

class OficiosSinEfectos {
    /**
     * @var OficiosRepo
     */
    private $oficiosRepo;
    /**
     * @var SinEfecto
     */
    private $sinEfecto;

    function __construct(OficiosRepo $oficiosRepo,SinEfecto $sinEfecto)
    {
        $this->oficiosRepo = $oficiosRepo;
        $this->sinEfecto = $sinEfecto;
    }


    public function dejarSinEfecto()
    {
        $oficios = $this->oficiosRepo->getOficiosVencidos();

        foreach($oficios as $oficio){

            $oficio->notMail = true;
            $data["id_oficio"] = $oficio->id;
            $date = \Carbon::now();
            $data["fecha_cese"] = $date->format('d/m/Y');
            $data["observaciones"] = "Requerimiento dejado sin efecto, de manera automatica, por cumplir el plazo de la medida";
            $this->sinEfecto->dejarSinEfecto($data);
        }




    }
}