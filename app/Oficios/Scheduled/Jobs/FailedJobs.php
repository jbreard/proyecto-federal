<?php namespace Oficios\Scheduled\Jobs;


use Oficios\Entities\FailedJob;

class FailedJobs {

    public function correrPendientes(){

        $failedJobs = new FailedJob();

        $failed_jobs =  $failedJobs->limit(50)->get();

        foreach($failed_jobs as $failed_job){
            $id = $failed_job->id;
            \Artisan::call('queue:retry',["id"=>$id]); // work fine

        }

    }

}