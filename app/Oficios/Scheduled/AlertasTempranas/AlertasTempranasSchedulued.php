<?php 

namespace Oficios\Scheduled\AlertasTempranas;
use Carbon\Carbon;
use Oficios\Repositories\AlertasTempranasRepo;
use Oficios\Repositories\MensajesRepo;
use Oficios\Repositories\DestinatariosMensajesRepo;
use Oficios\Managers\MensajesManager;
use Oficios\Managers\DestinatariosMensajesManager;
use Oficios\Managers\OficiosManager;
use Combos\Repositories\Organismos;
use Oficios\Repositories\OficiosRepo;

class AlertasTempranasSchedulued{

	private $alertastempranasRepo,$mensajesRepo,$destinatariosMensajesRepo,$oficiosNotificar,$organismosRepo;
	private $blade = "notificacion_alertas_tempranas_por_vencer";
	private $listaOrganismos = [];
    private $oficiosRepo;

    public function __construct(AlertasTempranasRepo $alertastempranasRepo,Organismos $organismosRepo,OficiosRepo $oficiosRepo){

		$this->alertastempranasRepo = $alertastempranasRepo;
		$this->organismosRepo = $organismosRepo;
        $this->oficiosRepo = $oficiosRepo;
        $this->fechaActual = date("Y-m-d H:i:s");
    }



	public function fire(){
		$alertas = $this->alertastempranasRepo->getIdWithOutFile();

		$alertasVencer    = $alertas["vencer"];
		$alertasNotificar = $alertas["notificar"];
		
		//$oficiosVencer    = $this->alertastempranasRepo->getByIds($alertasVencer);
		$this->oficiosNotificar = $this->alertastempranasRepo->getByIds($alertasNotificar);

		foreach ($alertasVencer as $value) {	
			$this->alertastempranasRepo->delete($value);
		}


        $this->profugosEvadidos();
        $this->roboAutomotor();

        if(!empty($alertasNotificar)) return $this->sendMensaje();





	}


	public function sendMensaje (Array $datos = []){
        
        $this->mensajesRepo = new MensajesRepo();
        $this->destinatariosMensajesRepo = new DestinatariosMensajesRepo();
        $data  = [
            "asunto"=>"Alertas tempranas a una semana de vencer",
            "contenido"=>\View::make($this->blade,["alertas"=>$this->oficiosNotificar])->render(),
        ];
        $this->listaOrganismos = $this->organismosRepo->getOrgMinseg();
        $mensajeRepo = $this->mensajesRepo->newMensaje();
        $mensaje = new MensajesManager($mensajeRepo,$data);

        \DB::transaction(function() use($mensaje,$mensajeRepo){
            if($mensaje->save()){
                foreach($this->listaOrganismos as $datosMail){
                    $destinatariosRepo = $this->destinatariosMensajesRepo->newDestMen($mensajeRepo->id,$datosMail->id);
                    $destinatariosManager = new DestinatariosMensajesManager($destinatariosRepo,[]);
                    $destinatariosManager->save();

                }
            }
            foreach ($this->oficiosNotificar as $oficio) {
            	$oficio->notificado = 1;
            	$oficioManager    =  new OficiosManager($oficio,[]);
            	$oficioManager->save();
            }
        });

    }



    private function profugosEvadidos()
    {

        $oficios = $this->oficiosRepo->getOficiosProfugoEvadidoCheck();

        foreach($oficios as $oficio)
        {
            $fechaInicio = $oficio->getOriginal()['created_at'];
            $fechaActual  = $this->fechaActual;

            $inicio = Carbon::parse($fechaInicio);
            $actual = Carbon::parse($fechaActual);
            if($inicio->diffInHours($actual) > 95)
            {
                $oficio->delete();
            }
        }
    }

    private function roboAutomotor()
    {

        $oficios = $this->oficiosRepo->getOficiosRoboAutomotorCheck();

//        dd($oficios);
        foreach($oficios as $oficio)
        {
            $fechaInicio = $oficio->getOriginal()['created_at'];
            $fechaActual  = $this->fechaActual;

            $inicio = Carbon::parse($fechaInicio);
            $actual = Carbon::parse($fechaActual);

            if($inicio->diffInHours($actual) > 95)
            {
                $oficio->delete();
            }
        }
    }


}
