<?php
namespace Oficios\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Buscador
{
    public $models;
    private $table;
    private $fields;
    private $pk;
    private $palabra;
    protected $resultados;
    protected $model;
    private $relations;
    private $busqueda;



    public function setModels($models)
    {
        $this->models = $models;
    }


    public function search($palabra){
        $resultados = [];
        foreach ($this->models as $this->model){
            $this->model     = (get_class($this->model)) ? get_class($this->model) : $this->model;
            $this->model     = App::make($this->model);
            $this->table     = $this->model->getTable();
            $this->pk        = $this->model->getPk();
            $this->fields    = $this->model->getFields();
            $this->palabra   = $palabra;
            $this->relations = $this->model->relations();
            $this->relationsPivot = $this->model->relationsPivot();
            $resultados = $this->query();
        }

        return $resultados;
    }

    private function query(){

        //$this->model = $this->buscarRelacionesPivot();
        $this->model = $this->buscarRelaciones();
        $this->buscarModel();
        return  $this->model;


    }


    private function buscarModel($fields = null){
        $fields = (empty($fields)) ? $this->fields : $fields;

        foreach($fields as $field){
            $this->model = $this->model->orWhere($field, 'LIKE', '%'.$this->palabra.'%');
        }
    }


    private function buscarRelacionesPivot(){

        $a = $this->model;

        foreach ($this->relationsPivot as $relation) {
            $a = $a->orWhereHas($relation,function($query) use($a) {
                $relations = $query->getModel()->relations();
                foreach ($relations as $relationsPivot) {
                    $query = $query->whereHas($relationsPivot,function($q) {
                        $fields = $q->getModel()->getFields();
                        foreach($fields as $field){
                            $q = $q->where($field, 'LIKE', '%'.$this->palabra.'%');
                        }
                    });
                }

            });

        }
        return ($a);

    }


    private function buscarRelaciones(){
        $a = $this->model;
        foreach ($this->relations as $relation) {

            $a = $a->orWhereHas($relation,function($query) {
                $fields = $query->getModel()->getFields();
                foreach($fields as $field){
                    $query = $query->orWhere($field, 'LIKE', '%'.$this->palabra.'%');
                }
            });

        }


        return ($a);

    }

    /**
     * @param mixed $models
     */



}