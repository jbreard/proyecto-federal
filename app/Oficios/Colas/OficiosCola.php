<?php
namespace Oficios\Colas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Oficios\Clases\RedisHandler;
use Oficios\Managers\ConsultasPositivas\ConsultasPositivasDestinatariosManager;
use Oficios\Managers\ConsultasPositivas\ConsultasPositivasManager;
use Oficios\Managers\ConsultasPositivas\ConsultasPositivasOficiosManager;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasDestinatariosRepo;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasOficiosRepo;
use Oficios\Repositories\ConsultasPositivas\ConsultasPositivasRepo;
use Oficios\Repositories\OficiosRepo;

class OficiosCola{

    protected $channelModificacion   = "oficio_modificado";
    protected $channelNuevo          = "oficio_nuevo";
    protected $redis;
    protected $oficiosRepo;

    public function __construct(){
        $this->redis = new RedisHandler();
        $this->oficiosRepo =  new OficiosRepo();

    }

    public function notificarModificacion ($data){
        $this->alertaModificacion($data);
    }

    public function notificarSinEfecto($tarea,$data){}

    public function alertaModificacion($data)
    {
        extract($data);
        $rutaPdf  = null;
        $rutaArchivo = null;
        if($archivo){
            $rutaPdf = route($ruta.".pdf",$id);
            $rutaArchivo =  \URL::asset($archivo);
        }

        \Notification::notificar($id,$rutaPdf,$rutaArchivo,$blade='email_nuevo_oficio',$mensaje = 'Se Modifico un Requerimiento Judicial');

        $this->redis->setChannel($this->channelModificacion);

        $data = "Se Modifico el oficio, ".$rutaPdf;

        //Redis::publish($this->channelModificacion,json_encode($data));
    }

    public function notificarNuevo($data)
    {
        extract($data);
        $rutaPdf  = null;
        $rutaArchivo = null;
        if($archivo){
            $rutaPdf = route($ruta.".pdf",$id);
            $rutaArchivo =  \URL::asset($archivo);
        }
        //$rutaPdf = route($oficios->tipo->ruta.".pdf",$id);

        $mensajeArgumento = "Nuevo Requerimiento Cargado, %s ";
        if($isAlertaTemprana) $mensajeArgumento = "Nueva Alerta Temprana, %s ";
        $mensajeArgumento = sprintf($mensajeArgumento,$tipoOficio);
        \Notification::notificar($id,$rutaPdf,$rutaArchivo,'email_nuevo_oficio',$mensajeArgumento);
        //$this->redis->setChannel($this->channelNuevo);
        //$this->redis->publish($data);
        //Redis::publish($this->channelNuevo,Session::getId());

    }

    public function consultaPositiva($tarea,&$data){
        $parametrosBusqueda = $data['parametros_busqueda'];
//        dd($parametrosBusqueda);
        //$resultados         = $this->oficiosRepo->Buscar($parametrosBusqueda)->get();
        $resultados = "";
        $parametrosBusqueda = json_encode($parametrosBusqueda);
        $idUsuario          = $data['id_usario'];

        $organismos = [6,7];


        $consultaPositivaRepo = new ConsultasPositivasRepo();


        $model = $consultaPositivaRepo->newModel($idUsuario,$parametrosBusqueda,$resultados);
        $consultaManager = new ConsultasPositivasManager($model,[]);
        DB::transaction(function () use($consultaManager,$model,$organismos,$resultados) {
            if($consultaManager->save()){
                $consultaPositivaDesintariosRepo =  new ConsultasPositivasDestinatariosRepo();
                $consultaPositivaOficiosRepo = new ConsultasPositivasOficiosRepo();
                foreach ($organismos as $index=>$value) {
                    $modelDestinatario = $consultaPositivaDesintariosRepo->newModel($model->id,$value);
                    $managerDestinatario = new ConsultasPositivasDestinatariosManager($modelDestinatario,[]);
                    $managerDestinatario->save();
                }
                /*foreach ($resultados as $index=>$resultado) {
                    $modelConsultaOficios = $consultaPositivaOficiosRepo->newModel($resultado["id"],$model->id);
                    $managerOficios = new ConsultasPositivasOficiosManager($modelConsultaOficios,[]);
                    $managerOficios->save();
                } */
            }
        });
       $tarea->delete();

    }





}
