<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 19:29
 */

namespace Oficios\Colas;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;
use Oficios\Repositories\OficiosRepo;

class FlujoTransaccionPersonaCola
{
    protected $apiBaseIncrementalManager;

    /**
     * FlujoTransaccionPersonaCola constructor.
     */
    public function __construct()
    {
        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
    }
    
    
    public function flujo($job, $data){

        try{

            Log::useDailyFiles(storage_path().'/logs/flujo_personas.log');
            Log::info('FlujoTransaccionPersonaCola.php');
	    Log::info($data);
	    #$data = $data[0];

            $apiStatusCodeResponse = 0;

            $dataType = Config::get('app.apis.base_incremental.codigos_tipo_dato.personas');//sifcop_personas
            
            $idOficio = array_get($data,'id_oficio');

            $oficiosRepo = new OficiosRepo();
            $oficiosData = $oficiosRepo->getDataForTransactionFlow($idOficio);

            $isOficioConfirmado  = $oficiosRepo->isConfirmadoByIdOficio($idOficio);
            
            $tieneDni            = array_get($data, 'tieneDni');
            $changeNombre        = array_get($data, 'changeNombre');
            $changeApellido      = array_get($data, 'changeApellido');
            $changeDni           = array_get($data, 'changeDni');

            if($isOficioConfirmado){

                if($tieneDni) {

                    if($changeNombre || $changeApellido || $changeDni) {

                        $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'update');

                    }

                }

            }

            if($apiStatusCodeResponse == 202 || $apiStatusCodeResponse === null) {
                $job->delete();
            } else {
                Log::info('FlujoTransaccionPersonaCola.php - ERROR - API STATUS CODE : ' . $apiStatusCodeResponse);
            }

        } catch (\Exception $e) {

            Log::useDailyFiles(storage_path().'/logs/flujo_personas.log');
            Log::info($e->getMessage());

        }

    }
}
