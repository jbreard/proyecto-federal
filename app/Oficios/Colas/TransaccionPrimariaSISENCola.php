<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 19:29
 */

namespace Oficios\Colas;

use Illuminate\Support\Facades\Log;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;

class TransaccionPrimariaSISENCola
{
    protected $apiBaseIncrementalManager;

    /**
     * FlujoTransaccionPersonaCola constructor.
     */
    public function __construct()
    {
        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
    }

    /**
     * @param $job
     * @param $data
     */
    public function send($job, $data){

        try {
            
            Log::useDailyFiles(storage_path().'/logs/TransaccionPrimariaSISENCola.log');
            Log::info($data);

            $apiStatusCodeResponse  = null;
            $oficiosData            = $data['data'];
            $dataType               = $data['data_type'];
            $sinEfecto              = $data['sin_efecto'];
            $eliminado              = $data['eliminado'];
            $confirmado             = $data['confirmado'];
            
            
            if( $sinEfecto || $eliminado )
            {
                $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'delete');

            } else {
                
                if( $confirmado )
                {
                    $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'insert');
                }

            }
            
            if($apiStatusCodeResponse == 202 || $apiStatusCodeResponse === null) {
                $job->delete();  
            } else {
                Log::info('ERROR - API STATUS CODE : ' . $apiStatusCodeResponse);
            }          
            

        } catch ( \Exception $e) {
            Log::useDailyFiles(storage_path().'/logs/TransaccionPrimariaSISENCola.log');
            Log::info($e->getMessage());
        }     
        
    }   

    
}