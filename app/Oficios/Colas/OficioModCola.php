<?php 
namespace Oficios\Colas;
use Oficios\Clases\RedisHandler;

class OficioModCola{
    
    protected $channel   = "oficio.update";

    public function notificarModificacion ($tarea,$data){
        $tarea->delete();
        extract($data);
        if(!$isDirtyEstado and !$isDirtyConfirmador){
            $rutaPdf  = null;
            $rutaArchivo = null;
            
            if($archivo){
                $rutaPdf = route($ruta.".pdf",$id);
                $rutaArchivo =  \URL::asset($archivo);
            }
            
            \Notification::notificar($id,$rutaPdf,$rutaArchivo,$blade='email_nuevo_oficio',$mensaje = 'Se Modifico un Requerimiento Judicial');

            //$redis = new RedisHandler();

            //$redis->setChannel($this->channel);

            //$data = "Se Modifico el oficio, ".$rutaPdf;

            //$redis->publish($data);

        }
    }




}


