<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 19:29
 */

namespace Oficios\Colas;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;
use Oficios\Repositories\OficiosRepo;

class FlujoTransaccionOficioCola
{
    protected $apiBaseIncrementalManager;
    protected $oficiosRepo;

    /**
     * FlujoTransaccionPersonaCola constructor.
     */
    public function __construct()
    {
        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
        $this->oficiosRepo = new OficiosRepo();
    }

    /**
     * @param $job
     * @param $data
     */
    public function flujo($job, $data){

        try {
            Log::useDailyFiles(storage_path().'/logs/flujo_oficios.log');
            #Log::error($data);
	    #Log::info('hola');

            $apiStatusCodeResponse = null;

            $idOficio = array_get($data,'id_oficio');

            $cambioConfirmado = array_get($data, 'cambioConfirmado');
            $isConfirmado = array_get($data, 'isConfirmado');
            $oficioEliminado = array_get($data, 'oficioEliminado');
            $oficioSinEfecto = array_get($data, 'oficioSinEfecto');

            $changePlazoAutorizado = array_get($data, 'changePlazoAutorizado');
            $changePlazoAutorizadoTo1 = array_get($data, 'changePlazoAutorizadoTo1');
            $changePlazoAutorizadoTo2 = array_get($data, 'changePlazoAutorizadoTo2');

            $dataType = $this->oficiosRepo->getDataTypeByTipoOficio($idOficio);
            
            $oficiosData = $this->prepareDataSent($dataType, $idOficio);

	    #Log::info($oficiosData);

            if($cambioConfirmado) {
                
                if($isConfirmado) 
                    $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'insert');

            } else {

                if($oficioEliminado || $oficioSinEfecto) {
                    
                    $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'delete');

                } else {

                    if($changePlazoAutorizado) {

                        if($changePlazoAutorizadoTo1) {

                            $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'delete');

                        } else if($changePlazoAutorizadoTo2) {

                            $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'insert');

                        }

                    }

                }

            }

            if($apiStatusCodeResponse == 202 || $apiStatusCodeResponse === null) {
                $job->delete();  
            } else {
                Log::error('FlujoTransaccionOficioCola.php - ERROR - API STATUS CODE : ' . $apiStatusCodeResponse);
            }          
            

        } catch ( \Exception $e) {
            Log::useDailyFiles(storage_path().'/logs/flujo_oficios.log');
            Log::info("FlujoTransaccionOficioCola.php - ERROR - Message : " . $e->getMessage());
        }     
        
    }    

    /**
     * @param $dataType
     * @param $idOficio
     * @return array
     */
    public function prepareDataSent($dataType, $idOficio)
    {
        //todo - Refactorizar switch
        switch ($dataType) {
            
            case 81:
                $oficiosData = $this->oficiosRepo->getDataForTransactionFlow($idOficio);
                break;

            case 82:
                $oficiosData = $this->oficiosRepo->getDataForTransactionFlowPedidosSecuestroVehicular($idOficio);
                break;

            default:
                $oficiosData = [];
                break;
        }
        
        return $oficiosData;
    }
}
