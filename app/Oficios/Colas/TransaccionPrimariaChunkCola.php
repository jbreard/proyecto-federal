<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 19:29
 */

namespace Oficios\Colas;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;

class TransaccionPrimariaChunkCola
{
    protected $apiBaseIncrementalManager;
    protected $oficiosTiposDato;
    protected $codigosTiposDato;
    protected $codigoPersona;

    /**
     * FlujoTransaccionPersonaCola constructor.
     */
    public function __construct()
    {
        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();

        $this->oficiosTiposDato = Config::get('app.apis.base_incremental.oficios_tipo_dato');

        $this->codigosTiposDato = Config::get('app.apis.base_incremental.codigos_tipo_dato');

        $this->codigoPersona = Config::get('app.apis.base_incremental.codigos_tipo_dato.personas');
    }

    /**
     * @param $job
     * @param $data
     */
    public function send($job, $data) {

        $oficios = $data['block'];

        $lote = $data['lote'];
        
        echo "Numero de lote: {$lote}\n";

        $limit = count($oficios);

        for($i=0; $i<$limit; $i++)
        {
            $oficio = $oficios[$i];

            $process = $this->process($oficio);

            if( $process['response'] ) {

                echo $echo = "END Lote: #{$lote} Oficio: #{$oficio['id']} Fila: {$i}  \n";

                #Log::info($echo);

            } else {

                Log::info('ERROR - API STATUS CODE : ' . $process['code'] . ' Oficio: #'.$oficio['id']);

            }

        }

        $job->delete();

    }

    private function process($oficio)
    {
        $idTipoOficio = $oficio['id_tipo_oficio'];

	Log::info("tipooficio ".$idTipoOficio);

        $tipo = array_get($this->oficiosTiposDato,$idTipoOficio,null);
	
	Log::info("tipo ".$tipo);

        $dataType = array_get($this->codigosTiposDato,$tipo, $this->codigoPersona);
	if($dataType == null){
		Log::info("dataType ".$dataType);
	}

        $eliminado = ( !is_null($oficio['deleted_at']) );

        $sinEfecto = ( $oficio['id_estado'] == 2 );

        $confirmado = ( $oficio['confirmado'] == 2 );

        $responseCode = null;

        $response = [
            'code' => null,
            'response' => false
        ];

        $filename = storage_path("json/transaccion.json");
        $filenameNot = storage_path("json/transaccion_not.json");


        if( $sinEfecto || $eliminado ) {

            File::append($filename, json_encode([
                'data' => $oficio,
                'data_type' => $dataType,
                'transaction_type' => 'insert',
            ]) . "|");

            File::append($filename, json_encode([
                'data' => $oficio,
                'data_type' => $dataType,
                'transaction_type' => 'delete',
            ]) . "|");

        } else {

            if( $confirmado ) {

                File::append($filename, json_encode([
                    'data' => $oficio,
                    'data_type' => $dataType,
                    'transaction_type' => 'insert',
                ]) . "|");

            } else {

                File::append($filenameNot, json_encode([
                    'data' => $oficio,
                    'data_type' => $dataType,
                    'transaction_type' => '',
                ]) . "\n");

            }

        }        

        array_set($response, 'response', true);

        return $response;
    }


}
