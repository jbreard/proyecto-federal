<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 19:29
 */

namespace Oficios\Colas;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;
use Oficios\Repositories\OficiosRepo;

class FlujoTransaccionPedidosSecuestroVehicularCola
{
    protected $apiBaseIncrementalManager;

    /**
     * FlujoTransaccionPersonaCola constructor.
     */
    public function __construct()
    {
        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
    }
    
    
    public function flujo($job, $data){

        try{

            Log::useDailyFiles(storage_path().'/logs/flujo_vehiculos.log');
            Log::info($data);

            $apiStatusCodeResponse = null;

            $dataType = Config::get('app.apis.base_incremental.codigos_tipo_dato.vehiculos');//sifcop_vehiculos
            
            $idOficio = array_get($data,'id_oficio');

            $oficiosRepo = new OficiosRepo();
            $oficiosData = $oficiosRepo->getDataForTransactionFlowPedidosSecuestroVehicular($idOficio);

            $isOficioConfirmado      = array_get($data, 'isOficioConfirmado');
            $hasDominio              = array_get($data, 'hasDominio');
            $hasAnio                 = array_get($data, 'hasAnio');
            $hasRadicacionVehiculo   = array_get($data, 'hasRadicacionVehiculo');
            $hasChasis               = array_get($data, 'hasChasis');
            $hasTipoVehiculo         = array_get($data, 'hasTipoVehiculo');
            $hasOtroTipoVehiculo     = array_get($data, 'hasOtroTipoVehiculo');
            $hasMarca                = array_get($data, 'hasMarca');
            $hasOtraMarca            = array_get($data, 'hasOtraMarca');
            $hasModelo               = array_get($data, 'hasModelo');
            $hasOtroModelo           = array_get($data, 'hasOtroModelo');
            $hasColor                = array_get($data, 'hasColor');
            $hasMarcaMotor           = array_get($data, 'hasMarcaMotor');
            $hasNumeroMotor          = array_get($data, 'hasNumeroMotor');

            if($isOficioConfirmado){

                if (    $hasDominio      || $hasAnio          || $hasRadicacionVehiculo
                    ||  $hasChasis       || $hasTipoVehiculo  || $hasOtroTipoVehiculo
                    ||  $hasMarca        || $hasOtraMarca     || $hasModelo
                    ||  $hasOtroModelo   || $hasColor         || $hasMarcaMotor
                    || $hasNumeroMotor) {


                    $changeDominio              = array_get($data, 'changeDominio');
                    $changeAnio                 = array_get($data, 'changeAnio');
                    $changeRadicacionVehiculo   = array_get($data, 'changeRadicacionVehiculo');
                    $changeChasis               = array_get($data, 'changeChasis');
                    $changeTipoVehiculo         = array_get($data, 'changeTipoVehiculo');
                    $changeOtroTipoVehiculo     = array_get($data, 'changeOtroTipoVehiculo');
                    $changeMarca                = array_get($data, 'changeMarca');
                    $changeOtraMarca            = array_get($data, 'changeOtraMarca');
                    $changeModelo               = array_get($data, 'changeModelo');
                    $changeOtroModelo           = array_get($data, 'changeOtroModelo');
                    $changeColor                = array_get($data, 'changeColor');
                    $changeMarcaMotor           = array_get($data, 'changeMarcaMotor');
                    $changeNumeroMotor          = array_get($data, 'changeNumeroMotor');

                    if (    $changeDominio      || $changeAnio          || $changeRadicacionVehiculo
                        ||  $changeChasis       || $changeTipoVehiculo  || $changeOtroTipoVehiculo
                        ||  $changeMarca        || $changeOtraMarca     || $changeModelo
                        ||  $changeOtroModelo   || $changeColor         || $changeMarcaMotor
                        || $changeNumeroMotor) {

                        $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'update');

                    } else {

                        $apiStatusCodeResponse = $this->apiBaseIncrementalManager->call($oficiosData, $dataType, 'insert');
                    }

                }

            }

            if($apiStatusCodeResponse == 202 || $apiStatusCodeResponse === null) {
                $job->delete();
            } else {
                Log::info('ERROR - API STATUS CODE : ' . $apiStatusCodeResponse);
            }

        } catch (\Exception $e) {

            Log::useDailyFiles(storage_path().'/logs/flujo_vehiculos.log');
            Log::info($e->getMessage());

        }

    }
}