<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 8/05/15
 * Time: 15:20
 */

namespace Oficios\Colas;


use Correos\Repositories\CorreoRepo;
use Oficios\Managers\DestinatariosMensajesManager;
use Oficios\Managers\MensajesManager;
use Oficios\Repositories\DestinatariosMensajesRepo;
use Oficios\Repositories\MensajesRepo;
use Oficios\Repositories\OficiosRepo;

class MensajesCola {


    private $listaMails = [];
    private $correoRepo;
    private $ruta;
    private $oficiosRepo;
    private $idOficio;
    private $mensajesRepo;
    private $destinatariosMensajesRepo;
    private $job;

    public function getListaMails(){
        $this->correoRepo = new CorreoRepo();
        $listaMails = $this->correoRepo->getMails();
        
        foreach ($listaMails as $mail){
            array_push($this->listaMails,$mail["mail"]);
        }
    }


    public function getRuta()
    {
        $this->oficiosRepo = new OficiosRepo();
        $this->ruta = $this->oficiosRepo->getRuta($this->idOficio);
        if(!empty($this->ruta)) return route($this->ruta->tipo->ruta.".show",$this->idOficio);
        return $this->destroyJob();

    }


    public function destroyJob(){
      return $this->job->delete();
    }


    public function setIdOficio($idOficio)
    {
        $this->idOficio = $idOficio;
    }

    public function sendMensaje ($tarea,$data){
        $this->job = $tarea;
        /*$this->mensajesRepo = new MensajesRepo();
        list($idOficio, $mensajeArgumento, $blade, $rutaPdf, $rutaArchivo) = $this->extractData($data);
        $this->setIdOficio($idOficio);
        $this->getListaMails();
        $ruta = $this->getRuta();
        $this->destinatariosMensajesRepo = new DestinatariosMensajesRepo();
        $data  = [
            "asunto"=>$mensajeArgumento,
            "contenido"=>\View::make($blade,compact("ruta","rutaPdf","rutaArchivo","mensajeArgumento"))->render(),
        ];

        $mensajeRepo = $this->mensajesRepo->newMensaje();
        $mensaje = new MensajesManager($mensajeRepo,$data);
        
        \DB::transaction(function() use($mensaje,$mensajeRepo){
            if($mensaje->save()){
                foreach($this->listaMails as $mail){
                    $datosMail = $this->correoRepo->getMailByMail($mail);
                    $destinatariosRepo = $this->destinatariosMensajesRepo->newDestMen($mensajeRepo->id,$datosMail->id);
                    $destinatariosManager = new DestinatariosMensajesManager($destinatariosRepo,[]);
                    $destinatariosManager->save();
                }
            }
        });*/
        $this->destroyJob();


    }
    public function sendMail($tarea,$data){
        $this->job = $tarea;
        /*$this->mensajesRepo = new MensajesRepo();
        $ruta = $this->getRuta();
        list($idOficio, $mensajeArgumento, $blade, $rutaPdf, $rutaArchivo) = $this->extractData($data);
        $this->setIdOficio($idOficio);
        $this->getListaMails();
              
 	   \Mail::send($blade,compact("ruta","rutaPdf","rutaArchivo","mensajeArgumento"),function($message) use($mensajeArgumento)
        {
            $message
                ->to($this->listaMails)
                ->subject($mensajeArgumento." ".date("d/m/Y h:i:s"));
        });

        */
        $this->destroyJob();





    }

    /**
     * @param $data
     * @return array
     */
    public function extractData($data)
    {
        $idOficio = $data['idOficio'];
        $mensajeArgumento = $data['mensajeArgumento'];
        $blade = $data["blade"];
        $rutaPdf = $data["rutaPdf"];
        $rutaArchivo = $data["rutaArchivo"];
        return array($idOficio, $mensajeArgumento, $blade, $rutaPdf, $rutaArchivo);
    }
}
