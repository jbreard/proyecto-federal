<?php


namespace Oficios\Repositories;


use Oficios\Entities\PedidosSecuestroVehicular;
use Transformer;

class SecuestroVehicularRepo extends BaseRepo {

    protected $oficios;
    protected $personas;
    protected $id_oficio;

    public function getModel()
    {
        return new PedidosSecuestroVehicular();
    }

    public function getSecuestroVehicular($id)
    {
        $this->oficios       = new OficiosRepo();
        $this->personas      = new PersonasRepo();
        $oficios             = $this->oficios->find($id);
        $titular             = $this->personas->getPersona($id,5);
        $responsable         = $this->personas->getPersona($id,6);
        $secuestro_vehicular = $this->getSolicitudParadero($titular->id);
        return compact("oficios","titular","responsable","secuestro_vehicular","fotos");
    }

    public function getRoboAutomotor($id)
    {
        $this->oficios       = new OficiosRepo();
        $this->personas      = new PersonasRepo();
        $oficios             = $this->oficios->find($id);
        $responsable         = $this->personas->getPersona($id,6);
//        dd($responsable);
        $secuestro_vehicular = $this->getSolicitudParadero($responsable->id);
//        dd($this->getSolicitudParadero($responsable->id));
//        if($responsable->id)
//        else
//            $secuestro_vehicular = null;
        return compact("oficios","responsable","secuestro_vehicular","fotos");
    }

    public function newSecuestro($idPersona,$idOficio)
    {
        $secuestro             = new PedidosSecuestroVehicular();
        $secuestro->id_persona = $idPersona;
        $this->id_oficio       = $idOficio;
        return $secuestro;
    }


    public function getAll(){
     $vehiculos = $this->getModel()->where("dominio",'!=','')->distinct('dominio')->whereHas('getPersona',function($q){
         $q->whereHas("oficio",function($z){
           $z->where('estado','=','1'); 
         });
     })->get()->load(["getModelo","getMarca"]);
        
        
      $v = $vehiculos->map(function($vehiculo){
            $v = [];
            $v["dominio"] = $vehiculo->dominio;
            $v['modelo'] = null;
            $v['marca'] = null;
            if($vehiculo->getModelo){
                $v['modelo'] = $vehiculo->getModelo->modelo;
            }
            if($vehiculo->getMarca){
                $v['marca'] = $vehiculo->getMarca->marca;
            }
            return $v;
        });

        return $v;
    }

    public function getPatente($datos)
    {
        extract($datos);
        $secuestro_vehicular = $this->getModel();
        $datosSecuestro =   $secuestro_vehicular->whereHas("oficio", function($oficio) {
            $oficio->where("confirmado","=","2");
            $oficio->where("estado","=","1");
        })
            ->where("dominio","=",$dominio)
            ->get();
        return $datosSecuestro;
    }

    public function getSecuestroVehicularByDominio($dominio)
    {
        return $this->getModel()->where('dominio',"LIKE","%$dominio%")->get();
    }

    public function getSecuestroVehicularByFields($fields)
    {
        $transformers = ['dominio'];
        $secuestroVehicular = $this->getModel();
        foreach ($fields as $key => $value) {
            if(!empty($value))
            {
                $needTransform = in_array($key, $transformers);
                $value = ($needTransform) ? Transformer::quitMidScriptSpaces($value) : $value;
                $secuestroVehicular = $secuestroVehicular->where($key, "LIKE", "%{$value}%");
            }
        }
        return $secuestroVehicular;
    }

}