<?php
namespace Oficios\Repositories;

use Oficios\Entities\ContadorNotificaciones;


class ContadorNotificacionesRepo extends BaseRepo {

    public function getModel()
    {
        return new ContadorNotificaciones();
    }

    public function getNotificacion($idOrganismo){
        return $this->model->where('id_organismo','=',$idOrganismo);
    }

    public function getMensajeNuevo($idOrganismo){
        return $this->getNotificacion($idOrganismo)->first(['mensajes_nuevos']);
    }
    
    public function getOficiosNuevos($idOrganismo){
        return $this->getNotificacion($idOrganismo)->first(['oficios_nuevos']);
    }
    
    public function getOficiosModificados($idOrganismo){
        return $this->getNotificacion($idOrganismo)->first(['oficios_modificados']);
    }

    public function getAll(){
        return $this->getNotificacion($idOrganismo)->first();
    }

}