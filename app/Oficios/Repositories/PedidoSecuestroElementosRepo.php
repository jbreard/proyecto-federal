<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 03/06/14
 * Time: 16:22
 */

namespace Oficios\Repositories;

use Oficios\Entities\SecuestroElementos;

class PedidoSecuestroElementosRepo extends BaseRepo{


    protected $oficios;

    protected $personas;

    protected $elemento;


    public function getModel()
    {

        return new SecuestroElementos();
    }

    public function   getSecuestroElementos($id){

        $this->oficios       = new OficiosRepo();

        $this->personas      = new PersonasRepo();

        $this->elemento      = new SecuestroElementos();


        $oficios             = $this->oficios->find($id);

        $titular             = $this->personas->getPersona($id,5);

        $secuestro_elementos = $this->getSolicitudParadero($titular->id);

        $documentos         = $titular->getDocumentos;

        return compact("oficios","titular","secuestro_elementos","documentos");
    }

    public function newSecuestroElementos($idPersona){

        $secuestro             = new SecuestroElementos();
        $secuestro->id_persona = $idPersona;

        return $secuestro;
    }


} 