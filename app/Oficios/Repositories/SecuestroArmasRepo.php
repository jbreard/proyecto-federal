<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 03/06/14
 * Time: 16:22
 */

namespace Oficios\Repositories;

use Oficios\Entities\SecuestroArmas;

class SecuestroArmasRepo extends BaseRepo{


    protected $oficios;

    protected $personas;

    protected $elemento;


    public function getModel()
    {

        return new SecuestroArmas();
    }

    public function   getSecuestroArmas($id){

        $this->oficios       = new OficiosRepo();

        $this->personas      = new PersonasRepo();

        $this->elemento      = new SecuestroArmas();

        $oficios             = $this->oficios->find($id);

        $titular             = $this->personas->getPersona($id,5);

        $secuestro_armas = $this->getSolicitudParadero($titular->id);

        $documentos          = $titular->getDocumentos;

        return compact("oficios","titular","secuestro_armas","documentos");
    }

    public function newSecuestroArmas($idPersona){

        $secuestro             = new SecuestroArmas();
        $secuestro->id_persona = $idPersona;

        return $secuestro;
    }


}