<?php

namespace Oficios\Repositories\ConsultasPositivas;
use Oficios\Entities\ConsultasPositivas\ConsultasPositivasDestinatarios;
use Oficios\Repositories\BaseRepo;

class ConsultasPositivasDestinatariosRepo extends BaseRepo {

    public function getModel()
    {
        return  new ConsultasPositivasDestinatarios();        
    }

    public function newModel($idConsultaPositiva,$idOrganismo){
    	$model = $this->getModel();
    	$model->id_consulta_positiva = $idConsultaPositiva;
    	$model->id_organismo = $idOrganismo;
    	$model->leido = 0;
        return $model;
    }

    public function getByIdOrganismo($id){
        return $this->model->where('id_organismo','=',$id)->get();
    }


    
}