<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 03/10/14
 * Time: 15:25
 */

namespace Oficios\Repositories\ConsultasPositivas;
use Oficios\Entities\ConsultasPositivas\ConsultasPositivas;
use Oficios\Repositories\BaseRepo;
class ConsultasPositivasRepo extends BaseRepo {

    public function getModel()
    {
        return  new ConsultasPositivas();        
    }

    public function newModel($idUsuario,$parametrosBusqueda,$resultados){
        $model = $this->getModel();
        $model->id_usuario = $idUsuario;
        $model->parametros_busqueda = $parametrosBusqueda;
        $model->resultados = $resultados;
        return $model;
    }
    
    
}