<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 03/10/14
 * Time: 15:25
 */

namespace Oficios\Repositories\ConsultasPositivas;
use Oficios\Entities\ConsultasPositivas\ConsultasPositivasOficios;
use Oficios\Repositories;
use Oficios\Repositories\BaseRepo;
class ConsultasPositivasOficiosRepo extends BaseRepo {

    public function getModel()
    {
        return  new ConsultasPositivasOficios();        
    }

    public function newModel($idOficio,$idConsulta){
        $model = $this->getModel();
        $model->id_oficio = $idOficio;
        $model->id_consulta = $idConsulta;
        return $model;
    }

    public function getByIdConsulta($id){
        return $this->model->with('oficios')->where('id_consulta','=',$id);
    }
    
    
}