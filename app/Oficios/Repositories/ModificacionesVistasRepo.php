<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 10/03/15
 * Time: 19:56
 */

namespace Oficios\Repositories;


use Oficios\Entities\ModificacionesVistas;

class ModificacionesVistasRepo extends BaseRepo{

    public function getModel()
    {
        return new ModificacionesVistas();
    }


    public function newModVistas($idMod){


        $entity = $this->getModel();

        $entity->id_mod = $idMod;

        $entity->id_usuario = \Auth::user()->id;

        return $entity;


    }


    public function existe($idMod){
        return $this->model->where('id_usuario','=',\Auth::user()->id)->where('id_mod','=',$idMod)->get();
    }


    public function getMod(){
        return $this->model->where('id_usuario','=',\Auth::user()->id)->get()->toArray();
    }




}