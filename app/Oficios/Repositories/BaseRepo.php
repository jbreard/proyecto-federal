<?php


namespace Oficios\Repositories;


abstract class BaseRepo {

    protected $model;


    abstract public function getModel();

    function __construct(){

        $this->model = $this->getModel();

    }

    public function newLog($field,$value){

        $oficio =  $this->getModel();

        $oficio -> field       = $field;
        $oficio -> value       = $value;
        $oficio->id_usuario  = (\Auth::user()) ? \Auth::user()->id :50;

        return $oficio;

    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function delete($id){
        $model = $this->find($id);
        return $model->delete();
    }

    public function all()
    {

        return $this->model->all();

    }

    public function getPersona($idOficio,$idTipoPersona){

        return $this->model->where('id_oficio','=',$idOficio)->where('id_tipo_persona','=',$idTipoPersona)->first();


    }



    public function where($campo,$valor,$param='='){
        return $this->model->where($campo,$param,$valor)->get();

    }



    public function paginate($limit = 15){
        return $this->model->paginate($limit);

    }

    public function getSolicitudParadero($idPersona){

        return $this->model->where('id_persona','=',$idPersona)->firstOrFail();
    }

} 