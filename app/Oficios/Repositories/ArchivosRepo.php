<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 03/10/14
 * Time: 15:25
 */

namespace Oficios\Repositories;

use Oficios\Entities\Archivos;

class ArchivosRepo extends BaseRepo {

    public function getModel()
    {
        return new Archivos();
    }


    public function findWhere($id){

    }


    public function getArchivo($idArchivo,$idOficio){
        return  $this->model
                    ->where('id','=',$idArchivo)
                    ->where('id_oficio','=',$idOficio)
                    ->first();
    }

    public function countFiles($idOficio)
    {
        return  $this->model
            ->where('id_oficio','=',$idOficio)
            ->count();
    }
    
    }
