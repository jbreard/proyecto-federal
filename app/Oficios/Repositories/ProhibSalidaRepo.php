<?php


namespace Oficios\Repositories;


use Oficios\Entities\SolicitudParadero;

class ProhibSalidaRepo extends BaseRepo {

    protected $oficios;

    protected $personas;

    protected $plazos;


    public function getModel()
    {

        return new SolicitudParadero;
    }

    public function  getProhibSalida($id){

        $this->oficios      = new OficiosRepo();
        $this->personas     = new PersonasRepo();
        $this->plazos       = new PlazosRepo();
        $oficios            = $this->oficios->find($id);
        $buscado            = $this->personas->getPersona($id,2);
        $plazos             = $this->plazos->getPlazos($id);
        $documentos         = $buscado->getDocumentos;

        return compact("oficios","buscado","plazos","documentos");
    }


}