<?php


namespace Oficios\Repositories;


use Oficios\Entities\TiposOficios;

class TiposOficiosRepo extends BaseRepo {




    public function getModel()
    {

        return new TiposOficios();
    }



    public function getRuta($tipoOficio){

        $ruta  = $this->where("id",$tipoOficio);


        return $ruta[0]->ruta;


    }

    public function getByOrder()
    {
        return $this->model->remember(360)->orderBy('orden','ASC')->get();

    }

    private function getByOderQuery(){
      return $this->model->remember(360)->orderBy('orden','ASC');
    }

    public function getAlertas(){
      return $this->model->remember(360)->where('alerta_temprana','=',1)->orderBy('orden','ASC')->get();

    }




}
