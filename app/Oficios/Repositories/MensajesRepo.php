<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 27/10/14
 * Time: 13:59
 */

namespace Oficios\Repositories;

use Illuminate\Support\Facades\DB;
use Oficios\Entities\Mensajes;
use Oficios\Helpers\Buscador;
use Time;
use Usuarios\Entities\User;

class MensajesRepo extends BaseRepo {

    private $leido;
    private $aLeidos  = ["2"=>"1","3"=>"0"];
    public function getModel()
    {
        return new Mensajes();
    }

    public function newMensaje(){

        $model = $this->getModel();
        $model->usuario_id   = (\Auth::user()) ? \Auth::user()->id : User::where('sistema','=',1)->first()->id;
        $model->id_organismo = (\Auth::user()) ? \Auth::user()->id_jurisdiccion : User::where('sistema','=',1)->first()->id_jurisdiccion;
        return $model;
    }


    private function busquedaLibre($q){
        $buscador = new Buscador();
        $buscador->setModels([$this->getModel()]);
        return $buscador->search($q);
    }



    public function getRecibidosPaginate($leido,$limit = 15,$data=null,$page=0){
        $this->leido = $leido;
        return  $this->buscar($data,15,1,$page);

    }


    public function getEnvidos($limit = 15,$buscar=null){

        return $this->buscar($buscar,$limit,0);

    }

    public function actualizarRecibidos($leido,$idUltimoRecibido){
        return $this->getRecibidos($leido)->where('id','>',$idUltimoRecibido)->get();
    }


    public function getWithRespuestas($idMensaje){
        return $this->model->with('respuestas')->where('id','=',$idMensaje)->first();
    }



    public function buscar(&$data,$limit=15,$recibidos=0,$page=0)
    {
        $mailFilters = null;
        $mailFilters = (!empty($data['mailsFilters'])) ?  $data['mailsFilters']  : $mailFilters ;
        $model  = ($recibidos==0) ? $this->getEnviadosModel($mailFilters) :  $this->getRecibidosModel($mailFilters);
        $model = $this->filterMensajes($model,$data);
        $skip = $page*$limit;
        $data =  $model->orderBy('mensajes.updated_at','DESC')->skip($skip)->take($limit)->get();
        //$data =  $model->orderBy('mensajes.updated_at','DESC')->paginate();
        return $data;

    }

    private function getEnviadosModel(&$mailFilters)
    {

        $tableName = $this->model->getTable();
        $idOrgUser = \Auth::user()->id_jurisdiccion;
        $model = DB::table($tableName)->select([$tableName.".id as idM",$tableName.".created_at",$tableName.".asunto","organismos.descripcion as organismo","usuarios.apellido as emisor_apellido","usuarios.apellido as emisor_nombre" ]);
        $model = $model->where('mensajes.id_organismo','=',$idOrgUser);
        $model = $this->joinsMensajes($model);
        if(!empty($mailFilters)) {
            $model = $model->join('destinatarios_mensajes',function($q) use($mailFilters){
               $q =  $q->on('mensajes.id','=','destinatarios_mensajes.id_mensaje');
               foreach($mailFilters as $mail){
                   $q = $q->orWhere('destinatarios_mensajes.id_organismo','=',$mail);
               };
            });
        }
        return $model;
    }


    private function getRecibidosModel(&$mailFilters)
    {
        $tableName = $this->model->getTable();
        $queryAdjunto = "IF(exists(
                                select archivos_mensajes.id from archivos_mensajes where id = archivos_mensajes.id_mensaje limit 1
                            ),true,false) as 'adjunto'";

        $model = DB::table($tableName)
            ->select([
                    $tableName.".id as idM",
                    $tableName.".created_at",
                    $tableName.".asunto",
                    "organismos.descripcion as organismo",
                    "usuarios.apellido as emisor_apellido",
                    "usuarios.apellido as emisor_nombre" ,
                    "destinatarios_mensajes.leido",
                    //DB::raw($queryLeido),
                    DB::raw($queryAdjunto)
                ]);
      

        $model = $model
            ->join('destinatarios_mensajes',function($join) use($tableName){
                  $join
                  ->on($tableName.'.id','=','destinatarios_mensajes.id_mensaje')
                  ->where('destinatarios_mensajes.id_organismo','=',\Auth::user()->id_jurisdiccion);
        });

        if(array_key_exists($this->leido,$this->aLeidos)){
            $model = $model->where('destinatarios_mensajes.leido','=',$this->aLeidos[$this->leido]);
        }
            
        $model = $this->filterByOrganismo($mailFilters, $model);
        $model = $this->joinsMensajes($model);

        return $model;
        }

    


    private function joinsMensajes(&$model)
    {
        $model = $model->join("organismos", "mensajes.id_organismo", "=", "organismos.id");
        $model = $model->join("usuarios", "mensajes.usuario_id", "=", "usuarios.id");
        return $model;
    }


    private function filterByOrganismo($mailsFilters, $model,$tabla='mensajes')
    {
        if (!empty($mailsFilters)) {
            foreach ($mailsFilters as $mail) {
                $model = $model->orWhere($tabla.'.id_organismo', '=', $mail);
            }
            return $model;
        }
        return $model;
    }

    private function filterMensajes(&$model, &$data)
    {
        if(empty($data)) return $model;
        $fechas = $data['fecha'];
        $this->transformDates($fechas);
        $model = (!empty($fechas['desde']))   ? $model->where('mensajes.created_at','>=' ,$fechas['desde']) : $model;
        $model = (!empty($fechas['hasta']))   ? $model->where('mensajes.created_at','<=',$fechas['hasta'])  : $model;
        $model = (!empty($data['asunto']))    ? $model->where('mensajes.asunto','LIKE',$data['asunto']."%") : $model;
        $model = (!empty($data['contenido'])) ? $model->where('mensajes.contenido','LIKE',$data['contenido']."%") : $model;


        return $model;

    }

    private function transformDates(Array &$fechas)
    {
        foreach($fechas as $index=>$value){
            $fechas[$index] = Time::FormatearHoraFechaMysql($value);
        }
    }

    
}
