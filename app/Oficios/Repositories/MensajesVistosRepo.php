<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 10/11/14
 * Time: 17:40
 */

namespace Oficios\Repositories;



use Oficios\Entities\MensajesVistos;

class MensajesVistosRepo extends BaseRepo {

    public function getModel()
    {
        return new MensajesVistos();
    }

    public function newMensajeVisto($idMensaje,$idUsuario){
        $mensaje  = $this->getModel();
        $mensaje->id_mensaje = $idMensaje;
        $mensaje->id_usuario  =  $idUsuario;
        return $mensaje;
    }

    public function getMensajesVistos($id){
        $visto =  $this->model->where("id_mensaje","=",$id)->where("id_usuario",'=',\Auth::user()->id)->limit(1)->get();
        return $visto;
    }

}
