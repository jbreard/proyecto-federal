<?php


namespace Oficios\Repositories;

use Oficios\Entities\Log;


class LogRepo extends BaseRepo {

    protected  $table;

    public function __construct($table){


        $this->table = $table;

        parent::__construct();


    }

    public function getModel()
    {
        $log = new Log();
        $log->setTable($this->table);
        return $log;

    }

    public function newOficioLog($idOficio,$field,$value){
        $oficio =  $this->newLog($field,$value);
        $oficio  -> id_oficio = $idOficio;
        return $oficio;
    }
    public function newPersonaLog($idPersona,$field,$value){

        $persona = $this->newLog($field,$value);
        $persona -> id_persona  = $idPersona;
        return $persona;
    }
    public function newSecuestroElmentoLog($idSecuestroElemento,$field,$value){

        $persona = $this->newLog($field,$value);
        $persona -> id_secuestro_elementos  = $idSecuestroElemento;
        return $persona;
    }
    public function newSecuestroVehicularLog($idSecuestroVehicular,$field,$value){

        $secuestroVehicular = $this->newLog($field,$value);
        $secuestroVehicular -> id_secuestro_vehicular  = $idSecuestroVehicular;
        return $secuestroVehicular;
    }
    public function newSolicitudParaderoLog($idSolicitudParadero,$field,$value){

        $solicitudParadero = $this->newLog($field,$value);
        $solicitudParadero-> id_solicitud_paradero  = $idSolicitudParadero;
        return $solicitudParadero;
    }

    public  function getOficioLog($campo,$id){

        return $this->where($campo,$id);

    }



}
