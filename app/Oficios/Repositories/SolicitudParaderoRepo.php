<?php


namespace Oficios\Repositories;


use Oficios\Entities\SolicitudParadero;

class SolicitudParaderoRepo extends BaseRepo {

    protected $oficios;

    protected $personas;


    public function getModel()
    {

        return new SolicitudParadero;
    }

    public function  getSolicitud($id){

        $this->oficios      = new OficiosRepo();
        $this->personas     = new PersonasRepo();

        $oficios            = $this->oficios->find($id);

        $buscado            = $this->personas->getPersona($id,3);

        $denunciante        = $this->personas->getPersona($id,4);

        $solicitud_paradero = $this->getSolicitudParadero($buscado->id);

        $documentos         = $buscado->getDocumentos;


        return compact("oficios","buscado","solicitud_paradero","denunciante","documentos");
    }

    public function newSolicitud($idPersona){

        $soliciutd             = new SolicitudParadero();
        $soliciutd->id_persona = $idPersona;

        return $soliciutd;
    }


}