<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 03/10/14
 * Time: 15:25
 */

namespace Oficios\Repositories;

use Oficios\Entities\Observaciones;

class ObservacionesRepo extends BaseRepo {

    public function getModel()
    {
        return new Observaciones();
    }


    public function findWhere($id){

    }

    public function newObservacion($id)
    {

        $observacion = $this->getModel();
        $observacion->id_oficio = $id;
        $observacion->id_usuario = \Auth::user()->id;
        return $observacion;
    }
}