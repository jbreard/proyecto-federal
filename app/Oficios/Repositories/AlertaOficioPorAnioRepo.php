<?php
namespace Oficios\Repositories;

use Oficios\Entities\Oficios;

class AlertaOficioPorAnioRepo extends BaseRepo{

    public function getModel()
    {
        return new Oficios();
    }

    public function getCumpleAnio() {
        return $this->model->where('cumple_anio','=',1)->get();
    }



}