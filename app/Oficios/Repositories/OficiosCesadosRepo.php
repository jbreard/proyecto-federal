<?php


namespace Oficios\Repositories;

use Oficios\Entities\OficiosCesados;
use Usuarios\Entities\User;


class OficiosCesadosRepo extends BaseRepo {

    public function getModel()
    {
        return new OficiosCesados();
    }

    public function newOficioCesado($idOficio){
        $oficio  = new OficiosCesados();
        $oficio->id_oficio   = $idOficio;
        $oficio->id_usuario  = (\Auth::user()) ? \Auth::user()->id : User::withTrashed()->where('sistema','=',1)->first()->id;
        return $oficio;
    }



   


}
