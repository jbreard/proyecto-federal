<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 03/11/14
 * Time: 11:21
 */

namespace Oficios\Repositories;




use Oficios\Entities\RespuestasMensajes;

class RespuestasMensajesRepo extends BaseRepo {

    public function getModel()
    {
        return new RespuestasMensajes();
    }


    public function newRespuesta($idMensaje){
        $respuesta = $this->getModel();
        $respuesta->id_mensaje = $idMensaje;
        $respuesta->id_usuario = \Auth::user()->id;
        return $respuesta;

    }
}