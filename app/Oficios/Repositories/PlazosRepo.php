<?php


namespace Oficios\Repositories;

use Carbon\Carbon;
use Oficios\Entities\Plazo;


class PlazosRepo extends BaseRepo {
    public function getModel()
    {
        return new Plazo();
    }

    public function newPlazo($idOficio)
    {
        $model = $this->getModel();
        $model->id_oficio = $idOficio;
        return $model;
    }

    public function getPlazos($idOficio)
    {
        $model = $this->getModel();
        return $model->where('id_oficio',$idOficio)->withTrashed()->get();
    }


    


    public function getPlazosVencidos()
    {
        $hoy = Carbon::now();
        return $this->model->where('fecha_final','<=',$hoy)->get();

    }

}
