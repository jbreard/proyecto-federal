<?php


namespace Oficios\Repositories;


use Oficios\Entities\SolicitudParadero;

class MedidasRepo extends BaseRepo {

    protected $oficios;

    protected $personas;


    public function getModel()
    {

        return new SolicitudParadero;
    }

    public function  getMedidas($id){

        $oficios        = new OficiosRepo();
        $persona        = new PersonasRepo();
        $oficios        = $oficios->find($id);
        $victima        = $persona->getPersona($id,1);
        $responsable    = $persona->getPersona($id,2);
        $documentos     = $responsable->getDocumentos;

        return compact("oficios","victima","responsable","documentos","persona");
    }




}