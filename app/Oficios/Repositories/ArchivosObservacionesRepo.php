<?php
namespace Oficios\Repositories;
use Oficios\Entities\ArchivosObservaciones;
use Oficios\Interfaces\ArchivosRepoInterface;

class ArchivosObservacionesRepo extends BaseRepo implements ArchivosRepoInterface {

    public function getModel()
    {
        return new ArchivosObservaciones();
    }

    public function newArchivo($idObservacion, $path)
    {
        $model = $this->getModel();
        $model->id_observacion = $idObservacion;
        $model->path  = $path;
        return $model;
    }
}