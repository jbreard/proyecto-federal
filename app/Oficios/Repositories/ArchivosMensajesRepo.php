<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 20/04/15
 * Time: 14:56
 */

namespace Oficios\Repositories;


use Oficios\Entities\ArchivosMensajes;

class ArchivosMensajesRepo extends BaseRepo{

    public function getModel()
    {
        return new ArchivosMensajes();
    }

    public function newArchivo($idMensaje,$path){
        $repo = $this->getModel();
        $repo->id_mensaje = $idMensaje;
        $repo->path       = $path;
        return $repo;
    }
}