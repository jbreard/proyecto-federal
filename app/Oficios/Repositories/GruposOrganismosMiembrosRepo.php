<?php 
namespace Oficios\Repositories;
use Oficios\Entities\GruposOrganismosMiembros;


class GruposOrganismosMiembrosRepo extends BaseRepo
{
	
	public function getModel(){
		return new GruposOrganismosMiembros();
	}

	public function newModel($data){
		$idGrupo = $data['idGrupo'];
		$idOrganismo = $data['idOrganismo'];
		$model = $this->getModel();
		$model->id_organismo = $idOrganismo;
		$model->id_grupo = $idGrupo;
		return $model;
	}

	public function getMiembro($data){
		$idGrupo = $data['idGrupo'];
		$idOrganismo = $data['idOrganismo'];
		$model = $this->getModel();
		$model = $model->where('id_organismo','=',$idOrganismo)->where('id_grupo','=',$idGrupo);
		return $model;	
	}

	public function getMiembrosByIdGrupo($id){
		return $this->model->where('id_grupo','=',$id)->get();
	}

}
