<?php


namespace Oficios\Repositories;


use Oficios\Entities\HabeasCorpus;


class HabeasCorpusRepo extends BaseRepo {

    protected $oficios;

    protected $personas;


    public function getModel()
    {

        return new HabeasCorpus;
    }

    public function     getHabeas($id){

        $this->oficios      = new OficiosRepo();
        $this->personas     = new PersonasRepo();
        $oficios            = $this->oficios->find($id);


        return compact("oficios","personas");
    }

    public function newHabeas($idPersona){

        $habeas             = new HabeasCorpus();
        $habeas->id_persona = $idPersona;
        return $habeas;
    }


}