<?php


namespace Oficios\Repositories;

use Oficios\Entities\Personas;


class PersonasRepo extends BaseRepo {
    public function getModel()
    {
        return new Personas();
    }

    public function newPersona($idOficio,$idTipoPersona){

        $persona                     = new Personas();

        $persona->id_oficio          = $idOficio;

        $persona->id_tipo_persona    = $idTipoPersona;

        return $persona;
    }

    public function getPersonaDni($datos){

        extract($datos);

        $personas = new Personas();

        $personasDatos = $personas->whereHas("oficio",function($q) use ($tipo_oficio){

            $q->where("tipo_oficio","=",$tipo_oficio);
            $q->where("confirmado","=","2");
            $q->where("estado","=","1");
            $q->orWhere("estado","=","2");

        })
        ->where("nro_documento","=",$dni)->get();

        return $personasDatos;





    }

    public function guardarPersona($id,$id_oficio,$nombres,$apellido,$dni="",$id_tipo_persona,$id_nacionalidad=149,$id_sexo=1,$id_genero=1,$id_tipo_documento=1)
    {
        $persona = $this->getModel()->firstOrNew(['id' => $id]);
        $persona->id_oficio = $id_oficio;
        $persona->nombre = $nombres;
        $persona->apellido = $apellido;
        $persona->nro_documento = $dni;
        $persona->id_tipo_persona = $id_tipo_persona;
        $persona->id_nacionalidad = $id_nacionalidad;
        $persona->id_sexo = $id_sexo;
        $persona->id_genero = $id_genero;
        $persona->id_tipo_documento = $id_tipo_documento;
        $persona->save();
//        dd("paso");
    }

    public function getPersonaPorId($id)
    {
        return $this->getModel()->where("id",$id)->first();
    }

     public function getPersonasPedidoCaptura(){
        $personas = $this->getModel()->where("nro_documento",'!=','')->whereHas('oficio',function($q){
            $q->where('tipo_oficio','=','4')->where('estado','=','1')->where('confirmado','=',2);
        })
        ->get(["nombre","apellido","nro_documento as dni"]);

        $personas->map(function($persona){
            $persona['motivo'] = 'Pedido de captura';
            $persona['clubes'] = [];
            return $persona;
        });

        return $personas;
    }

    public function getTipoOficio($id)
    {
        $model = $this->getModel()
            ->with('oficio')
            ->where('id',$id)
            ->first();

        return $model->oficio->tipo_oficio;
    }

}