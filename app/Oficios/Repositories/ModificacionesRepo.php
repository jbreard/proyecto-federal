<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 11/07/14
 * Time: 09:41
 */

namespace Oficios\Repositories;

use Illuminate\Support\Facades\View;
use Oficios\Entities\Modificaciones;




class ModificacionesRepo extends BaseRepo {

    protected $modVistasAux = [0];

    public function getModel()
    {
        // TODO: Implement getModel() method.
        return new Modificaciones();

    }


    public function newMod($idOficio){

        $mod = new Modificaciones();
        $mod->id_oficio  = $idOficio;
        $mod->id_usuario = (isset(\Auth::user()->id)) ? \Auth::user()->id : 50;
        return $mod;


    }

    public function consultaModel($idOficio){

        $a= $this->model
            ->with('getOficio')
            ->whereIdOficio($idOficio)->where('id_usuario','<>',\Auth::user()->id)->first();
        return $a;
    }

    public function existe($idOficio){

        $a = $this->consultaModel($idOficio);
        if(count($a)>=1){
            return true;
        }

        return false;

    }

    private  function consulta(){


        $id = \Auth::user()->id;


        $mod = \DB::select(\DB::raw('SELECT COUNT(*) AS cantidad

        FROM modificaciones
        INNER JOIN oficios ON modificaciones.id_oficio = oficios.id
        WHERE modificaciones.id NOT IN(SELECT modificaciones_vistas.id_mod  from modificaciones_vistas where id_usuario ='.$id.')
        '));

        return $mod;




    }


    public  function ids(){


        $id = \Auth::user()->id;

        $ids = [0];

        $mod = \DB::select(\DB::raw('SELECT modificaciones.id_oficio
        FROM modificaciones
        INNER JOIN oficios ON modificaciones.id_oficio = oficios.id
        WHERE modificaciones.id NOT IN(SELECT modificaciones_vistas.id_mod  from modificaciones_vistas where id_usuario ='.$id.')
        '));


        foreach ($mod as $key => $value) {
            $ids[$value->id_oficio]  = $value->id_oficio;
        }


        

        return $ids;




    }




    public function modNoVistas(){

        $mod = $this->consulta();

        return $mod[0]->cantidad;



    }


    public function cantOficios(){
        $cantidad = $this->consulta()->count();
        return $cantidad;
    }

    public function cantOficiosDetalle()
    {
        $id = \Auth::user()->id;

        $mod = \DB::select(\DB::raw('SELECT COUNT(*) AS cantidad,oficios.tipo_oficio as tipo_oficio

FROM modificaciones

INNER JOIN oficios ON modificaciones.id_oficio = oficios.id



WHERE modificaciones.id NOT IN(SELECT modificaciones_vistas.id_mod  from modificaciones_vistas where id_usuario ='.$id.')

GROUP BY oficios.tipo_oficio'));




        return json_encode($mod);
    }



} 