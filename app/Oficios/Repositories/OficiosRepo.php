<?php


namespace Oficios\Repositories;

use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Oficios\Entities\Oficios;
use Oficios\Entities\OficiosVistos;
use Oficios\Repositories\OficiosVistosRepo;
use Oficios\Repositories\ModificacionesRepo;
use PDF;
use Time;
use View;


class OficiosRepo extends BaseRepo
{

    public function getModel()
    {
        return new Oficios();
    }

    public function newOficio($idTipoOficio)
    {
        $oficio = new Oficios();
        $oficio->tipo_oficio = $idTipoOficio;
        $oficio->id_usuario = Auth::user()->id;
        return $oficio;
    }

    public function Buscar($data)
    {
        $oficios = new Oficios();

//        dd($data);
        if(!empty($data))
        {
            $oficios = $this->searchByFechaDesdeAndFechaHasta($data, $oficios);

            if (!empty($data['vigente'])) $oficios = $this->searchByVigente($data['vigente'], $oficios);

            if (!empty($data['organismos'])) $oficios = $this->searchByOrganismo($data, $oficios);

            if (!empty($data['sub_organismo'])) $oficios = $this->searchBySubOrganismo($data, $oficios);

            if (!empty($data['tipo_medida'])) $oficios = $this->searchByTipoMedida($data, $oficios);

            if (isset($data["autorizado"]) && ($data['autorizado']!=2)) $oficios = $this->searchByAutorizado($data['autorizado'], $oficios);

            if (!empty($data['req_pendientes'])) $oficios = $this->searchByRequerimientosPendientes($data, $oficios);

            if(!empty($data['req_mod_pendientes'])) $oficios = $this->searchByRequerimientosModificadosPendientes($data['req_mod_pendientes'], $oficios);

            if (!empty($data['buscar_por']) and $data['buscar_por'] == 'elementos') {

                $elemento = $data["tipo_elemento"];
                $elementosValidos = ["armas","vehiculo","otros"];
                if(in_array($elemento,$elementosValidos)) {
                    $method = "searchBy" . ucfirst($elemento);
                    $value = $data[$elemento];
//                    dd($method,$value);
                    $oficios = call_user_func_array([$this, $method],array($value, $oficios));
//                    dd($oficios);
                }

            }elseif(!empty($data['buscar_por']) and $data['buscar_por'] == 'personas'){
                $oficios = $this->searchByPersonas2($data["personas"], $oficios);
            }

//            dd($oficios);
            if(!empty($data['alerta_temp'])) $oficios = $this->searchByAlertaTemprana($data['alerta_temp'], $oficios);
            if(!empty($data['id'])) $oficios = $oficios->where('id','=',$data['id']);

        }

        return $oficios->orderBy('oficios.id', 'DESC')
            ->where('oficios.confirmado','=','2');
    }

    public function getCantidadOficiosNoVistos()
    {
        $idUsuario = Auth::user()->id;
        $queryOficiosVistos = "
        SELECT 	COUNT('id') AS 'cantidad_oficios'
        FROM oficios
        WHERE oficios.confirmado = 2
        AND oficios.id
        NOT IN (
          SELECT DISTINCT oficios_vistos.id_oficio
          FROM oficios_vistos
          WHERE oficios_vistos.id_usuario = {$idUsuario}
        )
        ";

        $oficiosVistos = Cache::remember('oficiosVistos' . $idUsuario, 10 , function() use($queryOficiosVistos){
            return DB::select(DB::raw($queryOficiosVistos));
        });

        return json_encode($oficiosVistos = array(1=>$oficiosVistos[0]));
    }

    public function getCantidadOficiosDetalle()
    {
        $idUsuario = Auth::user()->id;
        $query = "
        SELECT 	tipo_oficio, COUNT('id') AS 'cantidad_oficios'
        FROM oficios
        WHERE oficios.confirmado = 2
        AND oficios.id
        NOT IN (
          SELECT oficios_vistos.id_oficio
          FROM oficios_vistos
          WHERE oficios_vistos.id_usuario = {$idUsuario}
        )
        GROUP BY tipo_oficio";

        $oficiosGroupBy = DB::select($query);

        return json_encode($oficiosVistos = array( 1 => $oficiosGroupBy ));
    }

    public function getOficiosNoLeidos($idUsuario)
    {
        $query = "
        SELECT id
        FROM oficios
        WHERE oficios.id NOT IN (SELECT id_oficio FROM oficios_vistos WHERE id_usuario = {$idUsuario})";
        return DB::select($query);
    }

    public function getPdf($html)
    {
        $html = View::make('solo_lectura.pdf.pdf_general', array("html" => $html))->render();
        return PDF::load($html, 'A4', 'portrait')->show();
    }

    public function getPdfReporteNota($html)
    {
        $html = \View::make('solo_lectura.pdf.reporte_nota', array("html" => $html))->render();

        return \PDF::load($html, 'A4', 'portrait')->show();
    }

    public function getRuta($idOficio){
        return $this->model->with('tipo')->where('id','=',$idOficio)->first();
    }

    public function getOficiosVencidos()
    {
        $now = Carbon::now()->format('Y-m-d');
        return  $this->model->where('vencimiento_confirmado','=','1')
            ->where('plazo_fecha','<=',$now)
            ->where('plazo_fecha','<>','0000-00-00')
            ->where('tipo_oficio','<>',0)
            ->where('estado','<>',2)
            ->where(function($q){
                $q->where('tipo_oficio','=',3)
                    ->orWhere('tipo_oficio','=',5);
            })
            ->get();
    }

    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchByFechaDesdeAndFechaHasta($data, $oficios)
    {
        $fechaDesde = array_get($data,'fecha_desde');
        $fechaHasta = array_get($data,'fecha_hasta');

        if (!empty($fechaDesde) and !empty($fechaHasta)) {
            $oficios = $oficios->whereBetween('created_at', [
                Time::FormatearToMysql($fechaDesde),
                Time::FormatearToMysql($fechaHasta)
            ]);
        } elseif (!empty($fechaDesde)) {
            $oficios = $oficios->where('created_at', '>=', Time::FormatearToMysql($fechaDesde));
        } elseif(!empty($fechaHasta)) {
            $oficios = $oficios->where('created_at', '<=', Time::FormatearToMysql($fechaHasta));
        }

        return $oficios;
    }

    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchByOrganismo($data, $oficios)
    {
        $organismos = $data['organismos'];
        $oficios = $oficios->whereHas('user', function ($user) use ($organismos) {
            $user->where('id_jurisdiccion', '=', $organismos);
        });
        return $oficios;
    }
    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchBySubOrganismo($data, $oficios)
    {
        $suborganismo = $data['sub_organismo'];
        $oficios = $oficios->whereHas('user', function ($user) use ($suborganismo) {
            $user->where('sub_organismo', '=', $suborganismo);
        });
        return $oficios;
    }

    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchByTipoMedida($data, $oficios)
    {
        $oficios = ($data['tipo_medida'] != 'personas') ? $oficios->where('tipo_oficio', '=', $data['tipo_medida']) : $oficios;
        return $oficios;
    }

    /**
     * @param $autorizado
     * @param $oficios
     * @return mixed
     */
    private function searchByAutorizado($autorizado, $oficios)
    {
        return $oficios->where('plazo_autorizado', '=', $autorizado);
    }

    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchByRequerimientosPendientes($data, $oficios)
    {
        $oficiosVistosRepo = new OficiosVistosRepo();
        $oficiosLeidosByUsuario = $oficiosVistosRepo->getOficioNoLeidoUsuario(array_get($data, 'page', 0));

        if (count($oficiosLeidosByUsuario) > 0) {
            $requerimientosPendientes = $data['req_pendientes'];
            $oficios = ($requerimientosPendientes == 2)
                ? $oficios->whereNotIn('id', $oficiosLeidosByUsuario)
                : $oficios->whereIn('id', $oficiosLeidosByUsuario);
        } else {
            $oficios = $oficios->whereIn('id', [0]);
        }

        return $oficios;
    }

    /**
     * @param $requerimientosModificadosPendientes
     * @param $oficios
     * @return mixed
     */
    private function searchByRequerimientosModificadosPendientes($requerimientosModificadosPendientes, $oficios)
    {
        $modificacionesRepo = new ModificacionesRepo();
        $idOficios = $modificacionesRepo->ids();

        if (count($idOficios) > 0) {
            $oficios = ($requerimientosModificadosPendientes == 3)
                ? $oficios->whereNotIn('id', $idOficios)
                : $oficios->whereIn('id', $idOficios);
        } else {
            $oficios = $oficios->whereIn('id', [0]);
            //se que es una negrada pero me tiene los huevos al piso oficios
            //Idem Arriba
            //Si esto algun dia lo suben a produccion y ven el codigo yo me hago hincha del porvenir
            //Lo anterior fue escrito por Juan Guzman.
        }
        return $oficios;
    }

    public function getDataForTransactionFlow($idOficio)
    {

        $query = <<<QUERY
SELECT 

oficios.id, 

personas.nombre, 
personas.apellido, 
nacionalidades.descripcion as 'nacionalidad', 
personas.edad, 
sexo.descripcion as 'sexo', 
tipo_doc.descripcion as 'tipo_documento', 
personas.nro_documento,

tipos_oficios.descripcion as 'tipo_oficio', 

oficios.causa_medida, 
juzgados.juzgado,
oficios.juzgado_otro,
fiscalias.fiscalias,
oficios.otra_fiscalia, 
oficios.created_at,
oficios.updated_at, 
oficios.deleted_at, 
estados_oficios.descripcion as 'estado'

FROM   

oficios
LEFT JOIN tipos_oficios ON tipos_oficios.id = oficios.tipo_oficio
LEFT JOIN juzgados ON juzgados.id = oficios.juzgado
LEFT JOIN estados_oficios ON estados_oficios.id = oficios.estado
LEFT JOIN fiscalias ON fiscalias.id = oficios.fiscalia
LEFT JOIN personas ON personas.id_oficio = oficios.id
LEFT JOIN nacionalidades ON nacionalidades.id = personas.id_nacionalidad
LEFT JOIN sexo ON sexo.id = personas.id_sexo
LEFT JOIN tipo_doc ON tipo_doc.id = personas.id_tipo_documento

WHERE
oficios.id = {$idOficio}
QUERY;

        $tiposPersonaValidos = $this->getTiposPersonaValidosByTipoOficio($idOficio);

        if(!empty($tiposPersonaValidos))
            $query .= " AND personas.id_tipo_persona IN ({$tiposPersonaValidos}) ";

        $result = DB::select($query);

        return $result;
        
    }

    public function getDataForTransactionFlowFirstQueryPersonas()
    {

        $query = <<<QUERY
SELECT 

oficios.id, 

personas.nombre, 
personas.apellido, 
nacionalidades.descripcion as 'nacionalidad', 
personas.edad, 
sexo.descripcion as 'sexo', 
tipo_doc.descripcion as 'tipo_documento', 
personas.nro_documento,

tipos_oficios.id as 'id_tipo_oficio', 
tipos_oficios.descripcion as 'tipo_oficio', 

oficios.causa_medida, 
oficios.juzgado_otro,
fiscalias.fiscalias,
oficios.otra_fiscalia, 
oficios.created_at,
oficios.updated_at, 
oficios.deleted_at, 
oficios.confirmado, 
oficios.estado as 'id_estado', 
estados_oficios.descripcion as 'estado'

FROM   

oficios
LEFT JOIN tipos_oficios ON tipos_oficios.id = oficios.tipo_oficio
LEFT JOIN juzgados ON juzgados.id = oficios.juzgado
LEFT JOIN estados_oficios ON estados_oficios.id = oficios.estado
LEFT JOIN fiscalias ON fiscalias.id = oficios.fiscalia
LEFT JOIN personas ON personas.id_oficio = oficios.id
LEFT JOIN nacionalidades ON nacionalidades.id = personas.id_nacionalidad
LEFT JOIN sexo ON sexo.id = personas.id_sexo
LEFT JOIN tipo_doc ON tipo_doc.id = personas.id_tipo_documento

WHERE

(

( 
oficios.tipo_oficio = 1 AND personas.id_tipo_persona = 3)
OR ( oficios.tipo_oficio = 3 AND personas.id_tipo_persona = 2)
OR ( oficios.tipo_oficio = 4 AND personas.id_tipo_persona = 2)
OR ( oficios.tipo_oficio = 5 AND personas.id_tipo_persona = 2)
OR ( oficios.tipo_oficio = 6 AND personas.id_tipo_persona = 3)
OR ( oficios.tipo_oficio = 7 AND personas.id_tipo_persona = 2)
)

AND (
personas.nro_documento IS NOT NULL && personas.nro_documento <> '' 
)

ORDER BY oficios.id ASC
QUERY;

        $result = DB::select($query);

        return $result;

    }

    public function getDataForTransactionFlowPedidosSecuestroVehicular($idOficio)
    {

        $query = <<<QUERY
SELECT 
oficios.id, 

secuestro_vehicular.dominio, 
secuestro_vehicular.nro_chasis, 
secuestro_vehicular.nro_motor, 
secuestro_vehicular.radicacion, 
secuestro_vehicular.anio_vehiculo,
secuestro_vehicular.id_tipo_vehiculo,
secuestro_vehicular.otro_tipo_vehiculo,
secuestro_vehicular.otra_marca,
secuestro_vehicular.otro_modelo,
secuestro_vehicular.color,
secuestro_vehicular.marca_motor,
secuestro_vehicular.nro_motor,

vehiculos_marcas.marca, 
vehiculos_modelos.modelo,


tipos_oficios.id as 'id_tipo_oficio', 
tipos_oficios.descripcion as 'tipo_oficio', 

oficios.causa_medida, 
juzgados.juzgado,
oficios.juzgado_otro,
fiscalias.fiscalias,
oficios.otra_fiscalia, 
oficios.created_at,
oficios.updated_at, 
oficios.deleted_at, 
oficios.confirmado, 
oficios.estado as 'id_estado', 
estados_oficios.descripcion as 'estado'

FROM   oficios
INNER JOIN tipos_oficios ON tipos_oficios.id = oficios.tipo_oficio
INNER JOIN secuestro_vehicular ON secuestro_vehicular.id_oficio = oficios.id
LEFT JOIN vehiculos_marcas ON vehiculos_marcas.id = secuestro_vehicular.marca_vehiculo
LEFT JOIN vehiculos_modelos ON vehiculos_modelos.id = secuestro_vehicular.modelo_vehiculo
LEFT JOIN juzgados ON juzgados.id = oficios.juzgado
LEFT JOIN estados_oficios ON estados_oficios.id = oficios.estado
LEFT JOIN fiscalias ON fiscalias.id = oficios.fiscalia

WHERE 
oficios.id = {$idOficio}

GROUP BY oficios.id
QUERY;

        $result = \DB::select($query);

        return $result;

    }

    public function getDataForTransactionFlowFirstQueryVehiculos()
    {

        $query = <<<QUERY
SELECT 
oficios.id, 

secuestro_vehicular.dominio, 
secuestro_vehicular.nro_chasis, 
secuestro_vehicular.nro_motor, 
secuestro_vehicular.radicacion, 
secuestro_vehicular.anio_vehiculo,
secuestro_vehicular.id_tipo_vehiculo,
secuestro_vehicular.otro_tipo_vehiculo,
secuestro_vehicular.otra_marca,
secuestro_vehicular.otro_modelo,
secuestro_vehicular.color,
secuestro_vehicular.marca_motor,
secuestro_vehicular.nro_motor,

vehiculos_marcas.marca, 
vehiculos_modelos.modelo,


tipos_oficios.id as 'id_tipo_oficio', 
tipos_oficios.descripcion as 'tipo_oficio', 

oficios.causa_medida, 
juzgados.juzgado,
oficios.juzgado_otro,
fiscalias.fiscalias,
oficios.otra_fiscalia, 
oficios.created_at,
oficios.updated_at, 
oficios.deleted_at, 
oficios.confirmado, 
oficios.estado as 'id_estado', 
estados_oficios.descripcion as 'estado'

FROM   oficios
INNER JOIN tipos_oficios ON tipos_oficios.id = oficios.tipo_oficio
INNER JOIN secuestro_vehicular ON secuestro_vehicular.id_oficio = oficios.id
LEFT JOIN vehiculos_marcas ON vehiculos_marcas.id = secuestro_vehicular.marca_vehiculo
LEFT JOIN vehiculos_modelos ON vehiculos_modelos.id = secuestro_vehicular.modelo_vehiculo
LEFT JOIN juzgados ON juzgados.id = oficios.juzgado
LEFT JOIN estados_oficios ON estados_oficios.id = oficios.estado
LEFT JOIN fiscalias ON fiscalias.id = oficios.fiscalia

WHERE oficios.tipo_oficio = 2

ORDER BY oficios.id ASC
QUERY;

        $result = \DB::select($query);

        return $result;

    }

    /**
     * @param $idOficio
     */
    public function getTipoOficioByIdOficio($idOficio)
    {
        $oficio = $this->getModel()->where('id',$idOficio)->first();

        return $oficio->tipo_oficio;
    }

    public function isConfirmadoByIdOficio($idOficio)
    {
        $oficio = $this->getModel()->where('id',$idOficio)->first();

        return ($oficio->confirmado == 2);
    }

    /**
     * @param $idOficio
     * @return array|string
     */
    public function getTiposPersonaValidosByTipoOficio($idOficio)
    {
        $validations = Config::get('app.apis.base_incremental.settings.flow.validations');

        $tipoOficio = $this->getTipoOficioByIdOficio($idOficio);

        $tiposPersona = $validations[$tipoOficio];

        $tiposPersonaIsValid = [];

        $stringTiposPersonaIsValid = "";

        foreach ($tiposPersona as $tipo => $isValid) if ($isValid) array_push($tiposPersonaIsValid, $tipo);

        if(!empty($tiposPersonaIsValid)) $stringTiposPersonaIsValid = implode(",", $tiposPersonaIsValid);
        
        return $stringTiposPersonaIsValid;
    }

    public function getOficiosProfugoEvadidoCheck()
    {
        return $this->getModel()->where("tipo_oficio",12)->get();
    }

    public function getOficiosRoboAutomotorCheck()
    {
        return $this->getModel()->where("tipo_oficio",13)->get();
    }

    /**
     * @param $data
     * @param $oficios
     * @return mixed
     */
    private function searchByVehiculo($data, $oficios)
    {
        $secuestroVehicularRepo = New SecuestroVehicularRepo();
        $arrayPersonas = $secuestroVehicularRepo->getSecuestroVehicularByFields($data)
            ->get(['id_persona'])
            ->toArray();
        $oficios = $oficios->whereIn('tipo_oficio',['2','13'])
            ->whereHas('personas', function ($personas) use ($arrayPersonas) {
                $personas->whereIn('id', $arrayPersonas);
            });

        return $oficios;
    }

    /**
     * @param $armas
     * @param $oficios
     * @return mixed
     * esto se modifico para tratar de que la consulta tarde menos baja de 9sg a 2
     */
    private function searchByArmas($armas, $oficios)
    {
        $and='';
        $b=0;
        foreach ($armas as $key => $value) {
            if (!empty($value)) {
                $and.= "AND se.$key LIKE '%$value%'";
                $b++;
            }
        }
        if($b<>0) {
            $datos = \DB::select("SELECT ofi.id FROM oficios AS ofi
                                  INNER JOIN personas AS p 
                                  ON p.id_oficio = ofi.id 
                                  INNER JOIN secuestro_armas AS se 
                                  Where se.id_persona = p.id 
                                  $and
                                  ORDER by ofi.id desc");
            $i = 0;
            if ($datos) {
                foreach ($datos as $dato) {
                    //print_r($dato->id);
                    $id[$i] = $dato->id;
                    $i++;
                }
                return $oficios = $oficios->whereIn('id', $id);
            }

            return $oficios = $oficios->whereIn('id', 0);
        }else {
            //ini_set('memory_limit', '8192M');
            //ini_set('max_execution_time', 180); //3 minutes
            $oficios = $oficios->whereIn('tipo_oficio',['10']);
            return $oficios;
        }

/*      esta es la forma de hacerlo con Laravel pero tarda mucho
        $oficios = $oficios->where('tipo_oficio', '=', '10');
        $oficios = $oficios->whereHas('personas', function ($personas) use ($armas) {
            $personas->whereHas('getSecuestroArmas', function ($secuestrosArmas) use ($armas) {
                foreach ($armas as $key => $value) {
                    if (!empty($value)) $secuestrosArmas->where($key, "LIKE", "$value%");
                }
            });
        });
        return $oficios;*/
    }

    /**
     * @param $otros
     * @param $oficios
     * @return mixed
     */
    private function searchByOtros($otros, $oficios)
    {
        //dd($otros);
        /*$oficios = $oficios->where('tipo_oficio', '=', '8');
        $oficios = $oficios->whereHas('personas', function ($personas) use ($otros) {
            $personas->whereHas('getOtrosElementos', function ($otrosElementos) use ($otros) {
                foreach ($otros as $key => $value) {
                    if (!empty($value)) $otrosElementos->where($key, "LIKE", "$value%");
                }
            });
        });
        return $oficios;*/
        $fields  = $otros;
        $and = '';
        $b=0;
        foreach ($fields as $key => $value) {
            if (!empty($value)) {
                $pos = strpos($value, "%");

                $and .= " AND se.$key LIKE '%$value%'";
                $b++;
            }
        }

        $datos = \DB::select("SELECT ofi.id FROM oficios AS ofi
                     INNER JOIN personas AS p
                     ON ofi.id = p.id_oficio
                     INNER JOIN secuestro_elementos AS se 
                     WHERE p.id = se.id_persona $and
                     ORDER BY  ofi.id");
        //dd($datos);
        $i = 0;
        if ($datos) {
            foreach ($datos as $dato) {
                //print_r($dato->id);
                $id[$i] = $dato->id;
                $i++;
            }
            return $oficios = $oficios->whereIn('id', $id);
        }

        return $oficios = $oficios->whereIn('id', 0);

    }

    private function searchByPersonas2($fields, $oficios)
    {
        $and='';
        $b=0;
        foreach ($fields as $key => $value) {
            if (!empty($value)) {
                $value = ($key == 'edad') ? Time::FormatearToMysql($value) : $value;

                $pos = strpos($value, "%");


                $and.= "AND p.$key LIKE '%$value%'";
                $b++;
            }
        }
        if($b<>0) {
            $datos = \DB::select("SELECT ofi.id FROM oficios AS ofi
                     INNER JOIN personas AS p
                     WHERE ofi.id = p.id_oficio $and
                     ORDER BY  ofi.id");
            $i = 0;
            if ($datos) {
                foreach ($datos as $dato) {
                    //print_r($dato->id);
                    $id[$i] = $dato->id;
                    $i++;
                }
                return $oficios = $oficios->whereIn('id', $id);
            }

            return $oficios = $oficios->whereIn('id', 0);
        }else {
            //ini_set('memory_limit', '8192M');
            //ini_set('max_execution_time', 180); //3 minutes
            $oficios = $oficios->whereIn('tipo_oficio',['1','3','4','5','6','7','9','12']);
        return $oficios;
        }

    }


    /**
     * @param $fields
     * @param $oficios
     * @return mixed
     */
    private function searchByPersonas($fields, $oficios)
    {
        $oficios = $oficios->whereHas('personas', function ($personas) use ($fields) {
            foreach ($fields as $key => $value) {
                if (!empty($value)) {
                    $value = ($key == 'edad') ? Time::FormatearToMysql($value) : $value;

                    $pos = strpos($value, "%");
                    if($pos === false)
                        $personas->where($key, "LIKE", "%{$value}%");
                    else
                        $personas->where($key, "LIKE", "$value");
                }
            }
        });
        return $oficios;
    }
    /**
     * @param $fields
     * @param $oficios
     * @return mixed
     */
    private function searchByVehiculos($fields, $oficios)
    {
        dd($fields);
        $oficios = $oficios->whereHas('vehiculos_modelos', function ($personas) use ($fields) {
            foreach ($fields as $key => $value) {
//                if (!empty($value)) {
//                    $value = ($key == 'edad') ? Time::FormatearToMysql($value) : $value;
//                    $personas->where($key, "LIKE", "{$value}%");
//                }



            }
        });
        return $oficios;
    }

    /**
     * @param $alertaTemprana
     * @param $oficios
     * @return mixed
     */
    private function searchByAlertaTemprana($alertaTemprana, $oficios)
    {
        $value = ($alertaTemprana == 2) ? 0 : $alertaTemprana;
        return $oficios->where(function($q) use($value, $alertaTemprana){
	    if($alertaTemprana == 2) $q->whereNull('alerta_temprana');
            $q->orWhere('alerta_temprana', '=', $value);
        });

    }

    public function getOrganismos() {
        $dato = \DB::select("SELECT organismos.descripcion as 'organismo'
                 FROM oficios
                 INNER JOIN usuarios
                 INNER JOIN organismos
                 WHERE oficios.id_usuario = usuarios.id
                 AND usuarios.id_jurisdiccion = organismos.id
                 GROUP BY organismos.descripcion");
        return $dato;
    }

    public function getPersonaPorId($id)
    {
        return $this->getModel()->where("id",$id)->first();
    }

    public function queryOficiosByAnio(){
        return $this->model->where('created_at','<=',Carbon::now()->subYears(1))->where('revisado','!=',1)->where('estado','<>','2');
    }

    public function getOficiosByAnio(){

        return  $this->queryOficiosByAnio()->get();
    }

    public function getOficioByAnioCount(){
        return $this->queryOficiosByAnio()->count();
    }

    public function getTipoPersona($id_oficio)
    {
        $dato = \DB::select("select tp.id from oficios as o
                             inner join personas as p on o.id = p.id_oficio
                             inner join tipos_personas as tp on p.id_tipo_persona = tp.id
                             where o.id = $id_oficio");

        if(count($dato) > 0)
            return $dato[0]->id;
    }

    public function reqPorMes($anio  = null){
        $anio = (empty($anio)) ? \Carbon::now()->format("Y") : $anio;
        $dato = \DB::select("select count('id') AS 'cantidad',estado,MONTH(created_at) AS 'mes',YEAR(created_at) as 'anio' from oficios
        where YEAR(created_at) = $anio
        group by MONTH(created_at),estado");
        return $dato;

    }

    public function reqTipo($anio  = null){
        $dato = \DB::select("SELECT COUNT('id') AS 'cantidad',tipos_oficios.descripcion as 'tipo',oficios.estado as 'estado', YEAR(oficios.created_at) as 'anio' FROM oficios
                inner join tipos_oficios
                WHERE tipos_oficios.id = oficios.tipo_oficio
                AND YEAR(oficios.created_at) = $anio
                GROUP BY oficios.tipo_oficio,estado");
        return $dato;
    }

    public function reqPorOrganismo($anio = null) {
        $dato = \DB::select("SELECT COUNT('id') AS 'cantidad',estado,organismos.descripcion as 'organismo',YEAR(oficios.created_at) as 'anio'
                 FROM oficios
                 INNER JOIN usuarios
                 INNER JOIN organismos
                 WHERE oficios.id_usuario = usuarios.id
                 AND usuarios.id_jurisdiccion = organismos.id
                 AND YEAR(oficios.created_at) = $anio
                 GROUP BY organismos.descripcion,estado;");
        return $dato;
    }

    private function searchByVigente($vigente, $oficios)
    {
        return $oficios->where('estado', '=', $vigente);
    }

    public function darDatosViejosPersonasExtraviadas($id_persona)
    {
        return \DB::select("select p.id_oficio,sp.primera_desaparece,sp.enfermedad,sp.drogadiccion,sp.internado_institucion,sp.descripcion_caso,sp.novedades,sp.enfermedades,sp.concurrencia,sp.descipcion_persona,sp.medicacion,sp.vinculo_desaparecido,sp.pertenencia_referencia,sp.vestimenta,sp.descipcion_persona,sp.observaciones,
                                            p.observaciones_persona
                                    from solicitud_paradero as sp
                                    inner join personas as p on p.id = sp.id_persona
                                    inner join oficios as o on p.id_oficio = o.id
                                    where o.tipo_oficio = 1
                                    and p.id_tipo_persona = 3
                                    and p.id = $id_persona");
    }

    /**
     * @param $idOficio
     * @return mixed
     */
    public function getDataTypeByTipoOficio($idOficio)
    {
        $tipoOficio = $this->getTipoOficioByIdOficio($idOficio);

        $oficiosTiposDato = Config::get('app.apis.base_incremental.oficios_tipo_dato');

        $codigosTiposDato = Config::get('app.apis.base_incremental.codigos_tipo_dato');

        $tipo = $oficiosTiposDato[$tipoOficio];

        return $codigosTiposDato[$tipo];
    }

    public function searchByInterconsultas($type, $subject) {


        $urlApi = "http://10.100.83.33/{$type}/{$subject}";

        $client = new Client();

        $response = $client->request('GET',$urlApi);
        echo $response->getStatusCode();
        $body = $response->getBody();

        return json_decode($body);
    }
    static function getOficioCesado($id_oficio){
        return \DB::select("SELECT * FROM `oficios_cesados` WHERE `id_oficio` = $id_oficio");
    }
    static function delOficioCesado($id_oficio){
        return \DB::delete("DELETE FROM oficios_cesados WHERE id_oficio = $id_oficio");
    }

}
