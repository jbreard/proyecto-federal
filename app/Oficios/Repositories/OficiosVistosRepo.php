<?php


namespace Oficios\Repositories;

use Auth;
use Oficios\Entities\OficiosVistos;

class OficiosVistosRepo extends BaseRepo {

    public function getModel()
    {
        return new OficiosVistos();
    }

    public function newOficioVisto($idOficio,$marcado = 0){

        $oficio = new OficiosVistos();
        $oficio->id_oficio   = $idOficio;
        $oficio->id_usuario  = Auth::user()->id;
        $oficio->marco_leido = $marcado;
        return $oficio;
    }

    public function getOficiosLeidos($idOficio){

        return $this->where('id_oficio',$idOficio);
    }

    public function getOficioNoLeidoUsuario($page=0)
    {

        $oficios =  $this->model->distinct()->where('id_usuario','=', Auth::user()->id);
        $oficios = $oficios->take(15)->skip($page*15);
        $oficios = $oficios->get(['id_oficio'])->toArray();

        $a = [];
        foreach ($oficios as $oficio){
            $a[] = $oficio["id_oficio"];
        }
        return $a;

    }




}
