<?php
namespace Oficios\Repositories;
use Carbon\Carbon;
use Oficios\Entities\Oficios;

class AlertasTempranasRepo extends BaseRepo{

	private $created_at;

    public function getModel()
    {
        return new Oficios();
    }


    public function getIdWithOutFile(){
    	$alertas = [];
    	
    	$alertas["vencer"] = [];
    	
    	$alertas["notificar"] = [];

    	$model = $this->getModel();

    	$model = $model->where('alerta_temprana','=',1)->get();
    	
    	foreach($model as $m){
    		if(empty($m->archivos)){
    			
    			$this->created_at = $m->created_at;
    			
    			$weeks = $this->checkTime();
    			
    			$keyArray = $this->keyArray($weeks,$m->notificado);
    			
    			if($keyArray !=false) $alertas[$keyArray][] = $m->id;
    		}
    	}

    	return $alertas;
    }


    private function checkTime(){

    	$weeks =  \Carbon::createFromFormat('d/m/Y H:i:s',$this->created_at)->diffInWeeks(Carbon::now());
    	return $weeks;
    }

    private function keyArray($weeks,$notificado){

    	if(($weeks == 3) and ($notificado != 1)) return "notificar";

    	if($weeks >=4) return "vencer";

    	return false;
    }

    public function getByIds(Array $id = []){

    	return $this->model->whereIn('id',$id)->get();
    
    }

    public function setNotificado(){
        return $this->model->notificado = 1;
    }

    



}