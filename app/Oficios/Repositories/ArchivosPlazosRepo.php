<?php
namespace Oficios\Repositories;
use Oficios\Entities\ArchivosPlazos;
use Oficios\Interfaces\ArchivosRepoInterface;

class ArchivosPlazosRepo extends BaseRepo implements ArchivosRepoInterface {

    public function getModel()
    {
        return new ArchivosPlazos();
    }

    public function newArchivo($idPlazo, $path)
    {
        $model = $this->getModel();
        $model->id_plazo = $idPlazo;
        $model->path  = $path;
        return $model;
    }
}