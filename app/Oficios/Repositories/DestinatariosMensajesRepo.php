<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 27/10/14
 * Time: 15:40
 */

namespace Oficios\Repositories;


//use Combos\Repositories\BaseRepo;
use Oficios\Entities\DestinatariosMensajes;

class DestinatariosMensajesRepo extends BaseRepo
{

    public function getModel()
    {
        return new DestinatariosMensajes();
    }

    public function newDestMen($idMensaje,$idOrganismo){
        $men = $this->getModel();
        $men->id_mensaje = $idMensaje;
        $men->id_organismo  = $idOrganismo;
        return $men;
    }


    public function getNoLeidos(){

        $men = $this->getModel();
        $table = $men->getTable();
        $mensajesLeidosTable = "mensajes_vistos";
        $cantidad   = 0;
        $mail = \Auth::user()->getMail;

        if(!empty($mail))
        {
            $cantidad = \DB::table($table)
                ->leftJoin($mensajesLeidosTable ,$table.".id_mensaje","=",$mensajesLeidosTable .".id_mensaje")
                ->where($mensajesLeidosTable .'.id_mensaje','=',null)
                ->where($table.".id_correo","=",$mail->id)
                ->count();


        };

        if($cantidad>0) return "!";

        return null;

    }

    public function getCantidadSinLeer($idOrganismo){
        return $this->model->where('id_organismo','=',$idOrganismo)->where('leido','=',0)->count();
    }

    public function getByMensajeOrg(&$data){
        $id = $data['id'];
        $idOrganismo = $data['id_organismo'];
        
        return $this->model->where('id_mensaje','=',$id)
                ->where('id_organismo','=',$idOrganismo)
                ->where('leido','=',0)
                ->first();
    }




}