<?php


namespace Oficios\Repositories;

use Oficios\Entities\Oficios;


class OficiosAutorizadosRepo extends BaseRepo {

    public function getModel()
    {
        return new Oficios();
    }

    public function getOficiosEstado2()
    {
        $model = $this->getModel();

        $datos =
         $model
            ->where('estado','=','1')
            ->where('tipo_oficio','=','3')
            ->where('plazo_autorizado','=','0')
            ->orWhere('plazo_autorizado','=','1')
            ->orWhere('plazo_autorizado','=','2');



        $datos = $datos->get();




        return $datos;
    }


}
