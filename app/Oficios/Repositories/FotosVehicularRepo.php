<?php

namespace Oficios\Repositories;

use Oficios\Entities\FotosVehicular;
use Oficios\Interfaces\ArchivosRepoInterface;

class FotosVehicularRepo extends BaseRepo implements ArchivosRepoInterface{

    public function getModel()
    {
        return new FotosVehicular();
    }

    public function newArchivo($idPadre, $path)
    {
        $model = $this->getModel();
        $model->id_oficio = $idPadre;
        $model->path      = $path;
        return $model;

    }
}