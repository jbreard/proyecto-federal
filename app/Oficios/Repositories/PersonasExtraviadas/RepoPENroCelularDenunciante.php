<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 15/02/16
 * Time: 16:37
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_NroCelularDenunciante;

class RepoPENroCelularDenunciante {

    var $data = [];

    function __construct($data)
    {
        $this->data = $data;
    }

    function getModel()
    {
        return new PE_NroCelularDenunciante();
    }


    function save($id_oficio)
    {
        $data = $this->data;
        foreach($data as $celular)
        {
            $this->getModel()->insert(
                [
                    'id_oficio' => $id_oficio,
                    'cod_area' => $celular->cod_area,
                    'telefono' => $celular->telefono
                ]
            );
        }
    }

    function delete($id_oficio)
    {
        $this->getModel()->where('id_oficio',$id_oficio)->delete();
    }


} 