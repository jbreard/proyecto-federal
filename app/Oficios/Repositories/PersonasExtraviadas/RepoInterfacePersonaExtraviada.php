<?php namespace Oficios\Repositories\PersonasExtraviadas;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/01/16
 * Time: 15:06
 */
interface RepoInterfacePersonaExtraviada{

    public function __construct($data = []);

    public function guardar();

    public function validarDatos();

    public function darManager();

    public function getModel();

    public function newModel();




}