<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 26/01/16
 * Time: 16:35
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\Oficios;
use Oficios\Managers\PersonasExtraviadas\ManagerPEOficio;

class RepoPEAutoridadJudicial extends BaseInterface implements RepoInterfacePersonaExtraviada {

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function prepareData($data)
    {
        if($this->data['fecha_oficio'] != "__/__/____")
            $data['fecha_oficio'] = \Time::FormatearToMysql($this->data['fecha_oficio']);
        else
            $data['fecha_oficio'] = \Time::FormatearToMysql("0000-00-00");

        $data['fecha_oficio2'] = null;

        $data['tipo_oficio'] = 1;
        if($data['id_oficio'] == "")
        {
            $data['confirmado'] = 1;
            $data['id_usuario'] = \Auth::user()->id;
        }


        if($data['id_oficio'])
        {
            $this->guardarArchivo($data['archivo1'],$data['id_oficio']);
            $this->guardarArchivo($data['archivo2'],$data['id_oficio']);
        }

        return $data;
    }

    public function postSave($id,$data)
    {
//        dd($data);
        $data['id_oficio'] = $id;
        if(isset($data['nombres']))
        {
            $data['fecha_nacimiento'] = \Time::FormatearToMysql($data['fecha_nacimiento']);
            $repoPersonaBuscada = new RepoPEPersonaBuscada();
            $repoPersonaBuscada->saveNombreApellido($data);
        }
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEOficio($model,$data);
//        dd($manager);
        return $manager;
    }

    public function getModel()
    {
        return new Oficios();
    }



}