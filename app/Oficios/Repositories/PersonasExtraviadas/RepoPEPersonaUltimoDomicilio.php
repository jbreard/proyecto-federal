<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/01/16
 * Time: 15:16
 */
namespace Oficios\Repositories\PersonasExtraviadas;
use Oficios\Entities\PersonasExtraviadas\PE_PersonaUltimoDomicilio;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaUltimoDomicilio;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaUltimoDomicilio extends BaseInterface implements RepoInterfacePersonaExtraviada {

    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function getModel()
    {
        return new PE_PersonaUltimoDomicilio();
    }

    public function prepareData($data)
    {
        return $data;
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaUltimoDomicilio($model,$data);
        return $manager;

    }


}