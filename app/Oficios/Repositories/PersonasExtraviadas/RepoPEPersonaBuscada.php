<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/12/15
 * Time: 11:16
 */
namespace Oficios\Repositories\PersonasExtraviadas;
use Oficios\Entities\PersonasExtraviadas\PE_PersonaBuscada;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaBuscada;
use Oficios\Repositories\BaseRepo;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaBuscada extends BaseInterface implements RepoInterfacePersonaExtraviada {

    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function getModel()
    {
        return new PE_PersonaBuscada();
    }

    public function getRepoPersona()
    {
        return new PersonasRepo();
    }

    public function prepareData($data)
    {
        $data['fecha_nacimiento'] = \Time::FormatearToMysql($this->data['fecha_nacimiento']);
        $data['id_persona'] = trim($this->getRepoPersona()->guardarPersona($this->data['id_persona'],$this->data['id_oficio'],$this->data['nombres'],$this->data['apellido'],$this->data['nro_documento'],3,$data['id_nacionalidad'],$data['sexo'],$data['genero'],$data['id_tipo_documento']));
        $repoCelular = new RepoPENroCelular(json_decode($data['hidden_telefonos']));
        $repoCelular->delete($data['id_oficio']);
        $repoCelular->save($data['id_oficio']);
        return $data;
    }

    public function darManager()
    {
//        dd($this->data);
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaBuscada($model,$data);
        return $manager;

    }

    public function saveNombreApellido($data)
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$data['id_oficio']]);
        $manager = new ManagerPEPersonaBuscada($model,$data);
        $manager->save();
        $this->getRepoPersona()->guardarPersona("",$data['id_oficio'],$data['nombres'],$data['apellido'],"",3);
    }

    public function getEstadisticas($data)
    {
        $model = $this->getModel();
        $model = $model->join('pe_ultimos_domicilios','pe_ultimos_domicilios.id_oficio','=','pe_personas_buscadas.id_oficio');
        $model = $model->join('oficios','oficios.id','=','pe_personas_buscadas.id_oficio');

//        dd($data);

        if($data['id_provincia'] != '')
        $model = $model->where('id_provincia',$data['id_provincia']);

        if(isset($data['id_partido']))
            $model = $model->where('id_partido',$data['id_partido']);

        if($data['sexo'] != '')
            $model = $model->where('sexo',$data['sexo']);

        if($data['fecha_desde_carga'])
            $model = $model->whereDate('oficios.created_at','>=',\Time::FormatearToMysql($data['fecha_desde_carga']));

        if($data['fecha_hasta_carga'])
            $model = $model->whereDate('oficios.created_at','<=',\Time::FormatearToMysql($data['fecha_hasta_carga']));

        if($data['vigentes'] != "")
            $model = $model->where('oficios.estado',$data['vigentes']);


        if($data['edad_desde'])
            $model = $model->where('pe_personas_buscadas.edad','>=',$data['edad_desde']);

        if($data['edad_hasta'])
            $model = $model->where('pe_personas_buscadas.edad','<=',$data['edad_hasta']);


        return $model->count();
    }


}