<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/02/16
 * Time: 16:53
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_NroCelular;

class RepoPENroCelular {

    var $data = [];

    function __construct($data)
    {
        $this->data = $data;
    }

    function getModel()
    {
        return new PE_NroCelular();
    }


    function save($id_oficio)
    {
        $data = $this->data;
        foreach($data as $celular)
        {
            $this->getModel()->insert(
                [
                 'id_oficio' => $id_oficio,
                 'cod_area' => $celular->cod_area,
                 'telefono' => $celular->telefono,
                 'id_compania' => $celular->id_compania
                ]
            );
        }
    }

    function delete($id_oficio)
    {
        $this->getModel()->where('id_oficio',$id_oficio)->delete();
    }


} 