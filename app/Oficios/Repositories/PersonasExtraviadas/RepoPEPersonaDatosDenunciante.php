<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 21/01/16
 * Time: 17:01
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_PersonaDenunciante;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaDenunciante;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaDatosDenunciante extends BaseInterface implements RepoInterfacePersonaExtraviada
{
    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function getModel()
    {
        return new PE_PersonaDenunciante();
    }

    public function getRepoPersona()
    {
        return new PersonasRepo();
    }

    public function prepareData($data)
    {
//        dd($data);
        $data['fecha_denuncia'] = \Time::FormatearToMysql($this->data['fecha_denuncia']);
        $repoCelularDenunciante = new RepoPENroCelularDenunciante(json_decode($data['hidden_telefonos_denunciante']));
        $repoCelularDenunciante->delete($data['id_oficio']);
        $repoCelularDenunciante->save($data['id_oficio']);
        trim($this->getRepoPersona()->guardarPersona($this->data['id_persona_denunciante'],$this->data['id_oficio'],$this->data['nombre'],$this->data['apellido'],$this->data['nro_documento'],4));
        $archivo = $data["nota_adjunta"];
        $path = "archivos/".$data['id_oficio']."/denunciante/";
        if ($archivo) {
            $random_name = str_random(20);
            $array = explode('.',$archivo->getClientOriginalName());
            $extension = end($array);
            $archivo->move($path,$random_name.".".$extension);

            $data['nota_adjunta'] = $path.$random_name.".".$extension;
        }


        return $data;
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaDenunciante($model,$data);
        return $manager;

    }
} 