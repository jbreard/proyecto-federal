<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 18/04/17
 * Time: 11:02
 */
namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_BusquedasXAvances;

class RepoPEPersonaDatosBusquedaXAvance {

    var $data = [];

    function getModel()
    {
        return new PE_BusquedasXAvances();
    }

    function addAvance($id_oficio,$nro_registro,$id_usuario,$detalle)
    {
        $this->getModel()->insert(
            [
                'id_oficio' => $id_oficio,
                'nro_registro' => $nro_registro,
                'id_usuario' => $id_usuario,
                'detalle' => $detalle,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]
        );
    }

    function getMaxNroRegistro($id_oficio)
    {
        if($nro_registro = $this->getModel()->where('id_oficio',$id_oficio)->max('nro_registro'))
            return $nro_registro + 1;
        else
            return 1;
    }


}