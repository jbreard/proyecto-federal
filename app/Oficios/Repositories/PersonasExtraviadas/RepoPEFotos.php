<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 25/02/16
 * Time: 15:58
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_Fotos;
use Oficios\Managers\PersonasExtraviadas\ManagerPEFotos;

class RepoPEFotos extends BaseInterface implements RepoInterfacePersonaExtraviada {
    var $data = [];

    function __construct($data = [])
    {
        $this->data = $data;
    }

    function getModel()
    {
        return new PE_Fotos();
    }

    public function prepareData($data)
    {
//        dd($data);
        $archivos = $data['foto']['buscado'];
        //$path = "archivos/".$data['id_oficio']."/fotos/";
        $id_oficio = str_pad($data['id_oficio'],10,'0',STR_PAD_LEFT);
        $path   = Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/fotos/'.$id_oficio;

        foreach($archivos as $archivo)
        {
//            dd($archivo);
//            $data['nota_adjunta'] = $path.$archivo->getClientOriginalName();
            if ($archivo) {
                $archivo->move($path,$archivo->getClientOriginalName());
            }

        }


        return $data;
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEFotos($model,$data);
        return $manager;

    }

} 