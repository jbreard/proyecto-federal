<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 26/02/16
 * Time: 16:14
 */
namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_Autorizaciones;
use Oficios\Entities\PersonasExtraviadas\PE_Fotos;
use Oficios\Managers\PersonasExtraviadas\ManagerPEAutorizaciones;

class RepoPEAutorizaciones extends BaseInterface implements RepoInterfacePersonaExtraviada {
    var $data = [];

    function __construct($data = [])
    {
        $this->data = $data;
    }

    function getModel()
    {
        return new PE_Autorizaciones();
    }

    public function prepareData($data)
    {
        $archivos = $data['foto']['buscado'];
        //$path = "archivos/".$data['id_oficio']."/fotos/";
        $this->id_oficio = $id = str_pad($data['id_oficio'],10,'0',STR_PAD_LEFT);
         
        $path = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/'.$this->id_oficio.'/fotos';
        foreach($archivos as $archivo)
        {
            if ($archivo) {
                $foto = new PE_Fotos();
                $foto->id_oficio = $this->id_oficio;
                $foto->id_usuario = \Auth::user()->id;
                $random_name = str_random(20);
                $array = explode('.',$archivo->getClientOriginalName());
                $extension = end($array);
                $foto->save();
                $path = $path.$foto->id.'/';
                $foto->path = $path.$foto->id.'/'.$random_name.".".$extension;
                $foto->save();
                $archivo->move($path,$random_name.".".$extension);
            }

        }

        $archivos = $data['foto']['autorizacion'];
        //$path = "archivos/".$data['id_oficio']."/autorizacion/";
//        $this->
        foreach($archivos as $archivo)
        {
            if ($archivo) {
//                $data['path'] = $path.$archivo->getClientOriginalName();
//                $archivo->move($path,$archivo->getClientOriginalName());
                $foto = new PE_Autorizaciones();
                
               
                $foto->where('id_oficio',$data['id_oficio'])->delete();
                $foto->id_oficio = $data['id_oficio'];
                $foto->id_usuario = \Auth::user()->id;
                $name = Date('Ymd').'_'.$foto->id_oficio;
                //$name = str_random(20);
                $array = explode('.',$archivo->getClientOriginalName());
                // dd($array);
                $extension = end($array);
                $path_foto = str_pad($foto->id_oficio,10,'0',STR_PAD_LEFT);
                $foto->path = $path.'/'.$path_foto.'/'.$name.".".$extension;
                $foto->save();
                $archivo->move($path,$name.".".$extension);

            }

        }

        $data['id_usuario'] = \Auth::user()->id;


        return $data;
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEAutorizaciones($model,$data);
        return $manager;

    }

}