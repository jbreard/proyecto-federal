<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/01/16
 * Time: 15:37
 */
namespace Oficios\Repositories\PersonasExtraviadas;

use Oficios\Entities\Archivos;

abstract class BaseInterface{

    public function newModel(){
        $model = $this->getModel();
        return $model;
    }

    public function setData($data = [])
    {
        $this->data = $data;
    }

    public function ValidarDatos()
    {
        $manager = $this->darManager($this->data);
        $isValid = $manager->isValid();
        if(!$isValid)
            return $manager->mostrarErrores($isValid);

        return $manager->isValid();
    }

    public function guardar()
    {
        $data = $this->prepareData($this->data);
        $this->setData($data);
        $manager = $this->darManager();
//        dd($manager);
        $manager->save();
        $id = $manager->getEntity()->id;
        $this->postSave($id,$this->data);
        return $id;
    }

    public function postSave($id,$data){}

    public function guardarArchivo($archivo,$id_oficio)
    {
        if ($archivo)
        {
            $random_name = str_random(20);
            $array = explode('.',$archivo->getClientOriginalName());
            $extension = end($array);
            $id_oficio = str_pad($id_oficio,10,'0',STR_PAD_LEFT);
            $path   = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/plazos/'.$id_oficio;
            //$path = "archivos/".$id_oficio;
            
            $archivo->move($path,$random_name.".".$extension);
            $data = array("id_usuario" => \Auth::user()->id, "id_oficio" => $id_oficio, "path" => $path."/".$random_name.".".$extension);
            $archivo = new Archivos();
            $archivo->fill($data);
            $archivo->save();
        }
    }



}