<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 14/01/16
 * Time: 15:42
 */
namespace Oficios\Repositories\PersonasExtraviadas;
use Oficios\Entities\PersonasExtraviadas\PE_PersonaDatosLaborales;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaDatosLaborales;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaDatosLaborales extends BaseInterface implements RepoInterfacePersonaExtraviada {

    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function getModel()
    {
        return new PE_PersonaDatosLaborales();
    }

    public function prepareData($data)
    {
        return $data;
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaDatosLaborales($model,$data);
        return $manager;

    }


}