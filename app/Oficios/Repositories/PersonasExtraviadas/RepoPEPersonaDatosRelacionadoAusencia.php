<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 15/01/16
 * Time: 11:40
 */
namespace Oficios\Repositories\PersonasExtraviadas;
use Oficios\Entities\PersonasExtraviadas\PE_PersonaDatosRelacionadoAusencia;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaDatosRelacionadoAusencia;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaDatosRelacionadoAusencia extends BaseInterface implements RepoInterfacePersonaExtraviada {

    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function prepareData($data)
    {
//        dd($data);
        if($data['fecha_ultima_vez'] != "")
            $data['fecha_ultima_vez'] = \Time::FormatearToMysql($this->data['fecha_ultima_vez']);
        else
            unset($data['fecha_ultima_vez']);

        if($data['hora_ultima_vez'] == "")
            unset($data['hora_ultima_vez']);

        if(isset($this->data['fecha_tratamiento_institucion']))
            $data['fecha_tratamiento_institucion'] = \Time::FormatearToMysql($this->data['fecha_tratamiento_institucion']);
        if(isset($this->data['fecha_tratamiento_padecimiento_mental']))
            $data['fecha_tratamiento_padecimiento_mental'] = \Time::FormatearToMysql($this->data['fecha_tratamiento_padecimiento_mental']);
        return $data;
    }

    public function getModel()
    {
        return new PE_PersonaDatosRelacionadoAusencia();
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaDatosRelacionadoAusencia($model,$data);
        return $manager;

    }


}