<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/01/16
 * Time: 16:38
 */
namespace Oficios\Repositories\PersonasExtraviadas;
use Combos\Entities\ComboSenasParticulares;
use Functions;
use Oficios\Entities\PersonasExtraviadas\PE_PersonaDescripcionFisica;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaDescripcionFisica;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaDescripcionFisica extends BaseInterface implements RepoInterfacePersonaExtraviada {

    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function prepareData($data)
    {
        $data['id_senias_particulares'] = $this->darIdSeniaParticular($data);


        return $data;
    }

    public function getModel()
    {
        return new PE_PersonaDescripcionFisica();
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaDescripcionFisica($model,$data);
        return $manager;

    }

    private function darIdSeniaParticular($data)
    {
        $cantidad_senias = ComboSenasParticulares::max('id_senas_particulares');
        $id_senia_particular = "";
        for($i = 1;$i <= $cantidad_senias;$i++)
        {
            if(isset($data['chk_'.$i]))
                $id_senia_particular .= Functions::darComa($id_senia_particular).$i;
        }
        return $id_senia_particular;
    }




}