<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 17/04/17
 * Time: 11:49
 */

namespace Oficios\Repositories\PersonasExtraviadas;


use Oficios\Entities\PersonasExtraviadas\PE_Busqueda;
use Oficios\Managers\PersonasExtraviadas\ManagerPEPersonaDatosBusqueda;
use Oficios\Repositories\PersonasRepo;

class RepoPEPersonaDatosBusqueda extends BaseInterface implements RepoInterfacePersonaExtraviada
{
    var $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        if(isset($data['id_oficio']))
            $this->data['id_oficio'] = trim($data['id_oficio']);
    }

    public function getModel()
    {
        return new PE_Busqueda();
    }

    public function getRepoPersona()
    {
        return new PersonasRepo();
    }

    public function prepareData($data)
    {
        $data['fecha_aparicion'] = \Time::FormatearToMysql($this->data['fecha_aparicion']);

        if($data['fecha_aparicion'] == "0000-00-00")
            $data['fecha_aparicion'] = null;

        if(!isset($data['menor']))
            $data['menor'] = 0;

        if(!isset($data['difusion_foto']))
            $data['difusion_foto'] = 0;

        if(!isset($data['victima_ilicito']))
        {
            $data['victima_ilicito'] = 0;
            $data['id_victima_ilicito'] = 0;
        }
//        dd($data);

        /*$data['fecha_denuncia'] = \Time::FormatearToMysql($this->data['fecha_denuncia']);
        $repoCelularDenunciante = new RepoPENroCelularDenunciante(json_decode($data['hidden_telefonos_denunciante']));
        $repoCelularDenunciante->delete($data['id_oficio']);
        $repoCelularDenunciante->save($data['id_oficio']);
        trim($this->getRepoPersona()->guardarPersona($this->data['id_persona_denunciante'],$this->data['id_oficio'],$this->data['nombre'],$this->data['apellido'],$this->data['nro_documento'],4));
        $archivo = $data["nota_adjunta"];
        $path = "archivos/".$data['id_oficio']."/denunciante/";
        if ($archivo) {
            $random_name = str_random(20);
            $array = explode('.',$archivo->getClientOriginalName());
            $extension = end($array);
            $archivo->move($path,$random_name.".".$extension);

            $data['nota_adjunta'] = $path.$random_name.".".$extension;
        }

        */
        return $data;
    }

    public function postSave($id,$data)
    {
        if($data['avances'])
        {
            $repoPersonaDatosBusquedaXAvance = new RepoPEPersonaDatosBusquedaXAvance();
            $nro_registro = $repoPersonaDatosBusquedaXAvance->getMaxNroRegistro($data['id_oficio']);
            $repoPersonaDatosBusquedaXAvance->addAvance($data['id_oficio'],$nro_registro,\Auth::user()->id,$data['avances']);
        }
    }

    public function darManager()
    {
        $model = $this->getModel()->firstOrNew(["id_oficio"=>$this->data['id_oficio']]);
        $data = $this->data;
        $manager = new ManagerPEPersonaDatosBusqueda($model,$data);
        return $manager;

    }

    public function getByIdOficio($id)
    {

    }


}