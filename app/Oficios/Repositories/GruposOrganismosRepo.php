<?php 
namespace Oficios\Repositories;
use Oficios\Entities\GrupoOrganismos;

class GruposOrganismosRepo extends BaseRepo
{
	public function getModel(){
		return new GrupoOrganismos();
	}

	public function newModel($descripcion){
		$model = $this->getModel();
		$model->descripcion = $descripcion;
		return $model;
	}

	public function getGruposWithMiembros(){
		return $this->model->whereHas('miembros',function($q){return $q;})->get();
	}

	public function test(){
		return $this->model->take(1)->get();
	}


}