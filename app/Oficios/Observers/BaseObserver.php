<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 26/06/14
 * Time: 15:56
 */

namespace Oficios\Observers;


class BaseObserver {  

    public function saving($model){}

    public function saved($model){}

    public function updated($model){}

    public function creating($model){}

    public function created($model){}

    public function deleting($model){}

    public function deleted($model){}

    public function restoring($model){}

    public function restored($model){}  
} 
