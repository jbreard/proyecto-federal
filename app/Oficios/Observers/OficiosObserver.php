<?php
namespace Oficios\Observers;
use Oficios\Colas\OficiosCola;
use Oficios\Managers\Flujos\FlujoManager;



class OficiosObserver  extends BaseObserver{

    protected $oficiosCola;
    protected $flujoManager;

    function __construct() {
        
        $this->oficiosCola = new OficiosCola();
        $this->flujoManager = new FlujoManager();
        
    }

    public function updated($model)
    {

        $original = $model->getOriginal();
        $isDirtyConfirmado = $model->isDirty('confirmado');//se guardo efectivamente
        $isDirtyEstado     = $model->isDirty('estado');//vigente/sin efecto
        
//        dd($model->tipo);
        
        $data = [
            "isDirtyEstado"=>$model->isDirty('estado'),
            "isDirtyConfirmador"=>$model->isDirty('confirmado'),
            "id"=>$model->id,
            "archivo"=>($model->archivos->first())?$model->archivos->first()->path:false,
            "ruta"=>$model->tipo->ruta,
            "isAlertaTemprana"=>($model->alerta_temprana) ? true:false,
            "tipoOficio"=> $model->tipo->descripcion
        ];

        $this->flujoManager->initFlujoOficioTransaccion($model);

        if($isDirtyConfirmado) return $this->oficiosCola->notificarNuevo($data);
        //if(!$isDirtyConfirmado and !$isDirtyEstado) return$this->oficiosCola->alertaModificacion($data);
        //if( ($isDirtyEstado )  and ($model->estado == 2)) return \Queue::push('Oficios\Colas\OficiosCola@notificarSinEfecto',$data);


        
    }


    public function created($model)
    {
        
        $this->flujoManager->initFlujoOficioTransaccion($model);

    }

}