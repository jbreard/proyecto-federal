<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 26/06/14
 * Time: 16:13
 */

namespace Oficios\Observers;

use Oficios\Managers\Flujos\FlujoManager;

class PersonasObserver  extends BaseObserver {

    protected $flujoManager;

    public function __construct()
    {
        $this->flujoManager = new FlujoManager();
    }


    public function updated($model) {

        $this->flujoManager->initFlujoPersonaTransaccion($model);
        
    }

    public function created($model)
    {

        #no se utiliza por el momento
        #$this->flujoManager->initFlujoPersonaTransaccion($model);

    }

}