<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 12/05/15
 * Time: 16:38
 */

namespace Oficios\ClasesAbstractas;


abstract class ArchivosBaseManager {
    protected $entity;
    protected $data;
    protected $errors;





    abstract public function getRules();

    public function isValid(){
        $rules        = $this->getRules();
        $validation   = \Validator::make($this->data,$rules);
        $isValid      = $validation->passes();
        $this->errors = $validation->messages();

        return $isValid;
    }

    public function getErrors(){

        return $this->errors;

    }


    public function save(){

        if(!$this->isValid()) return false;
        $this->entity->fill($this->data);
        $this->entity->save();
        return true;

    }




    public function setEntity($entity)
    {
        $this->entity = $entity;
    }


    public function setData($data)
    {
        $this->data = $data;
    }




}