<?php

namespace Oficios\ClasesAbstractas;

use \Eloquent;

abstract class BusquedaAbstracta extends Eloquent {

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }

    public function getTable(){

        return $this->table;
    }

    abstract function getFields();

    abstract function relations();

    abstract function relationsPivot();

    public function getPk(){

        return $this->primaryKey;
    }




}