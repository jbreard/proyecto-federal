<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 12/05/15
 * Time: 16:52
 */

namespace Oficios\Interfaces;


interface ArchivosRepoInterface {

    public function newArchivo($idPadre,$path);

}