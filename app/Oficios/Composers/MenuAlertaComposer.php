<?php namespace  Oficios\Composers;

use Oficios\Entities\TiposOficios;
use Oficios\Repositories\TiposOficiosRepo;

class MenuAlertaComposer   {

    protected $oficiosRepo;

    public function __construct(TiposOficiosRepo $oficiosRepo){
        $this->oficiosRepo = $oficiosRepo;
    }

    public function compose($view)
    {



       $menu = $this->oficiosRepo->getAlertas();



       $view->with('menusAlertas',$menu);




    }


}
