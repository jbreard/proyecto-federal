<?php namespace  Oficios\Composers;

use Oficios\Entities\TiposOficios;
use Oficios\Repositories\TiposOficiosRepo;

class MenuComposer   {

    protected $oficiosRepo;

    public function __construct(TiposOficiosRepo $oficiosRepo){
        $this->oficiosRepo = $oficiosRepo;
    }

    public function compose($view)
    {



       $menu = $this->oficiosRepo->getByOrder();



       $view->with('menus',$menu);




    }


}
