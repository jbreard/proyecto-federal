<?php namespace Oficios\Composers;

use Illuminate\Support\ServiceProvider;



class ComposerServiceProvider extends ServiceProvider{

    public function register(){

        $this->app->view->composer('menu.oficios','Oficios\Composers\MenuComposer');
        $this->app->view->composer('menu.oficios_inicio','Oficios\Composers\MenuComposer');
        $this->app->view->composer('menu.notificaciones','Oficios\Composers\MenuComposer');
        $this->app->view->composer('menu.alertas_tempranas','Oficios\Composers\MenuAlertaComposer');
        $this->app->view->composer('menu.alertas_tempranas_menu','Oficios\Composers\MenuComposer');

    }

}
