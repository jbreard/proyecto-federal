<?php
namespace Oficios\WebSockets;
use Ratchet\Server\IoServer;

class RunWebSocket{

    private $server;
    private $port = 8080;
    private function setServer(){
      $this->server = IoServer::factory(
          new OficiosWebSocket(),
          $this->port
      );
    }

    public function run(){
      $this->setServer();
      $this->server->run();
      echo "Run server in port".$this->port;
    }


}
