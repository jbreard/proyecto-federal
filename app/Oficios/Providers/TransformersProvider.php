<?php
namespace Oficios\Providers;
use Illuminate\Support\ServiceProvider;
use App;


class TransformersProvider extends ServiceProvider {
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
          $this->app->bind('Transformers',function(){
            return App::make('Oficios\Helpers\Transformers');
        });
    }
}