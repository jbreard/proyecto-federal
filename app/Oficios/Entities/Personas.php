<?php namespace Oficios\Entities;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class Personas extends \Eloquent
{
    use SoftDeletingTrait;
    use RevisionableTrait;

    protected $table = 'personas';

    protected $fillable = [
                        "id_sexo","id_genero", "id_nacionalidad",
                        "id_tipo_documento", "id_ultimo_partido",
                        "id_provincia","id_provincia_laboral","id_partido_laboral","id_localidad_laboral", "nombre", "apellido",
                        "descripcion_persona", "edad", "nro_documento",
                        "apodos", "ultimo_calle",
                        "ultimo_numero", "ultimo_piso", "ultimo_departamento","departamento_laboral",
                        "ultima_localidad", "ultimo_codigo_postal", "ultima_provincia",
                        "ocupacion", "empleador", "cuit", "domicilio_laboral", "telefono_laboral",'codigo_postal_laboral',
                        "provincia_laboral", "calle_laboral", "numero_laboral", "piso_laboral", "observaciones",
                        "telefono", "vinculo_victima","apellido_materno","id_compania_celular","numero_celular","nro_tarjeta_credito","bco_tarjeta_credito"
                        ,"codArea_celular","observaciones_persona","interseccion","observaciones_domicilio" ];


    public  function guardar($idOficio,$idTipoPersona,&$data){
        $this->id_oficio = $idOficio;
        $this->id_tipo_persona = $idTipoPersona;

        $this->fill($data);
        $this->save();


    }


    public function getTipoPersona(){

        return $this->hasOne('Oficios\Entities\TipoPersona','id','id_tipo_persona');

    }


    public function sexo()
    {
        return $this->hasOne('Oficios\Entities\Sexo','id','id_sexo');
    }

    public function genero()
    {
        return $this->hasOne('Oficios\Entities\Genero','id','id_genero');
    }

    public function tipoDocumento()
    {
        return $this->hasOne('Oficios\Entities\TipoDocumento','id','id_tipo_documento');
    }

    public function nacionalidad()
    {
        return $this->hasOne('Oficios\Entities\Nacionalidad','id','id_nacionalidad');
    }


    public function getSecuestroVehicular(){

        return $this->hasOne('Oficios\Entities\PedidosSecuestroVehicular','id_persona','id');


    }


    public function getSecuestroArmas(){

        return $this->hasOne('Oficios\Entities\SecuestroArmas','id_persona','id');


    }
    public function getOtrosElementos(){

        return $this->hasOne('Oficios\Entities\SecuestroElementos','id_persona','id');


    }


    public function provincia()
    {
        return $this->hasOne('Oficios\Entities\Provincia','id','ultima_provincia');
    }

    public function provinciaLaboral()
    {
        return $this->hasOne('Oficios\Entities\Provincia','id','id_provincia_laboral');
    }

    public function ultimaProvincia()
    {
        return $this->hasOne('Oficios\Entities\Provincia','id','ultima_provincia');
    }

    public function ultimaLocalidad()
    {
        return $this->hasOne('Oficios\Entities\Localidad','id','ultima_localidad');
    }

    public function ultimoPartido()
    {
        return $this->hasOne('Oficios\Entities\Partido','id','id_ultimo_partido');
    }

    public function companiaCelular(){
        return $this->hasOne('Oficios\Entities\CompaniaCelular','id','id_compania_celular');
    }

    public function oficio()
    {
        return $this->belongsTo('Oficios\Entities\Oficios','id_oficio','id');
    }
    public function  getFotos(){

        return $this->hasMany('Oficios\Entities\Fotos','id_persona','id');
    }

    public function setEdadAttribute($value)
    {
        $this->attributes['edad'] = \Time::FormatearToMysql($value);
    }





    public function getEdadAttribute($value)
    {

         if($value == "0000-00-00"){
            return null;

        }


        return \Time::FormatearToNormal($value);
    }

    public function getNroDocumentoAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;
    }

    public function getCodAreaCelularAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;
    }

    public function getNumeroCelularAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;
    }


    public function scopeSearch($query, $q,$values)
    {
        $str = implode (", ", $values);
        $match = "MATCH(".$str.") AGAINST (? IN BOOLEAN MODE)";
        return $query->whereRaw($match, array($q));
    }

    public function getDocumentos(){

        return $this->hasMany('Oficios\Entities\Documentos','id_persona','id');

    }


}

?>
