<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 19:09
 */

namespace Oficios\Entities;


class SenasParticulares extends \Eloquent {
    protected $table = 'senas_particulares';
    protected $fillable = array('id_senas_particulares', 'senas_particulares');

} 