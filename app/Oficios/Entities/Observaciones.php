<?php namespace Oficios\Entities;

class Observaciones extends \Eloquent
{
    protected $table = 'observaciones';
    protected $fillable = array('id_usuario','id_oficio','nro_causa','fecha','observaciones');

    public function user()
    {
        return $this->hasOne('Usuarios\Entities\User', 'id', 'id_usuario');
    }

    public function archivos(){

        return $this->hasMany('Oficios\Entities\ArchivosObservaciones','id_observacion','id');

    }



}
?>
