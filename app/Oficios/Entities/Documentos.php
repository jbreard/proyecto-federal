<?php

namespace Oficios\Entities;


class Documentos extends \Eloquent{

    protected $table = "documentos";

    protected $fillable = ["id_tipo_documento","id_persona","nro_documento"];

    protected $primaryKey = 'id_documento';
}