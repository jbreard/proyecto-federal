<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 03/11/14
 * Time: 11:19
 */

namespace Oficios\Entities;


use Oficios\ClasesAbstractas\BusquedaAbstracta;

class RespuestasMensajes extends BusquedaAbstracta {

    protected $table = 'respuestas_mensajes';
    protected $fillable = ["respuesta"];

    public function usuario(){
        return $this->belongsTo('Usuarios\Entities\User','id_usuario','id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }


    function getFields()
    {
        return $this->fillable;
    }

    function relations()
    {
        return ['usuario'];
    }

    function relationsPivot()
    {
        // TODO: Implement relationsPivot() method.
        return [];
    }
}