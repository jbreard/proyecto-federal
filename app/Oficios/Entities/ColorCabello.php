<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 18:54
 */

namespace Oficios\Entities;


class ColorCabello extends \Eloquent {

    protected $table = 'color_cabello';
    protected $fillable = array('id_color_cabello', 'color_cabello');

} 