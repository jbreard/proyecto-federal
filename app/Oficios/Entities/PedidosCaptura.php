<?php namespace Oficios\Entities;

use Venturecraft\Revisionable\RevisionableTrait;

class PedidosCaptura extends \Eloquent
{
    use RevisionableTrait;

    public function guardar(&$data){

        $oficio           = new Oficios();
        $persona          = new Personas();
        $fotos            = new Fotos();
        $archivos         = new Archivos();
        $oficio->guardar($data,4);
        $persona->guardar($oficio->id,1,$data);
        $fotos->subirFotos($persona->id,$oficio->id,$data["foto"]);
        $archivos->subirArchivos($oficio->id,$data["archivo"]["oficio"]);

    }


}
?>