<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 12/05/15
 * Time: 12:29
 */

namespace Oficios\Entities;
use Oficios\Interfaces\ArchivosInterface;

class ArchivosObservaciones extends \Eloquent implements ArchivosInterface {

    public function setPath($idPadre=null)
    {
        $url = $_SERVER['REQUEST_URI'];
        $partes = explode('/', $url);

        $id_oficio = end($partes);
        $oficio = str_pad($id_oficio,10,'0',STR_PAD_LEFT);
     
        //$idPadre = id_observacion
        //dd('archivos/observaciones/'.$idPadre);
        $url = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/'.$oficio.'/observaciones';
        //dd($url);
        return $url;        
         //return 'archivos/observaciones/'.$idPadre;
    }



}