<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 18:53
 */

namespace Oficios\Entities;


class TipoCabello extends \Eloquent {

    protected $table = 'tipo_cabello';
    protected $fillable = array('id_tipo_cabello', 'tipo_cabello');
} 