<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 12/05/15
 * Time: 12:29
 */

namespace Oficios\Entities;
use Oficios\Interfaces\ArchivosInterface;

class ArchivosPlazos extends \Eloquent implements ArchivosInterface {

    protected $table = "archivos_plazos";

    public function setPath($idPadre=null)
    {
        $path   = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/plazos/'.$idPadre;
        return  $path;
        //return 'archivos/plazos/'.$idPadre;
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }
}