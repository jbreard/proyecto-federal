<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 21/01/16
 * Time: 14:10
 */

namespace Oficios\Entities\PersonasExtraviadas;


use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaDenunciante extends \Eloquent {
  //  use SoftDeletingTrait;

    use RevisionableTrait;
    protected $fillable = ["id_oficio","apellido","nombre","id_tipo_documento","nro_documento",
        "vinculo","direccion","nro","piso","dpto","codigo_postal","id_provincia","id_partido",
        "cod_area_telefono","telefono","id_compania_telefono","dependencia","fecha_denuncia",
        "relato","nota_adjunta","intervencion_otros_organismos","created_at","updated_at","deleted_at"];

    protected $dates = ['deleted_at'];

    protected $table = "pe_datos_denunciante";
    protected $primaryKey = "id_oficio";

    public function getFechaDenunciaAttribute()
    {
        if($this->attributes['fecha_denuncia'] != "0000-00-00")
            return \Time::FormatearToNormal($this->attributes['fecha_denuncia']);
        else
            return "";
    }

    public function provincia(){
        return $this->hasOne("Oficios\Entities\Provincia",'id','id_provincia');
    }

    public function partido(){
        return $this->hasOne("Oficios\Entities\Partido",'id','id_partido');
    }

    public function tipoDocumento(){
        return $this->hasOne("Oficios\Entities\TipoDocumento",'id','id_tipo_documento');
    }

    public function objVinculo(){
        return $this->hasOne("Oficios\Entities\Vinculo",'id_vinculo','vinculo');
    }

} 