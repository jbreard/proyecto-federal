<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/04/17
 * Time: 11:43
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Venturecraft\Revisionable\RevisionableTrait;

class PE_BusquedasXAvances extends \Eloquent
{

    use RevisionableTrait;
    protected $fillable = ["id_oficio", "nro_registro", "detelle", "id_usuario", "created_at", "updated_at"];

    protected $table = "pe_busquedasxavances";
    protected $primaryKey = "id_oficio";

    public function usuario()
    {
        return $this->hasOne('Usuarios\Entities\User', 'id', 'id_usuario');
    }
}