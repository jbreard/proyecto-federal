<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/01/16
 * Time: 16:23
 */

namespace Oficios\Entities\PersonasExtraviadas;

use Combos\Entities\ComboSenasParticulares;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaDescripcionFisica extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_oficio","id_contextura_fisica","altura","peso","id_tipo_cabello","largo_cabello","id_color_cabello",
        "id_color_ojos","id_tez","id_senias_particulares","detalle_senias",
        "observaciones","created_at","updated_at"];

    protected $table = "pe_descripciones_fisicas";
    protected $primaryKey = "id_oficio";


    public function largoCabello(){
        return $this->hasOne("Oficios\Entities\LargoCabello",'id_largo_cabello','largo_cabello');
    }

    public function contexturaFisica(){
        return $this->hasOne("Oficios\Entities\ContexturaFisica",'id_contextura_fisica','id_contextura_fisica');
    }

    public function tipoCabello(){
        return $this->hasOne("Oficios\Entities\TipoCabello",'id_tipo_cabello','id_tipo_cabello');
    }

    public function colorCabello(){
        return $this->hasOne("Oficios\Entities\ColorCabello",'id_color_cabello','id_color_cabello');
    }

    public function colorOjos(){
        return $this->hasOne("Oficios\Entities\ColorOjo",'id_color_ojos','id_color_ojos');
    }

    public function tez(){
        return $this->hasOne("Oficios\Entities\Tez",'id_tez','id_tez');
    }

    /*public function senasParticulares(){
        $this->setSenias();
//        return $this->hasOne("Oficios\Entities\SenasParticulares",'id_senas_particulares','id_senias_particulares');
    }*/

    public function getIdSeniasParticularesOriginalAttribute()
    {
        return $this->attributes['id_senias_particulares'];
    }


    public function getIdSeniasParticularesAttribute()
    {
        //return "Hola";
        $listado_senias = "";
        $senias = explode(",",$this->attributes['id_senias_particulares']);
        foreach($senias as $senia)
        {
            //dd("hola");
            $senia = ComboSenasParticulares::find($senia);
            if(is_object($senia))
                $listado_senias .= \Functions::darComaConEspacio($listado_senias).$senia->senas_particulares;
        }
        //dd($listado_senias);
        return $listado_senias;
    }


}