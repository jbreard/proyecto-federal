<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 14/01/16
 * Time: 16:19
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaDatosRelacionadoAusencia extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_oficio","fecha_ultima_vez","hora_ultima_vez","lugar_ultima_vez","descripcion_vestimenta",
        "detalle_drogas","adiccion_alcohol","tratamiento_institucion","fecha_tratamiento_institucion","padecimiento_mental","padecimiento_institucion","fecha_tratamiento_padecimiento_mental"
        ,"internacion_estadia_voluntaria","id_institucion_momento_ausencia","institucion_momento_ausencia","internacion_estadia_voluntaria","autoridad_ordeno_medida",
        "direccion","nro","piso","dpto","codigo_postal","id_provincia","id_partido","localidad","situacion_calle","lugares_frecuentaba",
        "especifique_enfermedad","especifique_tratamiento_medico","especifique_alguna_medicacion","detalle_enfermedad",
        "especifique_situacion_conflicto","sustraccion_parental","primera_vez_ausenta","especifique_documentacion_consigo",
        "compania_alguien","created_at","updated_at"];

    protected $table = "pe_datos_relacionado_ausencia";
    protected $primaryKey = "id_oficio";

    public function getFechaUltimaVezAttribute()
    {
        if($this->attributes['fecha_ultima_vez'] != "0000-00-00" && $this->attributes['fecha_ultima_vez'] != null)
            return \Time::FormatearToNormal($this->attributes['fecha_ultima_vez']);
        else
            return "";
    }

    public function getFechaTratamientoInstitucionAttribute()
    {
        if($this->attributes['fecha_tratamiento_institucion'] != "0000-00-00" && $this->attributes['fecha_tratamiento_institucion'] != null && $this->attributes['fecha_tratamiento_institucion'] != "")
            return \Time::FormatearToNormal($this->attributes['fecha_tratamiento_institucion']);
        else
            return "";
    }

    public function getFechaTratamientoPadecimientoMentalAttribute()
    {
        if($this->attributes['fecha_tratamiento_padecimiento_mental'] != "0000-00-00" && $this->attributes['fecha_tratamiento_padecimiento_mental'] != null)
            return \Time::FormatearToNormal($this->attributes['fecha_tratamiento_padecimiento_mental']);
        else
            return "";
    }

    public function getHoraUltimaVezAttribute()
    {
        if($this->attributes['hora_ultima_vez'])
        {
            $hora = explode(":",$this->attributes['hora_ultima_vez']);
            return $hora[0].":".$hora[1];
        }
        return "";
    }

    public function institucion(){
        return $this->hasOne("Combos\Entities\ComboInstitucion",'id_institucion','id_institucion_momento_ausencia');
    }

    public function provincia(){
        return $this->hasOne("Oficios\Entities\Provincia",'id','id_provincia');
    }

    public function partido(){
        return $this->hasOne("Oficios\Entities\Partido",'id','id_partido');
    }

    public function adiccionAlcohol(){
        return $this->hasOne("Combos\Entities\ComboSiNo",'id','adiccion_alcohol');
    }

    public function situacionCalle(){
        return $this->hasOne("Combos\Entities\ComboSiNo",'id','situacion_calle');
    }

    public function sustraccionParental(){
        return $this->hasOne("Combos\Entities\ComboSiNo",'id','sustraccion_parental');
    }

    public function primeraVezAusenta(){
        return $this->hasOne("Combos\Entities\ComboSiNo",'id','primera_vez_ausenta');
    }

}