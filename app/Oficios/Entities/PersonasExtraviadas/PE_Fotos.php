<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 25/02/16
 * Time: 15:55
 */

namespace Oficios\Entities\PersonasExtraviadas;


use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_Fotos extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_foto","id_oficio","id_usuario","path","created_at","updated_at"];

    protected $table = "pe_fotos";
    protected $primaryKey = "id_foto";

} 