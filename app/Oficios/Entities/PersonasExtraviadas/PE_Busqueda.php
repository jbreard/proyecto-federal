<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/04/17
 * Time: 11:33
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Venturecraft\Revisionable\RevisionableTrait;

class PE_Busqueda extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_oficio","nro_caso","id_procedencia","id_estado_actual","menor","difusion_foto","observaciones","fecha_aparicion","circunstancias_aparicion","victima_ilicito","id_victima_ilicito","created_at","updated_at"];

    protected $table = "pe_busquedas";
    protected $primaryKey = "id_oficio";

    public function getFechaAparicionAttribute()
    {
        if($this->attributes['fecha_aparicion'])
            return \Time::FormatearToNormal($this->attributes['fecha_aparicion']);
        else
            return "";
    }

    public function procedencia(){
        return $this->hasOne("Oficios\Entities\Procedencia",'id','id_procedencia');
    }

    public function estado_actual(){
        return $this->hasOne("Oficios\Entities\EstadoActual",'id','id_estado_actual');
    }

    public function obj_victima_ilicito(){
        return $this->hasOne("Oficios\Entities\VictimaIlicito",'id','id_victima_ilicito');
    }
    /*public function getFechaNacimientoAttribute()
    {
        return \Time::FormatearToNormal($this->attributes['fecha_nacimiento']);
    }
    public function nacionalidad(){
        return $this->hasOne("Oficios\Entities\Nacionalidad",'id','id_nacionalidad');
    }


    public function estadoCivil(){
        return $this->hasOne("Oficios\Entities\EstadoCivil",'id_estado_civil','id_estado_civil');
    }

    public function tipoDocumento(){
        return $this->hasOne("Oficios\Entities\TipoDocumento",'id','id_tipo_documento');
    }

    public function companiaTelefono(){
        return $this->hasOne("Oficios\Entities\CompaniaCelular",'id','id_compania_telefono');
    }*/
}