<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 14/01/16
 * Time: 15:38
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaDatosLaborales extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_oficio","profesion_ocupacion","empleador","telefono","direccion","nro","piso","dpto",
        "codigo_postal","id_provincia","id_partido","localidad","created_at","updated_at"];

    protected $table = "pe_datos_laborales";
    protected $primaryKey = "id_oficio";

    public function provincia(){
        return $this->hasOne("Oficios\Entities\Provincia",'id','id_provincia');
    }

    public function partido(){
        return $this->hasOne("Oficios\Entities\Partido",'id','id_partido');
    }

}