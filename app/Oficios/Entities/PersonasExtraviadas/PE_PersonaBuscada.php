<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/12/15
 * Time: 10:58
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaBuscada extends \Eloquent {
    use SoftDeletingTrait;
    use RevisionableTrait;
    protected $fillable = ["id_oficio","apellido","nombres","sobrenombre","id_nacionalidad","fecha_nacimiento","sexo","genero",
                           "id_estado_civil","nombre_conyuge","apellido_nombre_paterno","apellido_nombre_materno",
                           "id_tipo_documento","nro_documento","autoridad_expide","cod_area_telefono","id_compania_telefono","telefono","facebook","twitter",
                           "badoo","instagram","linkedin","snapchat","otro","edad","created_at","updated_at","deleted_at"];

    protected $table = "pe_personas_buscadas";
    protected $primaryKey = "id_oficio";

    public function getFechaNacimientoAttribute()
    {
        return \Time::FormatearToNormal($this->attributes['fecha_nacimiento']);
    }
    public function nacionalidad(){
      return $this->hasOne("Oficios\Entities\Nacionalidad",'id','id_nacionalidad');
    }
    
    public function objSexo(){
      return $this->hasOne("Oficios\Entities\Sexo",'id','sexo');
    }

    public function objGenero(){
        return $this->hasOne("Oficios\Entities\Genero",'id','genero');
    }

    public function estadoCivil(){
        return $this->hasOne("Oficios\Entities\EstadoCivil",'id_estado_civil','id_estado_civil');
    }

    public function tipoDocumento(){
        return $this->hasOne("Oficios\Entities\TipoDocumento",'id','id_tipo_documento');
    }

    public function companiaTelefono(){
        return $this->hasOne("Oficios\Entities\CompaniaCelular",'id','id_compania_telefono');
    }
}