<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 15/02/16
 * Time: 16:36
 */

namespace Oficios\Entities\PersonasExtraviadas;


use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_NroCelularDenunciante extends \Eloquent {
    use SoftDeletingTrait;

    use RevisionableTrait;
    protected $fillable = ["id_celular","id_oficio","cod_area","telefono","created_at","updated_at","deleted_at"];

    protected $table = "pe_nros_celulares_denunciante";
    protected $primaryKey = "id_celular";

}