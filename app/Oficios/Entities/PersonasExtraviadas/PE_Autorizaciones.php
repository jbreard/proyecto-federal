<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 25/02/16
 * Time: 18:12
 */

namespace Oficios\Entities\PersonasExtraviadas;


use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_Autorizaciones extends \Eloquent {
    use SoftDeletingTrait;

    use RevisionableTrait;
    protected $fillable = ["id_autorizacion","id_oficio","observaciones","id_usuario","path","created_at","updated_at","deleted_at"];

    protected $table = "pe_autorizacion";
    protected $primaryKey = "id_autorizacion";
} 