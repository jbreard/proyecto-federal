<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/02/16
 * Time: 16:29
 */

namespace Oficios\Entities\PersonasExtraviadas;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_NroCelular extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_celular","id_oficio","cod_area","telefono","id_compania","created_at","updated_at"];

    protected $table = "pe_nros_celulares";
    protected $primaryKey = "id_celular";

    public function ObjCompania()
    {
        return $this->hasOne('Oficios\Entities\CompaniaCelular','id','id_compania');
    }

}