<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/01/16
 * Time: 15:13
 */
namespace Oficios\Entities\PersonasExtraviadas;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class PE_PersonaUltimoDomicilio extends \Eloquent {

    use RevisionableTrait;
    protected $fillable = ["id_oficio","direccion","nro","piso","dpto","interseccion","codigo_postal",
        "id_provincia","id_partido","localidad","observaciones","created_at","updated_at"];

    protected $table = "pe_ultimos_domicilios";
    protected $primaryKey = "id_oficio";

    public function provincia(){
        return $this->hasOne("Oficios\Entities\Provincia",'id','id_provincia');
    }

    public function partido(){
        return $this->hasOne("Oficios\Entities\Partido",'id','id_partido');
    }


}