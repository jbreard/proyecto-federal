<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 18:51
 */

namespace Oficios\Entities;


class ContexturaFisica extends \Eloquent {

    protected $table = 'contexturas_fisicas';
    protected $fillable = array('id_contextura_fisica', 'contextura_fisica');
} 