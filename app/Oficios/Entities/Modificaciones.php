<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 11/07/14
 * Time: 09:31
 */

namespace Oficios\Entities;


class Modificaciones  extends \Eloquent{

    protected $table = 'modificaciones';
    protected $fillable = [];

    public function getOficio(){
        return $this->hasOne('Oficios\Entities\Oficios','id','id_oficio');
    }

} 