<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/04/17
 * Time: 12:15
 */

namespace Oficios\Entities;
class VictimaIlicito extends \Eloquent
{
    protected $table = 'victimas_ilicitos';
    protected $fillable = array('id', 'descripcion');

}
?>