<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/04/17
 * Time: 12:14
 */
namespace Oficios\Entities;
class EstadoActual extends \Eloquent
{
    protected $table = 'estados_actuales';
    protected $fillable = array('id', 'descripcion');

}
?>