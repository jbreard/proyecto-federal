<?php namespace Oficios\Entities;
class TipoDocumento extends \Eloquent
{
    protected $table = 'tipo_doc';
    protected $fillable = array('id', 'descripcion');

}
?>