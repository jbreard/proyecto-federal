<?php 
namespace Oficios\Entities\ConsultasPositivas;

class ConsultasPositivas extends \Eloquent
{
	
	protected $table = 'consultas_positivas';

	public function destinatarios(){
		return $this->hasMany('Oficios\Entities\ConsultasPositivas\ConsultasPositivasDestinatarios','id_consulta_positiva','id');
	}

	public function oficios(){
		return $this->hasMany('Oficios\Entities\ConsultasPositivas\ConsultasPositivasOficios','id_consulta','id');
	}

	public function user(){
		return $this->hasOne('Usuarios\Entities\User','id','id_usuario');
	}

	public function getFecha(){
		return  \Time::FormatearToNormal($this->created_at);
	}

	public function getHora(){
		return  \Time::FormatearHoraMysql($this->created_at);
	}

	

}
