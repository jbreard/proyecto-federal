<?php 
namespace Oficios\Entities\ConsultasPositivas;

class ConsultasPositivasDestinatarios extends \Eloquent
{
	
	protected $table = 'destinatarios_consultas_positivas';

	public function consultas(){
		return $this->belongsTo('Oficios\Entities\ConsultasPositivas\ConsultasPositivas','id_consulta_positiva','id');
	}
	
	

}
