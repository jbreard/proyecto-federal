<?php
namespace Oficios\Entities\ConsultasPositivas;

class ConsultasPositivasOficios extends \Eloquent
{
	
	protected $table = 'consultas_positivas_oficios';

	public function oficios(){
		return $this->hasOne('Oficios\Entities\Oficios','id','id_oficio')->withTrashed();;
	}

	public function consultas(){
		return $this->belongsTo('Oficios\Entities\ConsultasPositivas\ConsultasPositivas','id_consulta','id');
	}	
}