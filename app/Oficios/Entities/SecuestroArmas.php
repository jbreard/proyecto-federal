<?php
namespace Oficios\Entities;

use Venturecraft\Revisionable\RevisionableTrait;

class SecuestroArmas extends \Eloquent
{
    use RevisionableTrait;
    protected $table = 'secuestro_armas';

    protected $fillable = ["tipo_arma","marca","modelo","calibre","matricula","observaciones_armas"];

    public function scopeSearch($query, $q,$values)
    {
        $str = implode (", ", $values);
        $match = "MATCH(".$str.") AGAINST (? IN BOOLEAN MODE)";
        return $query->whereRaw($match, array($q));
    }


}
