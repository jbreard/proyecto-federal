<?php namespace Oficios\Entities;

class SolicitudParadero extends \Eloquent
{

    protected $table = 'solicitud_paradero';

    protected $fillable = ["primera_desaparece"
                          ,"enfermedad"
                          ,"drogadiccion"
                          ,"internado_institucion"
                          ,"descripcion_caso"
                          ,"novedades"
                          ,"enfermedades"
                          ,"concurrencia"
                          ,"descipcion_persona"
                          ,"lugar_visto"
                          ,"fecha_visto","hora_visto","medicacion","vinculo_desaparecido"
                          ,"pertenencia_referencia","vestimenta","descripcion_fisica","observaciones" ];

    public function esDrogadiccion(){
        return $this->hasOne('Combos\Entities\ComboSinoEtitie','id','drogadiccion');
    }

    public function esPrimeraDesaparece(){
        return $this->hasOne('Combos\Entities\ComboSinoEtitie','id','primera_desaparece');
    }

    public function esEnfermedad(){
        return $this->hasOne('Combos\Entities\ComboSinoEtitie','id','enfermedad');
    }

    public function esInternadoInstitucion(){
        return $this->hasOne('Combos\Entities\ComboSinoEtitie','id','internado_institucion');
    }

    public function setFechaVistoAttribute($value)
    {
        $this->attributes['fecha_visto'] = \Time::FormatearToMysql($value);

    }
    public function getFechaVistoAttribute($value)
    {
        if($value == "0000-00-00"){
            return "";
        }

       return \Time::FormatearToNormal($value);
    }

    public function getHoraVistoAttribute($value)
    {
        if($value == "00:00:00"){
            return "";
        }
        return $value;
    }

    public function getPersona(){

        return $this->hasOne('Oficios\Entities\Personas','id','id_persona');

    }


}
?>