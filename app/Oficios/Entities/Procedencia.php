<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/04/17
 * Time: 12:14
 */
namespace Oficios\Entities;
class Procedencia extends \Eloquent
{
    protected $table = 'procedencias';
    protected $fillable = array('id', 'descripcion');

}
?>