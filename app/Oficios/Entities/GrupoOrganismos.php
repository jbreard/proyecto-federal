<?php 
namespace Oficios\Entities;

class GrupoOrganismos  extends \Eloquent
{
	protected $table = 'grupo_organismos';
	
	public function miembros(){
		return $this->hasMany('Oficios\Entities\GruposOrganismosMiembros','id_grupo','id');
	}
}