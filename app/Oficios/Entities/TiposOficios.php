<?php  namespace Oficios\Entities;

class TiposOficios extends \Eloquent
{
    protected $table = 'tipos_oficios';
    protected $fillable = array('id', 'descripcion','descripcion_oficio');

    public function getDescripcion()
    {
        return strtolower(str_replace(' ','_',$this->get_attribute('descripcion')));
    }

    public function getDescripcionOficio()
    {
        return strtolower(str_replace(' ','_',$this->get_attribute('descripcion_oficio')));
    }

}
?>