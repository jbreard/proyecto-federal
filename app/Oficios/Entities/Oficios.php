<?php namespace Oficios\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class Oficios extends \Eloquent
{
    use RevisionableTrait;
    use SoftDeletingTrait;

    protected $table = 'oficios';

    protected $fillable = [
            "id","nro_oficio","juzgado","tipo_oficio",
            "secretaria","caratula_expediente","causa_medida",
            "nro_oficio2","juzgado2","caratula2","fiscalia2","observaciones_judicial2",
            "secretaria2","caratula_expediente2","fecha_oficio2","nro_causa2",
            "nro_causa_expediente2",
            "estado","expediente","fecha","observaciones","estadoOficio",
            "cesado","hora_denuncia","nombre_denunciante","fiscalia","nro_causa_expediente","nro_causa",
            "caratula","fecha_denuncia","juzgado_secretaria","medida_restrictiva","tipo_medida","fecha_oficio","confirmado","plazo_fecha","plazo_hr","observaciones_judicial","otra_fiscalia_2","otra_fiscalia","juzgado_otro","juzgado_otro_2","alerta_temprana","id_usuario"];


    public function user()
    {
       return $this->hasOne('Usuarios\Entities\User', 'id', 'id_usuario');
    }

    public function personas()
    {
        return $this->hasOne('Oficios\Entities\Personas','id_oficio','id');
    }

    public function fiscalias()
    {
        return $this->hasOne('Oficios\Entities\Fiscalias','id','fiscalia');
    }

    public function juzgados()
    {
        return $this->hasOne('Oficios\Entities\Juzgados','id','juzgado');
    }


    public function fiscaliasAniadida()
    {
        return $this->hasOne('Oficios\Entities\Fiscalias','id','fiscalia2');
    }

    public function juzgadosAniadida()
    {
        return $this->hasOne('Oficios\Entities\Juzgados','id','juzgado2');
    }

    public function oficiosLeidos(){

        return $this->hasMany('Oficios\Entities\OficiosVistos','id_oficio','id');
    }


    public function getSecuestroElementos(){


    }

    public function tipo()
    {
        return $this->hasOne('Oficios\Entities\TiposOficios','id','tipo_oficio');
    }
    public  function archivos(){

        return $this->hasMany('Oficios\Entities\Archivos','id_oficio','id');
    }

    public  function getObservaciones(){

        return $this->hasMany('Oficios\Entities\Observaciones','id_oficio','id');
    }

    public  function estadoOficio(){

        return $this->hasOne('Oficios\Entities\EstadosOficios','id','estado');

    }
    public function oficioCesado(){

        return $this->hasOne('Oficios\Entities\OficiosCesados','id_oficio','id');
    }

    public  function plazos(){
        return $this->hasMany('Oficios\Entities\Plazo','id_oficio','id')->withTrashed();
    }



    public function setFechaOficioAttribute($value)
    {
        $this->attributes['fecha_oficio'] = \Time::FormatearToMysql($value);
    }


    public function setFechaOficio2Attribute($value)
    {
        $this->attributes['fecha_oficio2'] = \Time::FormatearToMysql($value);
    }

    public function getFechaOficioAttribute($value)
    {
        if($value)
            return \Time::FormatearToNormal($value);


        return $value;
    }

    public function getFechaOficio2Attribute($value)
    {
        if($value)
            return \Time::FormatearToNormal($value);

        return $value;
    }



    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearHoraFecha($value);
    }


    public function getPlazoFechaAttribute($value)
    {
        if($value == "0000-00-00"){
            return "";
        }
        else if($value)
            return \Time::FormatearToNormal($value);

        return $value;
    }

    public function setPlazoFechaAttribute($value)
    {

        $this->attributes['plazo_fecha'] = \Time::FormatearToMysql($value);
    }

       public function getNroOficioAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }


    public function getNroOficio2Attribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }
    public function getNroCausaAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }

    public function getNroCausa2Attribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }
    public function getNroCausaExpedienteAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }

    public function getNroCausaExpediente2Attribute($value)
    {
        if(empty($value))
            return "";
        return $value;

    }



    public function test(){
      return 'hola mundo';
    }

    protected $revisionFormattedFields = array(

        'nro_oficio'=>'string:<strong>%s</strong>'

    );

    protected $revisionFormattedFieldNames = array(
        'nro_oficio' => 'Nro Oficio',
        'nro_oficio2' => 'Nro Oficio (Aut Jud Interviniente)',
        "caratula_expediente"=>'Caratula Expediente',
        "causa_medida"=>'Causa de La Medida',
        "fecha_oficio"=>"Fecha Oficio",
        "juzgado2"=>"Juzgado (Aut Jud Interviniente)",
        "caratula2"=>"Caratula (Aut Jud Interviniente)",
        "fiscalia2"=>"Fiscalia (Aut Jud Interviniente)",
        "observaciones_judicial2"=>"Observaciones (Aut Jud Interviniente)",
        "secretaria2"=>"Secretaria (Aut Jud Interviniente)",
        "caratula_expediente2"=>"Caratula Expediente (Aut Jud Interviniente)",
        "fecha_oficio2"=>"Fecha Oficio (Aut Jud Interviniente)",
        "nro_causa2"=>"Nro Causa (Aut Jud Interviniente)",
        "nro_causa"=>"Nro Causa",
        "nro_causa_expediente"=>"NRO CAUSA EXPEDIENTE"

    );

    public function PersonaBuscada()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaBuscada','id_oficio','id');
    }

    public function PersonaUltimoDomicilio()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaUltimoDomicilio','id_oficio','id');
    }

    public function PersonaDescripcionFisica()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaDescripcionFisica','id_oficio','id');
    }

    public function PersonaDatosLaborales()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaDatosLaborales','id_oficio','id');
    }

    public function PersonaDatosRelacionadoAusencia()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaDatosRelacionadoAusencia','id_oficio','id');
    }

    public function PersonaDatosDenunciante()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_PersonaDenunciante','id_oficio','id');
    }

    public function PersonaDatosBusqueda()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_Busqueda','id_oficio','id')->with('procedencia','estado_actual','obj_victima_ilicito');
    }

    public function PersonaDatosBusquedaXAvance()
    {
        return $this->hasMany('Oficios\Entities\PersonasExtraviadas\PE_BusquedasXAvances','id_oficio','id');
    }

    public function NrosCelulares()
    {
        return $this->hasMany('Oficios\Entities\PersonasExtraviadas\PE_NroCelular','id_oficio','id');
    }

    public function NrosCelularesDenunciante()
    {
        return $this->hasMany('Oficios\Entities\PersonasExtraviadas\PE_NroCelularDenunciante','id_oficio','id');
    }

    public function Autorizacion()
    {
        return $this->hasOne('Oficios\Entities\PersonasExtraviadas\PE_Autorizaciones','id_oficio','id');
    }

    public function Fotos()
    {
        return $this->hasMany('Oficios\Entities\PersonasExtraviadas\PE_Fotos','id_oficio','id');
    }


}
?>
