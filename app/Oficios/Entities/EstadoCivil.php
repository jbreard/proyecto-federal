<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 17:18
 */

namespace Oficios\Entities;


class EstadoCivil extends \Eloquent
{
    protected $table = 'estados_civiles';
    protected $fillable = array('id_estado_civil', 'estado_civil');

}