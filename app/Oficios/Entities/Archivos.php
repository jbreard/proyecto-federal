<?php namespace Oficios\Entities;


use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Venturecraft\Revisionable\RevisionableTrait;

class Archivos extends \Eloquent
{
    use RevisionableTrait;
    use SoftDeletingTrait;

    protected $table = 'archivos';

    protected $fillable  = ["id_oficio","id_usuario","path"];

    private function subir(&$archivo){
        $nombre = md5(rand());
        //$ruta   = "archivos/$this->id_oficio";
        $oficio = str_pad($this->id_oficio,10,'0',STR_PAD_LEFT);
        $ruta = \Config::get('config.base_path').'/'.date('Y').'/'.date('m').'/'.date('d').'/'.$oficio;
        $archivo -> move($ruta, $nombre);
        return "$ruta/$nombre";
    }

    private function guardar($id_oficio,&$archivo){
        $this->id_oficio = $id_oficio;
        $ruta = $this->subir($archivo);
        $this->path = $ruta;
        $this->save();
    }

    public function subirArchivos($idOficio,&$archivos){
        foreach($archivos as $index => $value){
            if (!empty($value)){
                $this->guardar($idOficio,$value);
            }
        }
    }
    public function getArchivoByIdOficio($idOficio){
        //dd($idOficio);
        return  $this->where('id_oficio','=',$idOficio)
                    ->first();
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

}
?>