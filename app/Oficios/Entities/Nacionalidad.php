<?php namespace Oficios\Entities;
class Nacionalidad extends \Eloquent
{
    protected $table = 'nacionalidades';
    protected $fillable = array('id', 'descripcion');

}
?>