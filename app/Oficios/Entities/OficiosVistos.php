<?php namespace Oficios\Entities;

use Carbon\Carbon;

class OficiosVistos extends \Eloquent
{
    protected $table = 'oficios_vistos';

    public function user()
    {
       return $this->hasOne('Usuarios\Entities\User', 'id', 'id_usuario');
    }

    public function oficio()
    {
        return $this->hasOne('Usuarios\Entities\Oficios', 'id', 'id_oficio');
    }

    public function getCreatedAtAttribute($value){

        return \Time::FormatearHoraFecha($value);

    }





}
?>