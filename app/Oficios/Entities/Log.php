<?php namespace Oficios\Entities;


class Log extends \Eloquent
{
    protected $table;




    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function setTable($table)
    {
        return $this->table = $table;
    }

    public function getTable(){
        return $this->table;
    }

    public function getPersona(){

        return $this->hasOne('Oficios\Entities\Personas','id','id_persona');

    }

    public function getUsuario(){

        return $this->hasOne('Usuarios\Entities\User','id','id_usuario');

    }

    public function getFieldAttribute($value){

        return ucwords(str_replace("_"," ",$value));
    }





}
?>