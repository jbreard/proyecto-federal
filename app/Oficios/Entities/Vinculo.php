<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/06/16
 * Time: 14:28
 */

namespace Oficios\Entities;


class Vinculo extends \Eloquent {

    protected $table = 'vinculos';
    protected $fillable = array('id_vinculo', 'vinculo');


} 