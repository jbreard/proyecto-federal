<?php namespace Oficios\Entities;

use Venturecraft\Revisionable\RevisionableTrait;

class Fotos extends \Eloquent
{
    use RevisionableTrait;

    protected $table = 'fotos';

    protected $fillable= ["id","id_persona","id_oficio","id_usuario","path"];


}
?>