<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 19:01
 */

namespace Oficios\Entities;


class Tez extends \Eloquent{
    protected $table = 'tez';
    protected $fillable = array('id_tez', 'tez');
} 