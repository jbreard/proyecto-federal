<?php  namespace Oficios\Entities;

class OficiosCesados extends \Eloquent
{
    protected $fillable = ["id_oficio","fecha_cese","tipo_medida","persona_auto","valor","path","id_usuario","descripcion","observaciones"];
    protected $table    = 'oficios_cesados';


    public function  setFechaCeseAttribute($value){
        $this->attributes['fecha_cese'] = \Time::FormatearToMysql($value);
    }

     public function getCreatedAtAttribute($value)
    {
        
        return \Time::FormatearHoraFecha($value);
    }


    public function  getFechaCeseAttribute($value){
        return \Time::FormatearToNormal($value);
    }

    public function user()
    {
        return $this->hasOne('Usuarios\Entities\User', 'id', 'id_usuario');
    }

}
?>