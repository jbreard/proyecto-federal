<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/03/16
 * Time: 18:58
 */

namespace Oficios\Entities;


class ColorOjo extends \Eloquent {
    protected $table = 'color_ojos';
    protected $fillable = array('id_color_ojo', 'color_ojo');

} 