<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 12/05/15
 * Time: 19:02
 */

namespace Oficios\Entities;


use Oficios\Interfaces\ArchivosInterface;

class FotosVehicular extends  \Eloquent implements ArchivosInterface{

    public function setPath($idPadre = null)
    {

        $oficio = str_pad($idPadre,10,'0',STR_PAD_LEFT);
        $path   = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d').'/'.$oficio.'/secuestro_vehicular/'.$idPadre;
        //dd($path);
        return $path;
        //return "archivos/secuestro_vehicular/".$idPadre;
    }

    protected  $table = 'fotos_vehicular';
}