<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 28/06/16
 * Time: 16:01
 */

namespace Oficios\Entities;


class LargoCabello extends \Eloquent{

    protected $table = 'largos_cabello';
    protected $fillable = array('id_largo_cabello', 'largo_cabello');

} 