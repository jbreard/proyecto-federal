<?php namespace Oficios\Entities;

class Genero extends \Eloquent
{
    protected $table = 'genero';
    protected $fillable = array('descripcion');

}
?>