<?php
namespace Oficios\Entities;

use Venturecraft\Revisionable\RevisionableTrait;

class SecuestroElementos extends \Eloquent
{
    use RevisionableTrait;
    protected $table = 'secuestro_elementos';

    protected $fillable = ["tipo_elemento","identificacion","marca","modelo"];

    public function scopeSearch($query, $q,$values)
    {
        $str = implode (", ", $values);
        $match = "MATCH(".$str.") AGAINST (? IN BOOLEAN MODE)";
        return $query->whereRaw($match, array($q));
    }

}

?>
