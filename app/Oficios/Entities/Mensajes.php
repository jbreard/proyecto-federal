<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 27/10/14
 * Time: 13:36
 */

namespace Oficios\Entities;


use Oficios\ClasesAbstractas\BusquedaAbstracta;

class Mensajes extends BusquedaAbstracta {

    protected $table = "mensajes";



    public  function getTable(){

        return $this->table;
    }




    protected $fillable =["asunto","contenido"];


    public function destinatarios(){
        return $this->hasMany('Oficios\Entities\DestinatariosMensajes','id_mensaje','id');

    }

    public function emisor(){
        return $this->belongsTo('Usuarios\Entities\User','usuario_id','id');
    }

    public function respuestas(){
        return $this->hasMany('Oficios\Entities\RespuestasMensajes','id_mensaje');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearHoraFecha($value);
    }

    public function leido(){
        return $this->hasOne('Oficios\Entities\MensajesVistos','id_mensaje')->where('id_usuario','=',\Auth::user()->id)->limit(1);
    }



    public function getFields()
    {
        return $this->fillable;
    }

    public function relations()
    {
        return ['respuestas','emisor',];
    }

    public function relationsPivot()
    {
        return [];
    }

    public function archivos(){

        return $this->hasMany('Oficios\Entities\ArchivosMensajes','id_mensaje','id');
    }



}