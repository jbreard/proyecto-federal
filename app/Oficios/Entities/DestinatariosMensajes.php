<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 27/10/14
 * Time: 15:37
 */

namespace Oficios\Entities;


class DestinatariosMensajes extends \Eloquent{

    protected $table = "destinatarios_mensajes";

    protected $fillable = ["id_mensaje","id_correo"];

    public function correo(){

        return $this->hasOne('Correos\Entities\Correo','id','id_correo');

    }

    public function mensaje(){

        return $this->belongsTo('Oficios\Entities\Mensajes','id_mensaje','id');

    }


    public function getOrganismo(){
        return $this->belongsTo('Combos\Entities\CombosOrganismos','id_organismo','id');
    }

    public function getTable()
    {
        return $this->table;

    }

} 