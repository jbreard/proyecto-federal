<?php namespace Oficios\Entities;

use Venturecraft\Revisionable\RevisionableTrait;

class PedidosSecuestroVehicular extends \Eloquent
{
    use RevisionableTrait;
    protected $table = 'secuestro_vehicular';
    protected $fillable = ["dominio","marca_vehiculo","modelo_vehiculo","nro_chasis","nro_motor","radicacion","juzgado_secretaria","anio_vehiculo","color","marca_motor","id_oficio","id_tipo_vehiculo","otra_marca","otro_tipo_vehiculo","otro_modelo"];

    public function getPersona(){
        return $this->belongsTo('Oficios\Entities\Personas','id_persona','id');
    }

    public function oficio(){
        return $this->belongsTo('Oficios\Entities\Oficios','id_oficio','id');
    }


    public function getModelo(){
        return $this->hasOne('Oficios\Entities\VehiculosModelos','id','modelo_vehiculo');
    }

    public function getMarca(){
        return $this->hasOne('Oficios\Entities\MarcaVehiculos','id','marca_vehiculo');
    }


    public function getFotos(){
        return $this->hasMany('Oficios\Entities\FotosVehicular','id_oficio','id_oficio');
    }


    public function getTipoVehiculo(){
        return $this->hasOne('Combos\Entities\ComboTipoVehiculo','id','id_tipo_vehiculo');
    }

    public function getAnioVehiculoAttribute($value)
    {
        if(empty($value))
            return "";
        return $value;
    }

    public function getDominioAttribute($value){
        return \Transformer::quitMidScriptSpaces($value);
    }

    public function setDominioAttribute($value){
        return $this->attributes['dominio'] = \Transformer::quitMidScriptSpaces($value);
    }

    public function scopeSearch($query, $q,$values)
    {
        $str = implode (", ", $values);
        $match = "MATCH(".$str.") AGAINST (?  IN BOOLEAN MODE)";
        return $query->whereRaw($match, array($q));
    }


}

?>
