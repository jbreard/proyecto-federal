<?php 
namespace Oficios\Entities;

use Illuminate\Database\Eloquent\SoftDeletingTrait;


class ContadorNotificaciones extends \Eloquent
{
    
    use SoftDeletingTrait;

    protected $table = 'contador_notificaciones';

}
