<?php  namespace
Oficios\Entities;
class Localidad extends \Eloquent
{
    protected $table = 'localidades';
    protected $fillable = array('id', 'descripcion');

}
?>