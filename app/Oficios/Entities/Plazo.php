<?php  namespace Oficios\Entities;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Plazo extends \Eloquent
{
    use SoftDeletingTrait;
    protected $table = 'plazos';
    protected $fillable = array(
        'id',
        'fecha_inicio',
        'fecha_final',
        'destino',
        'observaciones',
    );

    public  function archivos(){
        return $this->hasMany('Oficios\Entities\ArchivosPlazos','id_plazo','id');
    }


    public function setFechaFinalAttribute($value)
    {
        $this->attributes['fecha_final'] = \Time::FormatearToMysql($value);
    }


    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fecha_inicio'] = \Time::FormatearToMysql($value);
    }

    public function getFechaInicioAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getFechaFinalAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

}
?>