<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 10/11/14
 * Time: 17:04
 */

namespace Oficios\Entities;


class MensajesVistos extends \Eloquent {

    protected $table = 'mensajes_vistos';
    protected $fillable = ['id_mensaje','id_usuario'];

    public function usuario(){
        return $this->belongsTo('Usuarios\Entities\User','id_usuario');

    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearHoraFecha($value);
    }

} 