<?php namespace Oficios\Entities;
class MedidasRestrictivas extends \Eloquent
{


   public function guardar(&$data){

        $oficio           = new Oficios();
        $responsable      = new Personas();
        $fotosResponsable = new Fotos();
        $victima          = new Personas();
        $fotosVictima     = new Fotos();
        $archivos         = new Archivos();
        $oficio->guardar($data,5);
        $responsable->guardar($oficio->id,2,$data["datos_persona"]["responsable"]);
        $fotosResponsable->subirFotos($responsable->id,$oficio->id,$data["foto"]["responsable"]);
        $victima->guardar($oficio->id,1,$data);
        $fotosVictima->subirFotos($victima->id,$oficio->id,$data["foto"]["victima"]);
        $archivos->subirArchivos($oficio->id,$data["archivo"]["oficio"]);


    }


}
?>