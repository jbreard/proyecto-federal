<?php
namespace Oficios\Avisos;
use App;
use Illuminate\Support\ServiceProvider;


/**
 * Created by PhpStorm.
 * User: juan
 * Date: 30/10/14
 * Time: 15:07
 */

class AvisosServiceProvider extends ServiceProvider {
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
//        $this->app->bind('notificaciones',function() {
//            return new Emails(
//                App::make('Correos\Repositories\CorreoRepo'),
//                App::make('Oficios\Repositories\OficiosRepo')
//            );
//        });
          $this->app->bind('notificaciones',function() {
            return App::make('Oficios\Avisos\Emails');
        });
    }
}