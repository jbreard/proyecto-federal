<?php namespace Oficios\Avisos;

use Illuminate\Support\Facades\Facade;

class AvisosFacade extends Facade {
    protected  static function getFacadeAccessor(){
        return 'notificaciones';
    }
} 