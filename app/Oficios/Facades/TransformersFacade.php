<?php 

namespace Oficios\Facades;

use Illuminate\Support\Facades\Facade;

class TransformersFacade extends Facade {
    protected  static function getFacadeAccessor(){
        return 'Transformers';
    }
} 