<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 20/04/15
 * Time: 15:27
 */

namespace Oficios\Clases;

use Illuminate\Support\Facades\Config;
use Oficios\Managers\MensajesVistosManager;
use Oficios\Repositories\MensajesVistosRepo;

class Mensajes {

    public function sendEmail($tarea,$datos)
    {
        if(Config::get('app.modulos.mensajes'))
        {
            $inputData = $datos['data'];
            $remitente = $datos['remitente'];
            $destinatarios  = $datos['mails'];
            $asunto = $inputData['asunto'];
            $adjuntos = (isset($inputData['adjuntos']) and !empty($inputData['adjuntos'])) ? $datos['adjuntos'] : null;
            $dataMail = [
                "contenido" => $inputData["contenido"],
                "remitente" => $remitente
            ];

            \Mail::send('mails.cuerpo',
                $dataMail,
                function ($message) use ($destinatarios, $asunto, $adjuntos) {
                    $message->getheaders()->addTextHeader("Disposition-Notification-To", "sifcop@minseg.gob.ar");
                    $message->getheaders()->addTextHeader("X-Confirm-Reading-To", "sifcop@minseg.gob.ar");

                    if(!empty($destinatarios) && is_array($destinatarios))
                    {
                        foreach ($destinatarios as $destinatario) {
                            $email = $destinatario['mail'];
                            if (!empty($email)) $message->to($email);
                        }
                    }

                    $message->subject($asunto);

                    if (!is_null($adjuntos)) {
                        foreach($adjuntos as $adjunto){
                            $file = public_path($adjunto);
                            if(file_exists($file)) $message->attach($file);
                        }
                    }
                }
            );

            $tarea->delete();
        }
    }

    public function saveVisto($tarea,$datos){
        $id  = $datos['id'];
        $idUsuario = $datos['id_usuario'];
        $mensajesRepoClass = new MensajesVistosRepo();
        $mensajesRepo = $mensajesRepoClass->newMensajeVisto($id,$idUsuario);
        $mensajesManager = new MensajesVistosManager($mensajesRepo, []);
        $mensajesManager->save();
        $tarea->delete();

    }

}


