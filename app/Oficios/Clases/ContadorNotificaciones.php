<?php
namespace Oficios\Clases;
use Oficios\Repositories\ContadorNotificacionesRepo;
use Oficios\Managers\ContadorNotificacionesManager;

class ContadorNotificaciones 
{
	private $contadorNotificacionesRepo;

	public function __construct(ContadorNotificacionesRepo $contadorNotificacionesRepo){
		$this->contadorNotificacionesRepo = $contadorNotificacionesRepo;
	}
	

	public function setContador($idUser){
		$idUser = (empty($idUser)) ? \Auth::user()->id : $idUser;
		$contador = $this->contadorNotificacionesRepo->getAll($idUser);
		return $contador;
	}

	public function add($type,$idUser = null){
		$contador  = $this->setContador($idUser);
		$contador->{$type} += 1;
		return $this->save($contador);
	}

	public function rest($type,$idUser = null){
		$contador  = $this->setContador($idUser);
		$cantidad = $contador->{$type};
		$contador->{$type} = ($contador > 0) ? $contador-1 : 0;
		return $this->save($contador);
	}

	public function save($contador){
		$contadorManager = new ContadorNotificacionesManager($contador,[]);
		return $contadorManager->save();
	}

	public function addMensajeNuevo(){
		return $this->add('mensajes_nuevos');
	}

	

	public function addOficiosNuevos(){
		return $this->add('oficios_nuevos');
	}

	public function addOficiosModificados(){
		return $this->add('oficios_modificados');
	}

	public function restMensajeNuevo(){
		return $this->rest('mensajes_nuevos');
	}

	public function restOficiosNuevos(){
		return $this->rest('oficios_nuevos');
	}

	public function restOficiosModificados(){
		return $this->rest('oficios_modificados');
	}

}