<?php namespace Oficios\Clases;
use Oficios\ClasesAbstractas\ArchivosBaseManager;
use Oficios\Interfaces\ArchivosRepoInterface;

class ArchivosManager {

    private $path;
    private $baseManager;
    private $baseRepo;
    private $archivo;
    private $entity;
    protected $i = 1;

    function __construct($id=null)
    {

        $this->id = $id;
    }



    public function subir($nombreCampo){
        $archivos = \Input::file($nombreCampo);
        $multiple = is_array($archivos);

        if($multiple) return $this->multiplesArchivos($archivos);
        $this->archivo = $archivos;
        return $this->saveArchivo();

    }


    public function saveArchivo()
    {
        // aca suben las fotos de secuestro vehicular
        $archivo = $this->archivo;
        if(is_object($archivo)){
            $ext = $archivo->getClientOriginalExtension();
            //$nombre = md5(rand()) . ".$ext";
            $id_unic =rand(5, 35);
            $nombre = date('Ymd').'_'.$this->id.'_'.$id_unic.'.'.$ext;
            if ($archivo->move($this->path, $nombre)){

                $fullPath = $this->path . '/' . $nombre;
                $archivoBase = $this->baseRepo->newArchivo($this->id, $fullPath);
                $this->baseManager->setEntity($archivoBase);
                $this->baseManager->setData([]);
                $this->baseManager->save();

            }
        }

    }


    public function multiplesArchivos($archivos)
    {
        foreach ($archivos as $archivo) {
            $this->archivo = $archivo;
            $this->saveArchivo();
        }
    }

    /**
     * @param ArchivosBaseManager $baseManager
     */
    public function setBaseManager(ArchivosBaseManager $baseManager)
    {
        $this->baseManager = $baseManager;

    }

    /**
     * @param mixed $baseRepo
     */
    public function setBaseRepo(ArchivosRepoInterface $baseRepo)
    {
        $this->baseRepo = $baseRepo;
        $this->entity =  $this->baseRepo->getModel();
        $this->path = $this->entity->setPath($this->id);
    }

}