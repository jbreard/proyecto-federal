<?php 

namespace Oficios\Clases;

class RedisHandler{

	private $channel = 'oficios';



	public function publish($data){

		$redis = \Redis::connection();
		$redis->publish($this->channel,$data);
	}


	public function setChannel($channel){
		return $this->channel = $channel;
	}

	public function publishData($channel,$data){
		$this->setChannel($channel);
		$this->publish($data);
	}



}