<?php namespace Oficios\Clases;
use Oficios\Managers\OficiosCeseManager;
use Oficios\Managers\OficiosManager;
use Oficios\Repositories\OficiosCesadosRepo;
use Oficios\Repositories\OficiosRepo;

/**
 * Created by PhpStorm.
 * User: juan
 * Date: 15/03/15
 * Time: 02:32
 */

class SinEfecto {


    private $cesadosRepo;
    /**
     * @var OficiosRepo
     */
    private $oficiosRepo;

    function __construct(OficiosCesadosRepo $cesadosRepo,OficiosRepo $oficiosRepo)
    {
        $this->cesadosRepo = $cesadosRepo;
        $this->oficiosRepo = $oficiosRepo;
    }

    public function dejarSinEfecto($data,$mensaje = 'Requerimiento sin Efecto'){

        $id   = $data['id_oficio'];
        $oficios            = $this->cesadosRepo->newOficioCesado($id);
        $oficioManager      = new OficiosCeseManager($oficios,$data);
        $path = $oficioManager->subirArchivo($id,'escaneo_cese');
        $data['path']   = $path;
        $oficioManager  = new OficiosCeseManager($oficios,$data);
        $oficioManager->save();

        //Actualizo el estado del oficio
        $oficios = $this->oficiosRepo->find($id);
        $data['estado'] = 2;
        $oficiosManager  = new OficiosManager($oficios,$data);
        
        if($oficiosManager->save()) {
            $rutapdf =  route($oficios->tipo->ruta.".pdf",$id);
            $path    = \URL::asset($path);
            return \Notification::notificar($id,$rutapdf,$path,$blade='email_sin_efecto_oficio',$mensajeArgumento = 'Requerimiento sin Efecto');
        }

    }


}