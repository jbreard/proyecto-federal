<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 20:06
 */

namespace Oficios\Clases;


use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class ApiBaseIncrementalCaller
{
    protected $url;
    protected $apiKey;
    protected $client;
    protected $origen;
    protected $idOrigen;

    /**
     * ApiBaseIncrementalCaller constructor.
     */
    public function __construct()
    {
        $this->url = Config::get('app.apis.base_incremental.url');
        $this->apiKey = Config::get('app.apis.base_incremental.api_key');
        $this->origen = Config::get('app.apis.base_incremental.origen');
#        $this->idOrigen = Config::get('app.apis.base_incremental.id_origen');
        $this->client = new Client();
    }

     public function call_many($datos) {
	

            Log::useDailyFiles(storage_path().'/logs/api_base_incremental.log');

        try {

            $activeCallApi = Config::get('app.apis.base_incremental.active');

            $apiKey = $this->apiKey;

            $method = 'POST';

            $uri = $this->url;

            $json = [];


            foreach($datos as $data)
            {

		Log::info($data);

                $item = array_get($data,"data");
                $json[] = [
                      "origen" => $this->origen,
                      "id_origen" => $item->id,
                      "tipo_dato" => $data["data_type"],
                      "datos" => $item,
                      "tipo_transaccion" => $data["type_transaction"]
                ];
            }

            $options = [
                'headers' => [
                    'api-key' => $apiKey
                ],
                'json' =>$json
            ];

            #Log::useDailyFiles(storage_path().'/logs/api_base_incremental.log');
		
            if($activeCallApi) {

                $response = $this->client->request($method, $uri, $options);
                Log::info($this->url);
                Log::info($response->getBody());
                return $response->getStatusCode();

            }else {
                #Log::info('call to api -> off.');
            }

            return 0;

        } catch (Exception $ex) {

            Log::useDailyFiles(storage_path().'/logs/api_base_incremental.log');
            Log::info($ex->getMessage());

        }

        return 0;

    }











    /**
     * @param $data
     * @return int - Api Status Code Response
     */
    public function call($input) {


        try {

            $activeCallApi = Config::get('app.apis.base_incremental.active');

            $apiKey = $this->apiKey;

            $method = 'POST';

            $uri = $this->url;

            $json = [];

            $items = array_get($input,'data');

            foreach($items as $item)
            {

                if(is_array($item)){
                        $datos = array_get($data["data"],0,$data["data"]);
                }else{
                        $datos = $item;
                }

                $json[] = [
                      "origen" => $this->origen,
                      "id_origen" => $item->id,
                      "tipo_dato" => $input['data_type'],
                      "datos" => $datos,
                      "tipo_transaccion" => $input['type_transaction']
                ];

            }

            $options = [
                'headers' => [
                    'api-key' => $apiKey
                ],
                'json' =>$json
            ];

            #Log::useDailyFiles(storage_path().'/logs/api_base_incremental.log');
            #Log::info($options);

            if($activeCallApi) {

                $response = $this->client->request($method, $uri, $options);
                Log::info($this->url);
                Log::info($response->getBody());
                return $response->getStatusCode();

            }else {
                #Log::info('call to api -> off.');
            }

            return 0;

        } catch (Exception $ex) {

            Log::useDailyFiles(storage_path().'/logs/api_base_incremental.log');
            Log::info($ex->getMessage());

        }

        return 0;

    }
}

