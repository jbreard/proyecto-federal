<?php


namespace Oficios\Managers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Oficios\Entities\Archivos;
//use Oficios\Observers\OficiosObserver;


abstract class BaseManager {

    protected $entity;
    protected $data;
    protected $errors;
    protected $path;

    public function __construct($entity, $data=[]){

        $this->entity = $entity;
        //$this->data   = array_only($data,array_keys($this->getRules()));
        $this->data   = $data;
        $this->path   = \Config::get('config.base_path').date('Y').'/'.date('m').'/'.date('d');

    }




    abstract public function getRules();

    public function isValid(){
        $rules        = $this->getRules();
        $messages     = $this->getMessages();
        $validation   = \Validator::make($this->data,$rules,$messages);
        $isValid      = $validation->passes();
        $this->errors = $validation->messages();

        return $isValid;
    }

    public function getErrors(){

        return $this->errors;

    }

    public function subirArchivo($id,$nombre_campo,$id_persona=null,$foto=null){


        if (Input::hasFile($nombre_campo))
        {

            $ext = Input::file($nombre_campo)->getClientOriginalExtension();

            $aux = md5(rand(3, 8));
            $nombre = date('Ymd').'_'.$id.'_'.$aux.'.'.$ext;
            
            
            
             //dd($id);
             $archivo = new Archivos();
             $path = $archivo->getArchivoByIdOficio($id)->path;
             //dd($path);
             if($path){
                 $partes = explode('/',$path);
                 array_pop($partes);
                 //dd($partes);
                 $ruta='';
                 foreach ($partes as $parte){
                     echo $parte;
                     $ruta.=$parte.'/';
                 }
                 $ruta.="fotos/";
                 dd($ruta);
             }else{
                  $ruta ="";
             }
            
            
            if($foto){
                $id = str_pad($id,10,'0',STR_PAD_LEFT);
                $ruta  = $this->path.'/'.$id.'/fotos';
                $ruta   = ($id_persona!=null) ? $ruta.'/'.$id_persona : $ruta;
            }else{
               $id = str_pad($id,10,'0',STR_PAD_LEFT);
               $ruta  = $this->path.'/'.$id;
               $ruta   = ($id_persona!=null) ? $ruta.'/'.$id_persona : $ruta;
            }
            Input::file($nombre_campo) -> move($ruta, $nombre);

            $ruta = $ruta.'/'.$nombre;
            return $ruta;

        }

    }

    public function save($archivo=False,$nombre_campo=null){

        $this->entity->fill($this->data);
//        dd($this->entity->fill);
        $this->entity->save();

        if($archivo){
            return $this->saveArchivos($nombre_campo);
        }
        return true;
    }

    /**
     * @param $nombre_campo
     * @return string
     */
    private function saveArchivos($nombre_campo)
    {
        foreach ($nombre_campo as $key => $index) {
            $path = $this->subirArchivo($this->entity->id, $index);
            //echo"saveArchivos";
           // dd($path);
            if (!empty($path)) {
                $data = array("id_usuario" => Auth::user()->id, "id_oficio" => $this->entity->id, "path" => $path);
                $archivo = new Archivos();
                $archivo->fill($data);
                $archivo->save();
            }
        }
        return $path;
    }

    public function getMessages()
    {
        return [];
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function delete(){
        return $this->entity->delete();
    }

    public function mostrarErrores($isValid)
    {
        $mensaje = "";
        if(!$isValid)
        {
            foreach ($this->getErrors()->all() as $error)
            {
                $mensaje .= $error."<br>";
            }
            return $mensaje;
        }
    }

} 