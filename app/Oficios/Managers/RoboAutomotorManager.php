<?php
/**
 * Created by PhpStorm.
 * User: Damián
 * Date: 18/09/17
 * Time: 15:24
 */

namespace Oficios\Managers;


class RoboAutomotorManager extends BaseManager{

    public function getRules()
    {
        $rules = [
            'dominio' => 'required'

        ];

        return $rules;
    }

    public function getMessages()
    {
        return [
            'dominio.required' => 'Debe completar el campo dominio'

        ];
    }


}