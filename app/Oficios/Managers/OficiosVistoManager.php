<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 28/05/14
 * Time: 15:41
 */

namespace Oficios\Managers;
use Oficios\Repositories\OficiosVistosRepo;

class OficiosVistoManager
{

    public function getRules()
    {
        $rules = [


        ];

        return $rules;
    }

    public  function save($idOficio){
        $this->store($idOficio);
    }

    public function saveMarcado($idOficio){
        $this->store($idOficio,1);
    }

    /**
     * @param $idOficio
     * @param int $marcado
     */
    private function store($idOficio,$marcado=0)
    {
        $oficiosVistosRepo = new OficiosVistosRepo();
        $oficioVisto = $oficiosVistosRepo->newOficioVisto($idOficio,$marcado);
        $oficioVisto->save();
    }

}