<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 21/01/16
 * Time: 16:59
 */

namespace Oficios\Managers\PersonasExtraviadas;
use Oficios\Managers\BaseManager;


class ManagerPEPersonaDenunciante extends BaseManager{

    public function getRules()
    {
        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'dependencia' => 'required'
        ];
    }
} 