<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/01/16
 * Time: 16:37
 */
namespace Oficios\Managers\PersonasExtraviadas;

use Oficios\Managers\BaseManager;


class ManagerPEPersonaDescripcionFisica extends BaseManager {

    public function getRules()
    {
        return [
            'id_contextura_fisica' => 'required',
            'id_tipo_cabello' => 'required'
        ];
    }
}