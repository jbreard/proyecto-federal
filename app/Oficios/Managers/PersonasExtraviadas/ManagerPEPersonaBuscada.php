<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/12/15
 * Time: 11:18
 */
namespace Oficios\Managers\PersonasExtraviadas;

use Oficios\Managers\BaseManager;


class ManagerPEPersonaBuscada extends BaseManager {

    public function getRules()
    {
        return [
            'nombres' => 'required',
            'apellido' => 'required',
            'fecha_nacimiento' => 'date_format:d/m/Y',
        ];
    }

    public function getMessages()
    {
        return [
            'fecha_nacimiento.date_format' => 'El formato de la fecha debe ser dd/mm/aaaa',
            'nombres.required' => 'El campo nombre es obligatorio'
        ];
    }
}