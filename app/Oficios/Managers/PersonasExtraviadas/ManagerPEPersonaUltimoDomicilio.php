<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/01/16
 * Time: 15:18
 */
namespace Oficios\Managers\PersonasExtraviadas;

use Oficios\Managers\BaseManager;


class ManagerPEPersonaUltimoDomicilio extends BaseManager {

    public function getRules()
    {
        return [
            'id_provincia' => 'required'
        ];
    }
}