<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 27/01/16
 * Time: 15:02
 */

namespace Oficios\Managers\PersonasExtraviadas;

use Oficios\Managers\BaseManager;


class ManagerPEOficio extends BaseManager {

    public function getRules()
    {
        return [
            'archivo1' => 'required_with:nombres',
            'nombres' => 'sometimes | required',
            'apellido' => 'sometimes | required',
            'fecha_nacimiento' => 'sometimes | date_format:d/m/Y',
            'fecha_oficio' => 'sometimes | date_format:d/m/Y'

        ];
    }


    public function getMessages()
    {
        return [
            'archivo1.required_with' => 'Debe adjuntar un oficio',
            'fecha_oficio.date_format' => 'Debe ingresar una fecha en formato correcto',
            'fecha_nacimiento.date_format' => 'Debe ingresar una fecha en formato correcto'
        ];
    }

}