<?php

namespace Oficios\Managers;


class ProfugoEvadidoManager extends BaseManager{

    public function getRules()
    {
        $rules = [
            'nombre' => 'required',
            'apellido' => 'required'

        ];

        return $rules;
    }

    public function getMessages()
    {
        return [
            'nombre.required' => 'Debe completar el campo nombres',
            'apellido.required' => 'Debe completar el campo apellido'
        ];
    }


}

?>