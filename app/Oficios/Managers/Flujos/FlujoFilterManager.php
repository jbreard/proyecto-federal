<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 24/03/17
 * Time: 12:44
 */

namespace Oficios\Managers\Flujos;


use Illuminate\Support\Facades\Config;
use Oficios\Repositories\PersonasRepo;

class FlujoFilterManager
{

    /**
     * FlujoFilterManager constructor.
     */
    public function __construct()
    {
    }

    public function isValid($validationtype, $model)
    {
        $validations = [
            0 => 'persona',
        ];

        return call_user_func([$this,$validations[$validationtype]],$model);
    }


    /**
     * Valida que el tipo de persona y el tipo de oficio de la persona esten en 'True' para iniciar
     * el proceso de transaccion con el SISEN (Sistema de Seguridad Nacional).
     *
     * @param $model
     * @return mixed
     */
    public function persona($model)
    {
        $validations = Config::get('app.apis.base_incremental.settings.flow.validations');

        $personaRepo = new PersonasRepo();
        $tipoOficio = $personaRepo->getTipoOficio($model->id);
        $tipoPersona = $model->id_tipo_persona;

        if(array_key_exists($tipoOficio,$validations)) {
            
            if(array_key_exists($tipoPersona,$validations[$tipoOficio])) {
                
                return $validations[$tipoOficio][$tipoPersona];
                
            }
            
        }
        
        return false;     
    }
}