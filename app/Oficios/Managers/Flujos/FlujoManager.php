<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/03/17
 * Time: 16:21
 */

namespace Oficios\Managers\Flujos;


use Illuminate\Support\Facades\Queue;

class FlujoManager
{
    protected $flujoTransaccionManager;
    protected $apiBaseIncrementalManager;

    /**
     * FlujoManager constructor.
     */
    public function __construct()
    {

        $this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
    }

    public function initFlujoOficioTransaccion($oficiosModel)
    {

        $flujoTransaccionManager = new FlujoTransaccionOficiosManager();
        $flujoTransaccionManager->setModel($oficiosModel);

        $data = [
            'id_oficio'                  => $oficiosModel->id,
            'cambioConfirmado'           => $flujoTransaccionManager->changeConfirmado(),
            'isConfirmado'               => $flujoTransaccionManager->isConfirmado(),
            'oficioEliminado'            => $flujoTransaccionManager->isEliminado(),
            'oficioSinEfecto'            => $flujoTransaccionManager->isSinEfecto(),
            'changePlazoAutorizado'      => $flujoTransaccionManager->hasPlazoAutorizado(),
            'changePlazoAutorizadoTo1'   => $flujoTransaccionManager->changePlazoAutorizadoTo1(),
            'changePlazoAutorizadoTo2'   => $flujoTransaccionManager->changePlazoAutorizadoTo2(),//Oficio modifica campo 'plazo_autorizado'
        ];
        Queue::push('Oficios\Colas\FlujoTransaccionOficioCola@flujo',$data);
        
    }

    public function initFlujoPersonaTransaccion($personasModel)
    {

        $flujoFilterManager = new FlujoFilterManager();
        
        if($flujoFilterManager->isValid(0,$personasModel)) {

            $flujoTransaccionManager = new FlujoTransaccionPersonasManager();
            $flujoTransaccionManager->setModel($personasModel);

            $data = [
                'id_oficio'                  => $personasModel->id_oficio,
                'isOficioConfirmado'         => $flujoTransaccionManager->isOficioConfirmado(),
                'tieneDni'                   => $flujoTransaccionManager->hasDni(),
                'changeNombre'               => $flujoTransaccionManager->changeNombre(),
                'changeApellido'             => $flujoTransaccionManager->changeApellido(),
                'changeDni'                  => $flujoTransaccionManager->changeNroDocumento(),
            ];

            Queue::push('Oficios\Colas\FlujoTransaccionPersonaCola@flujo',$data);    
        }

    }

    public function initFlujoPedidoSecuestroVehicularTransaccion($model)
    {
        $flujoTransaccionManager = new FlujoTransaccionPedidoSecuestroVehicularManager();
        $flujoTransaccionManager->setModel($model);

        $data = [            
            'id_oficio'                => $model->id_oficio,

            'isOficioConfirmado'         => $flujoTransaccionManager->isOficioConfirmado(),
            
            'hasDominio'             => $flujoTransaccionManager->hasField('dominio'),
            'changeDominio'            => $flujoTransaccionManager->changeField('dominio'),
            
            'hasAnio'                => $flujoTransaccionManager->hasField('anio_vehiculo'),
            'changeAnio'               => $flujoTransaccionManager->changeField('anio_vehiculo'),
            
            'hasRadicacionVehiculo'  => $flujoTransaccionManager->hasField('radicacion'),
            'changeRadicacionVehiculo' => $flujoTransaccionManager->changeField('radicacion'),
            
            'hasChasis'              => $flujoTransaccionManager->hasField('nro_chasis'),
            'changeChasis'             => $flujoTransaccionManager->changeField('nro_chasis'),
            
            'hasTipoVehiculo'        => $flujoTransaccionManager->hasField('id_tipo_vehiculo'),
            'changeTipoVehiculo'       => $flujoTransaccionManager->changeField('id_tipo_vehiculo'),
            
            'hasOtroTipoVehiculo'    => $flujoTransaccionManager->hasField('otro_tipo_vehiculo'),
            'changeOtroTipoVehiculo'   => $flujoTransaccionManager->changeField('otro_tipo_vehiculo'),
            
            'hasMarca'               => $flujoTransaccionManager->hasField('marca_vehiculo'),
            'changeMarca'              => $flujoTransaccionManager->changeField('marca_vehiculo'),
            
            'hasOtraMarca'           => $flujoTransaccionManager->hasField('otra_marca'),
            'changeOtraMarca'          => $flujoTransaccionManager->changeField('otra_marca'),
            
            'hasModelo'              => $flujoTransaccionManager->hasField('modelo_vehiculo'),
            'changeModelo'             => $flujoTransaccionManager->changeField('modelo_vehiculo'),
            
            'hasOtroModelo'          => $flujoTransaccionManager->hasField('otro_modelo'),
            'changeOtroModelo'         => $flujoTransaccionManager->changeField('otro_modelo'),
            
            'hasColor'               => $flujoTransaccionManager->hasField('color'),
            'changeColor'              => $flujoTransaccionManager->changeField('color'),
            
            'hasMarcaMotor'          => $flujoTransaccionManager->hasField('marca_motor'),
            'changeMarcaMotor'         => $flujoTransaccionManager->changeField('marca_motor'),
            
            'hasNumeroMotor'         => $flujoTransaccionManager->hasField('nro_motor'),
            'changeNuemeroMotor'       => $flujoTransaccionManager->changeField('nro_motor'),
        ];

        Queue::push('Oficios\Colas\FlujoTransaccionPedidosSecuestroVehicularCola@flujo',$data);
    }

}
