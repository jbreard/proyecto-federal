<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 24/02/17
 * Time: 16:12
 */

namespace Oficios\Managers\Flujos;


class FlujoTransaccionOficiosManager
{

    protected $model;

    /**
     * FlujoTransaccionManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function changeEstado()
    {
        return $this->model->isDirty('estado');
    }
    
    public function changeConfirmado()
    {
        return $this->model->isDirty('confirmado');
    }

    public function isEliminado()
    {
        return ! is_null($this->model->deleted_at);
    }

    public function isSinEfecto()
    {
        $isSinEfecto = ($this->model->estado == 2);
        return $isSinEfecto;
    }

    public function isConfirmado()
    {
        return ($this->model->confirmado == 2);
    }

    public function hasPlazoAutorizado()
    {
        $hasPlazoAutorizado = $this->model->plazo_autorizado != 0;
        
        return $hasPlazoAutorizado;
    }

    public function changePlazoAutorizadoTo1()
    {
        return $this->model->plazoAutorizado == 1;
    }

    public function changePlazoAutorizadoTo2()
    {
        return $this->model->plazoAutorizado == 2;
    }

}