<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 24/02/17
 * Time: 16:12
 */

namespace Oficios\Managers\Flujos;


use Oficios\Repositories\OficiosRepo;

class FlujoTransaccionPersonasManager
{

    protected $model;

    /**
     * FlujoTransaccionManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function hasDni()
    {
        return $tienedni = isset($this->model->nro_documento) && ! empty($this->model->nro_documento);
    }
    
    public function isOficioConfirmado()
    {
        $oficiosRepo = new OficiosRepo();

        $idOficio = $this->model->id_oficio;
        
        $oficio = $oficiosRepo->getModel()->where('id',$idOficio)->first();
        
        return ($oficio->confirmado == 2);
    }

    
    public function changeNombre()
    {
        return $this->model->isDirty('nombre');
    }

    public function changeApellido()
    {
        return $this->model->isDirty('apellido');
    }

    public function changeNroDocumento()
    {
        return $this->model->isDirty('nro_documento');
    }

}