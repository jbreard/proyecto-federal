<?php namespace Oficios\Managers\Flujos; 

use Oficios\Clases\ApiBaseIncrementalCaller;


/**
 * Created by PhpStorm.
 * User: dc
 * Date: 24/02/17
 * Time: 15:44
 */
class ApiBaseIncrementalManager
{
    protected $api;

    /**
     * ApiBaseIncrementalManager constructor.
     */
    public function __construct()
    {
        $this->api = new ApiBaseIncrementalCaller();
    }

    /**
     * @param $data
     * @param $dataType
     * @param $transactionType
     * @return int - Api Status Code Response
     */
    public function call($data, $dataType, $transactionType)
    {
	#\Log::info('apiii');
	#\Log::info($data);
        
        return $this->api->call([
            'data' => $data,
            'data_type' => $dataType,
            'type_transaction' => $transactionType
        ]);        
        
    }
    public function call_many($data){
	return $this->api->call_many($data);
    }


}
