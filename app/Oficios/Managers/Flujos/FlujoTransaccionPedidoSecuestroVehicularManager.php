<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 22/03/17
 * Time: 17:34
 */

namespace Oficios\Managers\Flujos;


use Oficios\Repositories\OficiosRepo;

class FlujoTransaccionPedidoSecuestroVehicularManager
{

    protected $model;

    /**
     * FlujoTransaccionPedidoSecuestroVehicularManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function hasField($field)
    {
        return ( isset($this->model->{$field}) && ! empty($this->model->{$field}) );
    }


    public function changeField($field)
    {
        return $this->model->isDirty($field);
    }

    public function isOficioConfirmado()
    {
        $oficiosRepo = new OficiosRepo();

        $idOficio = $this->model->id_oficio;

        $oficio = $oficiosRepo->getModel()->where('id',$idOficio)->first();

        return ($oficio->confirmado == 2);
    }


}