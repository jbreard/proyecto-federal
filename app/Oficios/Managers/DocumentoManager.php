<?php


namespace Oficios\Managers;

class DocumentosManager extends BaseManager{

    public function getRules()
    {
        return [];
    }

    public function saveDocumento($idPersona)
    {
        $this->entity->id_persona = $idPersona;
        $this->save();
    }
}