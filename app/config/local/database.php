<?php

return array(
	'connections' => array(
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '173.10.0.96',
			'database'  => 'sifcop',
			'username'  => 'root',
			'password'  => 'C0nt3n3d0r',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

        'base_oficios_vistos' => array(
            'driver'    => 'mysql',
            'host'      => 'mysql',
            'database'  => 'base_oficios_vistos',
            'username'  => 'root',
            'password'  => 'root',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),
	),


	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => array(

		'cluster' => false,

		'default' => array(
			'host'     => 'redis',
			'port'     => 6379,
			'database' => 0,
		),

	),

);
