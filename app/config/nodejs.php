<?php

$url  = \Config::get('app.url');

$basePath = $url.":3000";
return [
    "url"=>$basePath."/socket.io/socket.io.js",
    "socket"=>$basePath
];
