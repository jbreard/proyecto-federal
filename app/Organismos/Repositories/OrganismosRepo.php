<?php

namespace Organismos\Repositories;

use Organismos\Entities\Organismos; 


class OrganismosRepo extends BaseRepo
 {

    public function getModel()
    {
        return new Organismos();
    }

   

    public function newOrganismo()
    {
        return $this->getModel();
    }


    public function getOrganismo()
    {
        return $this->model->remember(60)->get(['organismo'])->toArray();
    }
}