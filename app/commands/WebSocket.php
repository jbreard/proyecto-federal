<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class WebSocket extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'websocket:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Inicia el websocket';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	private $runWebSocket;

	public function __construct()
	{
		parent::__construct();
		$this->runWebSocket = \App::make('Oficios\WebSockets\RunWebSocket');
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->runWebSocket->run();

	}


}
