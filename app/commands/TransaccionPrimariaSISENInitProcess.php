<?php

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Oficios\Managers\Flujos\ApiBaseIncrementalManager;
use Symfony\Component\Console\Input\InputArgument;

class TransaccionPrimariaSISENInitProcess extends Command {

	protected $apiBaseIncrementalManager;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transaccion:02';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Procesa el archivo json del primer paso(transaccion:01) y envia data a SISEN';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->apiBaseIncrementalManager = new ApiBaseIncrementalManager();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$filename = storage_path("json/" . $this->input->getArgument('filename'));

		$contents = File::get($filename);

		$rows = explode("|", $contents);
		$this->line(count($rows));
		$rows = array_chunk($rows,$this->input->getArgument('chunk'));
		$data = [];
		$i = 0;
		foreach ($rows as $row)
		{
		$data = [];
#		try{

			#$item = json_decode($row);
			foreach($row as $item_original){
				$item = json_decode(trim($item_original," "));
				#$data = (array) $item->data;
				try{		
					$data[] = [
						"data"=>$item->data,
						"data_type"=>$item->data_type,
						"type_transaction"=>$item->transaction_type
					];
				}
				catch(Exception $e){
					$this->error($item_original);
					#$this->error($e->getMessage());
				}
			}
			$this->line(count($data));
			$responseCode = $this->apiBaseIncrementalManager->call_many($data);
			$this->line($i);
			$i++;
			if($responseCode == 202 ) {

				$this->line("EXITO: #{}");

			} else {

  				$this->error("ERROR: #{}".$responseCode);

			}
#		}catch(Exception $e){
#			$this->error([$row=>$e->getMessage()]);
#		}
		}
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('filename', InputArgument::REQUIRED, 'Nombre del archivo'),
			array('chunk',InputArgument::REQUIRED,'PONEME EL CHUNK AMEOH')
		);
	}


}
