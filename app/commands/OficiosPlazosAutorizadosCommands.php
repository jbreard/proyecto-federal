<?php

use Illuminate\Support\Facades\App;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OficiosPlazosAutorizadosCommands extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'oficios:checkplazo';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Actualiza la autorizacion de los oficios.';
    /**
     * @var OficiosSinEfectos

     */
    private $plazos;
    /**
     * Create a new command instance.
     *
     * @return \OficiosPlazosAutorizadosCommands
     */
	public function __construct()
	{
		parent::__construct();
        $this->plazos = App::make('Oficios\Scheduled\OficiosPlazosAutorizados\OficiosPlazosAutorizados');
    }

	public function schedule(Schedulable $scheduler)
	{
		return [
            $scheduler->daily()->hours(00)->minutes(00),
            $scheduler->args([])->everyMinutes(20),

        ];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->plazos->actualizarOficiosAutorizados();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

}
