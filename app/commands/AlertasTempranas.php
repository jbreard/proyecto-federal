<?php

use Illuminate\Support\Facades\App;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AlertasTempranas extends ScheduledCommand {

	protected $name = 'alertastempranas';


	protected $description = 'Verifica las Alertas Tempranas';

    private $plazos;


	public function __construct()
	{
		parent::__construct();
        $this->plazos = App::make('Oficios\Scheduled\AlertasTempranas\AlertasTempranasSchedulued');
    }

	public function schedule(Schedulable $scheduler)
	{
		return [
            $scheduler->everyMinutes(1),
            $scheduler->args([])->everyMinutes(1),

        ];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->plazos->fire();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

}
