<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FailedJobs extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'jobs:failed';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Agrega de a  50  las tareas fallidas';
    private $failedJobs;


    public function __construct()
	{
		parent::__construct();
        $this->failedJobs = App::make('Oficios\Scheduled\Jobs\FailedJobs');

    }


	public function schedule(Schedulable $scheduler)
	{

        return [
        $scheduler->daily()->hours(00)->minutes(00),
        $scheduler->args([])->everyMinutes(60),
        ];
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
	    $this->failedJobs->correrPendientes();
	}



}
