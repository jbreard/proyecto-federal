<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigracionLeidos extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migracion:leidos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Migra mensajes vistos a destinatarios.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$q = "	SELECT count('*') as total,id_mensaje, id_usuario, organismos.id AS id_organismo FROM mensajes_vistos
				INNER JOIN usuarios ON usuarios.id = mensajes_vistos.id_usuario
				INNER JOIN organismos ON organismos.id = usuarios.id_jurisdiccion";
		$cantidad = \DB::select($q);

		$cantidad = $cantidad[0]->total;

		$limit = 50000;

		$ciclos   = ceil($cantidad/$limit);

		for ($i=0; $i < $ciclos ; $i++) { 
			$inicio = $i*$limit;		
		
		$q = "SELECT id_mensaje, id_usuario, organismos.id AS id_organismo
FROM mensajes_vistos
INNER JOIN usuarios ON usuarios.id = mensajes_vistos.id_usuario
INNER JOIN organismos ON organismos.id = usuarios.id_jurisdiccion
GROUP BY id_mensaje, id_organismo
ORDER BY  `mensajes_vistos`.`id_mensaje` ASC LIMIT $i,50000 
			";

		$mensajes = \DB::select($q);
		$arrayIdMensaje   = [];
		$arrayIdOrganismo = [];

		$destinatario =  new Oficios\Entities\DestinatariosMensajes();
		foreach ($mensajes as $m) {
			$arrayIdMensaje[$m->id_mensaje]     = $m->id_mensaje;
			$arrayIdOrganismo[$m->id_organismo] = $m->id_organismo;			
			
		}

		
        $d = $destinatario->whereIn('id_mensaje',$arrayIdMensaje)->whereIn('id_organismo',$arrayIdOrganismo)->where('leido','=',0)->update(['leido'=>1]);		
			
		//if(!empty($d)){
		//	$d->leido = 1;
		//	$d->save();	
		//}
        }

	}

	


}
