<?php

use Illuminate\Support\Facades\App;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OficiosSinEfectoCommand extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'oficios:sinefecto';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deja sin Efecto a los Oficios Vencidos.';
    /**
     * @var OficiosSinEfectos
     */
    private $oficiosSinEfectos;

    /**
     * Create a new command instance.
     *
     * @return \OficiosSinEfectoCommand
     */
	public function __construct()
	{
		parent::__construct();
        $this->oficiosSinEfectos = App::make('Oficios\Scheduled\OficiosSinEfecto\OficiosSinEfectos');
    }

	public function schedule(Schedulable $scheduler)
	{
		return [
            $scheduler->daily()->hours(00)->minutes(00),
            $scheduler->args([])->daily()->hours(10)->minutes(9),
            $scheduler->args([])
        ];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->oficiosSinEfectos->dejarSinEfecto();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

}
