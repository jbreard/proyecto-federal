<?php

use Illuminate\Support\Facades\App;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EliminarPlazosAutorizadosCommands extends ScheduledCommand {

	protected $name = 'oficios:eliminarplazo';


	protected $description = 'Elimina Plazo Vencidos.';

    private $plazos;


	public function __construct()
	{
		parent::__construct();
        $this->plazos = App::make('Oficios\Scheduled\OficiosPlazosAutorizados\OficiosPlazosAutorizados');
    }

	public function schedule(Schedulable $scheduler)
	{
		return [
            $scheduler->daily()->hours(00)->minutes(00),
            $scheduler->args([])->everyMinutes(60),

        ];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->plazos->eliminarPlazos();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

}
