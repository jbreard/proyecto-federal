<?php

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Oficios\Repositories\OficiosRepo;

class TransaccionPrimariaSISEN extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transaccion:01';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Obtiene los datos de la base de sifcop para la trasaccion al SISEN y los deja en un json';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();		
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$oficiosRepo = new OficiosRepo();

#		$oficios = $oficiosRepo->getDataForTransactionFlowFirstQuery();
		

		$this->info("select Personas");
		$personas = $oficiosRepo->getDataForTransactionFlowFirstQueryPersonas();


		$this->info("select Vehiculos");
		$vehiculos = $oficiosRepo->getDataForTransactionFlowFirstQueryVehiculos();  
#		$vehiculos = [];
		
		$oficios = array_merge($personas,$vehiculos);


		$count = count($oficios);

		$this->info("Cantidad de registros: {$count}");
		
		$chunk = array_chunk($oficios, 1000);

		$limit = count($chunk);
		
		$filename = storage_path("json/transaccion.json");

		File::put($filename, '');

		$this->info("Nombre del archivo: {$filename}");

		for($i=0;$i<$limit;$i++)
		{
			$block = $chunk[$i];

			Queue::push('Oficios\Colas\TransaccionPrimariaChunkCola@send',['block' => $block,'lote'=>$i]);

			$this->info("Lote en cola: #{$i}");
		}
		
	}


}
