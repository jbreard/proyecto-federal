<?php namespace Interconsultas\Interfaces;


interface ClientInterface {
    /**
     * @param $data
     * @return mixed
     */
    public function call($data);
}