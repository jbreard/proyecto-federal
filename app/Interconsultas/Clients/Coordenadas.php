<?php
namespace Interconsultas\Clients;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Interconsultas\Interfaces\ClientInterface;
use Auth;
use Usuarios\Entities\User;

/**
 * Created by PhpStorm.
 * User: colma
 * Date: 10/8/2018
 * Time: 13:42
 */

class Coordenadas implements ClientInterface
{
    protected $client;

    /**
     * Persona constructor.
     */
    public function __construct($datos)
    {
        $host = Config::get('apiinterconsultas.coordenadas');
        $system = Config::get('apiinterconsultas.system');
        $user = User::find(Auth::user()->id);

        try {
            $this->client = new Client([
                'base_uri' => $host,
                'connect_timeout' => 5,
            ]);
//dd( $this->client);

        } catch (ConnectException $exception) {
            Log::info($exception->getMessage());
        } catch (Exception $exception) {
            Log::info($exception->getMessage());
        }   
    }

    /**
     * @param $data
     * @return mixed
     */
    public function call($data)
    {
        $data['ip'] = "190.183.92.101";
        //$uri = "/personas/{$data['documento']}";
        $uri = $data['ip'];
        $response = $this->client->request('GET',$uri);
        $body = $response->getBody();
        $data = json_decode($body,true);
        if(array_key_exists('sifcop_filtrado_1_4',$data)){
             $data['Sifcop'] = $data['sifcop_filtrado_1_4'];
             unset($data['sifcop_filtrado_1_4']);
        }

        //echo "hola";
/*echo "<pre>";
        var_dump($data);
echo "</pre>";*/

        return $data;

    }

}