<?php
namespace Interconsultas\Clients;

use Exception;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Interconsultas\Interfaces\ClientInterface;
use Auth;
use Usuarios\Entities\User;


class Vehiculos implements ClientInterface
{
    protected $client;
    
    var $latitud;
    var $longitud;
    var $ip_client;
    var $dispositivo;

    /**
     * Persona constructor.
     */
    public function __construct($datos) {
        $this->latitud = $datos[2];
        $this->longitud = $datos[3];
        $this->ip_client = $datos[5];
        $this->dispositivo = $datos[4];

        $host = Config::get('apiinterconsultas.host');
        try {
            $this->client = new Client([
                'base_uri' => $host,
                'connect_timeout' => Config::get('apiinterconsultas.timeOut')
            ]);
        } catch (ConnectException $exception) {
            Log::info($exception->getMessage());
        } catch (Exception $exception) {
            Log::info($exception->getMessage());
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function call($data)
    {     

        //chequear token en archivo
        $ruta = storage_path().'/token/token.txt';
      
        $fichero = fopen($ruta,"r");
        if(filesize($ruta)) {
           $accessToken = fread($fichero,filesize($ruta));
        }else{
           $accessToken = $this->getAccessToken();
        }
        
        //dd($accessToken);        
        $res = $this->getCall($accessToken,$data);
        
        if (array_key_exists('err_code', $res)){
            $token = $this->getAccessToken();
            $respuesta = $this->getCall($token,$data);
            $body = $respuesta->getBody();
            $data = json_decode($body,true);
            
        }

        return $res;

    }
    public function getAccessToken() {
           $form_params = ["username" => Config::get('apiinterconsultas.userApi'), 
                           "password" => Config::get('apiinterconsultas.passApi')];

            $uri = "api/login";
            $response = $this->client->request('POST', $uri,
                    ['form_params' => $form_params]);

            $data_res = json_decode($response->getBody(), true);

            $accessToken = $data_res['token']['access_token'];

            //obtengo el token  y guardo en archivo
	    $token_path=storage_path().'/token/token.txt';
           echo $token_path;
           $file=fopen($token_path,'w');
           // fwrite($file,$accessToken);
            try{
            fwrite($file,$accessToken);
            }catch (Exception $e){
                //echo "dsad";
    	        $data['err'] = true;
    	        $data['err_code'] = $e->getCode();
    	        $data['err_msg'] = $e->getMessage();
    	        $data['status_code'] =  Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR;
              
            }  
            fclose($file);
            return $accessToken;
    }
    
    public function getCall($accessToken,$data){
        $system = Config::get('apiinterconsultas.system');
        $user = User::find(Auth::user()->id);
               $headers = [
                    'X-Sistema' => $system,
                    'X-Organismo' => $user->getJurisdiccion->descripcion,
                    'X-Identificador' => $user->id,
                    'X-TipoIdentificador' => $this->dispositivo,
                    'X-ApellidoConsultante' => $user->apellido,
                    'X-NombreConsultante' => $user->nombre,
                    'X-DniConsultante' => $user->dni,
                    'X-Version' => '1',
                    'X-Lat' => $this->latitud,
                    'X-Lon' => $this->longitud,
                    'X-ip' => $this->ip_client,
                    'X-Renaper' => '1',
                    'X-Pfa' => '1',
                    'X-Dnrpa-V1' => '1',
                    'X-Pna' => '1',
                    'X-Gna' => '1',
                    'X-Cosef' => '1',
                    'X-Sifcop' => '1',
                    'X-SifcopTotal' => 'false',
                    'X-Sifcop' => '1',
                    'X-Renavese' => 'false',
                    'X-Sistrata-Imputados' => 'false',
                    'X-Dnrpa' => '1',
                    'X-Pba' => '1',
                    'Authorization' => 'Bearer '.$accessToken
        ];
//dd($headers);
        
        $options = [
                    'multipart' => [
                      [
                        'name' => 'username',
                        'contents' => 'admin'
                      ]
                    ]];
        $dominio = trim($data['dominio']);
        $uri = "api/vehiculos/".$dominio;
        Log::info($headers);
        Log::info($uri);
        $request = new Request('GET', $uri, $headers);
        try{
        $res = $this->client->send($request, $options);

        $body = $res->getBody(); 
        
        $dato = json_decode($body,true);
        //return $data;
        }catch(Exception $e){
            $dato['err'] = true;
            $dato['err_code'] = $e->getCode();
            $dato['err_msg'] = $e->getMessage();
            Log::error($dato);
            return $dato;
        }
        // var_dump($dato);
        return $dato;
       
    }

}
