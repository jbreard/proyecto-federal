<?php namespace Interconsultas\Repositories;

abstract class BaseRepo {

    protected $model;

    abstract public function getModel();

    function __construct(){

        $this->model = $this->getModel();

    }
}