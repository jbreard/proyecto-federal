<?php
/**
 * Created by PhpStorm.
 * User: colma
 * Date: 9/8/2018
 * Time: 19:23
 */

namespace Interconsultas\Repositories;


use Interconsultas\Interfaces\ClientInterface;

class InterconsultasRepo extends BaseRepo
{
    protected $client;

    /**
     * InterconsultasRepo constructor.
     * @param $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;

        parent::__construct();
    }

    public function getModel()
    {
        // TODO: Implement getModel() method.
    }

    public function get($endpoint,$data) {
        return call_user_func([$this,$endpoint],$data);
    }

    private function personas($data) {        
        return $this->client->call($data);
    }

    private function vehiculos($data) {        
        return $this->client->call($data);
    }

    private function armas($data) {        
        return $this->client->call($data);
    }
    
    private function coordenadas($data) {        
        return $this->client->call($data);
    }
}