<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/08/15
 * Time: 16:08
 */
namespace Administracion\Repositories;
use Administracion\Entities\MiembroxOperador;
use Combos\Repositories\BaseRepo;

class RepoMiembroxOperador extends BaseRepo {

    function getModel()
    {
        // TODO: Implement getModel() method.
        return new MiembroxOperador();

    }

    public function nuevoMiembroxOperador()
    {
        return $this->getModel();
    }

    public function agregarOperador($id_miembro, $operador=array())
    {
        $this->getModel()->insert(array(
                'id_miembro' => $id_miembro,
                'id_operador' => $operador['id_operador'],
                'nombre_operador' => $operador['nombre_operador']
            )
        );
    }

    public function darOperadoresXMiembro($id_miembro)
    {
        return $this->getModel()->where("id_miembro",$id_miembro)->get();
    }

    public function eliminarOperadorXMiembro($id_miembro)
    {
        $this->getModel()->where("id_miembro",$id_miembro)->delete();
    }
}