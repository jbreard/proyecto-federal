<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/08/15
 * Time: 14:19
 */
namespace Administracion\Repositories;
use Administracion\Entities\Miembro;
use Combos\Repositories\BaseRepo;

class RepoMiembro extends BaseRepo {

    function getModel()
    {
        // TODO: Implement getModel() method.
        return new Miembro();

    }

    public function nuevoMiembro()
    {
        return $this->getModel();
    }

    public function firstOrNew($id){
    	return $this->model->firstOrNew($id);
    }
}