<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/08/15
 * Time: 16:09
 */
namespace Administracion\Repositories;
use Administracion\Entities\MiembroxResponsable;
use Combos\Repositories\BaseRepo;

class RepoMiembroxResponsable extends BaseRepo {

    function getModel()
    {
        // TODO: Implement getModel() method.
        return new MiembroxResponsable();

    }

    public function nuevoMiembroxResponsable()
    {
        return $this->getModel();
    }

    public function agregarResponsable($id_miembro, $responsable=array())
    {
        $this->getModel()->insert(array(
                'id_miembro' => $id_miembro,
                'id_responsable' => $responsable['id_responsable'],
                'nombre_responsable' => $responsable['nombre_responsable'],
                'email_responsable' => $responsable['email_responsable'],
                'telefono_responsable' => $responsable['telefono_responsable'],
            )
        );

    }

    public function darResponsablesXMiembro($id_miembro)
    {
        return $this->getModel()->where('id_miembro',$id_miembro)->get();
    }

    public function eliminarResponsableXMiembro($id_miembro)
    {
        $this->getModel()->where("id_miembro",$id_miembro)->delete();
    }
}