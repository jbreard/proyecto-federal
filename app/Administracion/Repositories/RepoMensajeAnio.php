<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 04/09/15
 * Time: 12:20
 */
namespace Administracion\Repositories;
use Administracion\Entities\MensajeAnio;
use Combos\Repositories\BaseRepo;

class RepoMensajeAnio extends BaseRepo {

    function getModel()
    {
        // TODO: Implement getModel() method.
        return new MensajeAnio();

    }

    public function nuevoMensaje()
    {
        return $this->getModel();
    }

    public function darMensajeActual()
    {
        $anio = $this->getModel()->max('anio');
        $obj  =  $this->getModel()->where("anio",$anio)->first();

        return $obj->anio." - ".$obj->mensaje;
    }
}