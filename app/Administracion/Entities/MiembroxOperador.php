<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/08/15
 * Time: 14:41
 */
namespace Administracion\Entities;


class MiembroxOperador extends \Eloquent {

    protected $table = "miembrosxoperadores";
    protected $fillable = ["id","id_miembro","nombre_operador","created_at","updated_at"];
}