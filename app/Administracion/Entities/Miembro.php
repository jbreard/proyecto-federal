<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 11/08/15
 * Time: 15:30
 */

namespace Administracion\Entities;


class Miembro extends \Eloquent {

    protected $fillable = ["id","id_organismo","nombre_area","direccion_area","telefono_area","email_area","habilitado","created_at","updated_at"];


    public function getOrganismo(){
        return $this->hasOne('Combos\Entities\CombosOrganismos','id','id_organismo');
    }


    public function responsables(){

    	return $this->hasMany('Administracion\Entities\MiembroxResponsable','id_miembro','id');

    }

    public function operadores(){
    	return $this->hasMany('Administracion\Entities\MiembroxOperador','id_miembro','id');
    }


}