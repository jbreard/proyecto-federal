<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 04/09/15
 * Time: 12:19
 */
namespace Administracion\Entities;


class MensajeAnio extends \Eloquent {

    protected $fillable = ["anio","mensaje"];
    protected $table = "mensajes_anios";

    public function getOrganismo(){
        return $this->hasOne('Combos\Entities\CombosOrganismos','id','id_organismo');
    }

}