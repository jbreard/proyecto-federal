<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 12/08/15
 * Time: 14:39
 */

namespace Administracion\Entities;


class MiembroxResponsable extends \Eloquent {

    protected $table = "miembrosxresponsables";
    protected $fillable = ["id","id_miembro","nombre_responsable","telefono_responsable","email_responsable","created_at","updated_at"];
}