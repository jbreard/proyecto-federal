<?php
namespace Administracion\Manager;

class ManagerMiembro extends ManagerBase{

    public function getRules()
    {

        return [
            "id_organismo"=>"required",
            "nombre_area"=>"required|unique:miembros,id,".$this->getEntity()->id,
            "direccion_area"=>"required",
            "telefono_area"=>"required",
            "email_area"=>"required | email"
        ];
    }

    public function getMessages()
    {
        return [
            'id_jurisdiccion.required' => 'El campo organismo es obligatorio',
            'nombre_area.required' => 'El campo nombre de oficina/area es obligatorio',
            'nombre_area.unique' => 'Ya existe una oficina/area con ese nombre',
            'direccion_area.required' => 'El campo direccion de oficina/area es obligatorio',
            'telefono_area.required' => 'El campo telefono de oficina/area es obligatorio',
            'email_area.email' => 'El campo email de oficina/area debe tener formato de email',
            'email_area.required' => 'El campo email de oficina/area es obligatorio'
        ];
    }

}