<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 07/01/15
 * Time: 10:41
 */

namespace Administracion\Manager;



abstract class ManagerBase {

    protected $entity;
    protected $data;
    protected $errors;

    protected function getData()
    {
        return $this->data;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function __construct($entity, $data){

        $this->entity = $entity;
        //$this->data   = array_only($data,array_keys($this->getRules()));
        $this->data   = $data;

    }

    abstract public function getRules();

    public function isValid(){
        $rules        = $this->getRules();
        $messages     = $this->getMessages();
//        dd($this->data);
        $validation   = \Validator::make($this->data,$rules,$messages);
        $isValid      = $validation->passes();
        $this->errors = $validation->messages();

        return $this->mostrarErrores($isValid);
    }

    public function mostrarErrores($isValid)
    {
        $validacion = ["valid"=>true,"mensajes"=>null];
        $mensaje    = null;
        
        if(!$isValid)
        {
            foreach ($this->getErrors()->all() as $error)
            {
                $mensaje .= $error."<br>";
            }
            $validacion["valid"] = false;
            $validacion["mensajes"] = $mensaje;

        }
        return $validacion;
    }

    public function getErrors(){

        return $this->errors;

    }



    public function save(){
        $this->entity->fill($this->data);
        if($this->entity->save()) return true;

        return false;
    }



} 