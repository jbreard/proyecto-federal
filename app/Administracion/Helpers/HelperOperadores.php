<?php
namespace Administracion\Helpers;
class HelperOperadores
{
    public function agregarOperador()
    {
        $operador = $_POST['nombre_operador'];
        $lista = \Session::get("operadores");
        if(count($lista)>0)
        {
            $ban = true;
            foreach($lista as $l)
            {
                if($l == $operador)
                    $ban = false;
            }
            if($ban)
            {
                $array_operador = array();
                $array_operador['nombre_operador'] = $operador;
                $array_operador['id_operador'] = count(\Session::get("operadores"))+1;

                \Session::push("operadores",$array_operador);
            }
        }
        else
        {
            $array_operador = array();
            $array_operador['id_operador'] = 1;
            $array_operador['nombre_operador'] = $operador;

            \Session::push("operadores",$array_operador);
        }

        $lista = \Session::get("operadores");

//        dd($lista);
        return static::listadoOperadores($lista);
    }

    public function listadoOperadores($lista)
    {
        if(count($lista)>0)
            return \View::make("miembros/listadoOperadores",compact("lista"));
    }

    public function eliminarOperador()
    {
        $id_operador = $_POST['id_operador'];
        $lista = \Session::get("operadores");

        $lista_nueva = array();
        foreach($lista as $l)
        {
            if($l['id_operador'] != $id_operador)
                array_push($lista_nueva,$l);

        }

//        dd($lista_nueva);
        \Session::put('operadores',$lista_nueva);
        $lista = \Session::get("operadores");
        return static::listadoOperadores($lista);
    }

}


?>