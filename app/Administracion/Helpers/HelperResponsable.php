<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 13/08/15
 * Time: 10:41
 */
namespace Administracion\Helpers;
class HelperResponsable
{
    public function agregarResponsable()
    {
        $nombre_responsable = $_POST['nombre_responsable'];
        $email_responsable = $_POST['email_responsable'];
        $telefono_responsable = $_POST['telefono_responsable'];
        $lista = \Session::get("responsables");
        if(count($lista)>0)
        {
            $ban = true;
            foreach($lista as $l)
            {
                if($l['nombre_responsable'] == $nombre_responsable)
                    $ban = false;
            }
            if($ban)
            {
                $array_responsable = array();
                $array_responsable['nombre_responsable'] = $nombre_responsable;
                $array_responsable['email_responsable'] = $email_responsable;
                $array_responsable['telefono_responsable'] = $telefono_responsable;
                $array_responsable['id_responsable'] = count(\Session::get("responsables"))+1;

                \Session::push("responsables",$array_responsable);
            }
        }
        else
        {
            $array_responsable = array();
            $array_responsable['id_responsable'] = 1;
            $array_responsable['nombre_responsable'] = $nombre_responsable;
            $array_responsable['email_responsable'] = $email_responsable;
            $array_responsable['telefono_responsable'] = $telefono_responsable;

            \Session::push("responsables",$array_responsable);
        }

        $lista = \Session::get("responsables");

//        dd($lista);
        return static::listadoResponsables($lista);
    }

    public function listadoResponsables($lista)
    {
        if(count($lista)>0)
            return \View::make("miembros/listadoResponsables",compact("lista"));
    }

    public function eliminarResponsable()
    {
        $id_responsable = $_POST['id_responsable'];
        $lista = \Session::get("responsables");

        $lista_nueva = array();
        foreach($lista as $l)
        {
            if($l['id_responsable'] != $id_responsable)
                array_push($lista_nueva,$l);

        }

//        dd($lista_nueva);
        \Session::put('responsables',$lista_nueva);
        $lista = \Session::get("responsables");
        return static::listadoResponsables($lista);
    }

}


?>