<?php
/**
 * Created by PhpStorm.
 * User: juanfrsuarez
 * Date: 11/12/14
 * Time: 15:58
 */

namespace Combos\Entities;

class ComboJuzgados extends \Eloquent {
    protected $table = 'juzgados';
    protected $fillable = array('id','juzgado');
}