<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 04/11/14
 * Time: 18:44
 */

namespace Combos\Entities;


class CombosOrganismos extends \Eloquent {

    protected $table = "organismos";


    public function getCorreos(){
        return $this->hasMany('Correos\Entities\Correo','id','id_jurisdiccion');
    }

} 