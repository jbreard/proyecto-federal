<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:15
 */
class ComboSenasParticulares extends  \Eloquent{
    protected $table = "senas_particulares";
    protected $fillable = ["id_senas_particulares","senas_particulares"];
    protected $primaryKey = "id_senas_particulares";
}