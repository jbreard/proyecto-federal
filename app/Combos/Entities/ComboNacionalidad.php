<?php namespace Combos\Entities;

class ComboNacionalidad extends \Eloquent {
    protected $table = 'nacionalidades';
    protected $fillable = array('descripcion');
} 