<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 06/11/15
 * Time: 11:31
 */
class ComboEstadosCiviles extends  \Eloquent{
    protected $table = "estados_civiles";
    protected $fillable = ["id_estado_civil","estado_civil"];
    protected $primaryKey = "id_estado_civil";
}
