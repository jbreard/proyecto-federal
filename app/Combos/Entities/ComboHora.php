<?php namespace Combos\Entities;

class ComboHora extends \Eloquent {
    protected $table = 'horas';
    protected $fillable = array('descripcion');
} 