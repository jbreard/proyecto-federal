<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:14
 */
class ComboColorOjos extends  \Eloquent{
    protected $table = "color_ojos";
    protected $fillable = ["id_color_ojos","color_ojos"];
    protected $primaryKey = "id_color_ojos";
}