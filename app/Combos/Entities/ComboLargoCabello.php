<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:13
 */
class ComboLargoCabello extends  \Eloquent{
    protected $table = "largos_cabello";
    protected $fillable = ["id_largo_cabello","largo_cabello"];
    protected $primaryKey = "id_largo_cabello";
}