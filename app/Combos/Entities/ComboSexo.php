<?php namespace Combos\Entities;

class ComboSexo extends \Eloquent {
    protected $table = 'sexo';
    protected $fillable = array('descripcion');
} 