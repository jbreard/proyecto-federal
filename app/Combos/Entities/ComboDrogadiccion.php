<?php namespace Combos\Entities;

class ComboDrogadiccion extends \Eloquent {
    protected $table = 'combo_si_no';
    protected $fillable = array('descripcion');
} 