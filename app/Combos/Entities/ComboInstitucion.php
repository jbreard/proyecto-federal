<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 18/11/15
 * Time: 11:24
 */
class ComboInstitucion extends  \Eloquent{
    protected $table = "instituciones";
    protected $fillable = ["id_institucion","institucion"];
    protected $primaryKey = "id_institucion";
}