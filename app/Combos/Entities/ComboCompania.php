<?php namespace Combos\Entities;

class ComboCompania extends \Eloquent {
    protected $table = 'compania_celular';
    protected $fillable = array('descripcion');
} 