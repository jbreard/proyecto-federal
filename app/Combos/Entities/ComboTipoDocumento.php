<?php namespace Combos\Entities;

class ComboTipoDocumento extends \Eloquent {
    protected $table = 'tipo_doc';
    protected $fillable = array('descripcion');
} 