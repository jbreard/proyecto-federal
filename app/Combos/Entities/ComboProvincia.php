<?php namespace Combos\Entities;

class ComboProvincia extends \Eloquent {
    protected $table = 'provincias';
    protected $fillable = array('descripcion');
} 