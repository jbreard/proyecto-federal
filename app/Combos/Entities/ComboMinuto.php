<?php namespace Combos\Entities;

class ComboMinuto extends \Eloquent {
    protected $table = 'minutos';
    protected $fillable = array('descripcion');
} 