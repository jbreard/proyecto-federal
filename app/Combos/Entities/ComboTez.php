<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:15
 */
class ComboTez extends  \Eloquent{
    protected $table = "tez";
    protected $fillable = ["id_tez","tez"];
    protected $primaryKey = "id_tez";
}