<?php namespace Combos\Entities;

class ComboGenero extends \Eloquent {
    protected $table = 'genero';
    protected $fillable = array('descripcion');
} 