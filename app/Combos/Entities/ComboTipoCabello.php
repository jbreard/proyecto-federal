<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:12
 */
class ComboTipoCabello extends  \Eloquent{
    protected $table = "tipo_cabello";
    protected $fillable = ["id_tipo_cabello","tipo_cabello"];
    protected $primaryKey = "id_tipo_cabello";
}