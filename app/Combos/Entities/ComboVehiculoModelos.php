<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: juanfrsuarez
 * Date: 11/12/14
 * Time: 16:07
 */



class ComboVehiculoModelos extends \Eloquent {
    protected $table = 'vehiculos_modelos';
    protected $fillable = array('id','id_marca','modelo');
}