<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 15:14
 */
class ComboColorCabello extends  \Eloquent{
    protected $table = "color_cabello";
    protected $fillable = ["id_color_cabello","color_cabello"];
    protected $primaryKey = "id_color_cabello";
}