<?php
/**
 * Created by PhpStorm.
 * User: juanfrsuarez
 * Date: 11/12/14
 * Time: 15:59
 */
namespace Combos\Entities;

class ComboVehiculoMarcas extends \Eloquent {
    protected $table = 'vehiculos_marcas';
    protected $fillable = array('id','marca');
}