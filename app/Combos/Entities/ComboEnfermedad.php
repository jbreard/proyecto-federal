<?php namespace Combos\Entities;

class ComboEnfermedad extends \Eloquent {
    protected $table = 'combo_si_no';
    protected $fillable = array('descripcion');
} 