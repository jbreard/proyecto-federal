<?php namespace Combos\Entities;
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 16/11/15
 * Time: 14:58
 */
class ComboContexturaFisica extends  \Eloquent{
    protected $table = "contexturas_fisicas";
    protected $fillable = ["id_contextura_fisica","contextura_fisica"];
    protected $primaryKey = "id_contextura_fisica";
}