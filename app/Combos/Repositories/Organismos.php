<?php
/**
 * Created by PhpStorm.
 * User: fra
 * Date: 04/11/14
 * Time: 19:02
 */

namespace Combos\Repositories;


use Combos\Entities\CombosOrganismos;

class Organismos extends BaseRepo {

    public function getModel()
    {
        return new CombosOrganismos();
    }


    public function getOrganismos(){

        return $this->model->where('id','!=',1)->remember(180)->get();

    }
    public function getOrgMinseg(){

        return $this->model->where('descripcion','LIKE','%minseg%')->get(['id']);
    }

    public function getWitoutSd(){
        return $this->model->where('id','>',1)->get();
    }

     public  function getCombo(){

        return CombosOrganismos::all()->lists('descripcion','id');

    }


}