<?php


namespace Combos\Repositories;
use Combos\Entities\ComboProvincia;

class ProvinciaRepo extends BaseRepo{

    public function getModel()
    {
        return new ComboProvincia();
    }

    public  function getCombo(){

        return ComboProvincia::all()->lists('descripcion','id');

    }


} 