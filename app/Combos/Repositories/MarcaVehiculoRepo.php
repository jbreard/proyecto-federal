<?php


namespace Combos\Repositories;
use Combos\Entities\ComboVehiculoMarcas;

class MarcaVehiculoRepo extends BaseRepo{

    public function getModel()
    {
        return new ComboVehiculoMarcas();
    }

    public  function getCombo(){

        return ComboVehiculoMarcas::all()->lists('descripcion','id');

    }


}