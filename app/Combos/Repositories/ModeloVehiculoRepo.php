<?php


namespace Combos\Repositories;
use Combos\Entities\ComboVehiculoModelos;

class ModeloVehiculoRepo extends BaseRepo{

    public function getModel()
    {
        return new ComboVehiculoModelos();
    }

    public  function getCombo(){

        return ComboVehiculoModelos::all()->lists('descripcion','id');

    }

    public function getByMarca($idMarca,$campos=['*']){
        return $this->model->where('id_marca','=',$idMarca)->get($campos);

    }


}