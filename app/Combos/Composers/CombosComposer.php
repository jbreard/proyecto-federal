<?php namespace  Combos\Composers;

use Combos\Entities\ComboColorCabello;
use Combos\Entities\ComboColorOjos;
use Combos\Entities\ComboContexturaFisica;
use Combos\Entities\ComboInstitucion;
use Combos\Entities\ComboLargoCabello;
use Combos\Entities\ComboTez;
use Combos\Entities\ComboSenasParticulares;
use Combos\Entities\ComboTipoCabello;
use Combos\Entities\ComboEstadosCiviles;
use Combos\Entities\ComboTipoDocumento;
use Combos\Entities\ComboSexo;
use Combos\Entities\ComboGenero;
use Combos\Entities\ComboNacionalidad;
use Combos\Entities\ComboProvincia;
use Combos\Entities\ComboLocalidad;
use Combos\Entities\ComboDepartamento;
use Combos\Entities\ComboCompania;
use Combos\Entities\ComboHora;
use Combos\Entities\ComboMinuto;
use Combos\Entities\ComboSinoEtitie;
use Combos\Entities\ComboTipoVehiculo;
use Combos\Entities\ComboVehiculoModelos;
use Oficios\Entities\EstadoActual;
use Oficios\Entities\Procedencia;
use Oficios\Entities\TiposOficios;
use Combos\Entities\CombosOrganismos;
use Combos\Entities\ComboFiscalias;
use Combos\Entities\ComboVehiculoMarcas;
use Combos\Entities\ComboJuzgados;
use Oficios\Entities\VictimaIlicito;
use Oficios\Entities\Vinculo;
use Oficios\Repositories\TiposOficiosRepo;


class CombosComposer   {



    public function compose($view)
    {
        $timeCache = 60;//minutos

        $comboAnio = $this->getSelectAnio();

//        dd($comboAnio);
        $comboJuzgados      = ComboJuzgados::orderBy('id','ASC')->remember($timeCache)->lists('juzgado','id');
        $comboTipoDocumento = ComboTipoDocumento::remember($timeCache)->lists('descripcion','id');
        $comboSexo          = ComboSexo::orderBy('id','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboGenero        = ComboGenero::orderBy('id','DESC')->remember($timeCache)->lists('descripcion','id');
        $comboNacionalidad  = ComboNacionalidad::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboProvincia     = ComboProvincia::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboDepartamento  = ComboDepartamento::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        // $comboLocalidad  = ComboLocalidad::orderBy('descripcion','ASC')->lists('descripcion','id');
        $comboCompania      = ComboCompania::remember($timeCache)->lists('descripcion','id');
        $comboHora          = ComboHora::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboMinuto        = ComboMinuto::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboTipoVehiculo  = ComboTipoVehiculo::orderBy('id','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboEstadosCiviles= ComboEstadosCiviles::orderBy('id_estado_civil','ASC')->remember($timeCache)->lists('estado_civil','id_estado_civil');
        $comboLargosCabello = ComboLargoCabello::orderBy('id_largo_cabello','ASC')->remember($timeCache)->lists('largo_cabello','id_largo_cabello');
        $comboVinculos      = Vinculo::orderBy('id_vinculo','ASC')->remember($timeCache)->lists('vinculo','id_vinculo');


        //$comboSiNo  = [''=>'','1'=>'Si','0'=>'No'];
        $comboSiNo          = ComboSinoEtitie::orderBy('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');

        $combosOrganismos = CombosOrganismos::orderby('descripcion','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboVehiculosMarcas = ComboVehiculoMarcas::orderby('marca','ASC')->remember($timeCache)->lists('marca','id');

//        $comboVehiculosModelos = [];
        $comboVehiculosModelos = ComboVehiculoModelos::orderby('modelo','ASC')->remember($timeCache)->lists('modelo','id');

        $combosFiscalias = ComboFiscalias::orderby('id','ASC')->remember($timeCache)->lists('fiscalias','id');

        $comboTipoOficios = TiposOficios::orderBy('orden','ASC')->remember($timeCache)->lists('descripcion','id');
        
        $comboContexturasFisicas  = ComboContexturaFisica::orderBy('id_contextura_fisica','DESC')->remember($timeCache)->lists('contextura_fisica','id_contextura_fisica');
        $comboTipoCabello  = ComboTipoCabello::orderBy('id_tipo_cabello','DESC')->remember($timeCache)->lists('tipo_cabello','id_tipo_cabello');
//        $comboLargoCabello  = ComboLargoCabello::orderBy('largo_cabello','ASC')->remember($timeCache)->lists('largo_cabello','id_largo_cabello');
        $comboColorCabello  = ComboColorCabello::orderBy('id_color_cabello','DESC')->remember($timeCache)->lists('color_cabello','id_color_cabello');
        $comboColorOjos  = ComboColorOjos::orderBy('id_color_ojos','DESC')->remember($timeCache)->lists('color_ojos','id_color_ojos');
        $comboTez  = ComboTez::orderBy('id_tez','DESC')->remember($timeCache)->lists('tez','id_tez');
        $comboSenasParticulares  = ComboSenasParticulares::orderBy('id_senas_particulares','DESC')->remember($timeCache)->lists('senas_particulares','id_senas_particulares');
        $comboinstituciones  = ComboInstitucion::orderBy('institucion','ASC')->remember($timeCache)->lists('institucion','id_institucion');
        $comboProcedencias  = Procedencia::orderBy('id','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboEstadosActuales  = EstadoActual::orderBy('id','ASC')->remember($timeCache)->lists('descripcion','id');
        $comboVictimasIlicitos  = VictimaIlicito::orderBy('id','ASC')->remember($timeCache)->lists('descripcion','id');

        $view->with('combosFiscalias',$combosFiscalias);
        $view->with('comboVehiculosMarcas',$comboVehiculosMarcas);
        $view->with('comboVehiculosModelos',$comboVehiculosModelos);
        $view->with('comboTipoDocumento',$comboTipoDocumento);
        $view->with('comboSexo',$comboSexo);
        $view->with('comboGenero',$comboGenero);
        $view->with('comboNacionalidad',$comboNacionalidad);
        $view->with('comboProvincia',$comboProvincia);
        $view->with('comboDepartamento',$comboDepartamento);
        //$view->with('comboLocalidad',$comboLocalidad);
        $view->with('comboCompania',$comboCompania);
        $view->with('comboHora',$comboHora);
        $view->with('comboMinuto',$comboMinuto);
        $view->with('comboTipoOficios',$comboTipoOficios);
        $view->with('comboSiNo',$comboSiNo);
        $view->with('combosOrganismos',$combosOrganismos);
        $view->with('comboEstadosCiviles',$comboEstadosCiviles);
        $view->with('comboJuzgados',$comboJuzgados);
        $view->with('comboTipoVehiculo',$comboTipoVehiculo);
        $view->with('comboContexturasFisicas',$comboContexturasFisicas);
        $view->with('comboTipoCabello',$comboTipoCabello);
//        $view->with('comboLargoCabello',$comboLargoCabello);
        $view->with('comboColorCabello',$comboColorCabello);
        $view->with('comboColorOjos',$comboColorOjos);
        $view->with('comboTez',$comboTez);
        $view->with('comboSenasParticulares',$comboSenasParticulares);
        $view->with('comboInstituciones',$comboinstituciones);
        $view->with('comboLargosCabello',$comboLargosCabello);
        $view->with('comboVinculos',$comboVinculos);
        $view->with('comboProcedencias',$comboProcedencias);
        $view->with('comboEstadosActuales',$comboEstadosActuales);
        $view->with('comboVictimasIlicitos',$comboVictimasIlicitos);
        $view->with('comboAnio',$comboAnio);

        //Combos Si/No
        /* $view->with('comboPrimeraDesaparece',$comboPrimeraDesaparece);
         $view->with('comboEnfermedad',$comboEnfermedad);
         $view->with('comboDrogadiccion',$comboDrogadiccion);
         $view->with('comboInternadoInstitucion',$comboInternadoInstitucion);*/

    }

    function getSelectAnio()
    {

        $array = array();

        for($anio=((int)date("Y")); 1970<=$anio; $anio--)
            $array[$anio] = $anio;

        return $array;

    }

}
