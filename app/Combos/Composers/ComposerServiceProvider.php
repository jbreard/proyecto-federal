<?php 
namespace Combos\Composers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    public function register(){

        $this->app->view->composer('listados.oficios', 'Combos\Composers\CombosComposer');
        $this->app->view->composer('consultas_positivas.detail', 'Combos\Composers\CombosComposer');
        $this->app->view->composer('formularios.*', 'Combos\Composers\CombosComposer');
        $this->app->view->composer('estadisticas.personas_extraviadas', 'Combos\Composers\CombosComposer');
        $this->app->view->composer('usuarios.create', 'Combos\Composers\CombosComposer');
        $this->app->view->composer('mails.create', 'Combos\Composers\CombosComposer');

    }

} 