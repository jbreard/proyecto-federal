<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 24/06/14
 * Time: 11:29
 */

namespace Correos\Entities;

use Oficios\ClasesAbstractas\BusquedaAbstracta;

class Correo extends BusquedaAbstracta
{
    protected $table = 'lista_mails';
    protected $fillable = array('id','descripcion','mail','id_jurisdiccion');

    public function getOrganismo(){
        return $this->hasOne('Combos\Entities\CombosOrganismos','id','id_jurisdiccion');
    }

    function getFields()
    {
        // TODO: Implement getFields() method.
        return $this->fillableM;
    }

    function relations()
    {
        return ['getOrganismo',];
    }

    function relationsPivot()
    {
        return [];
    }
}
?>