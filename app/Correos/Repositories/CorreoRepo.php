<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 24/06/14
 * Time: 11:28
 */

namespace Correos\Repositories;

use Correos\Entities\Correo;


class CorreoRepo extends BaseRepo {

    public function getModel()
    {
        return new Correo();
    }

    public function  getCorreo()
    {


    }

    public function getCorreoByTerm($term){

        return
            $this->model->where('mail','LIKE',"$term%")->get(['mail']);
    }

    public function newCorreo()
    {
        $correo            = $this->getModel();
        return $correo;
    }

    public function getMailByMail($mail){
        return $this->model->where('mail',$mail)->first();
    }

    public function getMails(){
        return $this->model->remember(60)->get(['mail'])->toArray();
    }

    public function getMailByIdOrganismos(&$idOrganismos)
    {
        $mails = $this->getModel();
        foreach($idOrganismos as $idOrganismo) $mails = $mails->orWhere('id_jurisdiccion','=',$idOrganismo);
        $emails = $mails->get(['mail'])->toArray();
        return $emails;
    }


}