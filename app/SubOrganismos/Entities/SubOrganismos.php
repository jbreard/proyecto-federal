<?php namespace SubOrganismos\Entities;

class SubOrganismos extends \Eloquent
{
    protected $table = 'suborganismos';
    protected $fillable = array('id','id_organismo','descripcion');

	public function getOrganismo(){
        return $this->hasOne('Combos\Entities\CombosOrganismos','id','id_organismo');
    }

    function getFields()
    {
        return $this->fillable;
    }

    function relations()
    {
        return ['getOrganismo',];
    }

    function relationsPivot()
    {
        return [];
    }
}
