<?php

namespace SubOrganismos\Repositories;

use SubOrganismos\Entities\SubOrganismos; 


class SubOrganismosRepo extends BaseRepo
 {

    public function getModel()
    {
        return new SubOrganismos();
    }

   

    public function newOrganismo()
    {
        return $this->getModel();
    }


    public function getOrganismo()
    {
        return $this->model->remember(60)->get(['organismo'])->toArray();
    }
    public function comboSubOrganismoByIdOrg($idOrganismo){
        return $this->getSubOrganismoByIdOrgScope($idOrganismo)->lists('descripcion','id');
    }
    public function getSubOrganismoByIdOrgScope($idOrganismo){
        return $this->model->where('id_organismo','=',$idOrganismo);
    }
    public function getSubOrganismoByIdOrg($idOrganismo){
        return $this->getSubOrganismoByIdOrgScope($idOrganismo)->orderBy('descripcion', 'ASC')->get()->toArray();
    }
    



}