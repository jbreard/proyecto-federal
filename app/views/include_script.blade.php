{{ HTML::script('socket.io.js') }}
<!-- javascript
================================================== -->
{{ HTML::script('assets/js/jquery.js') }}
{{ HTML::script('assets/js/jquery.maskedinput.min.js') }}

{{ HTML::script('assets/js/jquery-ui.min.js') }}
<!-- Bootstrap -->
<!-- <script src="bootstrap/js/bootstrap.min.js"></script> -->
{{ HTML::script('bootstrap/bootstrap/js/bootstrap.min.js') }}

<!-- Modernizr -->
<!-- <script src='js/modernizr.min.js'></script> -->
{{ HTML::script('bootstrap/js/modernizr.min.js') }}

{{ HTML::script('bootstrap/js/bootstrap-datepicker.min.js') }}

{{ HTML::script('bootstrap/js/bootstrap-timepicker.min.js') }}

{{ HTML::script('bootstrap/js/jquery.gritter.min.js') }}
{{ HTML::script('bootstrap/js/modernizr.min.js') }}

<!-- Pace -->
<!-- <script src='js/pace.min.js'></script> -->
{{ HTML::script('bootstrap/js/pace.min.js') }}
<!-- Popup Overlay -->
<!-- <script src='js/jquery.popupoverlay.min.js'></script> -->
{{ HTML::script('bootstrap/js/jquery.popupoverlay.min.js') }}
<!-- <script src='js/jquery.slimscroll.min.js'></script>-->
{{ HTML::script('bootstrap/js/jquery.slimscroll.min.js') }}
<!-- Cookie -->
<!-- <script src='js/jquery.cookie.min.js'></script>-->
{{ HTML::script('bootstrap/js/jquery.cookie.min.js') }}
<!-- Endless -->
<!-- <script src="js/endless/endless.js"></script>-->
{{ HTML::script('bootstrap/js/endless/endless.js') }}
{{ HTML::script('assets/js/validacion_oficio.js') }}
{{ HTML::script('assets/js/notificaciones.js') }}
{{ HTML::script('assets/js/helper.js') }}
{{ HTML::script('assets/js/main.js') }}
{{ HTML::script('assets/js/partidoProvincia.js') }}
{{ HTML::script('assets/js/buscar_dni.js') }}
{{ HTML::script('assets/js/buscar_patente.js') }}
{{ HTML::script('assets/js/jquery.validate.min.js') }}
{{ HTML::script('assets/js/inputRules.js') }}
{{ HTML::script('assets/js/messages_es_AR.js') }}
{{ HTML::script('assets/js/bootbox.js') }}
{{ HTML::script('assets/js/vehiculos.js') }}
{{ HTML::script('assets/select2/select2.min.js') }}
{{ HTML::script('assets/select2/select2_locale_es.js') }}
{{ HTML::script('assets/sweetalert/dist/sweetalert.min.js') }}
{{ HTML::script('assets/bootstrap-notify/dist/bootstrap-notify.min.js') }}
{{ HTML::script('assets/bootstrap3-editable/bootstrap3-editable/js/bootstrap-editable.js') }}
<input type="hidden" id="sessionId" value="{{\Auth::user()->last_sessid}}">
<script>

    var socket = io('{{Config::get('nodejs.socket')}}');
    socket.on('oficio_nuevo', function (data) {
        notificaciones(data);
    });
    socket.on('oficio_modificado', function (data) {
        notificaciones(data);
    });
    $(function () {
        //socket.emit('register',{'id_session':"{{ Auth::user()->id }}"});
        var juzgadoFiscalia = function (el,valorComparacion){

            var valor = el.val();
            var destino  = el.data('destino');
            var juzgadoOtro = $(destino);

            if(valor == valorComparacion) return juzgadoOtro.removeAttr("disabled");
            juzgadoOtro.val('');
            return juzgadoOtro.attr("disabled","disabled");
        }


    var util = new Helper();
   // var socket = io('https://173.10.0.99:3000');
    function notificacionWeb(mensaje){
        $.gritter.add({
            title: '<i class="fa fa-info-circle"></i> Nuevo Requerimiento Judicial',
            text: mensaje,
            sticky: false,
            time: '',
            class_name: 'gritter-info'
        });

        return false;
    }

    function cantMod(){
//        var url  = util.getUrl('oficios/cantMod');
        var url  = "{{route('oficios.cantidad_mod')}}";
        util.ajax(null,'POST',url,null,function(data){
            var data = jQuery.parseJSON(data);
            $(".req_mod").html(data);
        });
    }


    function cantModDetalle(){
        var url ="{{route('oficios.cantidad_mod_detalle')}}";
        util.ajax(null,'POST',url,null,function(cantidad){
            console.log(cantidad);
            var cantidad = jQuery.parseJSON(cantidad);
            $.each(cantidad, function( index, value ) {
                $("#notificacion_modificados_"+value.tipo_oficio).html(value.cantidad);
            });
        });

    }

    function getCantidadOficios(){
        var url = "{{route('oficios.no_leidos')}}";

        util.ajax(null,'GET',url,null,function(cantidad){
            var cantidad = jQuery.parseJSON(cantidad);
            $(".cantidad_no_leidos").html(cantidad[1].cantidad_oficios);
        });
    }

    function getDetalleNoLeidos(){
        var url = "{{route('oficios.no_leidos_detalle')}}";
        util.ajax(null,'GET',url,null,function(cantidad){
            var cantidad = jQuery.parseJSON(cantidad);
            $.each(cantidad[1], function( index, value ) {
                $("#notificacion_"+value.tipo_oficio).html(value.cantidad_oficios);
            });
        });
    }

    function getMailsNoLeido(){
        var url = "{{route('mensajes.cantnoleidos')}}";
        util.ajax(null,'GET',url,null,function(cantidad){
            $("#cantidadMensajesNoLeidos").html(cantidad);
        });
    }

    $("#notificaciones_no_vistas").click(function(){
        var submenu = $("#no_vistos_detalle");
        if(submenu.is(':visible')){
            getDetalleNoLeidos();
        }
    });

    $("#notificaciones_modificaciones_no_vistas").click(function(){
        var submenu = $("#modificados_detalle");
        cantModDetalle();
    });


    socket.on('oficio_cesado', function (data) {
        var jdata = {mensaje:"Oficio Sin Efecto",ruta:data.id_oficio};
        notificaciones(jdata);
    });

    getCantidadOficios();
    getMailsNoLeido();
    cantMod();






        $(".agregar_foto").click(function (event) {
            event.preventDefault();
            var contenedor = $(this).attr('data-contenedor');
            var receptor = $(this).attr('data-receptor');

            var val = $(contenedor).clone().appendTo(receptor);

            // Le asigno por defecto el valor vacio
            var input = val.children().children().children()[1];
            input.value = '';
        });

        $(".select").select2();
        $(".juzgado").change(function(e){
            juzgadoFiscalia($(this),471)
        });
        $(".fiscalia").change(function(e){
            juzgadoFiscalia($(this),563)
        });
        $(".vehiculo").change(function(e){
            juzgadoFiscalia($(this),13)
        });
        $( ".marcas" ).change(function(e){
//            juzgadoFiscalia($(this),36)
            var el = $(this);
            juzgadoFiscalia($(this),13)
            otrosVehiculos(el);



        });
        $( ".modelos" ).change(function(e){
            juzgadoFiscalia($(this),1194);
        });


        $( ".juzgado" ).each(function( index ) {
             juzgadoFiscalia($(this),471)
        });
        $( ".fiscalia" ).each(function( index ) {
             juzgadoFiscalia($(this),563)
        });
        $( ".vehiculo" ).each(function( index ) {
            juzgadoFiscalia($(this),13)
        });
        $( ".marcas" ).each(function( index ) {
            var el = $(this);
            otrosVehiculos(el);

        });
        $( ".modelos" ).each(function( index ) {
            juzgadoFiscalia($(this),1194)
        })

         function otrosVehiculos(el){
            var valorComparacion = 36;
            var valor = el.val();
            juzgadoFiscalia(el,valorComparacion);
            if(valor == valorComparacion) return $("#otro_modelo").removeAttr("disabled");
        }

        function registrarSocket(){
            var ssid = $("#sessionId").val();
            socket.emit("connection",{"ssid":ssid});
        }

        function desloguear(){
            socket.on("desloguear",function(){
                //alert("Su cuenta fue registrada en otro dispositivo");
                swal({   title: "Atención!",   text: "Su cuenta fue registrada en otro dispositivo!",   type: "warning",   showCancelButton: false,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Aceptar",   closeOnConfirm: false }, function(){ window.location.href = "/";
 });
            });
        }

        registrarSocket();
        desloguear();



        @yield('jquery');
    });

    var idOficio = parseInt('{{Session::get('oficio_id') }}');
    @if(Session::has('oficio_id'))
        llenarCampos( idOficio , '{{ route('oficios.llenar_campos') }}' );
        {{Session::forget('oficio_id')}}
    @endif



</script>
@yield('scripts')
