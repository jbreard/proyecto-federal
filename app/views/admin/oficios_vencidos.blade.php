@extends('header')
@section('content')

	<table class="table responsive table-bordered table-hover table-striped" id="tablaOficios" style="margin-top: 1em !important">
    <thead>
    <tr>
        <td>N° Requerimiento</td>
        <td>Tipo de Medida</td>
        <td>Fecha de Carga del Req</td>
        <td>Estado</td>
        <td>Alerta Temprana</td>
        <td>Acciones</td>
    </tr>
    </thead>
    <tbody id="bodyOficios">
    @foreach ($oficios as $oficio)
    <tr class="{{ ($oficio->estado==2) ? 'danger' : 'info' }} {{ ($oficio->plazo_autorizado == 1) ? 'warning':'' }}">
        <td>{{ $oficio->id }}</td>
        <td>{{ $oficio->tipo->descripcion }} @if(($oficio->tipo->id == 9) or ($oficio->tipo->id == 5 )) ({{$oficio->medida_restrictiva}}) @endif</td>
        <td>{{ $oficio->created_at }}  </td>
        <td>
            @if($oficio->tipo_oficio==3)
                @if($oficio->estadoOficio->id == 1 && $oficio->plazo_autorizado == 1)
                    Autorizado
                @else
                    {{ $oficio->estadoOficio->descripcion }}
                @endif
            @else
                {{ $oficio->estadoOficio->descripcion }} <br>
                @if($oficio->estado == 2) {{$oficio->oficioCesado->created_at}} @endif
            @endif
        </td>
        <td>@if($oficio->alerta_temprana ==1)
            <span class="label label-danger">SI</span> @else <span class="label label-success">NO</span>@endif
        </td>
        <td>
            <a href='{{ route($oficio->tipo->ruta.".show",$oficio->id) }}' class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Ver Reporte"><i class="fa fa-eye"></i></a>
           
            <a href='{{ route($oficio->tipo->ruta.".pdf",$oficio->id) }}' target="_blank" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Descargar Reporte"><i class="fa fa-download"></i></a>

            <a href="" data-toggle="tooltip" data-placement="top" title="Marcar como revisado"><i class="fa fa-check"></i></a>
            
        </td>
     
    </tr>

    @endforeach
    


    </tbody>

</table>

@stop