<div class="modal fade" id="addOrganismo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Organismos @{{title}}</h4>
      </div>
      <div class="modal-body">

          <div class="row" style="margin-bottom:5px">
            <div class="col-md-12">
              <input type="text" v-model="filter" class="form-control" placeholder="Buscar...">
            </div>
          </div>
          <div class="row" style="margin-bottom:5px" v-repeat="organismos | filterBy filter 'descripcion'">
            <div class="col-md-10">
              <label> @{{descripcion}} </label>
            </div>
            <div class="col-md-2">
              <combos-organismos id-organismo="@{{id}}" v-ref="organismo_@{{id}}"></combos-organismos>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
<!--        <button type="button" class="btn btn-primary" v-on="click:guardarOrganismo()">Guardar »</button> -->
      </div>
    </div>
  </div>
</div>

<script type="x-template" id="combos">
  <select class="form-control organismo_si_no" id="organismo_combo_@{{idOrganismo}}" v-on="change:managerOrganismos()" v-model="select">
    <option value="0">No</option>
    <option value="1">Si</option>
  </select>
</script>

