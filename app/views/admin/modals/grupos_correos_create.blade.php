<div class="modal fade" id="createGrupo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear Grupo</h4>
      </div>
      <div class="modal-body">
          <form>
            <div class="row">
              <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Descripcion" v-model="formCreate.descripcion">
              </div>
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on="click:guardarGrupo()">Guardar »</button>
      </div>
    </div>
  </div>
</div>