@extends('header')
@section('content')

<div id="grupoCorreos">
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn-default" v-on="click:crearGrupo">Crear Grupo</button>
				
		</div>
	</div>

	<table class="table table-responsive" style="margin-top:10px;">
		<thead>
			<th>Descripcion</th>
			<th>Acciones</th>
		</thead>
		<tbody>
			<tr v-repeat="grupos">
				<td>
					<a href="#" id="descripcion_grupo" data-type="text" data-pk="1" data-url="/post" data-title="Enter username">@{{descripcion}}</a>
					
				</td>
				<td>
					<button class="btn btn-default" v-on="click:agregarOrganismo(id,descripcion)">Organismos</button>
					<!--<button class="btn btn-danger">Eliminar</button>-->
				</td>
			</tr>

		</tbody>
	</table>

	@include('admin.modals.grupos_correos_create')
	@include('admin.modals.grupos_correos_add_organismo')












</div>




@stop


@section('scripts')
{{ HTML::script('assets/js/vue.min.js') }}
{{ HTML::script('assets/js/bootstrap-switch.min.js') }}
{{ HTML::script('assets/js/gruposCorreos.js') }}

@stop

