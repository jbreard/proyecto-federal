@extends ('header')
@section('scripts')
<style>
    .row {
        margin-top: 5px !important;
    }
</style>
@stop
@section ('content')
<h2>Panel de Administración</h2>
<div class="panel panel-info">
    <div class="panel-heading">Administración de Usuarios</div>
    <div class="panel-body">
    <div class="row">
        <div class="form-group col-md-12">
            <a href={{ route('usuarios.create') }} ><button class="btn btn-primary" >Cargar Usuario <i class="fa fa-user"></i></button></a>
            <a href={{ route('usuarios.index')  }} ><button class="btn btn-primary" >Listado De Usuarios  <i class="fa fa-users"></i></button></a>
            <a href={{ route('usuarios.clave')  }} ><button class="btn btn-primary" >Cambiar Clave  <i class="fa fa-lock"></i></button></a>
            <!--<a href={{ route('estadistica.requerimientos')  }} ><button class="btn btn-primary" >Estadistica  <i class=""></i></button></a>-->
        </div>
    </div>
   </div>
 </div>

<div class="panel panel-info">
    <div class="panel-heading">Lista de Correos</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group col-md-12">
                <a href="{{ route('mails.create') }}"><button class="btn btn-info " >Cargar Mail <i class="fa fa-envelope"></i></button></a>
                <a href="{{ route('mails.index') }}"><button class="btn btn-info " >Listado De Mails  <i class="fa fa-list"></i></button></a>
                <a href="{{ route('grupo_correos.index') }}"><button class="btn btn-info " >Grupos de Correos  <i class="fa fa-list"></i></button></a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Organismos</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group col-md-12">
                <a href="{{ route('subOrganismos.create') }}"><button class="btn btn-info " >Cargar Sub-Organismo <i class="glyphicon glyphicon-book"></i></button></a>
                <a href="{{ route('subOrganismos.index') }}"><button class="btn btn-info " >Listado de Sub-Organismos <i class="fa fa-list"></i></button></a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Miembros</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group col-md-12">
                <a href={{ route('miembros.create') }} ><button class="btn btn-primary" >Cargar Miembro <i class="fa fa-user"></i></button></a>
                <a href="{{ route('miembros.index') }}"><button class="btn btn-primary " >Listado de Miembros  <i class="fa fa-users"></i></button></a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Estadísticas</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group col-md-12">
                <a href="{{ route('solicitud_paradero.estadisticas') }}"><button class="btn btn-primary " >Búsqueda de personas  <i class="fa fa fa-list"></i></button></a>
                <a href="{{ route('solicitud_paradero.estadisticas') }}"><button class="btn btn-primary " >Alerta de consulta positiva  <i class="fa fa fa-list"></i></button></a>
            </div>
        </div>
    </div>
</div>

<!--
<div class="panel panel-info">
    <div class="panel-heading">Requerimientos</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group col-md-4">
                <a href={{ route('consultas_positivas.index') }} ><button class="btn btn-primary" >Consultas Positivas<i class="fa fa-user"></i></button></a>
                
            </div>
            <div class="form-group col-md-4">
                <a href={{ route('oficios_anio.index') }} ><button class="btn btn-primary" >Requerimientos que cumplieron un año <i class="fa fa-user"></i></button></a>
                
            </div>
        </div>
    </div>
</div>-->
@stop
