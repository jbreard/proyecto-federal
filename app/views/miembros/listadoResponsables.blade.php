

<div class="panel panel-info" id="listado_archivos" data-bool="existe">
    <div class="panel-heading">Listado de responsables</div>
    <div class="panel-body">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lista as $value)
                    <tr >
                        <td style="text-transform: none">{{ $value['nombre_responsable'] }} </td>
                        <td style="text-transform: none">{{ $value['email_responsable'] }} </td>
                        <td >{{ $value['telefono_responsable'] }} </td>
                        <td><input type="button" value="Eliminar" id="{{ $value['id_responsable'] }}" class="btn btn-default btn-sm btnEliminarResponsable" /></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $().ready(function(){

        $(".btnEliminarResponsable").click(function(){
            var datos = "id_responsable="+this.id;
            var destino = "{{ Route('miembros.eliminarResponsable') }}";
            $.ajax({
                type: "Post",
                url: destino,
                data: datos,
                async: false,
                success: function(respuesta){
                    $("#div_responsables").html(respuesta);
                }

            })
        });
    });
</script>

