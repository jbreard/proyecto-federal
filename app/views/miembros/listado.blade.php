@extends ('header')
@section ('title') Listado de miembros @stop
@section ('content')
    @if(Auth::user()->id_perfil == 1 or Auth::user()->id_perfil == 5)
    <a href="{{ route('miembros.create') }}"><button class="btn btn-primary" >Cargar Miembro <i class="glyphicon glyphicon-user"></i></button></a>
    @endif

    <h2>Listados de miembros</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Organismo</th>
            <th>Area</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($miembros as $key => $value)
            <tr>
                <td>{{ $value->getOrganismo->descripcion }}</td>
                <td>{{ $value->nombre_area }}</td>
                <td>{{ $value->direccion_area }}</td>
                <td>{{ $value->telefono_area }}</td>
                <td>{{ $value->email_area }}</td>
                <td>
                    @if(Auth::user()->id_perfil == 1 or Auth::user()->id_perfil == 5)
                    <a href="{{ route('miembros.edit', $value->id) }}" class="btn btn-success btn-sm">
                        Editar
                    </a>
                    @endif
                    <a href="{{ route('miembros.show', $value->id) }}" class="btn btn-success btn-sm">
                        Ver
                    </a>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
@stop


@section('jquery')


@stop