@extends ('header')
@section ('title')Sifcop |  Datos del  Miembro @stop
@section ('content')
    <div class="panel panel-info" id="listado_archivos" data-bool="existe">
        <div class="panel-heading">Datos del Miembro</div>
        <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr><td>Organismo</td><td>{{$miembro->getOrganismo->descripcion}}</td></tr>
                    <tr><td>Nombre de la Oficina/Area</td><td>{{$miembro->nombre_area}}</td></tr>
                    <tr><td>Dirección de la Oficina/Area</td><td>{{$miembro->direccion_area}}</td></tr>
                    <tr><td>Teléfono de la Oficina/Area</td><td>{{$miembro->telefono_area}}</td></tr>
                    <tr><td>Email de la Oficina/Area</td><td>{{$miembro->email_area}}</td></tr>
                </table>
            </div>
        </div>
        </div>
    </div>

    <div class="panel panel-info" id="listado_archivos" data-bool="existe">
        <div class="panel-heading">Listado de operadores</div>
        <div class="panel-body">
            <div class="row">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Operador
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($miembro->operadores as $value)
                        <tr>
                            <td>{{ $value['nombre_operador'] }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-info" id="listado_archivos" data-bool="existe">
    <div class="panel-heading">Listado de responsables</div>
    <div class="panel-body">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($miembro->responsables as $value)
                    <tr >
                        <td style="text-transform: none">{{ $value['nombre_responsable'] }} </td>
                        <td style="text-transform: none">{{ $value['email_responsable'] }} </td>
                        <td >{{ $value['telefono_responsable'] }} </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@stop


@section('jquery')


@stop