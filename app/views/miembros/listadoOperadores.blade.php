<script>
    $().ready(function(){

        $(".btnEliminar").click(function(){
            var datos = "id_operador="+this.id;
            var destino = "{{ Route('miembros.eliminarOperador') }}";
            $.ajax({
                type: "Post",
                url: destino,
                data: datos,
                async: false,
                success: function(respuesta){
                    $("#div_operadores").html(respuesta);
                }

            })
        });
    });
</script>

<div class="panel panel-info" id="listado_archivos" data-bool="existe">
    <div class="panel-heading">Listado de operadores</div>
    <div class="panel-body">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        Operador
                    </th>
                    <th>
                        Eliminar
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($lista as $value)
                    <tr>
                        <td>{{ $value['nombre_operador'] }} </td>
                        <td><input type="button" value="Eliminar" id="{{ $value['id_operador'] }}" class="btn btn-default btn-sm btnEliminar" /></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

