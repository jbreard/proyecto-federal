@extends ('header')
<style>
    input{
        text-transform: none !important;
    }
</style>

@section('scripts')
<script>
    $().ready(function () {

        @if($model->id)
            var destino = "{{ Route('miembros.listadoResponsables') }}";
            $.ajax({
                type: "Post",
                url: destino,
                async: false,
                success: function(respuesta){
                    $("#div_responsables").html(respuesta);
                }
            });

            var destino = "{{ Route('miembros.listadoOperadores') }}";
            $.ajax({
                type: "Post",
                url: destino,
                async: false,
                success: function(respuesta){
                    $("#div_operadores").html(respuesta);
                }
            });

            $("form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });
        @endif

        function validarEmail(email) {
            var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if ( !expr.test(email) )
                return false;
            else
                return true;
        }

        $("#btnAgregarOperador").click(function(){
            if($("#nombre_operador").val())
            {
                var datos = "nombre_operador="+$("#nombre_operador").val();
                var destino = "{{ Route('miembros.agregarOperador') }}";
                $.ajax({
                    type: "Post",
                    url: destino,
                    data: datos,
                    async: false,
                    success: function(respuesta){
                        $("#nombre_operador").val("");
                        $("#div_operadores").html(respuesta);
                        $("#nombre_operador").focus();
                    }
                });

            }

        });

        $("#btnAgregarResponsable").click(function(){
            if($("#nombre_responsable").val() == "")
            {
                $("#contenido-modal").html("Debe ingresar un nombre de responsable");
                $("#confirmacion").modal(function(){show:true});
            }
            else if(!validarEmail($("#email_responsable").val()))
            {
                $("#contenido-modal").html("Debe ingresar un email de responsable correcto");
                $("#confirmacion").modal(function(){show:true});
            }
            else if($("#nombre_responsable").val())
            {
                var datos = "nombre_responsable="+$("#nombre_responsable").val()+"&email_responsable="+$("#email_responsable").val()+"&telefono_responsable="+$("#telefono_responsable").val();
                var destino = "{{ Route('miembros.agregarResponsable') }}";
                $.ajax({
                    type: "Post",
                    url: destino,
                    data: datos,
                    async: false,
                    success: function(respuesta){

                        $("#nombre_responsable").val("");
                        $("#email_responsable").val("");
                        $("#telefono_responsable").val("");
                        $("#div_responsables").html(respuesta);
                        $("#nombre_responsable").focus();
                    }
                });

            }


        });

        $("#btnAceptar").click(function(){
            var datos = $('#form').serialize();
            var destino = "{{ Route('miembros.validarDatos') }}";
            $.ajax({
                type: "Post",
                url: destino,
                data: datos,
                async: true,
                success: function(respuesta){
                    if(!respuesta.valid)
                    {
                        console.log(respuesta.mensajes);
                        $("#contenido-modal").html(respuesta.mensaje);
                        return $("#confirmacion").modal(function(){show:true});
                    }
                    
                    $('#form').submit();
                    
                }

            })

        });
    });


</script>
@endsection

@section ('title') {{$action}} Miembro @stop

@section ('content')
    <h1>{{$action}} Miembro</h1>
    <div class="alert alert-danger fade in" style="display:none" id="errores">


    </div>

    {{ Form::model($model,$form_data, array('role' => 'form')) }}
    {{ Form::hidden('id',null,['id' => 'id']) }}
    <div class="row">
        <div class="form-group col-md-4">
            {{ Form::label('id_organismo','Organismo') }}
            {{ Form::select('id_organismo',$combosOrganismos,null,array('class'=>'form-control'))}}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            {{ Form::label('nombre_area', 'Nombre de la Oficina/Area') }}
            {{ Form::text('nombre_area', null, array('placeholder' => 'Ingrese el nombre de la oficina/area', 'class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('direccion_area', 'Dirección de la Oficina/Area') }}
            {{ Form::text('direccion_area', null, array('placeholder' => 'Ingrese la direccion de la oficina/area', 'class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('telefono_area', 'Teléfono de la Oficina/Area') }}
            {{ Form::text('telefono_area', null, array('placeholder' => 'Ingrese el teléfono de la oficina/area', 'class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('email_area', 'Email de la Oficina/Area') }}
            {{ Form::text('email_area', null, array('placeholder' => 'Ingrese el email de la oficina/area', 'class' => 'form-control')) }}
        </div>
    </div>

    <br>


    <div class="panel panel-info">
        <div class="panel-heading">Datos del responsable</div>
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    {{ Form::label('nombre_responsable', 'Nombre del responsable',['class'=>'sr-only']) }}
                    {{ Form::text('nombre_responsable', null, array('placeholder' => 'Nombre del responsable', 'class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('email_responsable', 'Email del responsable',['class'=>'sr-only']) }}
                    {{ Form::text('email_responsable', null, array('placeholder' => 'Email del responsable', 'class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('telefono_responsable', 'Telefono del responsable',['class'=>'sr-only']) }}
                    {{ Form::text('telefono_responsable', null, array('placeholder' => 'Telefono del responsable', 'class' => 'form-control')) }}
                </div>
                {{ Form::button('Agregar responsable', array('type' => 'button', 'class' => 'btn btn-success','id'=>'btnAgregarResponsable')) }}

            </div>
        </div>
    </div>
    <div id="div_responsables"></div>
    <br>
    <div class="panel panel-info">
        <div class="panel-heading">Datos del operador</div>
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    {{ Form::label('nombre_operador', 'Nombre del operador',['class'=>'sr-only']) }}
                    {{ Form::text('nombre_operador', null, array('placeholder' => 'Nombre del operador', 'class' => 'form-control')) }}
                </div>

                {{ Form::button('Agregar operador', array('type' => 'button', 'class' => 'btn btn-success','id'=>'btnAgregarOperador')) }}

            </div>
        </div>
    </div>
    <div id="div_operadores"></div>
    <div class="row">

        <div class="form-group col-md-4">

            {{ Form::button('Guardar', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'btnAceptar')) }}
        </div>
    </div>
    @include("components.modal")
    {{ Form::close() }}
@stop
