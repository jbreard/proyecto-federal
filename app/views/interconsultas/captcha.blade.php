<script>
document.addEventListener('DOMContentLoaded', function() {
    var captchaImage = document.getElementById('protection-image');
    var refreshCaptchaLink = document.getElementById('refresh-protection');
    
    // Función para refrescar el CAPTCHA
    function refreshCaptcha() {
        captchaImage.src = "{{ url('protection') }}?" + new Date().getTime();
    }
    
    // Llamada inicial para refrescar el CAPTCHA al cargar la página
    refreshCaptcha();
    
    // Agregar evento de clic al enlace de refrescar
    refreshCaptchaLink.addEventListener('click', function(e) {
        e.preventDefault();
        refreshCaptcha();
    });
});
</script>
<img id="protection-image" src="  {{ url('protection').'?'.time()*1000; }}" alt="CAPTCHA">
<a href="#" id="refresh-protection">Refrescar CAPTCHA</a>
<input id ="protection" type="text" style="text-transform: none;" name="captcha" placeholder="Ingresa el CAPTCHA">


