@section('scripts')
    {{ HTML::script('assets/js/jquery.blockUI.js') }}
    <script>

        $().ready(function(){

            $(".oculto").hide();

            $("#personas").change(function(event) {
                $('#botones').show();
                $('#security').show();
                $('#botondrnpa').hide();
                $("#resultado").empty();
                $('#dato').val("");

            });
            $("#vehiculos").change(function(event) {
                $('#botones').hide();
                $('#security').hide();
                $('#botondrnpa').show();
                $('#security').show();
                $("#resultado").empty();
                $('#dato').val("");
            });
            $("#armas").change(function(event) {
                $('#botones').hide();
                $('#security').hide();
                $('#botondrnpa').hide();
                $("#resultado").empty();
                $('#dato').val("");
            });

            $("#formInterconsulta").on("submit", function(e){
                e.preventDefault();
                var destino = "{{ Route('interconsultas.buscar') }}";
                var formData = new FormData(document.getElementById("formInterconsulta"));
                peticionAjax(destino,formData);

            });

            $("#botonRenaper").click(function(e){
                e.preventDefault();
                var url = "{{ Route('interconsultas.renaper') }}";
                var formData = new FormData(document.getElementById("formInterconsulta"));
                peticionAjax(url,formData);

            });

            $("#botonMigraciones").click(function(e){
                e.preventDefault();
                var destino = "{{ Route('interconsultas.migraciones') }}";
                var formData = new FormData(document.getElementById("formInterconsulta"));
                peticionAjax(destino,formData);
            });

            $("#botonDrnpa").click(function(e){
                e.preventDefault();

                var destino = "{{ Route('interconsultas.drnpa') }}";
                var formData = new FormData(document.getElementById("formInterconsulta"));
                peticionAjax(destino,formData);

            });

            $("#botonAnses").click(function(e){
                e.preventDefault();

                var destino = "{{ Route('interconsultas.anses') }}";
                var formData = new FormData(document.getElementById("formInterconsulta"));
                peticionAjax(destino,formData);

            });

            function peticionAjax(destino,datos,redireccionar)
            {
                $.blockUI({ message: null });
                redireccionar = redireccionar || 0;
                $.ajax({
                    type: "Post",
                    url: destino,
                    data: datos,
                    async: true,
                    dataType: "html",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respuesta){
                        $.unblockUI();
                        $('#protection').val("");
                        var imageElement = $('#protection-image');
                        var currentSrc = imageElement.attr('src');
                        imageElement.attr('src', currentSrc + '?' + new Date().getTime());
                        $('#resultado').html(respuesta);
                    },
                    error:function(request){
                        $.unblockUI();
                        $('#resultado').html(request.statuss);
                    }

                })
            }

        });
    </script>
@stop
