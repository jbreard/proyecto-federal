
<!--Accordion wrapper-->
<div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">
  <!-- Accordion card -->
  <div class="card">
    <!-- Card header -->
    <div class="card-header" role="tab" id="headingTwo1">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#EstadoActual"
        aria-expanded="false" aria-controls="EstadoActual">
        <h5 class="mb-0">
          Estado Actual <i class="fa fa-angle-down rotate-icon"></i>
        </h5>
      </a>
    </div>
    <!-- Card body -->
    <div id="EstadoActual" class="collapse" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
      <div class="card-body">
        <table class="table table-bordered table-hover">                    
          <tr>
            <td><strong>Codigo Estado Actual: </strong></td>  
            <td><strong>Descripcion Estado Actual: </strong></td>
          </tr>   
          <tr>
            <td>{{ $servicio['EstadoActual']['CodigoEstadoActual']}}</td>  
            <td>{{ $servicio['EstadoActual']['DescripcionEstadoActual']}}</td>
          </tr>          
        </table>
      </div>
    </div>
  </div>
  <!-- Accordion card -->
  <!-- Accordion card -->
  <div class="card">
    <!-- Card header -->
    <div class="card-header" role="tab" id="headingTwo2">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#DatosPedidoDeSecuestro"
        aria-expanded="false" aria-controls="DatosPedidoDeSecuestro">
        <h5 class="mb-0">
          Datos Pedido De Secuestro <i class="fa fa-angle-down rotate-icon"></i>
        </h5>
      </a>
    </div>

    <!-- Card body -->
    <div id="DatosPedidoDeSecuestro" class="collapse" role="tabpanel" aria-labelledby="headingTwo21" data-parent="#accordionEx1">
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <tr>
            <td><strong>Descripcion</strong></td>
            <td><strong>Fecha</strong></td>
            <td><strong>Dominio</strong></td>
            <td><strong>Numero Motor</strong></td>
            <td><strong>Numero Chasis Cuadro</strong></td>
            <td><strong>Marca</strong></td>
            <td><strong>Modelo</strong></td>
            <td><strong>Tipo</strong></td>
            <td><strong>Color</strong></td>
            <td><strong>Delito</strong></td>
            <td><strong>Dependencia</strong></td>
            <td><strong>Juez</strong></td>
            <td><strong>Informa</strong></td>
          </tr>
          @foreach($servicio['DatosPedidoDeSecuestro'] as $key => $value )
          <tr>
            <td>{{ $value['Descripcion']}}</td>                          
            <td>{{ $value['Fecha']}}</td>                          
            <td>{{ $value['Dominio']}}</td>                          
            <td>{{ $value['NumeroMotor']}}</td>                          
            <td>{{ $value['NumeroChasisCuadro']}}</td>                          
            <td>{{ $value['Marca']}}</td>                          
            <td>{{ $value['Modelo']}}</td>                          
            <td>{{ $value['Tipo']}}</td>                          
            <td>{{ $value['Color']}}</td>                          
            <td>{{ $value['Delito']}}</td>                          
            <td>{{ $value['Dependencia']}}</td>                          
            <td>{{ $value['Juez']}}</td>                          
            <td>{{ $value['Informa']}}</td>                          
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
  <!-- Accordion card -->

  <!-- Accordion card -->
  <div class="card">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingThree31">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#DatosRegistrales"
        aria-expanded="false" aria-controls="DatosRegistrales">
        <h5 class="mb-0">
          Datos Registrales <i class="fa fa-angle-down rotate-icon"></i>
        </h5>
      </a>
    </div>
    
    <!-- Card body -->
    <div id="DatosRegistrales" class="collapse" role="tabpanel" aria-labelledby="headingThree31" data-parent="#accordionEx1">
      <div class="card-body">
        <!--Accordion wrapper-->
          <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">
            @foreach($servicio['DatosRegistrales'] as $id=> $registros)  
            {{-- <pre>{{ print_r($registros) }}</pre>  --}}
   
               @foreach($registros as $key => $value)                 
               <!-- Accordion card -->
                  <div class="card">
                    <!-- Card header -->
                    
                    <div class="card-header" role="tab" id="headingTwo1">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#{{ $key }}"
                        aria-expanded="false" aria-controls="{{ $key }}">
                        <h5 class="mb-0">
                          {{ $key }} <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                      </a>
                    </div>
                    <!-- Card body -->
                    <div id="{{ $key }}" class="collapse" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                      <div class="card-body">                                            
                           <table class="table table-bordered table-hover">          
                           @foreach($value as $id => $datos)                               
                              @if(is_array($datos))
                                @foreach($datos as $i => $row)                                
                                  @if(is_array($row))                                    
                                    <table class="table table-bordered table-hover">
                                      <tr>
                                        <td><strong>TipoDomicilio</strong></td>
                                        <td><strong>Calle</strong></td>
                                        <td><strong>Numero</strong></td>
                                        <td><strong>Piso</strong></td>
                                        <td><strong>Depto</strong></td>
                                        <td><strong>CodigoPostal</strong></td>
                                        <td><strong>Localidad</strong></td>
                                        <td><strong>Provincia</strong></td>                                        
                                      </tr>
                                    @foreach($datos['Domicilios'] as $k => $v)                                                                          
                                      <tr>
                                          <td>{{ $v['TipoDomicilio']}}</td>                          
                                          <td>{{ $v['Calle']}}</td>                          
                                          <td>{{ $v['Numero']}}</td>                          
                                          <td>{{ $v['Piso']}}</td>                          
                                          <td>{{ $v['Depto']}}</td>                          
                                          <td>{{ $v['CodigoPostal']}}</td>                          
                                          <td>{{ $v['Localidad']}}</td>                          
                                          <td>{{ $v['Provincia']}}</td>                                                                                          
                                        </tr>
                                    @endforeach
                                    </table>
                                  @else
                                    <tr><td><strong>{{ $i }}: </strong>{{ $datos[$i] }} </td></tr>
                                  @endif
                                @endforeach
                              @else                              
                                <tr><td><strong>{{ $id }}: </strong>{{ $value[$id] }} </td></tr>
                              @endif
                           @endforeach
                           </table>   
                    
                      </div>
                    </div>
                  </div>
                  <!-- Accordion card -->
               @endforeach

            @endforeach

          </div>
          <!-- Accordion wrapper -->
      </div>
    </div>

  </div>
  <!-- Accordion card -->

</div>
<!-- Accordion wrapper -->