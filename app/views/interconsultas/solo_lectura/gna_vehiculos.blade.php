@foreach($servicio as $serv => $valor)
<table class="table table-bordered table-hover" style="margin-top: 1em !important">
  <tr><td><strong>Respuesta:</strong>  {{ $valor['Respuesta']}}</td></tr>
  <tr><td><strong>Tipo de medida: </strong> {{ $valor['TipoMedida']}}</td></tr>
  <tr><td><strong>Estado:</strong>  {{ $valor['Estado']}}</td></tr>
  <tr><td><strong>Nro de Requerimiento:</strong>  {{ $valor['NroRequerimiento']}}</td></tr>
  <tr><td><strong>Fecha Carga:  </strong>{{ $valor['FechaCarga']}}</td></tr>
  <tr><td><strong>Organismo de Carga:  </strong>{{ $valor['OrganismoCarga']}}</td></tr>
  <tr><td><strong>Dominio:  </strong>{{ $valor['Dominio']}}</td></tr>
  <tr><td><strong>Chasis:</strong>  {{ $valor['Chasis']}}</td></tr>
  <tr><td><strong>Motor:</strong>  {{ $valor['Motor']}}</td></tr>
  <tr><td><strong>Marca Vehiculo:</strong>  {{ $valor['Marca']}}</td></tr>
  <tr><td><strong>Modelo Vehiculo:</strong>  {{ $valor['Modelo']}}</td></tr>
  <tr><td><strong>Modelo Vehiculo:</strong>  {{ $valor['OtroModelo']}}</td></tr>
  <tr><td><strong>Tipo Vehiculo: </strong>{{ $valor['TipoVehiculo']}}</td></tr>
  <tr><td><strong>Lugar de Radicacion: </strong>{{ $valor['LugarRadicacion']}}</td></tr>
  <tr><td><strong>Año: </strong>{{ $valor['Anio']}}</td></tr>
  <tr><td><strong>Otro Modelo: </strong>{{ $valor['OtroModelo']}}</td></tr>
  <tr><td><strong>Color: </strong>{{ $valor['Color']}}</td></tr>
  <tr><td><strong>Nombre:</strong>  {{ $valor['Nombre']}}</td></tr>
  <tr><td><strong>Apellido:</strong>  {{ $valor['Apellido']}}</td></tr>
  <tr><td><strong>Sexo:</strong>  {{ $valor['Sexo']}}</td></tr>
  <tr><td><strong>Tipo de Documento:</strong>  {{ $valor['TipoDocumento']}}</td></tr>
  <tr><td><strong>Nro de Documento:</strong>  {{ $valor['NumeroDocumento']}}</td></tr>
  <tr><td><strong>Nacionalidad:</strong>  {{ $valor['Nacionalidad']}}</td></tr>
  <tr><td><strong>Fecha de Grabacion:</strong>  {{ $valor['FechaGrabacion']}}</td></tr>
  <tr><td><strong>Nro de Oficio:</strong>  {{ $valor['NumeroOficio']}}</td></tr>
  <tr><td><strong>Caratula:</strong>  {{ $valor['Caratula']}}</td></tr>
  <tr><td><strong>Juzgado Interviniente:</strong>  {{ $valor['JuzgadoInterviniente']}}</td></tr>
  <tr><td><strong>Otro Juzgado:</strong>  {{ $valor['OtroJuzgados']}}</td></tr>
  <tr><td><strong>Secretaría:</strong>  {{ $valor['Secretaria']}}</td></tr>
  <tr><td><strong>Nro de Causa:</strong>  {{ $valor['NumeroCausaJuzgado']}}</td></tr>
  <tr><td><strong>Fiscalia Interviniente:</strong>  {{ $valor['FiscaliaInterviniente']}}</td></tr>
  <tr><td><strong>Otra Fiscalia Interviniente:</strong>  {{ $valor['OtraFiscaliaInterviniente']}}</td></tr>
  <tr><td><strong>Nro de Fiscalia:</strong>  {{ $valor['NumeroFiscalia']}}</td></tr>
  <tr><td><strong>Observaciones Judiciales:</strong>  {{ $valor['ObservacionesJudiciales']}}</td></tr>











</table>
  @endforeach