<table class="table table-bordered table-hover" style="margin-top: 1em !important">
  <tr>
    <td><strong>Tipo de Arma</strong></td>
    <td><strong>Marca</strong></td>
    <td><strong>Modelo</strong></td>
    <td><strong>Calibre</strong></td>
    <td><strong>Matrícula Nro.</strong></td>
    <td><strong>Datos Titular Registral</strong></td>
    <td><strong>Autoridad Interviniente</strong></td>
    <td><strong>Fecha de la medida</strong></td>
    <td><strong>Datos de la Causa</strong></td>
    <td><strong>Vigente/Sin Efecto</strong></td>
  </tr>
  @foreach($servicio as $key => $value )
    <tr>
      <td>{{ $value['TipoArma']}} </td>
      <td>{{ $value['Marca']}}</td>
      <td>{{ $value['Modelo']}}</td>
      <td>{{ $value['Calibre']}}</td>
      <td>{{ $value['Matricula']}} </td>
       <td>{{ $value['Titular']['Nombre']}} {{$value['Titular']['Apellido']}} / {{$value['Titular']['TipoDocumento']}}{{$value['Titular']['NroDocumento']}}</td>
      <td>{{$value['AutoridadJudicial']['Juzgado']}} / {{$value['AutoridadJudicial']['JuzgadoOtro']}} / {{$value['AutoridadJudicial']['Secretaria']}} / {{$value['AutoridadJudicial']['Fiscalia']}} / {{$value['AutoridadJudicial']['OtraFiscalia']}} / {{$value['AutoridadJudicial']['ObservacionesJudicial']}}</td>
      <td>{{ $value['AutoridadJudicial']['FechaOficio']}}</td>
      <td>{{ $value['AutoridadJudicial']['Caratula']}} {{ $value['AutoridadJudicial']['NroCausa']}}</td>
      <td>{{ $value['Medida']['Estado']}} {{ $value['Medida']['NroRequerimiento']}}</td>
    </tr>
    @endforeach

</table>