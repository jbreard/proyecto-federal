<table class="table table-bordered table-hover" style="margin-top: 1em !important">
  <tr><td><strong>Año Fabricacion:</strong>  {{ $servicio[0]['AnioFabricacion']}}</td></tr>
  <tr><td><strong>DescColor: </strong> {{ $servicio[0]['DescColor']}}</td></tr>
  <tr><td><strong>Dominio:</strong>  {{ $servicio[0]['Dominio']}}</td></tr>
  <tr><td><strong>Marca Vehiculo:</strong>  {{ $servicio[0]['DescMarcaVehiculo']}}</td></tr>
  <tr><td><strong>Modelo Vehiculo:</strong>  {{ $servicio[0]['DescModeloVehiculo']}}</td></tr>
  <tr><td><strong>Chasis:</strong>  {{ $servicio[0]['Chasis']}}</td></tr>
  <tr><td><strong>Motor:</strong>  {{ $servicio[0]['Motor']}}</td></tr>
  <tr><td><strong>Nombre y Apellido:</strong>  {{ $servicio[0]['NombreApellido']}}</td></tr>
  <tr><td><strong>Tipo Vehiculo: </strong>{{ $servicio[0]['DescTipoVehiculo']}}</td></tr>
  <tr><td><strong>Fecha Carga:  </strong>{{ $servicio[0]['FechaCarga']}}</td></tr>
  <tr><td><strong>FechaSefecto:  </strong>{{ $servicio[0]['FechaSefecto']}}</td></tr>
  <tr><td><strong>Tipo Delito: </strong>{{ $servicio[0]['DescTipoDelito']}}</td></tr>
  <tr><td><strong>SecTrans: </strong>{{ $servicio[0]['SecTrans']}}</td></tr>
  <tr><td><strong>Fecha: </strong> {{ $servicio[0]['Fecha']}}</td></tr>
  <tr><td><strong>Hora:  </strong>{{ $servicio[0]['Hora']}}</td></tr>
  <tr><td><strong>ClaseVehiculo: </strong> {{ $servicio[0]['ClaseVehiculo']}}</td></tr>
  <tr><td><strong>FechaSecuestro:</strong>  {{ $servicio[0]['FechaSecuestro']}}</td></tr>
  <tr><td><strong>Numero: </strong> {{ $servicio[0]['Numero']}}</td></tr>
  <tr><td><strong>Domicilio: </strong> {{ $servicio[0]['Domicilio']}}</td></tr>
  <tr><td><strong>LugarHecho: </strong> {{ $servicio[0]['LugarHecho']}}</td></tr>
  <tr><td><strong>JuezInterviniente: </strong> {{ $servicio[0]['JuezInterviniente']}}</td></tr>
  <tr><td><strong>Secretario: </strong> {{ $servicio[0]['Secretario']}}</td></tr>
  <tr><td><strong>TieneSinEfecto: </strong> {{ $servicio[0]['TieneSinEfecto']}}</td></tr>
  <tr><td><strong>FechaTrans: </strong> {{ $servicio[0]['FechaTrans']}}</td></tr>
  <tr><td><strong>DependenciaPfaSecuestro: </strong> {{ $servicio[0]['DescDependenciaPfaSecuestro']}}</td></tr>
  <tr><td><strong>DependenciaPfaDenuncia: </strong> {{ $servicio[0]['DescDependenciaPfaDenuncia']}}</td></tr>
  <tr><td><strong>Obs: </strong> {{ $servicio[0]['Obs']}}</td></tr>
  <tr><td><strong>PedSec:  </strong>{{ $servicio[0]['PedSec']}}</td></tr>
</table>