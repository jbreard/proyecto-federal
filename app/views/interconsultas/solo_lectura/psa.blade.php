<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Nacionalidad</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
    <tr>
        <td>{{ $value['Medida'] }}</td>
        <td>{{ $value['Nombres'] }} {{ $value['Apellido'] }}</td>
        <td>{{ $value['Tipo_Documento']}}: {{ $value['Documento']}} / {{ $value['Pasaporte'] }}</td>
        <td>{{ $value['FechaNacimiento']}}</td>
        <td> - </td>
        <td> - </td>
        <td>{{ $value['Juzgado']}}</td>
        <td>{{ $value['Fecha_Oficio']}}</td>
        <td>{{ $value['Autos']}}{{ $value['Observaciones']}}</td>
        <td>{{ $value['Estado']}}</td>
      </tr>
    @endforeach
  </table>
{{-- <table class="table table-bordered table-hover">
  <tr><td><strong>Tipo de medida: </strong>{{ $servicio[0]['Medida']}}</td></tr>
  <tr><td><strong>Nombre/s y Apellido/s: </strong>{{ $servicio[0]['Nombres'] }} {{ $servicio[0]['Apellido'] }} </td></tr>
  <tr><td><strong>Documento: </strong>{{ $servicio[0]['Tipo_Documento']}}: {{ $servicio[0]['Documento']}} / {{ $servicio[0]['Pasaporte']}}</td></tr>
  <tr><td><strong>Fecha de Nacimiento: <strong>{{ $servicio[0]['FechaNacimiento']}}</td></tr>
  <tr><td><strong>Nacionalidad: </strong></td></tr>
  <tr><td><strong>Domicilio: </strong></td></tr>
  <tr><td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente): </strong>{{ $servicio[0]['Juzgado']}}</td></tr>
  <tr><td><strong>Fecha de la medida (Oficio/Resolución): </strong>{{ $servicio[0]['Fecha_Oficio']}}</td></tr>
  <tr><td><strong>Datos de la Causa (Carátula, N° de Causa): </strong>{{ $servicio[0]['Autos']}}{{ $servicio[0]['Observaciones']}}</td></tr>
  <tr><td><strong>Vigente/Sin Efecto (Positivo/Negativo): </strong>{{ $servicio[0]['Estado']}}</td></tr>
</table> --}}