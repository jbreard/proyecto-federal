<table class="table table-bordered table-hover">
  <tr><td><strong>Nombre: </strong>{{ $servicio[0]['Nombre']}}</td></tr>
  <tr><td><strong>Apellido:</strong> {{ $servicio[0]['Apellido']}} </td></tr>
  <tr><td><strong>ApellidoMaterno:</strong> {{ $servicio[0]['ApellidoMaterno']}}</td></tr>
  <tr><td><strong>Sobrenombre:</strong> {{ $servicio[0]['Sobrenombre']}}</td></tr>
  <tr><td><strong>Sexo: </strong>{{ $servicio[0]['Sexo']}}</td></tr>
  <tr><td><strong>FechaNacimiento:</strong> {{ $servicio[0]['FechaNacimiento']}}</td></tr>
  <tr><td><strong>TipoDocumento:</strong> {{ $servicio[0]['TipoDocumento']}}</td></tr>
  <tr><td><strong>NroDocumento:</strong> {{ $servicio[0]['NroDocumento']}}</td></tr>
  <tr><td><strong>Nacionalidad: </strong>{{ $servicio[0]['Nacionalidad']}}</td></tr>     
  <tr><td><strong>DescripcionFisica:</strong> {{ $servicio[0]['DescripcionFisica']}}</td></tr>
  <tr><td><strong>ObservacionesPersona: </strong>{{ $servicio[0]['ObservacionesPersona']}}</td></tr>
  <tr><td><strong>Provincia: </strong>{{ $servicio[0]['Provincia']}}</td></tr>
  <tr><td><strong>Localidad:</strong> {{ $servicio[0]['Localidad']}}</td></tr>
  <tr><td><strong>Direccion: </strong>{{ $servicio[0]['Direccion']}}</td></tr>
  <tr><td><strong>Numero: </strong>{{ $servicio[0]['Numero']}}</td></tr>
  <tr><td><strong>Piso:</strong> {{ $servicio[0]['Piso']}}</td></tr>
  <tr><td><strong>Departamento:</strong> {{ $servicio[0]['Departamento']}}</td></tr>  
  <tr>
    <td><strong>Medida: </strong>
      <table class= "table table-bordered table-hover">         
          <tr><td>TipoMedida: {{ $servicio[0]['Medida']['TipoMedida']}}</td></tr>          
          <tr><td>Estado: {{ $servicio[0]['Medida']['Estado']}}</td></tr>          
          <tr><td>NroRequerimiento: {{ $servicio[0]['Medida']['NroRequerimiento']}}</td></tr>          
          <tr><td>FechaCargaRequerimiento: {{ $servicio[0]['Medida']['FechaCargaRequerimiento']}}</td></tr>          
          <tr><td>OrganismoCarga: {{ $servicio[0]['Medida']['OrganismoCarga']}}</td></tr>          
      </table>
    </td>      
  </tr>
  <tr><td><strong>AutoridadJudicial:</strong> 
    <table class= "table table-bordered table-hover">         
            <tr><td>FechaOficio: {{ $servicio[0]['AutoridadJudicial']['FechaOficio']}}</td></tr>          
            <tr><td>NroOficio: {{ $servicio[0]['AutoridadJudicial']['NroOficio']}}</td></tr>          
            <tr><td>Caratula: {{ $servicio[0]['AutoridadJudicial']['Caratula']}}</td></tr>          
            <tr><td>Juzgado: {{ $servicio[0]['AutoridadJudicial']['Juzgado']}}</td></tr>          
            <tr><td>JuzgadoOtro: {{ $servicio[0]['AutoridadJudicial']['JuzgadoOtro']}}</td></tr>          
            <tr><td>Secretaria: {{ $servicio[0]['AutoridadJudicial']['Secretaria']}}</td></tr>          
            <tr><td>NroCausa: {{ $servicio[0]['AutoridadJudicial']['NroCausa']}}</td></tr>          
            <tr><td>Fiscalia: {{ $servicio[0]['AutoridadJudicial']['Fiscalia']}}</td></tr>          
            <tr><td>OtraFiscalia: {{ $servicio[0]['AutoridadJudicial']['OtraFiscalia']}}</td></tr>          
            <tr><td>NroCausaExpediente: {{ $servicio[0]['AutoridadJudicial']['NroCausaExpediente']}}</td></tr>          
            <tr><td>ObservacionesJudicial: {{ $servicio[0]['AutoridadJudicial']['ObservacionesJudicial']}}</td></tr>          
        </table>
      </td></tr>
</table>  
