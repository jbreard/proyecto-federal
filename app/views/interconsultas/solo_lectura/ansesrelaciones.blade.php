<div  style="overflow-y:auto;">
    <table  class="table table-bordered table-hover" style="margin-top: 1em !important">
     	<tr>
            <td><strong>Relacion</strong></td>
            <td><strong>Apellido y Nombre</strong></td>
            <td><strong>CUIL</strong></td>
            <td><strong>Progenitor</strong></td>
    	</tr>
    	@foreach($servicio as $key => $value)
            <tr>
                <td>{{ $value['da_relacion'] }}</td>
                <td>{{ $value['ape_nom'] }}</td>
                <td>{{ $value['cuil_rela'] }}</td>
                <td>{{ $value['d_unico_progen'] }}</td>
            </tr>
        @endforeach
    </table>
</div>