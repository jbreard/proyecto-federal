 <table  class="table table-bordered table-hover" style="margin-top: 1em !important">
 	<tr>
 		<td>NOMBRES</td>
 		<td>APELLIDOS</td>
 		<td>TIPO DOC</td>
 		<td>NRO DOC</td>
 		<td>SEXO</td>
 		<td>NACIONALIDAD</td>
 		<td>FECHA CRUCE</td>
 		<td>PASO CRUCE</td>
 		<td>TIPO CRUCE</td>
	</tr>
	@foreach($servicio as $key => $value)
      <tr>
        <td>{{ $value['NOMBRES'] }}</td>
        <td>{{ $value['APELLIDOS'] }} </td>
        <td>{{ $value['TIPO_DOC'] }}</td>
        <td>{{ $value['NRO_DOC'] }}</td>
        <td>{{ $value['SEXO'] }}</td>
        <td>{{ $value['NACIONALIDAD'] }}</td>
        <td>{{ $value['FECHA_CRUCE'] }}</td>
        <td>{{ $value['PASO_CRUCE'] }}</td>
        <td>{{ $value['TIPO_CRUCE'] }}</td>
      </tr>
    @endforeach

 </table>