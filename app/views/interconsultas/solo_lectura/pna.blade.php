<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Nacionalidad</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
    <tr>
    	<td>{{ $value['causa']}} {{ $value['tipodelito']}} {{ $value['tipocausa'] }}</td>
    	<td>{{ $value['nombres']}} {{ $value['apellidos'] }}</td>
    	<td>{{ $value['tipodocumento']}}: {{ $value['numerodocumento'] }}</td>
    	<td>{{ $value['fechanac'] }}</td>
    	<td> - </td>
    	<td> - </td>
    	<td> {{  $value['juez']}} {{ $value['secretaria'] }} </td>
    	<td> {{  $value['fechapedi'] }} </td>
    	<td> {{  $value['causa'] }} </td>
    	<td> {{  $value['estado'] }} </td>
      </tr>
    @endforeach
  </table>
{{--
<table class="table table-bordered table-hover">
<tr><td><strong>Tipo de medida: </strong>{{ $servicio[0]['causa']}} {{ $servicio[0]['tipodelito']}} {{ $servicio[0]['tipocausa']}}</td></tr>
  <tr><td><strong>Nombre/s y Apellido/s: </strong>{{ $servicio[0]['nombres']}} {{ $servicio[0]['apellidos']}}</td></tr>
  <tr><td><strong>Documento: </strong>{{ $servicio[0]['tipodoc']}}: {{ $servicio[0]['nrodoc']}}</td></tr>
  <tr><td><strong>FechaNacimiento: </strong>{{ $servicio[0]['fechanac']}}</td></tr>
  <tr><td><strong>Nacionalidad: </strong></td></tr>
  <tr><td><strong>Domicilio: </strong></td></tr>
  <tr><td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente): </strong> {{ $servicio[0]['juez']}} {{ $servicio[0]['secretaria']}}</td></tr>
  <tr><td><strong>Fecha de la medida (Oficio/Resolución): </strong>{{ $servicio[0]['fechapedi']}}</td></tr>
  <tr><td><strong>Datos de la Causa (Carátula, N° de Causa): </strong>{{ $servicio[0]['causa']}}</td></tr>
  <tr><td><strong>Estado: </strong>{{ $servicio[0]['estado']}}</td></tr>
</table> --}}
