<h3>Datos Pedido De Secuestro</h3>
<table class="table table-bordered table-hover">
    <tr>
    	<td><strong>Modelo </strong></td>
		<td><strong>Dominio </strong></td>
		<td><strong>Nª Chasis </strong></td>
		<td><strong>Nª Motor </strong></td>
		<td><strong>Autoridad Interviniente </strong></td>
		<td><strong>Fecha de la medida </strong></td>
		<td><strong>Datos de la Causa </strong></td>
		<td><strong>Vigente/Sin Efecto </strong></td>
	</tr>

	 @foreach($servicio['DatosPedidoDeSecuestro'] as $key => $value )
          <tr>
            <td>{{ $value['Tipo']}} {{ $value['Marca']}} {{ $value['Modelo']}} {{ $value['Color']}}</td>
            <td>{{ $value['Dominio']}}</td>
            <td>{{ $value['NumeroChasisCuadro']}}</td>
            <td>{{ $value['NumeroMotor']}}</td>
            <td>{{ $value['Juez']}} {{ $value['Dependencia']}} {{ $value['Informa']}}</td>
            <td>{{ $value['Fecha']}}</td>
            <td>{{ $value['Delito']}}</td>
            <td>{{ $value['Descripcion']}}</td>
          </tr>
          @endforeach

</table>
<h3>Titulares</h3>
<table class="table table-bordered table-hover">
	<tr>
		<th>Tipo Persona</th>
		<th>Apellido Razón Social</th>
		<th>Nombre</th>
		<th>Tipo Documento</th>
		<th>Número Documento</th>
		<th>Cuit/Cuil</th>
		<th>Porcentaje Titular</th>
	</tr>
	@foreach($servicio['DatosRegistrales'][0]['Titulares'] as $k => $value)
	<tr>
        <td>{{ $value['TipoPersona']}} </td>
        <td>{{ $value['ApellidoRazonSocial']}}</td>
        <td>{{ $value['ApellidoRazonSocial']}}</td>
        <td>{{ $value['TipoDocumento']}}</td>
        <td>{{ $value['NroDocumento']}}</td>
        <td>{{ $value['CuitCuil']}}</td>
        <td>{{ $value['PorcentajeTitular']}}</td>
	</tr>
    @endforeach
</table>

<h3>Domicilios</h3>
<table class="table table-bordered table-hover">
  <tr>
    <td><strong>Tipo Domicilio</strong></td>
    <td><strong>Calle</strong></td>
    <td><strong>Numero</strong></td>
    <td><strong>Piso</strong></td>
    <td><strong>Depto</strong></td>
    <td><strong>Codigo Postal</strong></td>
    <td><strong>Localidad</strong></td>
    <td><strong>Provincia</strong></td>
  </tr>
@foreach($servicio['DatosRegistrales'][0]['Titulares'][0]['Domicilios'] as $v)
  <tr>
      <td>{{ $v['TipoDomicilio']}}</td>
      <td>{{ $v['Calle']}}</td>
      <td>{{ $v['Numero']}}</td>
      <td>{{ $v['Piso']}}</td>
      <td>{{ $v['Depto']}}</td>
      <td>{{ $v['CodigoPostal']}}</td>
      <td>{{ $v['Localidad']}}</td>
      <td>{{ $v['Provincia']}}</td>
    </tr>
@endforeach
</table>
