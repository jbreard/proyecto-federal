<div class="text-center"><h3>Datos Pedido De Secuestro</h3></div>

<table class="table table-bordered table-hover">
    <tr>
    	<td><strong>Delito </strong></td>
        <td><strong>Vigente/Sin Efecto </strong></td>
        <td><strong>Autoridad Interviniente</strong></td>
        <td><strong>Fecha de la medida </strong></td>
        <td><strong>Dominio </strong></td>
        <td><strong>Datos del Vehiculo </strong></td>
        <td><strong>Nª Chasis </strong></td>
        <td><strong>Nª Motor </strong></td>

    </tr>  
	 @foreach($servicio['datosPedidoDeSecuestro']['denuncia'] as $key => $value )

         <tr @if($value['descripcion']<>"SIN NOVEDADES [ESTADO ACTUAL]")class='alert alert-danger' role='alert'@endif>
            <td>{{ $value['delito']}}</td>
            <td>{{ $value['descripcion']}}</td>
            <td>{{ $value['juez']}} {{ $value['dependencia']}} {{ $value['informa']}}</td>
            <td>{{ $value['fecha']}}</td>
            <td>{{ $value['dominio']}}</td>
            <td>{{ $value['tipo']}} {{ $value['marca']}} {{ $value['modelo']}} {{ $value['color']}}</td>
            <td>{{ $value['numeroChasisCuadro']}}</td>
            <td>{{ $value['numeroMotor']}}</td>
          </tr>



    @endforeach

</table>
<div class="text-center"><h2>Datos Registrales</h2></div>

<h3>Datos del Vehiculo</h3>
<table class="table table-bordered table-hover">
    <tr>
        <td><strong>Fabrica</strong></td>
        <td><strong>Vehiculo</strong></td>
        <td><strong>Fecha de inscripcion</strong></td>
        <td><strong>Nro Chasis</strong></td>
        <td><strong>Nro Motor</strong></td>
        <td><strong>Placa</strong></td>
        <td><strong>Vehiculo</strong></td>
    </tr>
        <tr>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['descripcionFabrica']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['descripcionMarca']}} {{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['descripcionModelo']}} {{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['descripcionTipo']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['fechaInscripcionInicial']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['numeroChasisCuadro']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['numeroMotor']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['placa']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['datosVehiculo']['vehiculo']}}</td>
        </tr>
</table>
@if($servicio['datosRegistrales']['dominio'][0]['cedulas'])
<h3>Cedulas</h3>
<table class="table table-bordered table-hover">
    <tr>
        <th>Fecha de Emision</th>
        <th>Numero de Cedula</th>
        <th>Orden Titular</th>

    </tr>
    @foreach($servicio['datosRegistrales']['dominio'][0]['cedulas']['cedula'] as $k => $value)
        <tr>
            <td>{{ $value['fechaEmision']}} </td>
            <td>{{ $value['numeroCedula']}}</td>
            <td>{{ $value['ordenTitular']}}</td>
        </tr>
    @endforeach
</table>
@endif

@if($servicio['datosRegistrales']['dominio'][0]['cedulasAutorizados'])
<h3>Cedulas Autorizadas</h3>
<table class="table table-bordered table-hover">
    <tr>
        <td><strong>Apellido y Nombre</strong></td>
        <td><strong>Fecha de emision</strong></td>
        <td><strong>Numero de Cedula</strong></td>
        <td><strong>Documento</strong></td>
        <td><strong>Numero</strong></td>
        <td><strong>Orden titular</strong></td>
    </tr>
    @foreach($servicio['datosRegistrales']['dominio'][0]['cedulasAutorizados']['cedulaAutorizado'] as $v)
        <tr>
            <td>{{ $v['apellidoNombre']}}</td>
            <td>{{ $v['fechaEmision']}}</td>
            <td>{{ $v['numeroCedula']}}</td>
            <td>{{ $v['tipoDocumento']}}</td>
            <td>{{ $v['numeroDocumento']}}</td>
            <td>{{ $v['ordenTitular']}}</td>
        </tr>
    @endforeach
</table>
@endif

<h3>Estado Registral</h3>
<table class="table table-bordered table-hover">
    <tr>
        <td><strong>Estado</strong></td>
        <td><strong>Fecha</strong></td>
    </tr>
    @foreach($servicio['datosRegistrales']['dominio'][0]['estadoRegistral']['estado'] as $v)
        <tr>
            <td>{{ $v['descripcion']}}</td>
            <td>{{ $v['fecha']}}</td>
        </tr>
    @endforeach
</table>

<h3>Radicacion del Legajo</h3>
<table class="table table-bordered table-hover">
    <tr>
        <td><strong>Direccion</strong></td>
        <td><strong>Codigo Postal</strong></td>
        <td><strong>Seccional</strong></td>
        <td><strong>Provincia</strong></td>
        <td><strong>Telefono</strong></td>
        <td><strong>Dominio</strong></td>
        <td><strong>Dominio Anterior</strong></td>

    </tr>
        <tr>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['calle']}} {{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['numero']}} {{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['piso']}} {{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['localidad']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['codigoPostal']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['descripcionRegistroSeccional']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['provincia']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['telefono']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['codigoDominio']}}</td>
            <td>{{ $servicio['datosRegistrales']['dominio'][0]['radicacionLegajo']['codigoDominioAnterior']}}</td>
        </tr>
</table>



@foreach($servicio['datosRegistrales']['dominio'][0]['titulares']['titular'] as $v)
        <h3>Titular</h3>
        <div class="p-3 mb-2 bg-light text-dark">

<table class="table table-bordered table-hover">

    <tr>
        <td><strong>Apellido/Razon Social</strong></td>
        <td><strong>Cuit/Cuil</strong></td>
        <td><strong>Nombre</strong></td>
        <td><strong>Tipo Documento</strong></td>
        <td><strong>Nro Documento</strong></td>
        <td><strong>Tipo de Persona</strong></td>
        <td><strong>Porcentaje de Titularidad</strong></td>

    </tr>

    <tr>
        <td>{{ $v['apellidoRazonSocial']}}</td>
        <td>{{ $v['cuitCuil']}}</td>
        <td>{{ $v['nombre']}}</td>
        <td>{{ $v['tipoDocumento']}}</td>
        <td>{{ $v['nroDocumento']}}</td>
        <td>{{ $v['tipoPersona']}}</td>
        <td>{{ $v['porcentajeTitular']}}</td>

    </tr>
        <table class="table table-bordered table-hover">
            <h5>Domicilio</h5>

            <tr>
                <td><strong>Calle</strong></td>
                <td><strong>Nro</strong></td>
                <td><strong>Piso</strong></td>
                <td><strong>Dpto</strong></td>
                <td><strong>Codigo Postal</strong></td>
                <td><strong>Localidad</strong></td>
                <td><strong>Provincia</strong></td>
                <td><strong>Tipo de Domicilio</strong></td>

            </tr>
            @foreach($servicio['datosRegistrales']['dominio'][0]['titulares']['titular'][0]['domicilios']['domicilio'] as $v)

                <tr>
                    <td>{{ $v['calle']}}</td>
                    <td>{{ $v['numero']}}</td>
                    <td>{{ $v['piso']}}</td>
                    <td>{{ $v['depto']}}</td>
                    <td>{{ $v['codigoPostal']}}</td>
                    <td>{{ $v['localidad']}}</td>
                    <td>{{ $v['provincia']}}</td>
                    <td>{{ $v['tipoDomicilio']}}</td>

                </tr>

            @endforeach
        </table>
    @endforeach
</table>
</div>
<h3>Estado Actual</h3>
<table class="table table-bordered table-hover">
    <tr>
        <td><strong>Codigo de Estado</strong></td>
        <td><strong>Descripcion</strong></td>
    </tr>
    <tr>
        <td>{{ $servicio['estadoActual']['codigoEstadoActual']}}</td>
        <td>{{ $servicio['estadoActual']['descripcionEstadoActual']}}</td>
    </tr>
</table>






























