<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
      <tr>
        <td>{{ $value['TIPOMEDIDA'] }}</td>
        <td>{{ $value['NOMBRE'] }} {{ $value['APELLIDO'] }} {{ $value['SOBRENOMBRE'] }} </td>
        <td>{{ $value['TIPODOCUMENTO'] }} {{ $value['NUMERODOCUMENTO'] }}</td>
        <td>{{ $value['FECHANACIMIENTO'] }}</td>
        <td>{{ $value['OBSERVACIONESPERSONAS'] }} {{ $value['DIRECCION'] }} {{ $value['NUMERO'] }} {{ $value['PISO'] }} {{ $value['DEPARTAMENTO'] }} {{ $value['LOCALIDAD'] }} {{ $value['PROVINCIA'] }} {{ $value['PAIS'] }}</td>
        <td>{{ $value['JUZGADOINTERVENIENTE'] }} {{ $value['NUMEROJUZGADO'] }} {{ $value['FISCALIAINTERVINIENTE'] }} {{ $value['OTRAFISCALIAINTERVINIENTE'] }} {{ $value['NUMEROFISCALIA'] }}</td>
        <td class="danger">{{ $value['FECHACARGA'] }}</td>
        <td>{{ $value['CARATULA'] }} {{ $value['NUMEROOFICIO'] }} {{ $value['TIPODELITO'] }}</td>
        <td>{{ $value['ESTADO'] }}</td>
      </tr>
    @endforeach
  </table>