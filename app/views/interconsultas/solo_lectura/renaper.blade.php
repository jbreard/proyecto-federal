  <div class="row" style="margin-top: 1em !important">
    <div class="col-md-3">
        <div class="text-center">
          <img src="{{ $servicio['foto']}}">
        </div>
    </div>
      <!-- edit form column -->
    <div class="col-md-3 personal-info">
        <table class="table table-bordered table-hover">
          <tr><td><strong>Cuil:</strong></td><td>{{ $servicio['cuil']}}</td></tr>
          <tr><td><strong>Nombres:</strong></td><td>{{ $servicio['nombres']}}</td></tr>
          <tr><td><strong>Apellidos:</strong></td><td>{{ $servicio['apellido']}}</td></tr>
          <tr><td><strong>Fecha de Nacimiento:</strong></td><td>{{ $servicio['fechaNacimiento']}}</td></tr>
          <tr><td><strong>Fechaf:</strong></td><td>{{ $servicio['fechaf']}}</td></tr>
          <tr><td><strong>Calle:</strong></td><td>{{ $servicio['calle']}}</td></tr>
          <tr><td><strong>Numero:</strong></td><td>{{ $servicio['numero']}}</td></tr>
          <tr><td><strong>Piso:</strong></td><td>{{ $servicio['piso']}}</td></tr>
          <tr><td><strong>Departamento:</strong></td><td>{{ $servicio['departamento']}}</td></tr>
          <tr><td><strong>Cpostal:</strong></td><td>{{ $servicio['cpostal']}}</td></tr>
          <tr><td><strong>Barrio:</strong></td><td>{{ $servicio['monoblock']}}</td></tr>
          <tr><td><strong>Ciudad:</strong></td><td>{{ $servicio['ciudad']}}</td></tr>
          <tr><td><strong>Municipio:</strong></td><td>{{ $servicio['municipio']}}</td></tr>
	        <tr><td><strong>Provincia:</strong></td><td> {{ $servicio['provincia']}}</td></tr>
          <tr><td><strong>Pais:</strong></td><td> {{ $servicio['pais']}}</td></tr>
          <tr><td><strong>Emision:</strong></td><td> {{ $servicio['EMISION']}}</td></tr>
          <tr><td><strong>Vencimiento:</strong></td><td> {{ $servicio['VENCIMIENTO']}}</td></tr>
          <tr><td><strong>Número de Trámite:</strong></td><td> {{ $servicio['IDTRAMITEPRINCIPAL']}}</td></tr>


          

          
        </table>
    </div>
</div>
