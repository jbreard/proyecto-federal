<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
      <tr>
        <td>{{ $value['Desc_RESTRICCION'] }}</td>
        <td>{{ $value['nombre'] }} {{ $value['nombre2'] }} {{ $value['apellido'] }} {{ $value['nombre2'] }} </td>
        <td>{{ $value['Desc_TIPO_DOC'] }} {{ $value['Num_DOC'] }} {{ $value['Pais_EMISOR_DOC'] }}</td>
        <td>{{ $value['Fecha_NAC'] }}</td>
        <td>{{ $value['Nacionalidad'] }}</td>
        <td>{{ $value['Desc_RESTRICCION'] }} {{ $value['Desc_TIPO_JUSTICIA'] }} {{ $value['Desc_PROVINCIA'] }}
        {{ $value['Desc_JUZGADOS'] }} {{ $value['Desc_LOCALIDAD'] }} {{ $value['Juzgado'] }} {{ $value['Secretaria'] }} {{ $value['Sala'] }}</td>
        <td>{{ $value['Fec_RECEP_DNM'] }}</td>
        <td>{{ $value['Desc_CAUSA'] }}</td>
        <td>-----</td>
      </tr>
    @endforeach
  </table>


