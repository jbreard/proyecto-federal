<div  style="overflow-y:auto;">
<table class="table table-bordered table-hover" style="margin-top: 1em !important">
  <tr>
    <td><strong>Resolucion</strong></td>
    <td><strong>Causa</strong></td>
    <td><strong>Organismo</strong></td>
    <td><strong>Secretaria</strong></td>
    <td><strong>Responsable</strong></td>
    <td><strong>Provincia</strong></td>
    <td><strong>Vigente</strong></td>
    <td><strong>Documento</strong></td>
    <td><strong>TipoDocumento</strong></td>
    <td><strong>Apellido</strong></td>
    <td><strong>Nombre</strong></td>
    <td><strong>Sexo</strong></td>
    <td><strong>FechaNac</strong></td>
    <td><strong>AnioNac</strong></td>
    <td><strong>ApeMadre</strong></td>
    <td><strong>NomMadre</strong></td>
    <td><strong>NomPadre</strong></td>
    <td><strong>Alias</strong></td>
  </tr>
    @foreach($servicio as $key => $value )
        <tr>
            <td>{{ $value['Resolucion']}}</td>
            <td>{{ $value['Causa']}}</td>
            <td>{{ $value['Organismo']}}</td>
            <td>{{ $value['Secretaria']}}</td>
            <td>{{ $value['Responsable']}}</td>
            <td>{{ $value['Provincia']}}</td>
            <td>{{ $value['Vigente'] === "True" ? "SI" : "NO" }}</td>
            <td>{{ $value['Documento']}}</td>
            <td>{{ $value['TipoDocumento']}}</td>
            <td>{{ $value['Apellido']}}</td>
            <td>{{ $value['Nombre']}}</td>
            <td>{{ $value['Sexo']}}</td>
            <td>{{ $value['FechaNac']}}</td>
            <td>{{ $value['AnioNac']}}</td>
            <td>{{ $value['ApeMadre']}}</td>
            <td>{{ $value['NomMadre']}}</td>
            <td>{{ $value['NomPadre']}}</td>
            <td>{{ $value['Alias']}}</td>
        </tr>
    @endforeach
</table>
</div>