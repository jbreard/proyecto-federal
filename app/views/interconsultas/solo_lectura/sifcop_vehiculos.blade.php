@foreach($servicio as $serv => $valor)
<div class="col-md-6">
<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr><td><h2>Medida</h2></td></tr>
    <tr><td><strong>Tipo de Medida:</strong></td><td>  {{ $valor['Medida']['TipoMedida']}}</td></tr>
    <tr><td><strong>Estado:</strong></td><td>  {{ $valor['Medida']['Estado']}}</td></tr>
    <tr><td><strong>Nro de Requerimiento:</strong></td><td>  {{ $valor['Medida']['NroRequerimiento']}}</td></tr>
    <tr><td><strong>fecha de Carga:</strong></td><td>  {{ $valor['Medida']['FechaCargaRequerimiento']}}</td></tr>
    <tr><td><strong>Organismo de Carga:</strong></td><td>  {{ $valor['Medida']['OrganismoCarga']}}</td></tr>
    <tr><td><strong>Dominio:</strong></td><td>  {{ $valor['Dominio']}}</td></tr>
    <tr><td><strong>Nro Chasis:</strong></td><td>  {{ $valor['NroChasis']}}</td></tr>
    <tr><td><strong>Nro Motor:</strong></td><td>  {{ $valor['NroMotor']}}</td></tr>
    <tr><td><strong>Año:</strong></td><td>  {{ $valor['Anio']}}</td></tr>
    <tr><td><strong>Lugar de Radicacion:</strong></td><td>  {{ $valor['LugarRadicacion']}}</td></tr>
    <tr><td><strong>Otro Modelo:</strong></td><td>  {{ $valor['OtroModelo']}}</td></tr>
    <tr><td><strong>Marca de Motor:</strong></td><td>  {{ $valor['MarcaMotor']}}</td></tr>
    <tr><td><strong>Color:</strong></td><td>  {{ $valor['Color']}}</td></tr>
    <tr><td><strong>Tipo de Vehiculo:</strong></td><td>  {{ $valor['TipoVehiculo']}}</td></tr>
    <tr><td><strong>Marca:</strong></td><td>  {{ $valor['Marca']}}</td></tr>
    <tr><td><strong>Modelo:</strong></td><td>  {{ $valor['Modelo']}}</td></tr>
    <tr><td><h2>Titular</h2></td></tr>
    <tr><td><strong>Nombre:</strong></td><td>  {{ $valor['Titular']['Nombre']}}</td></tr>
    <tr><td><strong>Apellido:</strong></td><td>  {{ $valor['Titular']['Apellido']}}</td></tr>
    <tr><td><strong>Sexo:</strong></td><td>  {{ $valor['Titular']['Sexo']}}</td></tr>
    <tr><td><strong>Tipo de Documento:</strong></td><td>  {{ $valor['Titular']['TipoDocumento']}}</td></tr>
    <tr><td><strong>Nro de Documento:</strong></td><td>  {{ $valor['Titular']['NroDocumento']}}</td></tr>
    <tr><td><strong>Nacionalidad:</strong></td><td>  {{ $valor['Titular']['Nacionalidad']}}</td></tr>
    <tr><td><h2>Autoridad Judicial</h2></td></tr>
    <tr><td><strong>Fecha de Oficio:</strong></td><td>  {{ $valor['AutoridadJudicial']['FechaOficio']}}</td></tr>
    <tr><td><strong>Nro de Oficio:</strong></td><td>  {{ $valor['AutoridadJudicial']['NroOficio']}}</td></tr>
    <tr><td><strong>Caratula:</strong></td><td>  {{ $valor['AutoridadJudicial']['Caratula']}}</td></tr>
    <tr><td><strong>Juzgado:</strong></td><td>  {{ $valor['AutoridadJudicial']['Juzgado']}}</td></tr>
    <tr><td><strong>Otro Juzgado:</strong></td><td>  {{ $valor['AutoridadJudicial']['JuzgadoOtro']}}</td></tr>
    <tr><td><strong>Secretaria:</strong></td><td>  {{ $valor['AutoridadJudicial']['Secretaria']}}</td></tr>
    <tr><td><strong>Nro de Causa:</strong></td><td>  {{ $valor['AutoridadJudicial']['NroCausa']}}</td></tr>
    <tr><td><strong>Fiscalia:</strong></td><td>  {{ $valor['AutoridadJudicial']['Fiscalia']}}</td></tr>
    <tr><td><strong>Otra Fiscalia:</strong></td><td>  {{ $valor['AutoridadJudicial']['OtraFiscalia']}}</td></tr>
    <tr><td><strong>Nro de Causa/Expediente:</strong></td><td>  {{ $valor['AutoridadJudicial']['NroCausaExpediente']}}</td></tr>
    <tr><td><strong>Observaciones Judiciales:</strong></td><td>  {{ $valor['AutoridadJudicial']['ObservacionesJudicial']}}</td></tr>









</table>
</div>
@endforeach