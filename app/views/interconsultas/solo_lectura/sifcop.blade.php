<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Nacionalidad</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
    <tr>
    	<td>{{ $value['Medida']['TipoMedida'] }}</td>
    	<td>{{ $value['Nombre'] }} {{ $value['Apellido'] }}</td>
    	<td>{{ $value['TipoDocumento']}} {{ $value['NroDocumento'] }}</td>
    	<td>{{ $value['FechaNacimiento']}} </td>
    	<td>{{ $value['Nacionalidad']}} </td>
    	<td>{{ $value['Provincia']}}-{{ $value['Localidad']}}-{{ $value['Direccion']}}-{{ $value['Numero']}}-{{ $value['Piso']}}-{{ $value['Departamento']}}</td>
    	<td>{{ $value['AutoridadJudicial']['Juzgado']}}{{ $value['AutoridadJudicial']['JuzgadoOtro'] }} </td>
    	<td>{{ $value['AutoridadJudicial']['FechaOficio'] }} </td>
    	<td>{{ $value['AutoridadJudicial']['Caratula']}}{{ $value['AutoridadJudicial']['NroCausa']}}{{ $value['AutoridadJudicial']['ObservacionesJudicial'] }}</td>
    	<td>{{ $value['Medida']['Estado']}}{{ $value['Medida']['NroRequerimiento'] }}</td>
      </tr>
    @endforeach
  </table>


{{-- <table class="table table-bordered table-hover">
<tr><td><strong>Tipo de medida: </strong>{{ $servicio[0]['Medida']['TipoMedida']}} </td></tr>
<tr><td><strong>Nombre/s y Apellido/s: </strong>{{ $servicio[0]['Nombre'] }} {{ $servicio[0]['Apellido'] }} </td></tr>
<tr><td><strong>Documento: </strong>{{ $servicio[0]['TipoDocumento']}} {{ $servicio[0]['NroDocumento'] }} </td></tr>
<tr><td><strong>Fecha de Nacimiento: </strong> {{ $servicio[0]['FechaNacimiento']}}</td></tr>
<tr><td><strong>Nacionalidad: </strong> {{ $servicio[0]['Nacionalidad']}}</td></tr>
<tr><td><strong>Domicilio: </strong>{{ $servicio[0]['Provincia']}}-{{ $servicio[0]['Localidad']}}-{{ $servicio[0]['Direccion']}}-{{ $servicio[0]['Numero']}}-{{ $servicio[0]['Piso']}}-{{ $servicio[0]['Departamento']}}</td></tr>
<tr><td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente): </strong> {{ $servicio[0]['AutoridadJudicial']['Juzgado']}}{{ $servicio[0]['AutoridadJudicial']['JuzgadoOtro']}}</td></tr>
<tr><td><strong>Fecha de la medida (Oficio/Resolución): </strong>{{ $servicio[0]['AutoridadJudicial']['FechaOficio']}} </td></tr>
<tr><td><strong>Datos de la Causa (Carátula, N° de Causa): </strong> {{ $servicio[0]['AutoridadJudicial']['Caratula']}}{{ $servicio[0]['AutoridadJudicial']['NroCausa']}}{{ $servicio[0]['AutoridadJudicial']['ObservacionesJudicial']}}</td></tr>
<tr><td><strong>Vigente/Sin Efecto (Positivo/Negativo): </strong>{{ $servicio[0]['Medida']['Estado']}}{{ $servicio[0]['Medida']['NroRequerimiento']}} </td></tr>
</table> --}}