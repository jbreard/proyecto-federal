<table class="table table-bordered table-hover" style="margin-top: 1em !important">
    <tr>
      <td><strong>Tipo de medida</strong></td>
      <td><strong>Nombre/s y Apellido/s </strong></td>
      <td><strong>Documento</strong></td>
      <td><strong>Fecha de Nacimiento</strong></td>
      <td><strong>Nacionalidad</strong></td>
      <td><strong>Domicilio</strong></td>
      <td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente)</strong></td>
      <td><strong>Fecha de la medida (Oficio/Resolución)</strong></td>
      <td><strong>Datos de la Causa (Carátula, N° de Causa)</strong></td>
      <td><strong>Vigente/Sin Efecto (Positivo/Negativo)</strong></td>
    </tr>
    @foreach($servicio as $key => $value)
    <tr>
    	<td>{{ $value['TipoResolucion'] }}</td>
    	<td>{{ $value['Nombres']}} {{ $value['Apellidos'] }}</td>
    	<td>{{ $value['TipoDocumento']}}: {{ $value['NumeroDocumento'] }}</td>
    	<td>{{ $value['FechaNacimiento'] }}</td>
    	<td>{{ $value['Nacionalidad'] }}</td>
    	<td>{{ $value['Localidad'] }}</td>
    	<td>{{ $value['Organismo']}} {{ $value['Provincia']}} {{ $value['Responsable']}} {{ $value['Secretaria'] }}</td>
    	<td>{{ $value['FechaOficio'] }}</td>
    	<td>{{ $value['TipoCausa']}} {{ $value['NumeroCausa'] }}</td>
    	<td>{{ $value['Vigente']}}</td>
      </tr>
    @endforeach
  </table>
{{-- <table class="table table-bordered table-hover">
  <tr><td><strong>Tipo de medida: </strong>{{ $servicio[0]['TipoResolucion']}}</td></tr>
  <tr><td><strong>Nombre/s y Apellido/s:  </strong>{{ $servicio[0]['Nombres']}} {{ $servicio[0]['Apellidos']}}</td></tr>
  <tr><td><strong>Documento: </strong>{{ $servicio[0]['TipoDocumento']}}: {{ $servicio[0]['NumeroDocumento']}}</td></tr>
  <tr><td><strong>FechaNacimiento: </strong>{{ $servicio[0]['FechaNacimiento']}}</td></tr>
  <tr><td><strong>Nacionalidad: </strong> {{ $servicio[0]['Nacionalidad']}}</td></tr>
  <tr><td><strong>Domicilio: </strong> {{ $servicio[0]['Localidad']}}</td></tr>
  <tr><td><strong>Autoridad Interviniente (Juzgado y Secretaría, Fiscalía Interviniente): </strong>{{ $servicio[0]['Organismo']}} {{ $servicio[0]['Provincia']}} {{ $servicio[0]['Responsable']}} {{ $servicio[0]['Secretaria']}}</td></tr>
  <tr><td><strong>Fecha de la medida (Oficio/Resolución): </strong>{{ $servicio[0]['FechaOficio']}}</td></tr>
  <tr><td><strong>Datos de la Causa (Carátula, N° de Causa): </strong> {{ $servicio[0]['TipoCausa']}} {{ $servicio[0]['NumeroCausa']}}</td></tr>
 <tr><td><strong>Vigente/Sin Efecto (Positivo/Negativo): </strong>{{ $servicio[0]['Vigente']}}</td></tr>

</table> --}}