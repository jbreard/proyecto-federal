<div class="tabbable">
	<ul class="nav nav-tabs">
            @foreach($indices as $key => $value)
			@if($key==0)
				<li class="active"><a href="#{{ $value }}" data-toggle="tab">{{strtoupper($value)}}</a></li>
			@else
				<li><a href="#{{ $value }}"  data-toggle="tab">{{strtoupper($value)}}</a></li>
			@endif
		@endforeach
	</ul>
	<div class="tab-content ">
		@foreach($servicios as $tiposervicio => $servicio)

			@if($indices[0]===$tiposervicio)
				<div class="tab-pane  active " id="{{$tiposervicio}}">
					@else
						<div class="tab-pane " id="{{$tiposervicio}}">
							@endif
							@if($options === 'armas')
								@include("interconsultas.solo_lectura.{$tiposervicio}_arma")
							@else
								@include("interconsultas.solo_lectura.{$tiposervicio}")
								{{-- @include("interconsultas.solo_lectura.migraciones") --}}
							@endif
						</div>
						@endforeach
				</div>
	</div>

