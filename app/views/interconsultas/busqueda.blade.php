@extends('header')
@include('interconsultas.scripts')
@section('content')
<div class="modal fade bd-example-modal-lg"  style="display: none" id="ubicacion" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Alerta</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <div  id="mensaje"><h3>Para usar Interconsultas debe habilitar la ubicacion  
                            <img src="{{ URL::asset('bootstrap/images/geo-alt.svg')}}" id="geo"> en el navegador </h3>
                   </div>
                    
                </div>
            </div>
    </div>
  </div>
</div>
  <h3>Interconsultas</h3>
  {{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
  <div class="panel panel-info">
    <div class="panel-heading">Busqueda</div>
    <div class="panel-body">
      <div class="form-group col-md-12">
        <div class="btn-group btn-group-toggle col-md-6" data-toggle="buttons">
          <label class="btn btn-default">
            <input type="radio" name="options" id="personas" value="personas"required > Persona
          </label>
          <label class="btn btn-default">
            <input type="radio" name="options" id="vehiculos" value="vehiculos" required> Vehiculo
          </label>
          <label class="btn btn-default">
            <input type="radio" name="options" id="armas" value="armas" required> Armas
          </label>
        </div>
      </div>
      <div class="form-group col-md-12">
        <div class="form-group col-md-5">
          <label> Dato</label>
           <div id='security' class=" oculto" >
          <br>@include("interconsultas.captcha")<br><br>
           </div>
          <div class="input-group ">
            <input type="hidden" id="latitud" name="latitud">
            <input type="hidden" id="longitud" name="longitud">
            <input type="hidden" id="dispositivo" name="dispositivo">
            <input type="hidden" id="ip" name="ip">
            <input type="text" class="form-control" placeholder="Buscar" id="dato" name="dato" required>
            <div class="input-group-btn">
              <button class="btn btn-success" type="submit" id="buscar">Consulta de restricciones</button>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group col-md-12">
        <div id='botones' class=" oculto" >
          <div class="col-md-4">
            <button id="botonRenaper" class="btn btn-success">Renaper <i class="fa fa-search"></i> </button></div>
          {{--<div class="col-md-4"><button id="botonMigraciones" class="btn btn-success">Migraciones <i class="fa fa-search"></i> </button></div>--}}
          {{--<div class="col-md-4"><button id="botonAnses" class="btn btn-success">ANSES <i class="fa fa-search"></i> </button></div>--}}
        </div>
        {{--<div id='botondrnpa' class=" oculto">--}}
        {{--<div class="col-md-4"><button id="botonDrnpa" class="btn btn-success">DRNPA <i class="fa fa-search"></i> </button></div>--}}
        {{--</div>--}}
      </div>
    </div>
  </div>
  <div class="alert alert-warning" id="cartel" hidden="true" >
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul style="list-style: none">
                        <li>Debe habilitar la ubicación de su navegador y recargar la página (F5)</li>
                        
                    </ul>
                </div>
  <div id="resultado"></div>
<script>
    let navegador = navigator.userAgent;
 document.getElementById('dispositivo').value=navegador;
    //Pedir activación de ubicación

if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function(pos) {
    
    //Si es aceptada guardamos lo latitud y longitud
     var lat = pos.coords.latitude;
     var long = pos.coords.longitude;
     document.getElementById('latitud').value=lat;
     document.getElementById('longitud').value=long;

}, function(error) {

    //Si es rechazada enviamos de error por consola

   // $('#ubicacion').modal('show'); 
     document.getElementById('dato').disabled = true;
     document.getElementById('personas').disabled;
     document.getElementById('vehiculos').disabled;
     document.getElementById('armas').disabled;
     document.getElementById('buscar').disabled = true;
     document.getElementById('cartel').hidden = false;
     document.getElementById('botonRenaper').disabled = true;

});
   

    
</script>




  {{Form::close() }}
@stop
