<li class="openable open"  >
    <a href="#" id="notificaciones_no_vistas"> <span class="menu-icon">
        <i class="fa fa-bell fa-lg"></i>
    </span>

        <span class="text" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Requerimentos Pendientes">
            Requerimientos
        </span>
    <span class="menu-hover"></span>
    <span class="badge badge-danger bounceIn animation-delay6 cantidad_no_leidos" ></span>
    </a>
    @include('menu.listado_oficios',['notificacion'=>'notificacion','colorBadge'=>'danger','idMenu'=>'no_vistos_detalle','param'=>['req_pendientes'=>2]])
</li>

<li class="openable open">
    <a href="#" id="notificaciones_modificaciones_no_vistas"> <span class="menu-icon">
        <i class="fa fa-envelope fa-lg"></i>
    </span>

        <span class="text" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Requerimentos Modificados">
            Req Modificados
        </span>
        <span class="menu-hover"></span>
        <span class="badge badge-warning bounceIn animation-delay6 req_mod" ></span>
    </a>
    @include('menu.listado_oficios',['notificacion'=>'notificacion_modificados','colorBadge'=>'warning','idMenu'=>'modificados_detalle','param'=>['req_mod_pendientes'=>2]])
</li>



