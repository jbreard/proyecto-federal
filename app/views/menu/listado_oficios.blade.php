<ul class="submenu" id="{{ $idMenu }}">
  @foreach($menus as $menu)
   <li>
        <a href="{{ URL::route('oficios.index',$param+['tipo_medida'=>$menu->id]) }}">
          <span class="menu-icon">
            <span class="badge badge-{{$colorBadge}} bounceIn animation-delay6" id="{{$notificacion}}_{{ $menu->id }}"></span>
          </span>

        <span class="text" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="{{  $menu->descripcion  }}">
            {{ $menu->descripcion }}
        </span>
            <span class="menu-hover"></span>

        </a>
    </li>
    @endforeach

</ul>
