<li class="openable open alertas_tempranas">
    <a href="#" id="alertas_no_vistas"> <span class="menu-icon">
        <i class="fa fa-bell fa-lg"></i>
    </span>

        <span class="text" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Alertas Tempranas">
            Alertas Tempranas
        </span>
        <span class="menu-hover"></span>
        <span class="badge badge-danger bounceIn animation-delay6 " ></span>
    </a>
      @include('menu.alertas_tempranas_menu',['notificacion'=>'notificacion','colorBadge'=>'danger','idMenu'=>'alertas_no_vistos_detalle','param'=>['alerta_temp'=>1]])
</li>

<li class="openable open alertas_tempranas" >
    <a href="#"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>
        <span class="text">Cargar Alertas </span> <span class="menu-hover"></span> </a>
    <ul class="submenu">
        @foreach($menusAlertas as $menu)
            @if($menu->alerta_temprana==1)
            <li>
                <a href="{{ route($menu->ruta.'.create',['alerta_temp'=>1]) }}"><span class="submenu-label">{{ $menu->titulo_alerta_temprana or $menu->descripcion }}</span></a>
            </li>
            @endif
        @endforeach

    </ul>
</li>


