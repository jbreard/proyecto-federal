<ul class="submenu" id="{{ $idMenu }}">
@foreach($menus as $menu)
@if($menu->alerta_temprana==1)
@if($menu->id != 2)
    <li>
    <!--           <a href=""><span class="submenu-label">{{ $menu->descripcion }}</span>-->
    <!---->
    <!--            </a>-->

        <a href="{{ URL::route('oficios.index',$param+['tipo_medida'=>$menu->id]) }}">
            <span class="menu-icon">
                <span class="badge badge-{{$colorBadge}} bounceIn animation-delay6" id="{{$notificacion}}_{{ $menu->id }}"></span>
            </span>
            <span class="text" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="{{  $menu->titulo_alerta_temprana  }}">
                {{ $menu->descripcion }}
            </span>
        </a>
    </li>
@endif
@endif
@endforeach
</ul>
