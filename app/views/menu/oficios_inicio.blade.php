@foreach($menus as $menu)
    @if($menu->alerta_temprana)
        <a href="{{ route($menu->ruta.'.create') }}" class="btn btn-danger block">{{ $menu->descripcion }}</a>
    @else
        <a href="{{ route($menu->ruta.'.create') }}" class="btn btn-default block">{{ $menu->descripcion }}</a>
    @endif
    <div class="seperator"></div>
@endforeach


