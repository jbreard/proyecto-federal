<li class="openable open">
    <a href="#"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>
        <span class="text">Cargar Requerimiento </span> <span class="menu-hover"></span> </a>
    <ul class="submenu">
        @foreach($menus as $menu)
            <!--
                No son tenidas en cuenta como requerimiento las opciones
                de profugo / evadido y robo automotor
            -->
            @if($menu->id != 12 && $menu->id != 13)
                <li >
                    <a href="{{ route($menu->ruta.'.create') }}"><span class="submenu-label">{{ $menu->descripcion }}</span></a>
                </li>
            @endif
        @endforeach

    </ul>
</li>
