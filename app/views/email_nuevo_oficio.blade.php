<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <header>

        </header>
        <section>
            <h1>{{$mensajeArgumento}} en el: <br><b>{{Config::get('nombre.nombre')}}</b></h1>
            <br><br>
            <b>Usuario con acceso a sifcop:</b>
            Ingrese <b><a href="{{$ruta}}">aquí</a></b> para verlo.
            <br><br>
            <b>Usuario con acceso al Servicio De Mensajería:</b>
            <br><br>
            Reporte Generado por el sistema: Ingrese <b><a href="{{$rutaPdf}}">aquí</a></b> para verlo.
            <br><br>
            @if(!empty($rutaArchivo))
            Oficio Respaldatorio de la Medida:  Ingrese <b><a href="{{$rutaArchivo}}">aquí</a></b> para verlo.
            @endif

        </section>
    </body>

</html>
