<div class="panel panel-info">
    <div class="panel-heading">Autoridad Judicial Interviniente</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-4">
                {{ Form::label('fecha_oficio', 'Fecha Requerimiento:') }}
                {{ Form::text( 'fecha_oficio2', null, array('placeholder' => '', 'class' =>'form-control fecha')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('nro_oficio', 'N° de Oficio:') }}
                {{ Form::text( 'nro_oficio2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('caratula', 'Carátula de la Causa:') }}
                {{ Form::text( 'caratula2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('juzgado', 'Juzgado Interviniente:') }}
                {{ Form::select('juzgado2',$comboJuzgados ,isset($oficios) ? $oficios->juzgado2 : null, array('placeholder'=> '', 'class' => 'form-control select juzgado','data-destino'=>'#juzgado_otro_2')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('juzgado', 'Otro Juzgado Interviniente:') }}
                {{ Form::text('juzgado_otro_2',isset($oficios->juzgado_otro_2) ? $oficios->juzgado_otro_2 : null, array('placeholder'=> '', 'class' => 'form-control','id'=>'juzgado_otro_2','disabled'=>'disabled')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('secretaria', 'Secretaría:') }}
                {{ Form::text( 'secretaria2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('nro_causa', 'N° de Causa (Juzgado):') }}
                {{ Form::text( 'nro_causa2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('fiscalia', 'Fiscalía Interviniente:') }}
                {{ Form::select('fiscalia2',$combosFiscalias ,isset($oficios) ? $oficios->fiscalia2 : null, array('placeholder'=> '', 'class' => 'select form-control fiscalia','data-destino'=>'#otra_fiscalia_2')) }}
            </div>
             <div class="col-md-4">
                {{ Form::label('fiscalia', 'Otra Fiscalía Interviniente:') }}
                {{ Form::text('otra_fiscalia_2',isset($oficios->otra_fiscalia_2) ? $oficios->otra_fiscalia_2: null, array('placeholder'=> '', 'class' => 'form-control','disabled'=>'disabled','id'=>'otra_fiscalia_2')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('nro_causa_expediente', 'N° de Causa (Fiscalia):') }}
                {{ Form::text('nro_causa_expediente2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_judicial', 'Observaciones:') }}
                {{ Form::textarea( 'observaciones_judicial2', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Adjuntar Oficio</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-4">
                @include('formularios.formulario_archivo',array('nombre'=>'archivo3','id'=>'escaneo_oficio','i'=>0,'eliminar'=>false,'nombre_campo'=>'Cargar Oficio'))
            </div>
            <div class="col-md-4">
                @include('formularios.formulario_archivo',array('nombre'=>'archivo4','i'=>1,'eliminar'=>false,'nombre_campo'=>'Otros Archivos'))
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"> Atención... </h4>
            </div>
            <div class="modal-body">
                <center> <b id="mensaje"> </b></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar »</button>
            </div>
        </div>
    </div>
</div>
@include('listados.listado_documentos')