<div class="panel panel-info">
    @if(isset($profugo_evadido))
        <div class="panel-heading">Adjuntar comunicación</div>
    @elseif(isset($robo_automotor))
        <div class="panel-heading">Adjuntar denuncia/comunicación</div>
    @else
        <div class="panel-heading">Adjuntar Oficio</div>
    @endif
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-4">
                @include('formularios.formulario_archivo',array('nombre'=>'archivo1','id'=>'escaneo_oficio','i'=>0,'eliminar'=>false,'nombre_campo'=>'Cargar Oficio'))
            </div>
            <div class="col-md-4">
                @include('formularios.formulario_archivo',array('nombre'=>'archivo2','i'=>1,'eliminar'=>false,'nombre_campo'=>'Otros Archivos'))
            </div>
        </div>
    </div>
</div>