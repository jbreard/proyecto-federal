@extends('header')
@section('scripts')
    {{ HTML::script('assets/js/empty.js') }}
    {{ HTML::script('assets/js/pedido_secuestro_vehicular.js') }}
@stop
@section('content')
<h3>Pedido de Secuestro Vehícular @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')



{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}

<div class="panel panel-info">
    <div class="panel-heading">Datos del Vehículo</div>
       <div class="panel-body">
          <div class="form-group">
                <div class="col-md-3">

                {{ Form::label('dominio', 'Dominio') }}
                {{ Form::text( 'dominio',isset($secuestro_vehicular) ? $secuestro_vehicular->dominio: null, array('placeholder' => '', 'class' => 'form-control buscarPatente')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('anio_vehiculo', 'Año') }}
                {{ Form::text( 'anio_vehiculo',isset($secuestro_vehicular) ? $secuestro_vehicular->anio_vehiculo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('radicacion', 'Lugar de radicación del Vehículo') }}
                {{ Form::text( 'radicacion',isset($secuestro_vehicular) ? $secuestro_vehicular->radicacion: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('nro_chasis', 'N° de Chasis') }}
                {{ Form::text( 'nro_chasis', isset($secuestro_vehicular) ? $secuestro_vehicular->nro_chasis: null, array('placeholder'=> '', 'class' => 'form-control nro_chasis')) }}

            </div>

              <div class="col-md-3">
                  {{ Form::label('marca_vehiculo', 'Marca') }}
                  {{ Form::select('marca_vehiculo',$comboVehiculosMarcas ,isset($secuestro_vehicular) ? $secuestro_vehicular->getMarca->id : null, array('placeholder'=> '', 'class' => 'form-control vehiculo','data-target'=>'.marca')) }}
              </div>

            <div class="col-md-3">
                {{ Form::label('modelo_vehiculo', 'Tipo Vehículo') }}
                {{ Form::select('id_tipo_vehiculo',$comboTipoVehiculo,isset($secuestro_vehicular) ? $secuestro_vehicular->getModelo->id : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
              <div class="col-md-3">
                {{ Form::label('modelo_vehiculo', 'Modelo Vehículo') }}
                {{ Form::select('modelo_vehiculo',$comboVehiculosModelos ,isset($secuestro_vehicular) ? $secuestro_vehicular->getModelo->id : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
              <div class="col-md-3">
                  {{ Form::label('Color', 'Color') }}
                  {{ Form::text( 'color', isset($secuestro_vehicular) ? $secuestro_vehicular->color_auto: null, array('placeholder'=> '', 'class' => 'form-control')) }}
              </div>
              <div class="col-md-3">
                  {{ Form::label('marca_motor', 'Marca Motor') }}
                  {{ Form::text( 'marca_motor', isset($secuestro_vehicular) ? $secuestro_vehicular->marca_motor: null, array('placeholder'=> '', 'class' => 'form-control')) }}
              </div>
             <div class="col-md-3">
                {{ Form::label('nro_motor', 'N° de Motor') }}
                {{ Form::text( 'nro_motor', isset($secuestro_vehicular) ? $secuestro_vehicular->nro_motor: null, array('placeholder'=> '', 'class' => 'form-control nro_motor')) }}
             </div>
             <div class="col-md-3">
                {{ Form::label('otra_marca', 'Otra marca') }}
                {{ Form::text( 'otra_marca', isset($secuestro_vehicular) ? $secuestro_vehicular->otra_marca: null, array('placeholder'=> '', 'class' => 'form-control')) }}
             </div>
          </div>
    </div>
</div>

<div class="panl panel-info">
    <div class="panel-heading">Cargar Fotografias del Vehiculo</div>
    <div class="panel-body">
        @for ($i = 0; $i < 4; $i++)
            <div class="row">
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
            </div>
        @endfor
    </div>
</div>


@if(isset($secuestro_vehicular))
    @include('galeria',array("titulo"=>"Fotografias del Vehiculo","fotos"=>$secuestro_vehicular->getFotos))
@endif


<div class="panel panel-info">
    <div class="panel-heading">Titular Registral </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">

                {{ Form::label('datos_persona[titular][nombre]', 'Nombres') }}
                {{ Form::text( 'datos_persona[titular][nombre]', isset($titular)? $titular->nombre: null, array('placeholder' => '', 'class' => 'form-control nombre','data-novalidate'=>"true")) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[titular][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[titular][apellido]',isset($titular)? $titular->apellido: null, array('placeholder'=> '', 'class' => 'form-control apellido','data-novalidate'=>"true")) }}
            </div>

        <div class="col-md-3">
            {{ Form::label('datos_persona[titular][apellido_materno]', 'Apellido Materno') }}
            {{ Form::text( 'datos_persona[titular][apellido_materno]',isset($titular)? $titular->apellido_materno:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
        </div>
            <div class="col-md-3">
                {{ Form::label('tipo_documento', 'Tipo de Documento') }}
                {{ Form::select('datos_persona[titular][id_tipo_documento]',$comboTipoDocumento ,isset($titular)? $titular->id_tipo_documento: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('nro_documento', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[titular][nro_documento]', isset($titular) ? $titular->nro_documento: null, array('placeholder'=> '', 'class' => 'form-control buscar_dni',"data-tipo_oficio"=>"2")) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_sexo', 'Sexo') }}
                {{ Form::select( 'datos_persona[titular][id_sexo]',$comboSexo,isset($titular) ? $titular->id_sexo: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('edad', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[titular][edad]', isset($titular) ? $titular->edad: null, array('placeholder'=> '', 'class' => 'form-control fecha')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
                {{ Form::select('datos_persona[titular][id_nacionalidad]',$comboNacionalidad,isset($titular) ? $titular->id_nacionalidad: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_calle', 'Direccion') }}
                {{ Form::text( 'datos_persona[titular][ultimo_calle]',isset($titular) ? $titular->ultimo_calle: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('interseccion', 'Interseccion') }}
                {{ Form::text( 'datos_persona[titular][interseccion]',isset($titular) ? $titular->interseccion: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_numero', 'N°') }}
                {{ Form::text( 'datos_persona[titular][ultimo_numero]',isset($titular) ? $titular->ultimo_numero: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Piso') }}
                {{ Form::text( 'datos_persona[titular][ultimo_piso]',isset($titular) ? $titular->ultimo_piso: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_departamento', 'Departamento') }}
                {{ Form::text( 'datos_persona[titular][ultimo_departamento]',isset($titular) ? $titular->ultimo_departamento: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_codigo_postal', 'Codigo Postal') }}
                {{ Form::text( 'datos_persona[titular][ultimo_codigo_postal]',isset($titular) ? $titular->ultimo_codigo_postal: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>


            <div class="col-md-3">
                {{ Form::label('provincia', 'Provincia') }}
                {{ Form::select( 'datos_persona[titular][ultima_provincia]',$comboProvincia,isset($titular) ? $titular->ultima_provincia: null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_partido', 'Partido') }}
                {{ Form::select( 'datos_persona[titular][id_ultimo_partido]',[],null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($titular) ? $titular->id_ultimo_partido:null)) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_localidad', 'Localidad') }}
                {{ Form::text( 'datos_persona[titular][ultima_localidad]',null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_persona', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[titular][observaciones_persona]',null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Datos del Denunciante</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nombre]','Nombres') }}
                {{ Form::text('datos_persona[responsable][nombre]',isset($responsable)? $responsable->nombre : null,array('placeholder' => '', 'class' =>'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[responsable][apellido]',isset($responsable)? $responsable->apellido : null, array('placeholder'=> '', 'class' =>'form-control apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[responsable][apellido_materno]', isset($responsable)? $responsable->apellido_materno: null, array('placeholder'=> '', 'class' =>'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[responsable][apodos]',isset($responsable) ? $responsable->apodos:null, array('placeholder' => '', 'class' =>'form-control alphaonly')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_tipo_documento]', 'Tipo de Documento') }}
                {{ Form::select( 'datos_persona[responsable][id_tipo_documento]',$comboTipoDocumento ,isset($responsable)? $responsable->id_tipo_documento : null, array('placeholder'=> '','class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[responsable][nro_documento]',isset($responsable) ? $responsable->nro_documento :null, array('placeholder'=> '', 'class' =>'form-control alphanumericonly')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[responsable][id_sexo]',$comboSexo,isset($responsable)? $responsable->id_sexo : null, array('placeholder'=> '', 'class' =>'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[responsable][edad]', isset($responsable)? $responsable->edad : null, array('placeholder'=> '', 'class' =>'fecha form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[responsable][id_nacionalidad]',$comboNacionalidad,isset($responsable)? $responsable->id_nacionalidad : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}

            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[responsable][observaciones_persona]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[responsable][observaciones_persona]',isset($responsable)? $responsable->observaciones_persona : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

        </div>
    </div>
</div>

@include('formularios.autoridad_judicial')

<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class= "btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>


<button class="btn btn-primary" id="guardar_form" data-tipo_oficio="2">Guardar »</button>
</div>
{{Form::close() }}

@stop
