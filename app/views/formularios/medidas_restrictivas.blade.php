@extends('header')
@section('breadcrumb')
<li>Cargar Requerimiento</li>
<li>Medidas Restrictivas</li>
@stop
@section('content')
<h3>Medidas Restrictivas @if(Session::has("alerta_temprana"))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')

{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Medida</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('medida_restrictiva', 'Tipo de Medida Restrictiva') }}
                {{ Form::text( 'medida_restrictiva', null, array('placeholder'=> '', 'class' => 'form-control medida_restrictiva')) }}
            </div>
           <!-- <div class="col-md-3">
                @{{ Form::label('objeto_medida', 'Objeto de la Medida') }}
                @{{ Form::text( 'objeto_medida', null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div> -->
        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Datos del Denunciado</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nombre]','Nombres') }}
                {{ Form::text('datos_persona[responsable][nombre]',isset($responsable)? $responsable->nombre : null,array('placeholder' => '', 'class' =>'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[responsable][apellido]',isset($responsable)? $responsable->apellido : null, array('placeholder'=> '', 'class' =>'form-control apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[responsable][apellido_materno]', isset($responsable)? $responsable->apellido_materno: null, array('placeholder'=> '', 'class' =>'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[responsable][apodos]',isset($responsable) ? $responsable->apodos:null, array('placeholder' => '', 'class' =>'form-control alphaonly')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_tipo_documento]', 'Tipo de Documento') }}
                {{ Form::select( 'datos_persona[responsable][id_tipo_documento]',$comboTipoDocumento ,isset($responsable)? $responsable->id_tipo_documento : null, array('placeholder'=> '','class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[responsable][nro_documento]',isset($responsable) ? $responsable->nro_documento :null, array('placeholder'=> '', 'class' =>'form-control alphanumericonly buscar_dni',"data-tipo_oficio"=>"5")) }}


            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>
	    <!--
            <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div>-->

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[responsable][id_sexo]',$comboSexo,isset($responsable)? $responsable->id_sexo : null, array('placeholder'=> '', 'class' =>'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_genero]', 'Genero') }}
                {{ Form::select( 'datos_persona[responsable][id_genero]',$comboGenero,isset($responsable)? $responsable->id_genero : null, array('placeholder'=> '', 'class' =>'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[responsable][edad]', isset($responsable)? $responsable->edad : null, array('placeholder'=> '', 'class' =>'fecha form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[responsable][id_nacionalidad]',$comboNacionalidad,isset($responsable)? $responsable->id_nacionalidad : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}

            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[responsable][observaciones_persona]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[responsable][observaciones_persona]',isset($responsable)? $responsable->observaciones_persona : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultimo_calle]', 'Dirección') }}
                {{ Form::text( 'datos_persona[responsable][ultimo_calle]',isset($responsable)? $responsable->ultimo_calle : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultimo_numero]', 'N°') }}
                {{ Form::text( 'datos_persona[responsable][ultimo_numero]',isset($responsable)? $responsable->ultimo_numero : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultimo_piso]', 'Piso') }}
                {{ Form::text( 'datos_persona[responsable][ultimo_piso]',isset($responsable)? $responsable->ultimo_piso : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][interseccion]', 'Interseccion') }}
                {{ Form::text( 'datos_persona[responsable][interseccion]',isset($responsable)? $responsable->interseccion : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultimo_departamento]', 'Departamento') }}
                {{ Form::text( 'datos_persona[responsable][ultimo_departamento]',isset($responsable)? $responsable->ultimo_departamento: null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultima_provincia]', 'Provincia') }}
                {{ Form::select( 'datos_persona[responsable][ultima_provincia]',$comboProvincia,isset($responsable)? $responsable->ultima_provincia : null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_ultimo_partido]', 'Partido') }}
                {{ Form::select( 'datos_persona[responsable][id_ultimo_partido]',[],null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($responsable)? $responsable->id_ultimo_partido : null)) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][ultima_localidad]', 'Localidad') }}
                {{ Form::text( 'datos_persona[responsable][ultima_localidad]',isset($responsable)? $responsable->ultima_localidad : null, array('placeholder'=> '', 'class' =>
                'form-control localidad')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][observaciones]', 'Datos Adicionales') }}
                {{ Form::text( 'datos_persona[responsable][observaciones]',isset($responsable)? $responsable->observaciones : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>

        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Fotografías del Denunciado</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'fotoResponsable[responsable][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Foto'))
                 </div>
                <div id="fotos_append">

                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('galeria',["fotos"=>isset($responsable)? $responsable->getFotos: null,"titulo"=>"Fotos Responsable"])

<div class="panel panel-info">
    <div class="panel-heading">Datos de la Víctima</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][nombre]', 'Nombre') }}
                {{ Form::text( 'datos_persona[victima][nombre]', isset($victima)? $victima->nombre: null, array('placeholder' => '', 'class' =>
                'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[victima][apellido]', isset($victima)? $victima->apellido: null, array('placeholder'=> '', 'class' =>
                'form-control alphaonly')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[victima][apellido_materno]', isset($victima)? $victima->apellido_materno: null, array('placeholder'=> '', 'class' =>
                'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][id_tipo_documento]', 'Tipo de Documento') }}
                {{ Form::select( 'datos_persona[victima][id_tipo_documento]',[$comboTipoDocumento] ,isset($victima)? $victima->id_tipo_documento: null, array('placeholder'=> '',
                'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[victima][nro_documento]', isset($victima)? $victima->nro_documento:null, array('placeholder'=> '', 'class' =>
                'form-control buscar_dni',"data-tipo_oficio"=>"5")) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[victima][id_sexo]',[$comboSexo],isset($victima)? $victima->id_sexo: null, array('placeholder'=> '', 'class' =>
                'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[victima][edad]',isset($victima)? $victima->edad: null, array('placeholder'=> '', 'class' =>
                'form-control fecha')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[victima][id_nacionalidad]',$comboNacionalidad,isset($victima)? $victima->id_nacionalidad: null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido de la Victima</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultimo_calle]', 'Dirección') }}
                {{ Form::text( 'datos_persona[victima][ultimo_calle]',isset($responsable)? $responsable->ultimo_calle : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultimo_numero]', 'N°') }}
                {{ Form::text( 'datos_persona[victima][ultimo_numero]',isset($responsable)? $responsable->ultimo_numero : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultimo_piso]', 'Piso') }}
                {{ Form::text( 'datos_persona[victima][ultimo_piso]',isset($responsable)? $responsable->ultimo_piso : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][interseccion]', 'Interseccion') }}
                {{ Form::text( 'datos_persona[victima][interseccion]',isset($responsable)? $responsable->interseccion : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultimo_departamento]', 'Departamento') }}
                {{ Form::text( 'datos_persona[victima][ultimo_departamento]',isset($responsable)? $responsable->ultimo_departamento: null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultima_provincia]', 'Provincia') }}
                {{ Form::select( 'datos_persona[victima][ultima_provincia]',$comboProvincia,isset($responsable)? $responsable->ultima_provincia : null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][id_ultimo_partido]', 'Partido') }}
                {{ Form::select( 'datos_persona[victima][id_ultimo_partido]',[],null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($responsable)? $responsable->id_ultimo_partido : null)) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][ultima_localidad]', 'Localidad') }}
                {{ Form::text( 'datos_persona[victima][ultima_localidad]',isset($responsable)? $responsable->ultima_localidad : null, array('placeholder'=> '', 'class' =>
                'form-control localidad')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[victima][observaciones]', 'Datos Adicionales') }}
                {{ Form::text( 'datos_persona[victima][observaciones]',isset($responsable)? $responsable->observaciones : null, array('placeholder'=> '', 'class' =>
                'form-control')) }}
            </div>

        </div>
    </div>
</div>


<div class="panel panel-info">
    <div class="panel-heading">Fotografías de la Victima</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="fotoVictima">
                    @include('formularios.formulario_archivo',array('nombre'=>'fotoVictima[victima][]','i'=>0,'eliminar'=>false))
                </div>
                <div id="fotos_append_victima"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="agregar_foto" data-contenedor="#fotoVictima" data-receptor="#fotos_append_victima">Agregar Más Fotos</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('galeria',["fotos"=>isset($victima)? $victima->getFotos: null,"titulo"=>"Fotos Victima"])
@include('formularios.autoridad_judicial')
<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class=btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar »</button>
{{Form::close() }}
@stop
