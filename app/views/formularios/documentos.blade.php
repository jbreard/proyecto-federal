
<?php
$i = (isset($i)) ? $i : "0";
$id = (isset($datos->id_documento))?$datos->id_documento:$i;

?>
<div class="col-md-3">
    {{ Form::label('id_tipo_documento', 'Tipo de documento') }}
    {{ Form::select('datos_documento['.$id.'][id_tipo_documento]',$comboTipoDocumento, isset($datos->id_tipo_documento) ? $datos->id_tipo_documento:null, array('placeholder' => '',
    'class' => 'form-control id_tipo_documento'.$id)) }}
    @if(isset($datos->id_documento))
        {{ Form::hidden('datos_documento['.$id.'][id_documento]',$id) }}
    @endif
</div>
<div class="col-md-3">
    {{ Form::label('nro_documento', 'N° de Documento') }}
    {{ Form::text('datos_documento['.$id.'][nro_documento]', isset($datos->nro_documento) ? $datos->nro_documento:null, array('placeholder' => '',
    'class' => 'form-control nro_documento'.$id)) }}
    @if(isset($datos->id_documento))
        {{ Form::hidden('datos_documento['.$id.'][id_documento]',$id) }}
    @endif
</div>
