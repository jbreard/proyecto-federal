@extends('header')
@section('breadcrumb')
<li>Cargar Requerimiento</li>
<li>Otras Medidas</li>
@stop
@section('content')
<h3>Otras Medidas @if(Session::has("alerta_temprana"))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')

{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Medida</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('medida_restrictiva', 'Tipo de Medida') }}
                {{ Form::text( 'medida_restrictiva', null, array('placeholder'=> '', 'class' => 'form-control medida_restrictiva')) }}
            </div>
        </div>
    </div>
</div>


<div class="panel panel-info">
    <div class="panel-heading">Datos de la Persona</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nombre]','Nombres') }}
                {{ Form::text('datos_persona[responsable][nombre]',isset($responsable)? $responsable->nombre : null,array('placeholder' => '', 'class' =>'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[responsable][apellido]',isset($responsable)? $responsable->apellido : null, array('placeholder'=> '', 'class' =>'form-control apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[responsable][apellido_materno]', isset($responsable)? $responsable->apellido_materno: null, array('placeholder'=> '', 'class' =>'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[responsable][apodos]',isset($responsable) ? $responsable->apodos:null, array('placeholder' => '', 'class' =>'form-control alphaonly')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_tipo_documento]', 'Tipo de Documento') }}
                {{ Form::select( 'datos_persona[responsable][id_tipo_documento]',$comboTipoDocumento ,isset($responsable)? $responsable->id_tipo_documento : null, array('placeholder'=> '','class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[responsable][nro_documento]',isset($responsable) ? $responsable->nro_documento :null, array('placeholder'=> '', 'class' =>'form-control alphanumericonly buscar_dni',"data-tipo_oficio"=>"9")) }}
            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>

           <!-- <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div> -->

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[responsable][id_sexo]',$comboSexo,isset($responsable)? $responsable->id_sexo : null, array('placeholder'=> '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_genero]', 'Genero') }}
                {{ Form::select( 'datos_persona[responsable][id_genero]',$comboGenero,isset($responsable)? $responsable->id_genero : null, array('placeholder'=> '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[responsable][edad]', isset($responsable)? $responsable->edad : null, array('placeholder'=> '', 'class' =>'fecha form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[responsable][id_nacionalidad]',$comboNacionalidad,isset($responsable)? $responsable->id_nacionalidad : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[responsable][observaciones_persona]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[responsable][observaciones_persona]',isset($responsable)? $responsable->observaciones_persona : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Descripción</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-12">
                {{ Form::label('datos_persona[responsable][nombre]','Descripción') }}
                {{ Form::textarea('datos_persona[responsable][observaciones_persona]',isset($responsable)? $responsable->observaciones_persona : null,array('placeholder' => '', 'class' =>
                'form-control ')) }}
            </div>

        </div>
    </div>
</div>

@include('formularios.autoridad_judicial')


<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class=btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar »</button>
{{Form::close() }}
@stop
