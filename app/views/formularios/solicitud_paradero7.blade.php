@extends('header')
@section('scripts')

<script>
    $().ready(function(){
        $("#facebook").prop("disabled",true);
        $("#twitter").prop("disabled",true);
        $("#instagram").prop("disabled",true);
        $("#badoo").prop("disabled",true);
        $("#snapchat").prop("disabled",true);
        $("#linkedin").prop("disabled",true);
        $("#otro").prop("disabled",true);

        $("#id_estado_civil").change(function(){
            var text = $("#id_estado_civil option:selected").text();
            if(text == "Casado/a")
                $("#nombre_conyuge").prop('disabled', false);
            else
                $("#nombre_conyuge").prop('disabled', true);

        });

        $('#chk_facebook').change(function(){
            if ($('#chk_facebook').prop('checked'))
                $("#facebook").prop('disabled', false);
            else
                $("#facebook").prop("disabled",true);
        });

        $('#chk_twitter').change(function(){
            if ($('#chk_twitter').prop('checked'))
                $("#twitter").prop('disabled', false);
            else
                $("#twitter").prop("disabled",true);
        });

        $('#chk_badoo').change(function(){
            if ($('#chk_badoo').prop('checked'))
                $("#badoo").prop('disabled', false);
            else
                $("#badoo").prop("disabled",true);
        });

        $('#chk_instagram').change(function(){
            if ($('#chk_instagram').prop('checked'))
                $("#instagram").prop('disabled', false);
            else
                $("#instagram").prop("disabled",true);
        });

        $('#chk_linkedin').change(function(){
            if ($('#chk_linkedin').prop('checked'))
                $("#linkedin").prop('disabled', false);
            else
                $("#linkedin").prop("disabled",true);
        });

        $('#chk_snapchat').change(function(){
            if ($('#chk_snapchat').prop('checked'))
                $("#snapchat").prop('disabled', false);
            else
                $("#snapchat").prop("disabled",true);
        });

        $('#chk_otro').change(function(){
            if ($('#chk_otro').prop('checked'))
                $("#otro").prop('disabled', false);
            else
                $("#otro").prop("disabled",true);
        });
    });
</script>
@stop
@section('content')
<h3>{{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')
{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Persona Buscada</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[desaparecido][apellido]',isset($buscado) ? $buscado->apellido:null, array('placeholder' => '', 'class' =>
                'form-control apellido','id'=>'apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][nombre]', 'Nombres') }}
                {{ Form::text( 'datos_persona[desaparecido][nombre]',isset($buscado) ? $buscado->nombre:null, array('placeholder' => '', 'class' =>
                'form-control nombre','id'=>'nombre')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[desaparecido][apodos]',isset($buscado) ? $buscado->apodos:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[desaparecido][id_nacionalidad]',$comboNacionalidad,isset($buscado) ? $buscado->id_nacionalidad:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>


            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[desaparecido][edad]',isset($buscado) ? $buscado->edad:null, array('placeholder' => '', 'class' =>
                'form-control fecha')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[desaparecido][id_sexo]',$comboSexo,isset($buscado) ? $buscado->sexo:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_estado_civil]', 'Estado civil') }}
                {{ Form::select( 'datos_persona[desaparecido][id_estado_civil]',$comboEstadosCiviles,isset($buscado) ? $buscado->id_estado_civil:null, array('placeholder' => '',
                'class' => 'form-control','id' => 'id_estado_civil')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][nombre_conyuge]', 'Nombre del cónyuge') }}
                {{ Form::text( 'datos_persona[desaparecido][nombre_conyuge]',isset($buscado) ? $buscado->nombre_conyuge:null, array('placeholder' => '', 'class' =>
                'form-control','id' => 'nombre_conyuge')) }}
            </div>

            <div class="col-md-6">
                {{ Form::label('datos_persona[desaparecido][apellido_paterno]', 'Apellido y nombre paterno') }}
                {{ Form::text( 'datos_persona[desaparecido][apellido_materno]',isset($buscado) ? $buscado->apellido_paterno:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('datos_persona[desaparecido][apellido_materno]', 'Apellido y nombre materno') }}
                {{ Form::text( 'datos_persona[desaparecido][apellido_materno]',isset($buscado) ? $buscado->apellido_materno:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

          

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_tipo_documento]', 'Tipo Documento') }}
                {{ Form::select( 'datos_persona[desaparecido][id_tipo_documento]',$comboTipoDocumento,isset($buscado) ? $buscado->id_tipo_documento:null, array('placeholder' =>
                '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[desaparecido][nro_documento]',isset($buscado) ? $buscado->nro_documento:null, array('placeholder' => '', 'class' =>
                'form-control buscar_dni',"data-tipo_oficio"=>"1")) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('datos_persona[desaparecido][autoridad_expide_documento]', 'Autoridad de que lo expide') }}
                {{ Form::text( 'datos_persona[desaparecido][autoridad_expide_documento]',isset($buscado) ? $buscado->autoridad_expide_documento:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="row">
                <div class="col-md-1">
                    {{ Form::label('datos_persona[desaparecido][codArea_telefono]','Cod.Area') }}
                    {{ Form::text( 'datos_persona[desaparecido][codArea_telefono]',isset($buscado) ? $buscado->codArea_telefono:null, array('placeholder' => '011', 'class' =>
                    'form-control')) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('datos_persona[desaparecido][numero_telefono]', 'Telefono fijo') }}
                    {{ Form::text( 'datos_persona[desaparecido][numero_telefono]',isset($buscado) ? $buscado->numero_telefono:null, array('placeholder' => '', 'class' =>
                    'form-control')) }}
                </div>

                <div class="col-md-3">
                    {{ Form::label('datos_persona[desaparecido][id_compania_telefono]', 'Compañía') }}
                    {{ Form::select( 'datos_persona[desaparecido][id_compania_telefono]',$comboCompania,isset($buscado) ? $buscado->id_compania_telefono:null, array('placeholder'
                    => '', 'class' => 'form-control')) }}
                </div>
            </div>
            <div class="col-md-1">
                {{ Form::label('datos_persona[desaparecido][codArea_celular]','Cod.Area') }}
                {{ Form::text( 'datos_persona[desaparecido][codArea_celular]',isset($buscado) ? $buscado->codArea_celular:null, array('placeholder' => '011', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('datos_persona[desaparecido][numero_celular]', 'N° de Celular') }}
                {{ Form::text( 'datos_persona[desaparecido][numero_celular]',isset($buscado) ? $buscado->numero_celular:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_compania_celular]', 'Compañía') }}
                {{ Form::select( 'datos_persona[desaparecido][id_compania_celular]',$comboCompania,isset($buscado) ? $buscado->id_compania_celular:null, array('placeholder'
                => '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-6">
                <button class="btn btn-success btn-sm" id="agregar_celular" style="margin-top: 23px">Agregar</button>
            </div>

            <div class="col-md-12">
                <div class="panel panel-info" style="margin-top: 20px">
                    <div class="panel-heading">Redes sociales</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                    {{ Form::checkbox('chk_facebook', 'value',null,['id' => 'chk_facebook','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Facebook
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][facebook]',isset($buscado) ? $buscado->facebook:null, array('id' => 'facebook','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_twitter', 'value',null,['id' => 'chk_twitter','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Twitter
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][twitter]',isset($buscado) ? $buscado->twitter:null, array('id' => 'twitter','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_badoo', 'value',null,['id' => 'chk_badoo','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Badoo
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][badoo]',isset($buscado) ? $buscado->badoo:null, array('id' => 'badoo','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_instagram', 'value',null,['id' => 'chk_instagram','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Instagram
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][instagram]',isset($buscado) ? $buscado->twitter:null, array('id' => 'instagram','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_linkedin', 'value',null,['id' => 'chk_linkedin','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Linkedin
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][linkedin]',isset($buscado) ? $buscado->twitter:null, array('id' => 'linkedin','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_snapchat', 'value',null,['id' => 'chk_snapchat','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Snapchat
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][snapchat]',isset($buscado) ? $buscado->snapchat:null, array('id' => 'snapchat','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 checkbox">
                                {{ Form::checkbox('chk_otro', 'value',null,['id' => 'chk_otro','style' => 'opacity: inherit; position: relative; margin-top: 0px; !important']); }} Otro
                            </div>
                            <div class="col-md-3">
                                {{ Form::text( 'datos_persona[desaparecido][otro]',isset($buscado) ? $buscado->otro:null, array('id' => 'otro','placeholder' => '', 'class' =>
                                'form-control','style' => 'display: inline-block; float:left')) }}
                            </div>
                        </div>

                    </div>

            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_calle]', 'Direccion')}}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_calle]', isset($denunciante) ? $denunciante->ultimo_calle:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_numero]', 'N°') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_numero]', isset($buscado) ? $buscado->ultimo_numero:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_piso]', 'Piso') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_piso]', isset($buscado) ? $buscado->ultimo_piso:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_departamento]', 'Departamento') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_departamento]',isset($buscado) ? $buscado->ultimo_departamento:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][interseccion]', 'Interseccion') }}
                {{ Form::text( 'datos_persona[desaparecido][interseccion]', isset($buscado) ? $buscado->interseccion:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_codigo_postal]', 'Codigo Postal') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_codigo_postal]',isset($buscado) ? $buscado->ultimo_codigo_postal:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">

                {{ Form::label('datos_persona[desaparecido][ultima_provincia]', 'Provincia') }}
                {{ Form::select( 'datos_persona[desaparecido][ultima_provincia]',$comboProvincia,isset($buscado) ? $buscado->ultima_provincia:null, array('placeholder' => '',
                'class' => 'form-control provincia','data-target'=>'.partido_desaparecido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_ultimo_partido]', 'Partido') }}
                {{ Form::select( 'datos_persona[desaparecido][id_ultimo_partido]',[],null, array('placeholder' =>
                '', 'class' => 'form-control partido partido_desaparecido','data-val'=>isset($buscado) ? $buscado->id_ultimo_partido:null)) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultima_localidad]', 'Localidad') }}
                {{ Form::text( 'datos_persona[desaparecido][ultima_localidad]',isset($buscado) ? $buscado->ultima_localidad:null, array('placeholder' =>
                '', 'class' => 'form-control localidad')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[desaparecido][observaciones_domicilio]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[desaparecido][observaciones_domicilio]',isset($buscado) ? $buscado->observaciones_domicilio:null, array('placeholder' =>
                '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Fotografías de la Persona Buscada</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'foto[buscado][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Fotos'))
                </div>
                <div id="fotos_append">

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
            </div>
        </div>
    </div>
</div>

@include("galeria",["fotos"=>isset($buscado)? $buscado->getFotos:null,"titulo"=>"Fotografías Cargadas"])

    <div class="panel panel-info">
        <div class="panel-heading">Autorizacion para difundir las fotos</div>
        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div id="foto">
                        @include('formularios.formulario_archivo',array('nombre'=>'foto[buscado][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Fotos'))
                    </div>
                    <div id="fotos_append">

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">Observaciones</div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-12">
                    {{ Form::label('datos_persona[desaparecido][observaciones_persona]', 'Observaciones') }}
                    {{ Form::textarea( 'datos_persona[desaparecido][observaciones_persona]',isset($buscado) ? $buscado->observaciones_persona:null, array('placeholder'
                    => '', 'class' => 'form-control')) }}
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-info">
    <div class="panel-heading">Descripción Fisica</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ctxFisica]', 'Contextura Físca') }}
                {{ Form::text( 'datos_persona[desaparecido][ctxFisica]', isset($buscado) ? $buscado->ocupacion:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][altura]', 'Altura') }}
                {{ Form::text( 'datos_persona[desaparecido][altura]', isset($buscado) ? $buscado->empleador:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][peso]', 'Peso') }}
                {{ Form::text( 'datos_persona[desaparecido][peso]', isset($buscado) ? $buscado->cuit:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][idTipoCabello]', 'Cabello Tipo') }}
                {{ Form::text( 'datos_persona[desaparecido][idTipoCabello]',isset($buscado) ? $buscado->telefono_laboral:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][lCabello]', 'Largo del Cabello') }}
                {{ Form::text( 'datos_persona[desaparecido][lCabello]', isset($buscado) ? $buscado->calle_laboral:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][cCabello]', 'Color de Cabello') }}
                {{ Form::text( 'datos_persona[desaparecido][cCabello]', isset($buscado) ? $buscado->numero_laboral:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][idColorOjos]', 'Color de Ojos') }}
                {{ Form::text( 'datos_persona[desaparecido][idColorOjos]', isset($buscado) ? $buscado->piso_laboral:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][tez]', 'Tez') }}
                {{ Form::text( 'datos_persona[desaparecido][tez]',isset($buscado) ? $buscado->departamento_laboral:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][sParticulares]', 'Señas Particulares') }}
                {{ Form::text( 'datos_persona[desaparecido][sParticulares]',isset($buscado) ? $buscado->codigo_postal_laboral:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][descripcion_fisica]', 'Detalles') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][descripcion_fisica]',isset($solicitud_paradero) ? $solicitud_paradero->descripcion_fisica:null, array('placeholder' => '',
                    'class' => 'form-control','style'=>'height:8em')) }}
            </div>
        </div>
    </div>
</div>



<div class="panel panel-info">
    <div class="panel-heading">Datos Laborales</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ocupacion]', 'Profesión/Ocupación') }}
                {{ Form::text( 'datos_persona[desaparecido][ocupacion]', isset($buscado) ? $buscado->ocupacion:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][empleador]', 'Empleador') }}
                {{ Form::text( 'datos_persona[desaparecido][empleador]', isset($buscado) ? $buscado->empleador:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][cuit]', 'Cuit') }}
                {{ Form::text( 'datos_persona[desaparecido][cuit]', isset($buscado) ? $buscado->cuit:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][telefono_laboral]', 'Teléfono') }}
                {{ Form::text( 'datos_persona[desaparecido][telefono_laboral]',isset($buscado) ? $buscado->telefono_laboral:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][calle_laboral]', 'Calle') }}
                {{ Form::text( 'datos_persona[desaparecido][calle_laboral]', isset($buscado) ? $buscado->calle_laboral:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][numero_laboral]', 'N°') }}
                {{ Form::text( 'datos_persona[desaparecido][numero_laboral]', isset($buscado) ? $buscado->numero_laboral:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][piso_laboral]', 'Piso') }}
                {{ Form::text( 'datos_persona[desaparecido][piso_laboral]', isset($buscado) ? $buscado->piso_laboral:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][departamento_laboral]', 'Departamento') }}
                {{ Form::text( 'datos_persona[desaparecido][departamento_laboral]',isset($buscado) ? $buscado->departamento_laboral:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][codigo_postal_laboral]', 'Codigo Postal') }}
                {{ Form::text( 'datos_persona[desaparecido][codigo_postal_laboral]',isset($buscado) ? $buscado->codigo_postal_laboral:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_provincia_laboral]', 'Provincia') }}
                {{ Form::select( 'datos_persona[desaparecido][id_provincia_laboral]',$comboProvincia,isset($buscado) ? $buscado->id_provincia_laboral:null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido_laboral')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_partido_laboral]', 'Partido') }}
                {{ Form::select( 'datos_persona[desaparecido][id_partido_laboral]',[],null, array('placeholder'=> '', 'class' => 'form-control partido partido_laboral','data-val'=>isset($buscado) ? $buscado->id_partido_laboral:null)) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_localidad_laboral]', 'Localidad') }}
                {{ Form::text( 'datos_persona[desaparecido][id_localidad_laboral]',isset($buscado) ? $buscado->id_localidad_laboral:null, array('placeholder'=> '', 'class' => 'form-control localidad_laboral')) }}
            </div>
        </div>
    </div>
</div>

    <div class="panel panel-info">
        <div class="panel-heading">Datos Relacionados Con la Ausencia</div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][fecha_visto]', 'Fecha en la que fue vista por última vez')}}
                    {{ Form::text( 'datos_persona[solicitud_paradero][fecha_visto]',isset($solicitud_paradero) ? $solicitud_paradero->fecha_visto:null, array('placeholder' => '', 'class' =>
                    'form-control fecha')) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][tarjeta_credito]', 'Hora en la que fue vista por última
                    vez') }}
                    {{ Form::text( 'datos_persona[solicitud_paradero][hora_visto]',isset($solicitud_paradero) ? $solicitud_paradero->hora_visto:null, array('placeholder' => '','class' => 'form-control time')) }}
                </div>
                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][lugar_visto]', 'Lugar en la que fue vista por última vez')
                    }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][lugar_visto]', isset($solicitud_paradero) ? $solicitud_paradero->lugar_visto:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>

                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][vestimenta]', 'Descripción de la vestimenta y elementos que llevaba consigo') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][vestimenta]',isset($solicitud_paradero) ? $solicitud_paradero->vestimenta:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][drogadiccion]', '¿Es consumidor de estupefacientes/alcohol?') }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][drogadiccion]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->drogadiccion:null, array('placeholder' => '',
                    'class' => 'form-control')) }}
                </div>

               <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][detalle_drogas]', 'Detalle Drogas: ') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][detalle_drogas]',isset($solicitud_paradero) ? $solicitud_paradero->vestimenta:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][internado_institucion]', '¿Se encontraba en alguna
                    institución?') }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][internado_institucion]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->internado_institucion:null, array('placeholder'
                    => '', 'class' => 'form-control')) }}
                </div>
                
                 <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][internado_institucion]', '¿Se encontraba en alguna
                    institución?') }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][internado_institucion]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->internado_institucion:null, array('placeholder'
                    => '', 'class' => 'form-control')) }}
                </div>


                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][fecha_internacion]', 'Fecha que estuvo en la instiucion')}}
                    {{ Form::text( 'datos_persona[solicitud_paradero][fecha_internacion]',isset($solicitud_paradero) ? $solicitud_paradero->fecha_visto:null, array('placeholder' => '', 'class' =>
                    'form-control fecha')) }}
                </div>

                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][detalle_institucion]', 'Detalle Institucion: ') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][detalle_institucion]',isset($solicitud_paradero) ? $solicitud_paradero->vestimenta:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>




                





                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][enfermedad]', '¿Posee alguna enfermedad?') }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][enfermedad]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->enfermedad:null, array('placeholder' => '',
                    'class' => 'form-control')) }}
                </div>






                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][detalle_enfermedad]', 'Detalle Enfermedad: ') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][detalle_enfermedad]',isset($solicitud_paradero) ? $solicitud_paradero->vestimenta:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>









                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][primera_desaparece]', '¿Es la primera vez que se extravía?')
                    }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][primera_desaparece]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->primera_desaparece:null, array('placeholder' =>
                    '', 'class' => 'form-control')) }}
                </div>

      
                 <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][situacion_calle]', '¿Se encontraba en situacion de calle?')
                    }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][situacion_calle]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->primera_desaparece:null, array('placeholder' =>
                    '', 'class' => 'form-control')) }}
                </div>


                <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][sustraccion_parental]', '¿Se trata de una sustraccion parental?')
                    }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][sustraccion_parental]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->primera_desaparece:null, array('placeholder' =>
                    '', 'class' => 'form-control')) }}
                </div>


                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][pertenencia_referencia]', 'Núcleo de referencia o
                    pertenencia') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][pertenencia_referencia]', isset($solicitud_paradero) ? $solicitud_paradero->pertenencia_referencia:null, array('placeholder' =>'', 'class' => 'form-control','style'=>'height:8em')) }}
                </div>

                 <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][conflicto]', '¿Se encontraba en situacion de Conflicto en el ultimo tiempo?')
                    }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][conflicto]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->primera_desaparece:null, array('placeholder' =>
                    '', 'class' => 'form-control')) }}
                </div>

                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][concurrencia]', 'Lugares de concurrencia frecuente') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][concurrencia]',isset($solicitud_paradero) ? $solicitud_paradero->concurrencia:null, array('placeholder' => '', 'class'
                    => 'form-control','style'=>'height:8em')) }}
                </div>

                 <div class="col-md-4">
                    {{ Form::label('datos_persona[solicitud_paradero][documentacion]', '¿Llevaba documentacion consigo?')
                    }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][documentacion]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->primera_desaparece:null, array('placeholder' =>
                    '', 'class' => 'form-control')) }}
                </div>



                <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][observaciones]', 'Observaciones') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][observaciones]', isset($solicitud_paradero) ? $solicitud_paradero->observaciones:null, array('placeholder' => '', 'class'
                    => 'form-control','spellcheck'=>'<true></true>','style'=>'height:8em')) }}
                </div>

            </div>
        </div>
    </div>

<div class="panel panel-info">
    <div class="panel-heading">Datos del Denunciante</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][apellido]', 'Apellido')}}
                {{ Form::text( 'datos_persona[denunciante][apellido]',isset($denunciante) ? $denunciante->apellido:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][apellido_materno]', 'Apellido Materno')}}
                {{ Form::text( 'datos_persona[denunciante][apellido_materno]',isset($denunciante) ? $denunciante->apellido_materno:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][nombre]', 'Nombre')}}
                {{ Form::text( 'datos_persona[denunciante][nombre]', isset($denunciante) ? $denunciante->nombre:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][id_tipo_documento]', 'Tipo Documento')}}
                {{ Form::select( 'datos_persona[denunciante][id_tipo_documento]',$comboTipoDocumento,isset($denunciante) ? $denunciante->tipo_documento:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][nro_documento]', 'N° de Documento')}}
                {{ Form::text( 'datos_persona[denunciante][nro_documento]', isset($denunciante) ? $denunciante->nro_documento:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][id_sexo]', 'Sexo')}}
                {{ Form::select( 'datos_persona[denunciante][id_sexo]',$comboSexo, isset($denunciante) ? $denunciante->sexo:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][edad]', 'Fecha de Nacimiento')}}
                {{ Form::text( 'datos_persona[denunciante][edad]', isset($denunciante) ? $denunciante->edad:null, array('placeholder' => '', 'class'=>'form-control fecha')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][id_nacionalidad]', 'Nacionalidad')}}
                {{ Form::select('datos_persona[denunciante][id_nacionalidad]',$comboNacionalidad,isset($denunciante) ? $denunciante->nacionalidad:null,array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultimo_calle]', 'Dirección')}}
                {{ Form::text( 'datos_persona[denunciante][ultimo_calle]', isset($denunciante) ? $denunciante->ultimo_calle:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultimo_numero]', 'N°')}}
                {{ Form::text( 'datos_persona[denunciante][ultimo_numero]', isset($denunciante) ? $denunciante->ultimo_numero:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultimo_piso]', 'Piso')}}
                {{ Form::text( 'datos_persona[denunciante][ultimo_piso]', isset($denunciante) ? $denunciante->ultimo_piso:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>


            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultimo_departamento]', 'Departamento')}}
                     {{ Form::text( 'datos_persona[denunciante][ultimo_departamento]', isset($denunciante) ? $denunciante->ultimo_departamento:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultimo_codigo_postal]', 'Codigo Postal')}}
                {{ Form::text( 'datos_persona[denunciante][ultimo_codigo_postal]',isset($denunciante) ? $denunciante->ultimo_codigo_postal:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][ultima_provincia]', 'Provincia')}}
                {{ Form::select( 'datos_persona[denunciante][ultima_provincia]',$comboProvincia,isset($denunciante) ? $denunciante->ultima_provincia:null, array('placeholder' => '', 'class'=>'form-control provincia','data-target'=>'.partido_denunciante')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][id_ultimo_partido]', 'Partido')}}
                {{ Form::select( 'datos_persona[denunciante][id_ultimo_partido]',[],null, array('placeholder' => '', 'class'=>'form-control partido partido_denunciante','data-val'=>isset($denunciante) ? $denunciante->id_ultimo_partido:null)) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][telefono]', 'Telefono')}}
                {{ Form::text( 'datos_persona[denunciante][telefono]',isset($denunciante) ? $denunciante->ultimo_codigo_postal:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
             <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][dependencia]', 'Dependencia donde hizo la Denuncia: ')}}
                {{ Form::text( 'datos_persona[denunciante][dependencia]',isset($denunciante) ? $denunciante->ultimo_codigo_postal:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-12">
                    {{ Form::label('datos_persona[solicitud_paradero][relato_denuncia]', 'Relato de los Hechos') }}
                    {{ Form::textarea( 'datos_persona[solicitud_paradero][relato_denuncia]', isset($solicitud_paradero) ? $solicitud_paradero->observaciones:null, array('placeholder' => '', 'class'
                    => 'form-control','spellcheck'=>'<true></true>','style'=>'height:8em')) }}
            </div>
            
            <div class="col-md-3">
                    {{ Form::label('datos_persona[solicitud_paradero][victima_delito]', '¿Fue victima de Delito?') }}
                    {{ Form::select( 'datos_persona[solicitud_paradero][victima_delito]',$comboSiNo,isset($solicitud_paradero) ? $solicitud_paradero->enfermedad:null, array('placeholder' => '',
                    'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[denunciante][victima_delito_cual]', 'Cual?')}}
                {{ Form::text( 'datos_persona[denunciante][victima_delito_cual]',isset($denunciante) ? $denunciante->ultimo_codigo_postal:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>






        </div>
    </div>
</div>

@include('formularios.autoridad_judicial')

<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class="btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar</button>
{{ Form::close() }}
@stop