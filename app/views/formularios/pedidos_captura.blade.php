@extends('header')


@section('scripts')
    {{ HTML::script('assets/js/empty.js') }}
    {{ HTML::script('assets/js/pedido_secuestro_vehicular.js') }}

    <script>
        $().ready(function(){


            @if(isset($oficios))
            @if(is_object($oficios))
            $("#id").val({{ $oficios->id }});
            @endif
        @endif
            $("#guardar").on("click", function(e) {
//        console.log("hola");
                        $("#guardar").attr("disabled","disabled");
                        $("#guardar").html("Guardando...");
                        e.preventDefault();
                        var formData = new FormData(document.getElementById("frmProfugoEvadido"));
                        formData.append("id_oficio", $("#id").val());
                        var destino = "{{ Route('profugo_evadido.validarProfugoEvadido') }}";

                        $.ajax({
                            type: "Post",
                            url: destino,
                            data: formData,
                            async: true,
                            dataType: "html",
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (respuesta) {
//                    console.log(respuesta);
                                if (isNaN(respuesta)) {
                                    if (respuesta == "Ok") {
                                        $("#frmProfugoEvadido").submit();
                                    }
                                    else {
                                        $("#guardar").attr("disabled",false);
                                        $("#guardar").html("Guardar »");

                                        $("#contenido-modal").html(respuesta);
                                        $("#confirmacion").modal(function () {
                                            show:true
                                        });
                                    }

                                }
                            },
                            error: function () {
                                $("#contenido-modal").html("Hubo un problema, consulte con el administrador");
                                $("#confirmacion").modal(function () {
                                    show:true
                                });
                            }


                        });



                    });

        });


    </script>

@stop

@section('content')
<h3>{{ $titulo_oficio }}    @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')

{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
{{ Form::hidden('id',null,['id' => 'id']) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Persona</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">

                {{ Form::label('nombre', 'Nombres') }}
                {{ Form::text( 'nombre',isset($oficios) ? $oficios->personas->nombre:null  , array('placeholder' => '', 'class' => 'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apellido', 'Apellido') }}
                {{ Form::text( 'apellido',isset($oficios) ? $oficios->personas->apellido:null  , array('placeholder'=> '', 'class' => 'form-control apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apellido_materno', 'Apellido Materno') }}
                {{ Form::text( 'apellido_materno',isset($oficios) ? $oficios->personas->apellido_materno:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('apodos', 'Sobrenombre') }}
                {{ Form::text( 'apodos',isset($oficios) ? $oficios->personas->apodos:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('id_tipo_documento', 'Tipo de Documento') }}
                {{ Form::select( 'id_tipo_documento',$comboTipoDocumento ,isset($oficios) ? $oficios->personas->id_tipo_documento:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('nro_documento', 'N° de Documento') }}
                {{ Form::text( 'nro_documento',isset($oficios) ? $oficios->personas->nro_documento:null  , array('placeholder'=> '', 'class' => 'form-control buscar_dni',"data-tipo_oficio"=>"4" )) }}
            </div>

            <div class="documentos">
                @if (isset($documentos))
                    @foreach ($documentos as $datos)
                        @include('formularios.documentos',$datos)
                    @endforeach
                @endif
            </div>
	    <!--
            <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div>-->

            <div class="col-md-3">
                {{ Form::label('id_sexo', 'Sexo') }}
                {{ Form::select( 'id_sexo',$comboSexo,isset($oficios) ? $oficios->personas->id_sexo:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_genero', 'Genero') }}
                {{ Form::select( 'id_genero',$comboGenero,isset($oficios) ? $oficios->personas->id_genero:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('edad', 'Fecha Nacimiento') }}
                {{ Form::text( 'edad',isset($oficios) ? $oficios->personas->edad:null  , array('placeholder'=> '', 'class' => 'fecha form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
                {{ Form::select( 'id_nacionalidad',$comboNacionalidad,isset($oficios) ? $oficios->personas->id_nacionalidad:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{Form::label('descripcion_persona','Descripcion Física') }}
                {{ Form::text( 'descripcion_persona',isset($oficios) ? $oficios->personas->descripcion_persona:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_persona','Observaciones') }}
                {{ Form::textarea('observaciones_persona',isset($oficios) ? $oficios->personas->observaciones_persona:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('ultimo_calle', 'Dirección') }}
                {{ Form::text( 'ultimo_calle',isset($oficios) ? $oficios->personas->ultimo_calle:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_numero', 'N°') }}
                {{ Form::text( 'ultimo_numero',isset($oficios) ? $oficios->personas->ultimo_numero:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Piso') }}
                {{ Form::text( 'ultimo_piso',isset($oficios) ? $oficios->personas->ultimo_piso:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_departamento', 'Departamento') }}
                {{ Form::text( 'ultimo_departamento',isset($oficios) ? $oficios->personas->ultimo_departamento:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][interseccion]', 'Interseccion') }}
                {{ Form::text( 'interseccion',isset($oficios) ? $oficios->personas->interseccion:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_codigo_postal', 'Codigo Postal') }}
                {{ Form::text( 'ultimo_codigo_postal',isset($oficios) ? $oficios->personas->ultimo_codigo_postal:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_provincia', 'Provincia') }}
                {{ Form::select( 'ultima_provincia',$comboProvincia,isset($oficios) ? $oficios->personas->ultima_provincia:null  , array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_ultimo_partido', 'Partido') }}
                {{ Form::select( 'id_ultimo_partido',[],isset($oficios) ? $oficios->personas->id_ultimo_partido:null  , array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($oficios) ? $oficios->personas->id_ultimo_partido:null)) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_localidad', 'Localidad') }}
                {{ Form::text( 'ultima_localidad',isset($oficios) ? $oficios->personas->ultima_localidad:null  , array('placeholder'=> '', 'class' => 'form-control localidad')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('observaciones', 'Datos Adicionales') }}
                {{ Form::text( 'observaciones',isset($oficios) ? $oficios->personas->observaciones:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

        </div>
    </div>
</div>
<div class="panel panel-info">
    @if(isset($profugo_evadido))
        <div class="panel-heading">Fotografías de la Persona/Huellas dactilares </div>
    @else
        <div class="panel-heading">Fotografías de la Persona </div>
    @endif
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'foto[captura][]','i'=>0,'eliminar'=>false))
                </div>
                <div id="fotos_append">

                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('galeria',["fotos"=>isset($oficios) ? $oficios->personas->getFotos: null,"titulo"=>"Fotos Victima"])

@if(!isset($profugo_evadido))
    @include('formularios.autoridad_judicial')

    <div id="newpost" style="display:none">
        @include('formularios.autoridad_judicial2')
    </div>

@else
    @include('formularios.input_alerta_temprana')
    @include('formularios.adjuntar_oficio')
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Atención... </h4>
                </div>
                <div class="modal-body">
                    <center> <b id="mensaje"> </b></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar »</button>
                </div>
            </div>
        </div>
    </div>
@endif

{{ Form::hidden('tipo', '4', array('id' => 'tipoOficio')) }}

<button class="btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

@if(!isset($profugo_evadido))
    <button class="btn btn-primary" id="guardar_form" data-tipo_oficio="4">Guardar »</button>
@else
    <button class="btn btn-primary" data-tipo_oficio="2" type="button" id="guardar">Guardar »</button>
@endif
@include("components.modal")
{{Form::close() }}
@stop
