@extends('header')
@section('content')
<h3>Prohibición de Salida del País / Provincia @if(Session::has("alerta_temprana") or  ( isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1 ) )(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')

{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Persona</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">

                {{ Form::label('nombre', 'Nombres') }}
                {{ Form::text( 'nombre',isset($oficios) ? $oficios->personas->nombre : null, array('placeholder' => '', 'class' => 'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apellido', 'Apellido') }}
                {{ Form::text( 'apellido',isset($oficios) ? $oficios->personas->apellido : null, array('placeholder'=> '', 'class' => 'form-control apellido')) }}
            </div>
             <div class="col-md-3">
                {{ Form::label('apellido_materno', 'Apellido Materno') }}
                {{ Form::text( 'apellido_materno',isset($oficios) ? $oficios->personas->apellido_materno:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apodos', 'Sobrenombre') }}
                {{ Form::text( 'apodos',isset($oficios) ? $oficios->personas->apodos : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('id_tipo_documento', 'Tipo de Documento') }}
                {{ Form::select( 'id_tipo_documento',$comboTipoDocumento ,isset($oficios) ? $oficios->personas->id_tipo_documento : null, array('placeholder'=> '', 'class' => 'form-control  buscar_dni')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('nro_documento', 'N° de Documento') }}
                {{ Form::text( 'nro_documento',isset($oficios) ? $oficios->personas->nro_documento : null, array('placeholder'=> '', 'class' => 'form-control buscar_dni',"data-tipo_oficio"=>"3")) }}
            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>
	    <!--
            <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div>-->


            <div class="col-md-3">
                {{ Form::label('sexo', 'Sexo') }}
                {{ Form::select( 'id_sexo',$comboSexo,isset($oficios) ? $oficios->personas->id_sexo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('genero', 'Genero') }}
                {{ Form::select( 'id_genero',$comboGenero,isset($oficios) ? $oficios->personas->id_genero: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('edad', 'Fecha Nacimiento') }}
                {{ Form::text( 'edad',isset($oficios) ? $oficios->personas->edad : null, array('placeholder'=> '', 'class' => 'form-control fecha')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
                {{ Form::select( 'id_nacionalidad',$comboNacionalidad,isset($oficios) ? $oficios->personas->id_nacionalidad : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_persona', 'Observaciones:') }}
                {{ Form::textarea( 'observaciones_persona',isset($oficios) ? $oficios->personas->observaciones_persona:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('ultimo_calle', 'Dirección') }}
                {{ Form::text( 'ultimo_calle',isset($oficios) ? $oficios->personas->ultimo_calle:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_numero', 'N°') }}
                {{ Form::text( 'ultimo_numero',isset($oficios) ? $oficios->personas->ultimo_numero:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Piso') }}
                {{ Form::text( 'ultimo_piso',isset($oficios) ? $oficios->personas->ultimo_piso:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Interseccion') }}
                {{ Form::text( 'interseccion',isset($oficios) ? $oficios->personas->interseccion:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_departamento', 'Departamento') }}
                {{ Form::text( 'ultimo_departamento',isset($oficios) ? $oficios->personas->ultimo_departamento:null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_codigo_postal', 'Codigo Postal') }}
                {{ Form::text( 'ultimo_codigo_postal',isset($oficios) ? $oficios->personas->ultimo_codigo_postal:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_provincia', 'Provincia') }}
                {{ Form::select( 'ultima_provincia',$comboProvincia,isset($oficios) ? $oficios->personas->ultima_provincia:null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_ultimo_partido', 'Partido') }}
                {{ Form::select( 'id_ultimo_partido',[],isset($oficios) ? $oficios->personas->id_ultimo_partido:null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($oficios) ? $oficios->personas->id_ultimo_partido:null)) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_localidad', 'Localidad') }}
                {{ Form::text( 'ultima_localidad',isset($oficios) ? $oficios->personas->ultima_localidad:null, array('placeholder'=> '', 'class' =>'form-control localidad')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('Datos Adicionales', 'Datos Adicionales') }}
                {{ Form::text( 'observaciones',isset($oficios) ? $oficios->personas->observaciones:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Fotografías de la Persona </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
            <div id="foto">
                @include('formularios.formulario_archivo',array('nombre'=>'foto[persona][]','i'=>0,'eliminar'=>false))
            </div>
            <div id="fotos_append">

            </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include("galeria",["fotos"=>isset($buscado)? $oficios->personas->getFotos:null,"titulo"=>"Fotografias Cargadas"])
@include('formularios.autoridad_judicial')

<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class="btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar »</button>
{{Form::close() }}
@stop
