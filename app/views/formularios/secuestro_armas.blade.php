@extends('header')
@section('content')
<h3>Pedido de Secuestro de Armas
    @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1)))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')

{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos del Arma</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('tipo_arma', 'Tipo de Arma') }}
                {{ Form::text( 'tipo_arma',isset($secuestro_armas) ? $secuestro_armas->tipo_arma: null, array('placeholder' => '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('marca', 'Marca') }}
                {{ Form::text( 'marca',isset($secuestro_armas) ? $secuestro_armas->marca: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('modelo', 'Modelo') }}
                {{ Form::text( 'modelo',isset($secuestro_armas) ? $secuestro_armas->modelo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('calibre', 'Calibre') }}
                {{ Form::text('calibre',isset($secuestro_armas) ? $secuestro_armas->modelo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('matricula', 'Matrícula Nro.') }}
                {{ Form::text('matricula',isset($secuestro_armas) ? $secuestro_armas->modelo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_armas', 'Observaciones') }}
                {{ Form::textarea('observaciones_armas',isset($secuestro_armas) ? $secuestro_armas->observaciones_armas: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Titular Registral </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('nombre', 'Nombres') }}
                {{ Form::text( 'nombre', isset($titular) ? $titular->nombre: null, array('placeholder' => '', 'class' => 'form-control nombre','data-novalidate'=>"true")) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apellido', 'Apellido') }}
                {{ Form::text( 'apellido',isset($titular) ? $titular->apellido: null, array('placeholder'=> '', 'class' => 'form-control apellido','data-novalidate'=>"true")) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('apellido_materno', 'Apellido Materno') }}
                {{ Form::text( 'apellido_materno',isset($titular) ? $titular->apellido_materno: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('tipo_documento', 'Tipo de Documento') }}
                {{ Form::select( 'id_tipo_documento',$comboTipoDocumento ,isset($titular) ? $titular->id_tipo_documento: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('nro_documento', 'N° de Documento') }}
                {{ Form::text( 'nro_documento', isset($titular) ? $titular->nro_documento: null, array('placeholder'=> '', 'class' => 'form-control buscar_dni','data-tipo_oficio'=>'10')) }}
            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>
	    <!--
            <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div>-->

            <div class="col-md-3">
                {{ Form::label('id_sexo', 'Sexo') }}
                {{ Form::select( 'id_sexo',$comboSexo,isset($titular) ? $titular->id_sexo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('id_genero', 'Genero') }}
                {{ Form::select( 'id_genero',$comboGenero,isset($titular) ? $titular->id_genero: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('edad', 'Fecha Nacimiento') }}
                {{ Form::text( 'edad', isset($titular) ? $titular->edad: null, array('placeholder'=> '', 'class' => 'form-control fecha')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
                {{ Form::select('id_nacionalidad',$comboNacionalidad,isset($titular) ? $titular->id_nacionalidad: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_calle', 'Direccion') }}
                {{ Form::text( 'ultimo_calle',isset($titular) ? $titular->ultimo_calle: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_numero', 'N°') }}
                {{ Form::text( 'ultimo_numero',isset($titular) ? $titular->ultimo_numero: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('interseccion', 'Interseccion') }}
                {{ Form::text( 'interseccion',isset($titular) ? $titular->interseccion: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Piso') }}
                {{ Form::text( 'ultimo_piso',isset($titular) ? $titular->ultimo_piso:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_departamento', 'Departamento') }}
                {{ Form::text( 'ultimo_departamento',isset($titular) ? $titular->ultimo_departamento:null  , array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('provincia', 'Provincia') }}
                {{ Form::select( 'ultima_provincia',$comboProvincia,isset($titular) ? $titular->ultima_provincia: null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_partido', 'Partido') }}
                {{ Form::select( 'id_ultimo_partido',$comboDepartamento,null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($titular) ? $titular->id_ultimo_partido:null)) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_localidad', 'Localidad') }}
                {{ Form::text('ultima_localidad',isset($titular) ? $titular->ultima_localidad:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_persona', 'Observaciones:') }}
                {{ Form::textarea('observaciones_persona',isset($titular) ? $titular->observaciones_persona:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
@include('formularios.autoridad_judicial')

<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class="btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar »</button>
{{Form::close() }}
@stop
