<div class="panel panel-info">
    <div class="panel-heading">Autoridad Judicial Interviniente</div>
    <div class="panel-body">
        <div class="form-group">
            @include('formularios.input_alerta_temprana')

            <div class="col-md-4">
                {{ Form::label('fecha_oficio', 'Fecha Requerimiento:') }}
                {{ Form::text( 'fecha_oficio', null, array('placeholder' => '', 'class' =>'form-control fecha')) }}
            </div>
            {{--@if (Session::has("plazo_fecha"))--}}
            {{--{{ Session::forget("plazo_fecha") }}--}}
            <div class="col-md-4">
                {{ Form::label('plazo_fecha', 'Plazo:') }}
                {{ Form::text( 'plazo_fecha', null, array('placeholder' => '', 'class' =>'form-control fecha')) }}
            </div>
            {{--@endif--}}
            @if (Session::has("plazo_hr"))
            <div class="col-md-4">
                {{ Form::label('plazo_hr', 'Plazo:') }}
                {{ Form::text( 'plazo_hr', null, array('placeholder' => '', 'class' =>'form-control')) }}
                {{ Session::forget("plazo_hr") }}
            </div>
            @endif
            <div class="col-md-4">
                {{ Form::label('nro_oficio', 'N° de Oficio:') }}
                {{ Form::text( 'nro_oficio', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('caratula', 'Carátula de la Causa:') }}
                {{ Form::text( 'caratula', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('juzgado', 'Juzgado Interviniente:') }}
                {{ Form::select('juzgado',$comboJuzgados ,null, array('placeholder'=> '', 'class' => 'form-control select juzgado','id'=>'juzgado','data-destino'=>'#juzgado_otro')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('juzgado', 'Otro Juzgado Interviniente:') }}
                {{ Form::text('juzgado_otro', null, array('placeholder'=> '', 'class' => 'form-control','id'=>'juzgado_otro','disabled'=>'disabled')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('secretaria', 'Secretaría:') }}
                {{ Form::text( 'secretaria', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('nro_causa', 'N° de Causa (Juzgado):') }}
                {{ Form::text( 'nro_causa', null, array('placeholder' => '', 'class' =>'form-control buscar_dni,"data-tipo_oficio"=>"4"')) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('fiscalia', 'Fiscalía Interviniente:') }}
                {{ Form::select('fiscalia',$combosFiscalias ,null, array('placeholder'=> '', 'class' => 'form-control select fiscalia','data-destino'=>"#otra_fiscalia")) }}
            </div>
            <div class="col-md-4">
                {{ Form::label('fiscalia', 'Otra Fiscalía Interviniente:') }}
                {{ Form::text('otra_fiscalia',null, array('placeholder'=> '', 'class' => 'form-control','disabled'=>'disabled','id'=>'otra_fiscalia')) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('nro_causa_expediente', 'N° de Causa (Fiscalia):') }}
                {{ Form::text('nro_causa_expediente', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>


            <div class="col-md-12">
                {{ Form::label('observaciones_judicial', 'Observaciones:') }}
                {{ Form::textarea( 'observaciones_judicial', null, array('placeholder' => '', 'class' =>'form-control')) }}
            </div>

        </div>
    </div>
</div>
@include('formularios.adjuntar_oficio')

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"> Atención... </h4>
            </div>
            <div class="modal-body">
                <center> <b id="mensaje"> </b></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar »</button>
            </div>
        </div>
    </div>
</div>
@include('listados.listado_documentos')



