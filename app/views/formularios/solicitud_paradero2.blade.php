@extends('header')
@section('scripts')
@stop
@section('content')
<h3>{{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')



{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Datos de la Persona</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][nombre]', 'Nombres') }}
                {{ Form::text( 'datos_persona[desaparecido][nombre]',isset($buscado) ? $buscado->nombre:null, array('placeholder' => '', 'class' =>
                'form-control nombre','id'=>'nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[desaparecido][apellido]',isset($buscado) ? $buscado->apellido:null, array('placeholder' => '', 'class' =>
                'form-control apellido','id'=>'apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[desaparecido][apellido_materno]',isset($buscado) ? $buscado->apellido_materno:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[desaparecido][apodos]',isset($buscado) ? $buscado->apodos:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[desaparecido][id_sexo]',$comboSexo,isset($buscado) ? $buscado->sexo:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_genero]', 'Genero') }}
                {{ Form::select( 'datos_persona[desaparecido][id_genero]',$comboGenero,isset($buscado) ? $buscado->genero:null, array('placeholder' => '', 'class'
                => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[desaparecido][edad]',isset($buscado) ? $buscado->edad:null, array('placeholder' => '', 'class' =>
                'form-control fecha')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[desaparecido][id_nacionalidad]',$comboNacionalidad,isset($buscado) ? $buscado->id_nacionalidad:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_tipo_documento]', 'Tipo Documento') }}
                {{ Form::select( 'datos_persona[desaparecido][id_tipo_documento]',$comboTipoDocumento,isset($buscado) ? $buscado->id_tipo_documento:null, array('placeholder' =>
                '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[desaparecido][nro_documento]',isset($buscado) ? $buscado->nro_documento:null, array('placeholder' => '', 'class' =>
                'form-control buscar_dni',"data-tipo_oficio"=>"1")) }}
            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>



            <div class="col-md-1">
                {{ Form::label('datos_persona[desaparecido][codArea_celular]','Cod.Area') }}
                {{ Form::text( 'datos_persona[desaparecido][codArea_celular]',isset($buscado) ? $buscado->codArea_celular:null, array('placeholder' => '011', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('datos_persona[desaparecido][numero_celular]', 'N° de Celular') }}
                {{ Form::text( 'datos_persona[desaparecido][numero_celular]',isset($buscado) ? $buscado->numero_celular:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_compania_celular]', 'Compañía') }}
                {{ Form::select( 'datos_persona[desaparecido][id_compania_celular]',$comboCompania,isset($buscado) ? $buscado->id_compania_celular:null, array('placeholder'
                => '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-12">
                            {{ Form::label('datos_persona[desaparecido][observaciones_persona]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[desaparecido][observaciones_persona]',isset($buscado) ? $buscado->observaciones_persona:null, array('placeholder'
                => '', 'class' => 'form-control')) }}
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Ultimo Domicilio Conocido</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_calle]', 'Direccion')}}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_calle]',  isset($buscado) ? $buscado->ultimo_calle:null, array('placeholder' => '', 'class'=>'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_numero]', 'N°') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_numero]', isset($buscado) ? $buscado->ultimo_numero:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_piso]', 'Piso') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_piso]', isset($buscado) ? $buscado->ultimo_piso:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][interseccion]', 'Interseccion') }}
                {{ Form::text( 'datos_persona[desaparecido][interseccion]', isset($buscado) ? $buscado->interseccion:null, array('placeholder' => '', 'class' =>
                'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_departamento]', 'Departamento') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_departamento]',isset($buscado) ? $buscado->ultimo_departamento:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultimo_codigo_postal]', 'Codigo Postal') }}
                {{ Form::text( 'datos_persona[desaparecido][ultimo_codigo_postal]',isset($buscado) ? $buscado->ultimo_codigo_postal:null, array('placeholder' => '',
                'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">

                {{ Form::label('datos_persona[desaparecido][ultima_provincia]', 'Provincia') }}
                {{ Form::select( 'datos_persona[desaparecido][ultima_provincia]',$comboProvincia,isset($buscado) ? $buscado->ultima_provincia:null, array('placeholder' => '',
                'class' => 'form-control provincia','data-target'=>'.partido_desaparecido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][id_ultimo_partido]', 'Partido') }}
                {{ Form::select( 'datos_persona[desaparecido][id_ultimo_partido]',[],null, array('placeholder' =>
                '', 'class' => 'form-control partido partido_desaparecido','data-val'=>isset($buscado) ? $buscado->id_ultimo_partido:null)) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[desaparecido][ultima_localidad]', 'Localidad') }}
                {{ Form::text( 'datos_persona[desaparecido][ultima_localidad]',isset($buscado) ? $buscado->ultima_localidad:null, array('placeholder' =>
                '', 'class' => 'form-control localidad')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[desaparecido][observaciones_domicilio]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[desaparecido][observaciones_domicilio]',isset($buscado) ? $buscado->observaciones_domicilio:null, array('placeholder' =>
                '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">Fotografías de la Persona</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'foto[buscado][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Fotos'))
                </div>
                <div id="fotos_append">

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
            </div>
        </div>
    </div>
</div>

@include("galeria",["fotos"=>isset($buscado)? $buscado->getFotos:null,"titulo"=>"Fotografías Cargadas"])



@include('formularios.autoridad_judicial')

<div id="newpost" style="display:none">
    @include('formularios.autoridad_judicial2')
</div>

<button class="btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>

<button class="btn btn-primary" id="guardar_form">Guardar</button>
{{ Form::close() }}
@stop
