@extends('header')
@section('scripts')
    {{ HTML::script('assets/js/empty.js') }}
    {{ HTML::script('assets/js/pedido_secuestro_vehicular.js') }}

<script>
    $().ready(function(){


        @if(isset($oficios))
            @if(is_object($oficios))
                $("#id").val({{ $oficios->id }});
            @endif
        @endif
            $("#guardar").on("click", function(e) {
//        console.log("hola");
            $("#guardar").attr("disabled","disabled");
            $("#guardar").html("Guardando...");
            e.preventDefault();
            var formData = new FormData(document.getElementById("frmRoboAutomotor"));
            formData.append("id_oficio", $("#id").val());
            var destino = "{{ Route('robo_automotor.validarRoboAutomotor') }}";

            $.ajax({
                type: "Post",
                url: destino,
                data: formData,
                async: true,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function (respuesta) {
//                    console.log(respuesta);
                    if (isNaN(respuesta)) {
                        if (respuesta == "Ok") {
                            $("#frmRoboAutomotor").submit();
                        }
                        else {
                            $("#guardar").attr("disabled",false);
                            $("#guardar").html("Guardar »");

                            $("#contenido-modal").html(respuesta);
                            $("#confirmacion").modal(function () {
                                show:true
                            });
                        }

                    }
                },
                error: function () {
                    $("#contenido-modal").html("Hubo un problema, consulte con el administrador");
                    $("#confirmacion").modal(function () {
                        show:true
                    });
                }


            });



        });

    });


</script>

@stop
@section('content')
<h3>
@if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))
    {{$tipoOficio->titulo_alerta_temprana}} (Alerta temprana)
@else
    {{$tipoOficio->titulo_alerta_temprana}}
@endif
</h3>
@include('mensaje_activo_cesado')



{{ Form::model(isset($oficios) ? $oficios : null ,$form_data, array('role' => 'form')) }}
{{ Form::hidden('id',null,['id' => 'id']) }}

<div class="panel panel-info">
    <div class="panel-heading">Datos del Vehículo</div>
       <div class="panel-body">
          <div class="form-group">
                <div class="col-md-3">
                {{ Form::label('dominio', 'Dominio') }}
                {{ Form::text( 'dominio',isset($secuestro_vehicular) ? $secuestro_vehicular->dominio: null, array('placeholder' => '', 'class' => 'form-control buscar_patente')) }}
                </div>

          <div class="col-md-3">
                {{ Form::label('anio_vehiculo', 'Año') }}
                {{ Form::text( 'anio_vehiculo',isset($secuestro_vehicular) ? $secuestro_vehicular->anio_vehiculo: null, array('placeholder'=> '', 'class' => 'form-control')) }}
          </div>
            <div class="col-md-3">
                {{ Form::label('radicacion', 'Lugar de radicación del Vehículo') }}
                {{ Form::text( 'radicacion',isset($secuestro_vehicular) ? $secuestro_vehicular->radicacion: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('nro_chasis', 'N° de Chasis') }}
                {{ Form::text( 'nro_chasis', isset($secuestro_vehicular) ? $secuestro_vehicular->nro_chasis: null, array('placeholder'=> '', 'class' => 'form-control nro_chasis')) }}

            </div>

              <div class="col-md-3">
                  {{ Form::label('modelo_vehiculo', 'Tipo Vehículo') }}
                  {{ Form::select('id_tipo_vehiculo',$comboTipoVehiculo,isset($secuestro_vehicular) ? $secuestro_vehicular->getTipoVehiculo->id : null, array('placeholder'=> '', 'class' => 'form-control vehiculo ', 'data-destino'=>'#otro_tipo_vehiculo')) }}
              </div>
              <div class="col-md-3">
                  {{ Form::label('otro_vehiculo', 'Otro Tipo Vehiculo') }}
                  {{ Form::text('otro_tipo_vehiculo',isset($secuestro_vehicular->otro_tipo_vehiculo) ? $secuestro_vehicular->otro_tipo_vehiculo: null, array('placeholder'=> '', 'class' => 'form-control','disabled'=>'disabled','id'=>'otro_tipo_vehiculo')) }}
              </div>

              <div class="col-md-3">
                  {{ Form::label('marca_vehiculo', 'Marca') }}
                  {{ Form::select('marca_vehiculo',$comboVehiculosMarcas ,isset($secuestro_vehicular) ? $secuestro_vehicular->getMarca->id : null, array('placeholder'=> '', 'class' => ' form-control marcas ','data-target'=>'.marca', 'data-destino'=>'#otra_marca')) }}
              </div>
              <div class="col-md-3">
                  {{ Form::label('otra_marca', 'Otra marca') }}
                  {{ Form::text( 'otra_marca', isset($secuestro_vehicular) ? $secuestro_vehicular->otra_marca: null, array('placeholder'=> '', 'class' => 'form-control','disabled'=>'disabled','id'=>'otra_marca')) }}
              </div>

              <div class="col-md-3">
                {{ Form::label('modelo_vehiculo', 'Modelo Vehículo') }}
                {{ Form::select('modelo_vehiculo',$comboVehiculosModelos ,isset($secuestro_vehicular) ? $secuestro_vehicular->getModelo->id : null, array('placeholder'=> '', 'class' => 'form-control modelos','data-target'=>'.modelo', 'data-destino'=>'#otro_modelo')) }}
            </div>
              <div class="col-md-3">
                  {{ Form::label('otro_modelo', 'Otro Modelo') }}
                  {{ Form::text( 'otro_modelo', isset($secuestro_vehicular) ? $secuestro_vehicular->otro_modelo: null, array('placeholder'=> '', 'class' => 'form-control','disabled'=>'disabled','id'=>'otro_modelo')) }}
              </div>

              <div class="col-md-3">
                  {{ Form::label('Color', 'Color') }}
                  {{ Form::text( 'color', isset($secuestro_vehicular) ? $secuestro_vehicular->color_auto: null, array('placeholder'=> '', 'class' => 'form-control')) }}
              </div>
              <div class="col-md-3">
                  {{ Form::label('marca_motor', 'Marca Motor') }}
                  {{ Form::text( 'marca_motor', isset($secuestro_vehicular) ? $secuestro_vehicular->marca_motor: null, array('placeholder'=> '', 'class' => 'form-control')) }}
              </div>
             <div class="col-md-3">
                {{ Form::label('nro_motor', 'N° de Motor') }}
                {{ Form::text( 'nro_motor', isset($secuestro_vehicular) ? $secuestro_vehicular->nro_motor: null, array('placeholder'=> '', 'class' => 'form-control nro_motor')) }}
             </div>
          </div>
    </div>
</div>

{{--@if(!Session::has("alerta_temprana"))--}}
<div class="panl panel-info">
    <div class="panel-heading">Cargar Fotografias del Vehiculo</div>
    <div class="panel-body">
        @for ($i = 0; $i < 4; $i++)
            <div class="row">
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('fotos_vehicular[]')}}
                </div>
            </div>
        @endfor
    </div>
</div>
{{--@endif--}}

{{--@if(!Session::has("alerta_temprana"))--}}
    @if(isset($secuestro_vehicular))
        @include('galeria',array("titulo"=>"Fotografias del Vehiculo","fotos"=>$secuestro_vehicular->getFotos))
    @endif
{{--@endif--}}


@if(!isset($robo_automotor))
<div class="panel panel-info">
    <div class="panel-heading">Titular Registral </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-3">

                {{ Form::label('datos_persona[titular][nombre]', 'Nombres') }}
                {{ Form::text( 'datos_persona[titular][nombre]', isset($titular)? $titular->nombre: null, array('placeholder' => '', 'class' => 'form-control nombre','data-novalidate'=>"true")) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[titular][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[titular][apellido]',isset($titular)? $titular->apellido: null, array('placeholder'=> '', 'class' => 'form-control apellido','data-novalidate'=>"true")) }}
            </div>

        <div class="col-md-3">
            {{ Form::label('datos_persona[titular][apellido_materno]', 'Apellido Materno') }}
            {{ Form::text( 'datos_persona[titular][apellido_materno]',isset($titular)? $titular->apellido_materno:null  , array('placeholder'=> '', 'class' => 'form-control')) }}
        </div>
            <div class="col-md-3">
                {{ Form::label('tipo_documento', 'Tipo de Documento') }}
                {{ Form::select('datos_persona[titular][id_tipo_documento]',$comboTipoDocumento ,isset($titular)? $titular->id_tipo_documento: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('nro_documento', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[titular][nro_documento]', isset($titular) ? $titular->nro_documento: null, array('placeholder'=> '', 'class' => 'form-control buscar_dni',"data-tipo_oficio"=>"2")) }}

            </div>

            <div class="documentos">
                @if (isset($documentos))
                @foreach ($documentos as $datos)
                @include('formularios.documentos',$datos)
                @endforeach
                @endif
            </div>
	    <!--
            <div class="col-md-3">
                {{ Form::button('Agregar Documento', array('type' => 'button', 'class' => 'btn btn-success','id'=>'agregar_documento')) }}
                {{ Form::hidden('sarasa',route('pedidos_captura.agregarDocumento'),array('id' => 'sarasa')) }}
            </div>-->


            <div class="col-md-3">
                {{ Form::label('id_sexo', 'Sexo') }}
                {{ Form::select( 'datos_persona[titular][id_sexo]',$comboSexo,isset($titular) ? $titular->id_sexo: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_genero', 'Genero') }}
                {{ Form::select( 'datos_persona[titular][id_genero]',$comboGenero,isset($titular) ? $titular->id_genero: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('edad', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[titular][edad]', isset($titular) ? $titular->edad: null, array('placeholder'=> '', 'class' => 'form-control fecha')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
                {{ Form::select('datos_persona[titular][id_nacionalidad]',$comboNacionalidad,isset($titular) ? $titular->id_nacionalidad: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_calle', 'Direccion') }}
                {{ Form::text( 'datos_persona[titular][ultimo_calle]',isset($titular) ? $titular->ultimo_calle: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('interseccion', 'Interseccion') }}
                {{ Form::text( 'datos_persona[titular][interseccion]',isset($titular) ? $titular->interseccion: null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_numero', 'N°') }}
                {{ Form::text( 'datos_persona[titular][ultimo_numero]',isset($titular) ? $titular->ultimo_numero: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_piso', 'Piso') }}
                {{ Form::text( 'datos_persona[titular][ultimo_piso]',isset($titular) ? $titular->ultimo_piso: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('ultimo_departamento', 'Departamento') }}
                {{ Form::text( 'datos_persona[titular][ultimo_departamento]',isset($titular) ? $titular->ultimo_departamento: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_codigo_postal', 'Codigo Postal') }}
                {{ Form::text( 'datos_persona[titular][ultimo_codigo_postal]',isset($titular) ? $titular->ultimo_codigo_postal: null, array('placeholder'=> '', 'class' => 'form-control')) }}

            </div>


            <div class="col-md-3">
                {{ Form::label('provincia', 'Provincia') }}
                {{ Form::select( 'datos_persona[titular][ultima_provincia]',$comboProvincia,isset($titular) ? $titular->ultima_provincia: null, array('placeholder'=> '', 'class' => 'form-control provincia','data-target'=>'.partido')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultimo_partido', 'Partido') }}
                {{ Form::select( 'datos_persona[titular][id_ultimo_partido]',[],null, array('placeholder'=> '', 'class' => 'form-control partido','data-val'=>isset($titular) ? $titular->id_ultimo_partido:null)) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('ultima_localidad', 'Localidad') }}
                {{ Form::text( 'datos_persona[titular][ultima_localidad]',isset($titular)? $titular->ultima_localidad : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-12">
                {{ Form::label('observaciones_persona', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[titular][observaciones_persona]',isset($titular)? $titular->observaciones_persona : null, array('placeholder'=> '', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
</div>
@endif
<div class="panel panel-info">
    <div class="panel-heading">Datos del Denunciante</div>
    <div class="panel-body">
        <div class="form-group">

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nombre]','Nombres') }}
                {{ Form::text('datos_persona[responsable][nombre]',isset($responsable)? $responsable->nombre : null,array('placeholder' => '', 'class' =>'form-control nombre')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido]', 'Apellido') }}
                {{ Form::text( 'datos_persona[responsable][apellido]',isset($responsable)? $responsable->apellido : null, array('placeholder'=> '', 'class' =>'form-control apellido')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apellido_materno]', 'Apellido Materno') }}
                {{ Form::text( 'datos_persona[responsable][apellido_materno]', isset($responsable)? $responsable->apellido_materno: null, array('placeholder'=> '', 'class' =>'form-control alphaonly')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][apodos]', 'Sobrenombre') }}
                {{ Form::text( 'datos_persona[responsable][apodos]',isset($responsable) ? $responsable->apodos:null, array('placeholder' => '', 'class' =>'form-control alphaonly')) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_tipo_documento]', 'Tipo de Documento') }}
                {{ Form::select( 'datos_persona[responsable][id_tipo_documento]',$comboTipoDocumento ,isset($responsable)? $responsable->id_tipo_documento : null, array('placeholder'=> '','class' => 'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][nro_documento]', 'N° de Documento') }}
                {{ Form::text( 'datos_persona[responsable][nro_documento]',isset($responsable) ? $responsable->nro_documento :null, array('placeholder'=> '', 'class' =>'form-control alphanumericonly')) }}

            </div>

            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_sexo]', 'Sexo') }}
                {{ Form::select( 'datos_persona[responsable][id_sexo]',$comboSexo,isset($responsable)? $responsable->id_sexo : null, array('placeholder'=> '', 'class' =>'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_genero]', 'Genero') }}
                {{ Form::select( 'datos_persona[responsable][id_genero]',$comboGenero,isset($responsable)? $responsable->id_genero : null, array('placeholder'=> '', 'class' =>'form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][edad]', 'Fecha Nacimiento') }}
                {{ Form::text( 'datos_persona[responsable][edad]', isset($responsable)? $responsable->edad : null, array('placeholder'=> '', 'class' =>'fecha form-control')) }}

            </div>
            <div class="col-md-3">
                {{ Form::label('datos_persona[responsable][id_nacionalidad]', 'Nacionalidad') }}
                {{ Form::select( 'datos_persona[responsable][id_nacionalidad]',$comboNacionalidad,isset($responsable)? $responsable->id_nacionalidad : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}

            </div>
            <div class="col-md-12">
                {{ Form::label('datos_persona[responsable][observaciones_persona]', 'Observaciones') }}
                {{ Form::textarea( 'datos_persona[responsable][observaciones_persona]',isset($responsable)? $responsable->observaciones_persona : null, array('placeholder'=> '', 'class'
                => 'form-control')) }}
            </div>

        </div>
    </div>
</div>
@if(!isset($robo_automotor))
    @include('formularios.autoridad_judicial')

    <div id="newpost" style="display:none">
        @include('formularios.autoridad_judicial2')
    </div>

@else
    @include('formularios.input_alerta_temprana')
    @include('formularios.adjuntar_oficio')
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Atención... </h4>
                </div>
                <div class="modal-body">
                    <center> <b id="mensaje"> </b></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar »</button>
                </div>
            </div>
        </div>
    </div>
@endif
<button class= "btn btn-default" id="ocultar" >Añadir Autoridad Judicial</button>


@if(!isset($robo_automotor))
    <button class="btn btn-primary" id="guardar_form" data-tipo_oficio="2">Guardar »</button>
@else
    <button class="btn btn-primary" data-tipo_oficio="2" type="button" id="guardar">Guardar »</button>
@endif
</div>
{{Form::close() }}
@include("components.modal")
@stop

