@if( (Request::segment(2) != 'pdf') and ($fotos != null))
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $titulo }}</h3>
        </div>
        <div class="panel-body">
            <div class="your-class">
                <div class="row">
                    @foreach($fotos as $foto)
                        @if(is_object($foto))
                            <div class="col-md-2" id="div_id_{{$foto->id_foto}}">
                                <a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" >

                                @if(substr($foto->path, -3) == "pdf")
                                    <img src="{{ Route('archivos.descargar')}}?q=imagen.jpg" width="150px" height="150px">
                                @elseif(substr($foto->path, -3) == "doc" or substr($foto->path, -4) == "docx")
                                    <img src="{{ Route('archivos.descargar')}}?q=doc.png" width="150px" height="150px"></a>
                                @elseif(substr($foto->path, -3) == "xls" or substr($foto->path, -4) == "xlsx")
                                    <img src="{{ Route('archivos.descargar')}}?q=excel.png" width="150px" height="150px"></a>
                                @elseif(substr($foto->path, -3) == "ppt" or substr($foto->path, -4) == "pptx")
                                    <img src="{{ Route('archivos.descargar')}}?q=ppt.jpg" width="150px" height="150px"></a>
                                @else
                                    <img src="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" width="150px" height="150px">
                                @endif
                                </a>
                                <a class="eliminar_foto" style="cursor: pointer;" id="id_{{ $foto->id_foto }}">Eliminar</a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <span>Si elimina alguna foto, el cambio se reflejará cuando guarde el requerimiento</span>
            </div>
        </div>
    </div>
@endif