{{ Form::model($oficio->PersonaDescripcionFisica,['method' => 'POST', 'id' => 'frmDescripcionFisica'],['role' => 'form']) }}
<div class="form-group">
    <button type="button" class="btn btn-success btn-sm" id="btnMostrarHistorico" style="display: block">Mostrar datos historicos</button>

    <div class="col-md-3">
        {{ Form::label('id_contextura_fisica', 'Contextura Física') }}
        {{ Form::select( 'id_contextura_fisica',$comboContexturasFisicas,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('altura', 'Altura') }}
        {{ Form::text( 'altura', null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('peso', 'Peso') }}
        {{ Form::text( 'peso',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_tipo_cabello', 'Cabello Tipo') }}
        {{ Form::select( 'id_tipo_cabello',$comboTipoCabello,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('largo_cabello', 'Largo del Cabello') }}
        {{ Form::select( 'largo_cabello',$comboLargosCabello,null,['class' => 'form-control','id' => 'cargo_cabello']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_color_cabello', 'Color de Cabello') }}
        {{ Form::select( 'id_color_cabello',$comboColorCabello,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_color_ojos', 'Color de Ojos') }}
        {{ Form::select( 'id_color_ojos',$comboColorOjos,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_tez', 'Tez') }}
        {{ Form::select( 'id_tez',$comboTez,null,['class' => 'form-control']) }}
    </div>

    <div class="col-md-12">
        <div class="panel panel-info" style="margin-top: 20px">
            <div class="panel-heading">Señas particulares</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2 checkbox hidden">
                        {{ Form::checkbox('chk_', 'value',null,['id' => 'chk_facebook','class' => 'checkboxRedes']); }}
                    </div>
                    @foreach($comboSenasParticulares as $id => $senia_particular)
                        <div class="col-md-2 checkbox">
                            <?php $i = 'chk_'.$id; ?>
                            @if(isset($oficio->PersonaDescripcionFisica->$i))
                               {{ Form::checkbox('chk_'.$id, 'value',null,['class' => 'checkboxRedes','checked' => true]); }} {{ $senia_particular }}
                            @else
                                {{ Form::checkbox('chk_'.$id, 'value',null,['class' => 'checkboxRedes']); }} {{ $senia_particular }}
                            @endif
                        </div>
                    @endforeach
                    <div class="col-md-12">
                        {{ Form::label('detalle_senias', 'Detalles') }}
                        {{ Form::textarea( 'detalle_senias',null,['class' => 'form-control','style'=>'height:8em']) }}
                    </div>
                </div>
            </div>

        </div>

    </div>


    <div class="col-md-12">
        {{ Form::label('observaciones', 'Observaciones') }}
        {{ Form::textarea( 'observaciones',null,['class' => 'form-control','style'=>'height:8em']) }}
    </div>
</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDescripcionFisica" style="display: block">Guardar datos descripción física</button>
</div>
{{ Form::close() }}