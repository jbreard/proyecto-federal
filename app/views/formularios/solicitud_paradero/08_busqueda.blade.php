{{ Form::model($oficio->PersonaDatosBusqueda,['method' => 'POST', 'id' => 'frmDatosBusqueda','enctype' => 'multipart/form-data', 'files'=>true],['role' => 'form']) }}

<div class="form-group">
    {{ Form::hidden('id_persona_busqueda',null) }}
    <div class="row">
        <div class="col-md-2">
            {{ Form::label('nro_caso', 'N° de caso')}}
            {{ Form::text( 'nro_caso',null,['class'=>'form-control']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('anio', 'Año')}}
            {{ Form::select( 'anio',$comboAnio,null,['class'=>'form-control']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('fecha_aparicion', 'Fecha de aparición')}}
            {{ Form::text( 'fecha_aparicion',null,['class'=>'form-control fecha']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('id_procedencia', 'Procedencia')}}
            {{ Form::select( 'id_procedencia',$comboProcedencias,null,['class'=>'form-control']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('id_estado_actual', 'Estado actual')}}
            {{ Form::select( 'id_estado_actual',$comboEstadosActuales,null,['class'=>'form-control']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('circunstancias_aparicion', 'Circunstancias de aparición')}}
            {{ Form::textarea( 'circunstancias_aparicion',null,['class'=>'form-control']) }}
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-info" style="margin-top: 20px">
            <div class="panel-heading">Avances</div>
            <div class="panel-body">
                <div class="col-md-12">
                    {{ Form::label('avances', 'Nuevo avance')}}
                    {{ Form::text('avances',null,['class'=>'form-control']) }}
                </div>
                <div class="col-md-12" style="margin-top: 10px">
                    @if(count($oficio->PersonaDatosBusquedaXAvance) > 0)
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Detalle</th>
                                <th>Usuario</th>
                                <th>Fecha</th>
                            </tr>
                            <tr>
                                @foreach ($oficio->PersonaDatosBusquedaXAvance as $avance)
                                    <tr>
                                        <td>{{ $avance->detalle }}</td>
                                        @if($avance->usuario)
                                            <td>{{ $avance->usuario->username }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td width="150px">{{ $avance->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tr>
                        </table>
                    @endif
                </div>
            </div>

        </div>

    </div>
    <div class="col-md-12">
        <div class="panel panel-info" style="margin-top: 20px">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3 checkbox">
                        {{ Form::checkbox('menor', '1',null,['class' => 'checkboxRedes']); }} Menor
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 checkbox">
                        {{ Form::checkbox('difusion_foto', '1',null,['class' => 'checkboxRedes']); }} Difusión de foto
                    </div>
                </div>
                <!--<div class="row">
                    <div class="col-md-2 checkbox">
                        {{ Form::checkbox('victima_ilicito', '1',null,['id' => 'victima_ilicito','class' => 'checkboxRedes']); }} Víctima de ilícito
                    </div>
                    <div class="col-md-3">
                        @if(is_object($oficio->PersonaDatosBusqueda))
                            {{ Form::select( 'id_victima_ilicito',$comboVictimasIlicitos,$oficio->PersonaDatosBusqueda->id_victima_ilicito,['class'=>'form-control','id' => 'id_victima_ilicito']) }}
                        @else
                            {{ Form::select( 'id_victima_ilicito',$comboVictimasIlicitos,null,['class'=>'form-control','id' => 'id_victima_ilicito']) }}
                        @endif
                    </div>
                </div>-->
            </div>

        </div>

    </div>
    <div class="col-md-12">
        <div class="panel panel-info" style="margin-top: 20px">
            <div class="panel-heading">Observaciones</div>
            {{ Form::textarea( 'observaciones',null,['class'=>'form-control']) }}
        </div>
    </div>

</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosBusqueda" style="display: block">Guardar datos de búsqueda</button>
</div>

{{ Form::close() }}
