@extends('header')


@include('formularios.solicitud_paradero.scripts')

@section('content')
<div>
    @if($oficio->confirmado == 1)
        <a class="btn btn-primary btn-sm pull-right margin-topmeq" id="btnConfirmarRequerimiento" style="display: block" href="{{ route($oficio->tipo->ruta.".show",$oficio->id) }}">Confirmar requerimiento</a>
    @elseif($oficio->confirmado == 2)
        <a class="btn btn-primary btn-sm pull-right margin-topmeq" style="display: block" href="{{ route($oficio->tipo->ruta.".show",$oficio->id) }}">Ver requerimiento</a>
        <a class="btn btn-success btn-sm pull-right margin-topmeq" style="display: block" href="{{ route($oficio->tipo->ruta.".showComplete",$oficio->id) }}">Ver campos completados</a>
    @endif
</div>
<h3>{{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset($oficios->alerta_temprana) and $oficios->alerta_temprana==1))(Alerta Temprana)@endif</h3>
@include('mensaje_activo_cesado')


<div class="tab-content" >
    <ul class="nav nav-pills nav-justified" style="margin-bottom: 20px" >
        <li class="active"><a class="sinPadding" href="#DatosAutoridadJudicial" data-toggle="tab">Datos autoridad judicial</a></li>
        @if($oficio->id)
            <li><a class="sinPadding" id="datos" href="#DatosPersonaBuscada" data-toggle="tab">Persona Buscada</a></li>
            <li><a class="sinPadding" href="#UltimoDomicilioConocido" data-toggle="tab">Ultimo Domicilio</a></li>
            <li><a class="sinPadding" id="destino" href="#fotos" data-toggle="tab">Fotos</a></li>
            <li><a class="sinPadding" href="#DescripcionFisica" data-toggle="tab">Descripción Fisica</a></li>
            <li><a class="sinPadding" href="#DatosLaborales" data-toggle="tab">Datos Laborales</a></li>
            <li><a class="sinPadding" href="#DatosImportantesRelacionadosConLaAusencia" data-toggle="tab">Relacionado con la ausencia</a></li>
            <li><a class="sinPadding" href="#DatosDelDenunciante" data-toggle="tab">Denunciante</a></li>
            @if($permiso_pe)
                <li><a class="sinPadding" href="#Busqueda" data-toggle="tab">Búsqueda</a></li>
            @endif
        @endif
    </ul>



    <!-- Tab panes -->
    <div class="tab-content" >


        <div class="tab-pane active" id="DatosAutoridadJudicial">
            {{ Form::model($oficio,['method' => 'POST', 'id' => 'frmDatosAutoridadJudicial'],['role' => 'form','enctype' => 'multipart/form-data']) }}


            @include('formularios.autoridad_judicial')

            <div id="newpost" style="display:none">
                @include('formularios.autoridad_judicial2')
            </div>

            @if($oficio->id == "")
            <div class="col-md-12">
                <div class="panel panel-info" style="margin-top: 20px">
                    <div class="panel-heading">Datos obligatorios</div>
                    <div class="panel-body">
                        @include('formularios.solicitud_paradero.datos_compartidos')
                    </div>
                </div>
            </div>
            @endif
            @if(is_object($oficio->archivos->first()))
                @include('galeria',array("titulo"=>"Archivos del oficio","fotos"=>$oficio->archivos))
            @endif
            <div>
                <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosAutoridadJudicial" style="display: block">Guardar datos de la autoridad judicial</button>
            </div>
            {{ Form::close() }}
        </div>
        @if($oficio->id)
        {{ Form::hidden('id',null,['id' => 'id_persona']) }}
        {{ Form::hidden('id',null,['id' => 'id']) }}
        <div class="tab-pane" id="DatosPersonaBuscada">
            @include('formularios.solicitud_paradero.01_datos_persona_buscada')
        </div>
        <div class="tab-pane" id="UltimoDomicilioConocido">
            @include('formularios.solicitud_paradero.02_ultimo_domicilio_conocido')
        </div>
        <div class="tab-pane" id="fotos" >
            @include('formularios.solicitud_paradero.03_fotos')
        </div>
        <div class="tab-pane" id="DescripcionFisica" >
            @include('formularios.solicitud_paradero.04_descripcion_fisica')
        </div>
        <div class="tab-pane" id="DatosLaborales" >
            @include('formularios.solicitud_paradero.05_datos_laborales')
        </div>
        <div class="tab-pane" id="DatosImportantesRelacionadosConLaAusencia" >
            @include('formularios.solicitud_paradero.06_datos_importantes_relacionados_ausencia')
        </div>
        <div class="tab-pane" id="DatosDelDenunciante" >
            @include('formularios.solicitud_paradero.07_datos_denunciante')
        </div>
        <div class="tab-pane" id="Busqueda" >
            @include('formularios.solicitud_paradero.08_busqueda')
        </div>
        @endif


        @include("components.modal")

    </div>

</div>


@endsection

@stop
