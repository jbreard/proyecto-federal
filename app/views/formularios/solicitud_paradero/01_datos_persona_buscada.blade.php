{{ Form::model($oficio->persona_buscada,['method' => 'POST', 'id' => 'frmPersonaBuscada'], array('role' => 'form','enctype' => 'multipart/form-data')) }}
<input type="hidden" id="hidden_telefonos" name="hidden_telefonos"/>

<div class="form-group">

    {{ Form::hidden('id_persona',null) }}

    @include('formularios.solicitud_paradero.datos_compartidos')

    <div class="col-md-3">
        {{ Form::label('sobrenombre', 'Sobrenombre') }}
        {{ Form::text( 'sobrenombre',null,['class' => 'form-control','maxlength' => 50]) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_nacionalidad', 'Nacionalidad') }}
        {{ Form::select( 'id_nacionalidad',$comboNacionalidad,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('sexo', 'Sexo') }}
        {{ Form::select('sexo',$comboSexo,null, ['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('genero', 'Genero') }}
        {{ Form::select('genero',$comboGenero,null, ['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_estado_civil', 'Estado civil') }}
        {{ Form::select( 'id_estado_civil',$comboEstadosCiviles,null,['class' => 'form-control','id' => 'id_estado_civil']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('nombre_conyuge', 'Nombre del cónyuge') }}
        {{ Form::text( 'nombre_conyuge',null,['class' => 'form-control','id' => 'nombre_conyuge','maxlength' => 50]) }}
    </div>

    <div class="col-md-6">
        {{ Form::label('apellido_nombre_paterno', 'Apellido y nombre paterno') }}
        {{ Form::text( 'apellido_nombre_paterno',null,['class' =>'form-control','maxlength' => 100]) }}
    </div>
    <div class="col-md-6">
        {{ Form::label('apellido_nombre_materno', 'Apellido y nombre materno') }}
        {{ Form::text( 'apellido_nombre_materno',null,['class' => 'form-control','maxlength' => 100]) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('id_tipo_documento', 'Tipo Documento') }}
        {{ Form::select( 'id_tipo_documento',$comboTipoDocumento,null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('nro_documento', 'N° de Documento') }}
        {{ Form::text( 'nro_documento',null,['class' => 'form-control buscar_dni',"data-tipo_oficio"=>"1","data-tipo"=>"1",'maxlength' => 15]) }}
    </div>
    <div class="col-md-6">
        {{ Form::label('autoridad_expide', 'Autoridad que lo expide') }}
        {{ Form::text( 'autoridad_expide',null,['class' => 'form-control','maxlength' => 100]) }}
    </div>
    <div class="row">
        <div class="col-md-1">
            {{ Form::label('cod_area_telefono','Cod.Area') }}
            {{ Form::text( 'cod_area_telefono',null, ['placeholder' => '011', 'class' => 'form-control']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('telefono', 'Telefono fijo') }}
            {{ Form::text( 'telefono',null,['class' =>'form-control','maxlength' => 20]) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('id_compania_telefono', 'Compañía') }}
            {{ Form::select('id_compania_telefono',$comboCompania,null,['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-md-1">
        {{ Form::label('cod_area_celular','Cod.Area') }}
        {{ Form::text( 'cod_area_celular',null, ['placeholder' => '011', 'class' => 'form-control','id' => 'cod_area_celular_persona_buscada','maxlength' => 10]) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('celular', 'N° de Celular') }}
        {{ Form::text( 'celular',null, ['class' => 'form-control', 'id' => 'celular_persona_buscada','maxlength' => 20]) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('id_compania_celular', 'Compañía') }}
        {{ Form::select( 'id_compania_celular',$comboCompania,null,['class' => 'form-control','id' => 'id_compania_celular_persona_buscada']) }}
    </div>
    <div class="col-md-6">
        <button type="button" class="btn btn-success btn-sm" id="btnAgregarCelularPersonaBuscada" style="margin-top: 23px">Agregar</button>
    </div>

    <div class="col-md-12 margin-top" id="div_celulares_persona_buscada"></div>


    <div class="col-md-12">
        <div class="panel panel-info" style="margin-top: 20px">
            <div class="panel-heading">Redes sociales</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_facebook', 'value',null,['id' => 'chk_facebook','class' => 'checkboxRedes']); }} Facebook
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'facebook',null,['id' => 'facebook','class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_twitter', 'value',null,['id' => 'chk_twitter','class' => 'checkboxRedes']); }} Twitter
                    </div>
                    <div class="col-md-3">
                        {{ Form::text('twitter',null, ['id' => 'twitter', 'class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_badoo', 'value',null,['id' => 'chk_badoo','class' => 'checkboxRedes']); }} Badoo
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'badoo',null,['id' => 'badoo','class' =>'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_instagram', 'value',null,['id' => 'chk_instagram','class' => 'checkboxRedes']); }} Instagram
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'instagram',null, ['id' => 'instagram','class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_linkedin', 'value',null,['id' => 'chk_linkedin','class' => 'checkboxRedes']); }} Linkedin
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'linkedin',null, ['id' => 'linkedin','class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_snapchat', 'value',null,['id' => 'chk_snapchat','class' => 'checkboxRedes']); }} Snapchat
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'snapchat',null,['id' => 'snapchat','class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 checkbox">
                        {{ Form::checkbox('chk_otro', 'value',null,['id' => 'chk_otro','class' => 'checkboxRedes']); }} Otro
                    </div>
                    <div class="col-md-3">
                        {{ Form::text( 'otro',null,['id' => 'otro','class' => 'form-control textRedes','maxlength' => 50]) }}
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosPersonaBuscada" style="display: block">Guardar datos persona buscada</button>
</div>
{{ Form::close() }}