{{ Form::model($oficio->PersonaDatosLaborales,['method' => 'POST', 'id' => 'frmDatosLaborales'],['role' => 'form']) }}
<div class="form-group">
    <div class="col-md-4">
        {{ Form::label('profesion_ocupacion', 'Profesión/Ocupación') }}
        {{ Form::text( 'profesion_ocupacion',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-4">
        {{ Form::label('empleador', 'Empleador') }}
        {{ Form::text( 'empleador',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-4">
        {{ Form::label('telefono', 'Teléfono') }}
        {{ Form::text( 'telefono',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('direccion', 'Dirección') }}
        {{ Form::text( 'direccion',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('nro', 'Número') }}
        {{ Form::text( 'nro',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('piso', 'Piso') }}
        {{ Form::text( 'piso',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('dpto','Departamento') }}
        {{ Form::text( 'dpto',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('codigo_postal', 'Codigo Postal') }}
        {{ Form::text( 'codigo_postal',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_provincia', 'Provincia') }}
        {{ Form::select('id_provincia',$comboProvincia,null,['class' => 'form-control provincia','data-target'=>'.partido_laboral']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_partido', 'Partido') }}
        {{ Form::select( 'id_partido',[],null,['class' => 'form-control partido partido_laboral']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('localidad', 'Barrio - Localidad') }}
        {{ Form::text( 'localidad',null,['class' => 'form-control localidad_laboral']) }}
    </div>
</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosLaborales" style="display: block">Guardar datos laborales</button>
</div>

{{ Form::close() }}