<div class="col-md-6">
    {{ Form::label('primera_desaparece', '¿Es la primera vez que desaparece? ') }}
    {{ $resultado->primera_desaparece }}
</div>
<div class="col-md-6">
    {{ Form::label('enfermedad', '¿Tiene alguna enfermedad? ') }}
    {{ $resultado->enfermedad }}
</div>
<div class="col-md-6">
    {{ Form::label('drogadiccion', '¿Alguna adiccion? ') }}
    {{ $resultado->drogadiccion }}
</div>
<div class="col-md-6">
    {{ Form::label('internado_institucion', '¿Internado en alguna institucion?') }}
    {{ $resultado->internado_institucion }}
</div>
<div class="col-md-6">
    {{ Form::label('descripcion_caso', 'Descripción del caso: ') }}
    {{ $resultado->descripcion_caso }}
</div>
<div class="col-md-6">
    {{ Form::label('novedades', 'Novedades: ') }}
    {{ $resultado->novedades }}
</div>
<div class="col-md-6">
    {{ Form::label('enfermedades', 'Enfermedades: ') }}
    {{ $resultado->enfermedades }}
</div>
<div class="col-md-6">
    {{ Form::label('concurrencia', 'Concurrencia: ') }}
    {{ $resultado->concurrencia }}
</div>
<div class="col-md-6">
    {{ Form::label('descripcion_persona', 'Descripción de la persona: ') }}
    {{ $resultado->descipcion_persona }}
</div>
<div class="col-md-6">
    {{ Form::label('medicacion', 'Medicación: ') }}
    {{ $resultado->medicacion }}
</div>
<div class="col-md-6">
    {{ Form::label('vinculo_desaparecido', 'Vínculo con el desaparecido: ') }}
    {{ $resultado->vinculo_desaparecido }}
</div>
<div class="col-md-6">
    {{ Form::label('pertenencia_referencia', 'Pertenencia referencia: ') }}
    {{ $resultado->pertenencia_referencia }}
</div>
<div class="col-md-6">
    {{ Form::label('vestimenta', 'Vestimenta: ') }}
    {{ $resultado->vestimenta }}
</div>
<div class="col-md-6">
    {{ Form::label('observaciones_persona', 'Observaciones de la persona: ') }}
    {{ $resultado->observaciones_persona }}
</div>
<div class="col-md-6">
    {{ Form::label('observaciones_generales', 'Observaciones generales: ') }}
    {{ $resultado->observaciones }}
</div>



