{{ Form::model($oficio->autorizacion,['method' => 'POST', 'id' => 'frmFotos'], array('role' => 'form')) }}
<div class="panel panel-info">
    <div class="panel-heading">Fotografías de la Persona Buscada</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'foto[buscado][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Fotos'))
                </div>
                <div id="fotos_append">

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="agregar_foto" data-contenedor="#foto" data-receptor="#fotos_append">Agregar Más Fotos</a>
            </div>
        </div>
    </div>
</div>
@if(is_object($oficio->fotos->first()))
@include('galeria',array("titulo"=>"Fotos cargadas","fotos"=>$oficio->fotos))
@endif

@include("galeria",["fotos"=>isset($buscado)? $buscado->getFotos:null,"titulo"=>"Fotografías Cargadas"])

<div class="panel panel-info">
    <div class="panel-heading">Autorizacion para difundir las fotos</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div id="foto">
                    @include('formularios.formulario_archivo',array('nombre'=>'foto[autorizacion][]','i'=>0,'eliminar'=>false,'nombre_campo'=>'Fotos'))
                </div>
                @if(is_object($oficio->autorizacion))
                    @if($oficio->autorizacion->path)
                        <a href="{{ Route('archivos.descargar')}}?q={{ $oficio->autorizacion->path }}" target="_blank">Descargar</a>
                    @endif
                @endif
                <div id="fotos_append">

                </div>
            </div>

        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">Observaciones</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-12">
                {{ Form::label('observaciones', 'Observaciones')}}
                {{ Form::textarea( 'observaciones', null,['class'=>'form-control']) }}
            </div>

        </div>
    </div>
</div>

<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarFotos" style="display: block">Guardar fotos</button>
</div>
{{ Form::close() }}