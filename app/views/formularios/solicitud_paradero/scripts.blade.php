@section('scripts')
{{ HTML::script('assets/js/moment.js') }}
{{ HTML::script('assets/js/chosen.jquery.min.js') }}


<style>

    .sinPadding{
        padding: 10px 0px !important;
    }

    .checkboxRedes{
        opacity: 100 !important;
        position: relative;
        margin-top: 0px !important;
    }

    .textRedes{
        display: inline-block;
        float:left;
    }

    .margin-top{
        margin-top: 20px;
    }

</style>

<script>

function existeFecha (fecha) {
    var fechaf = fecha.split("/");
    var d = fechaf[0];
    var m = fechaf[1];
    var y = fechaf[2];
    return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
}

function getAge(date) {

    var years = 0;
    if(date)
    {
        var fecha = date.split("/");
        years = moment().diff(fecha[2]+"-"+fecha[1]+"-"+fecha[0], 'years',false);
    }
    if(years > 1)
    {
        return years + " Años"
    }
    else if(years == 1)
        return years + " Año";
    else
        return "0 Años"
}

/*  ////////////////////////////// Celulares de la persona extraviada ////////////////////////////////  */

function limpiarCamposTelefono()
{
    $("#cod_area_celular_persona_buscada").val("");
    $("#celular_persona_buscada").val("");
    $("#id_compania_celular_persona_buscada").val("");
}


function validarTelefono()
{
    var cod_area_celular_persona_buscada = $("#cod_area_celular_persona_buscada").val();
    var celular_persona_buscada = $("#celular_persona_buscada").val();
    var id_compania_persona_buscada = $("#id_compania_celular_persona_buscada").val();
    var compania_persona_buscada = $("#id_compania_celular_persona_buscada option:selected").html();
    if(cod_area_celular_persona_buscada != "" && celular_persona_buscada != "" && id_compania_persona_buscada != "")
    {
        return false;
    }
    else
    {
        return "Debe completar todos los campos";
    }
}

function agregarCelular()
{
    if($("#hidden_telefonos").val() != "")
        var lista = jQuery.parseJSON($("#hidden_telefonos").val());
    else
        var lista = [];

    if(respuesta = validarTelefono())
    {
        $("#contenido-modal").html(respuesta);
        $("#confirmacion").modal(function(){show:true});
    }
    else
    {
        var cod_area_celular_persona_buscada = $("#cod_area_celular_persona_buscada").val();
        var celular_persona_buscada = $("#celular_persona_buscada").val();
        var id_compania_persona_buscada = $("#id_compania_celular_persona_buscada").val();
        var compania_persona_buscada = $("#id_compania_celular_persona_buscada option:selected").html();

        var nuevo = {'id': lista.length + 1,'cod_area':cod_area_celular_persona_buscada, 'telefono':celular_persona_buscada,'compania':compania_persona_buscada,'id_compania':id_compania_persona_buscada};

        lista.push(nuevo);
        $("#hidden_telefonos").val(JSON.stringify(lista));
        darListaCelulares(lista);
        limpiarCamposTelefono();
    }


}

function darListaCelulares(arr)
{
    arr = arr || [];
    if(arr.length == 0)
        $("#div_celulares_persona_buscada").html("");
    else
    {
        var salida = "<div class='panel panel-info' id='listado_archivos' data-bool='existe'><div class='panel-heading'>Listado de telefonos</div><div class='panel-body'><div class='row'><table class='table table-bordered'><thead><tr><th>Cod. Area</th><th>Telefono</th><th>Compañia</th><th>Eliminar</th></tr></thead><tbody>";
        $.each(arr,function(index, value){
            salida = salida + "<tr>";

            salida = salida + "<td style='text-transform: none'>" + value.cod_area + "</td>";
            salida = salida + "<td style='text-transform: none'>" + value.telefono + "</td>";
            salida = salida + "<td style='text-transform: none'>" + value.compania + "</td>";
            salida = salida + "<td><input type='button' value='Eliminar' onclick='eliminarCelular(" + value.id_celular + ")' class='btn btn-danger btn-sm btnEliminarCelular' /></td>";

            salida = salida + "</tr>";
            //console.log('My array has at position ' + index + ', this value: ' + value.id);
        });
        salida = salida + "</tbody></table></div></div></div>";
        $("#div_celulares_persona_buscada").html(salida);
    }

}

function eliminarCelular(id)
{
    var lista = jQuery.parseJSON($("#hidden_telefonos").val());
//    console.log(lista);
    var nueva_lista = [];
    $.each(lista,function(index, value){
        if (value.id_celular != id)
            nueva_lista.push(value);
    });
    $("#hidden_telefonos").val(JSON.stringify(nueva_lista));
    darListaCelulares(nueva_lista);
}

/*  ////////////////////////////// Celulares de la persona denunciante ////////////////////////////////  */

function limpiarCamposTelefonoDenunciante()
{
    $("#cod_area_celular_denunciante").val("");
    $("#celular_denunciante").val("");
}


function validarTelefonoDenunciante()
{
    var cod_area_celular = $("#cod_area_celular_denunciante").val();
    var celular = $("#celular_denunciante").val();
    if(cod_area_celular != "" && celular)
    {
        return false;
    }
    else
    {
        return "Debe completar todos los campos";
    }
}

function agregarCelularDenunciante()
{
    if($("#hidden_telefonos_denunciante").val() != "")
        var lista = jQuery.parseJSON($("#hidden_telefonos_denunciante").val());
    else
        var lista = [];

    if(respuesta = validarTelefonoDenunciante())
    {
        $("#contenido-modal").html(respuesta);
        $("#confirmacion").modal(function(){show:true});
    }
    else
    {
        var cod_area_celular_denunciante = $("#cod_area_celular_denunciante").val();
        var celular_denunciante= $("#celular_denunciante").val();

        var nuevo = {'id': lista.length + 1,'cod_area':cod_area_celular_denunciante, 'telefono':celular_denunciante};

        lista.push(nuevo);
        $("#hidden_telefonos_denunciante").val(JSON.stringify(lista));
        darListaCelularesDenunciante(lista);
        limpiarCamposTelefonoDenunciante();
    }


}

function darListaCelularesDenunciante(arr)
{
    arr = arr || [];
    if(arr.length == 0)
        $("#div_celulares_denunciante").html("");
    else
    {
        var salida = "<div class='panel panel-info' id='listado_celulares_denunciante' data-bool='existe'><div class='panel-heading'>Listado de celulares</div><div class='panel-body'><div class='row'><table class='table table-bordered'><thead><tr><th>Cod. Area</th><th>Telefono</th><th>Eliminar</th></tr></thead><tbody>";
        $.each(arr,function(index, value){
            salida = salida + "<tr>";

            salida = salida + "<td style='text-transform: none'>" + value.cod_area + "</td>";
            salida = salida + "<td style='text-transform: none'>" + value.telefono + "</td>";
            salida = salida + "<td><input type='button' value='Eliminar' onclick='eliminarCelularDenunciante(" + value.id_celular + ")' class='btn btn-danger btn-sm btnEliminarCelularDenunciante' /></td>";

            salida = salida + "</tr>";
            //console.log('My array has at position ' + index + ', this value: ' + value.id);
        });
        salida = salida + "</tbody></table></div></div></div>";
        $("#div_celulares_denunciante").html(salida);
    }

}

function eliminarCelularDenunciante(id)
{
    var lista = jQuery.parseJSON($("#hidden_telefonos_denunciante").val());
    var nueva_lista = [];
    $.each(lista,function(index, value){
        if (value.id_celular != id)
            nueva_lista.push(value);
    });
    $("#hidden_telefonos_denunciante").val(JSON.stringify(nueva_lista));
    darListaCelularesDenunciante(nueva_lista);
}



$().ready(function(){

    // Variable to store your files
    var files;

    // Add events
    $('input[type=file]').on('change', prepareUpload);

    // Grab the files and set them to our variable
    function prepareUpload(event)
    {
        files = event.target.files;
    }

    @if(is_object($oficio))
        $("#id").val({{ $oficio->id }});
    @endif

    @if($oficio->NrosCelulares)
        darListaCelulares({{ $oficio->NrosCelulares }});
        $("#hidden_telefonos").val(JSON.stringify({{ $oficio->NrosCelulares }}));
        darListaCelularesDenunciante({{ $oficio->NrosCelularesDenunciante }});
        $("#hidden_telefonos_denunciante").val(JSON.stringify({{ $oficio->NrosCelularesDenunciante }}));
    @endif


    function peticionAjax(destino,datos,redireccionar)
    {
        redireccionar = redireccionar || 0;
        $.ajax({
            type: "Post",
            url: destino,
            data: datos,
            async: true,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta){
                console.log(respuesta);
                if(isNaN(respuesta) ) {
                    if(respuesta.substr(0,4) == "http")
                    {
                        window.location.href = respuesta;
                        respuesta = "Datos guardados correctamente";
                    }
                    else
                    {
                        $("#btnGuardarDatosAutoridadJudicial").removeAttr("disabled");
                        $("#contenido-modal").html(respuesta);
                        $("#confirmacion").modal(function(){show:true});
                    }

                }
                else
                {

                    if(redireccionar == 1)
                    {
                        window.location.href = respuesta;
                        respuesta = "Datos guardados correctamente";
                    }

//                    $("#id_persona").val(respuesta);
                    $("#contenido-modal").html("Datos guardados correctamente");
                    $("#confirmacion").modal(function(){show:true});
                }
            },
            error:function(xhr, status, error){
                 var err = eval("(" + xhr.responseText + ")");
                console.log(err);
                $("#contenido-modal").html("Hubo un problema, consulte con el administrador");
                $("#confirmacion").modal(function(){show:true});
            }


        })
    }

    $("#frmDatosAutoridadJudicial").on("submit", function(e){
        e.preventDefault();
        $("#btnGuardarDatosAutoridadJudicial").attr("disabled",true);
        var formData = new FormData(document.getElementById("frmDatosAutoridadJudicial"));

        if($("#id").length > 0)
            formData.append("id_oficio",$("#id").val());
        else
            formData.append("id_oficio","");



        var destino = "{{ Route('solicitud_paradero.guardarPEAutoridadJudicial') }}";
//        console.log(formData);
//        if($("#id").val())
            peticionAjax(destino,formData,1);
//        else
//            peticionAjax(destino,formData,1);
    });

    $("#frmPersonaBuscada").on("submit", function(e){
//        console.log("hola");
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmPersonaBuscada"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEPersonaBuscada') }}";
        //pongo en 0 para que no se rompa
        peticionAjax(destino,formData,0);
    });

    $("#frmUltimoDomicilio").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmUltimoDomicilio"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEUltimoDomicilio') }}";
        peticionAjax(destino,formData);
    });

    $("#frmFotos").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmFotos"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEFotos') }}";
        peticionAjax(destino,formData,1);
    });

    $("#frmDescripcionFisica").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmDescripcionFisica"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEDescripcionFisica') }}";
        peticionAjax(destino,formData);
    });


    $("#frmDatosLaborales").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmDatosLaborales"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEDatosLaborales') }}";
        peticionAjax(destino,formData);
    });

    $("#frmDatosRelacionadoAusencia").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmDatosRelacionadoAusencia"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEDatosRelacionadoAusencia') }}";
        peticionAjax(destino,formData);
    });


    $("#frmDatosDenunciante").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmDatosDenunciante"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEDatosDenunciante') }}";
        peticionAjax(destino,formData,1);
    });

    $("#frmDatosBusqueda").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmDatosBusqueda"));
        formData.append("id_oficio",$("#id").val());
        var destino = "{{ Route('solicitud_paradero.guardarPEDatosBusqueda') }}";
        peticionAjax(destino,formData,1);
    });


    function darCheckboxesSeleccionados(habilitado,name)
    {
        if(habilitado)
        {
            $("#"+name).prop("readOnly",false);
            $("#chk_"+name).prop('checked',true)
        }
        else
        {
            $("#"+name).prop("readOnly",true);
            $("#chk_"+name).prop('checked',false)
        }

    }

    function darCheckboxSeleccionadoVictimaIlicito(habilitado,selected)
    {
        if(habilitado == 1)
        {
            $("#id_victima_ilicito").removeClass("hidden");
            $("#victima_ilicito").prop('checked',true)
        }
        else
        {
            $("#id_victima_ilicito").addClass("hidden");
            $("#victima_ilicito").prop('checked',false)
        }

    }

    ////////////////////////////* Persona buscada *//////////////////////////////////////////////////////

    $("#btnAgregarCelularPersonaBuscada").click(function(){
        agregarCelular();
    });

    $("#btnAgregarCelularDenunciante").click(function(){
        agregarCelularDenunciante();
    });

    @if(is_object($oficio->persona_buscada))

//        $("input:text[name=edad]").val(getAge($("#fecha_nacimiento").val()));

        @if($oficio->persona_buscada->facebook)
            darCheckboxesSeleccionados(1,"facebook");
        @else
            darCheckboxesSeleccionados(0,"facebook");
        @endif

        @if($oficio->persona_buscada->twitter)
            darCheckboxesSeleccionados(1,"twitter");
        @else
            darCheckboxesSeleccionados(0,"twitter");
        @endif

        @if($oficio->persona_buscada->instagram)
            darCheckboxesSeleccionados(1,"instagram");
        @else
            darCheckboxesSeleccionados(0,"instagram");
        @endif

        @if($oficio->persona_buscada->badoo)
            darCheckboxesSeleccionados(1,"badoo");
        @else
            darCheckboxesSeleccionados(0,"badoo");
        @endif

        @if($oficio->persona_buscada->snapchat)
            darCheckboxesSeleccionados(1,"snapchat");
        @else
            darCheckboxesSeleccionados(0,"snapchat");
        @endif

        @if($oficio->persona_buscada->linkedin)
            darCheckboxesSeleccionados(1,"linkedin");
        @else
            darCheckboxesSeleccionados(0,"linkedin");
        @endif

        @if($oficio->persona_buscada->otro)
            darCheckboxesSeleccionados(1,"otro");
        @else
            darCheckboxesSeleccionados(0,"otro");
        @endif

        @if(is_object($oficio->PersonaDatosBusqueda))
            darCheckboxSeleccionadoVictimaIlicito("{{ $oficio->PersonaDatosBusqueda->victima_ilicito  }}","{{ $oficio->PersonaDatosBusqueda->id_victima_ilicito  }}");
        @else
            darCheckboxSeleccionadoVictimaIlicito(0,0);
        @endif

        habilitarDeshabilitarSelectVictimaIlicito();
    @else
        $("#id_estado_civil").val("1");
        $("#facebook").prop("readOnly",true);
        $("#twitter").prop("readOnly",true);
        $("#instagram").prop("readOnly",true);
        $("#badoo").prop("readOnly",true);
        $("#snapchat").prop("readOnly",true);
        $("#linkedin").prop("readOnly",true);
        $("#otro").prop("readOnly",true);
        $("#colorCabello").prop("readOnly",true);

    @endif

    $("#id_estado_civil").change(function(){
        var text = $("#id_estado_civil option:selected").text();
        $("#nombre_conyuge").val("");
        if(text == "Casado")
            $("#nombre_conyuge").prop('disabled', false);
        else
            $("#nombre_conyuge").prop('disabled', true);

    });

    $("#cCabello").change(function(){
        var text = $("#cCabello option:selected").text();
        if(text == "Otro")
            $("#colorCabello").prop('disabled', false);
        else
            $("#colorCabello").prop('disabled', true);

    });

    $("#cCabello").change(function(){
        var text = $("#cCabello option:selected").text();
        if(text == "Otro")
            $("#colorCabello").prop('disabled', false);
        else
            $("#colorCabello").prop('disabled', true);

    });

    function habilitarDeshabilitarCheckboxes(name)
    {
        if ($('#chk_'+name).prop('checked'))
            $("#"+name).prop('readOnly', false);
        else
        {
            $("#"+name).prop("readOnly",true);
            $("#"+name).val("");
        }

    }



    $('#chk_facebook').change(function(){
        habilitarDeshabilitarCheckboxes("facebook");
    });

    $('#chk_twitter').change(function(){
        habilitarDeshabilitarCheckboxes("twitter");
    });

    $('#chk_badoo').change(function(){
        habilitarDeshabilitarCheckboxes("badoo");
    });

    $('#chk_instagram').change(function(){
        habilitarDeshabilitarCheckboxes("instagram");
    });

    $('#chk_linkedin').change(function(){
        habilitarDeshabilitarCheckboxes("linkedin");
    });

    $('#chk_snapchat').change(function(){
        habilitarDeshabilitarCheckboxes("snapchat");
    });

    $('#chk_otro').change(function(){
        habilitarDeshabilitarCheckboxes("otro");
    });



    function habilitarDeshabilitarSelectVictimaIlicito()
    {
        if($("#victima_ilicito").prop('checked'))
            $("#id_victima_ilicito").removeClass('hidden');
        else
            $("#id_victima_ilicito").addClass('hidden');

    }

    $('#victima_ilicito').change(function(){
        habilitarDeshabilitarSelectVictimaIlicito();
    });

    // Relacionado con la ausencia


    @if(is_object($oficio->PersonaDatosRelacionadoAusencia))
        @if($oficio->PersonaDatosRelacionadoAusencia->detalle_drogas)
            $("#cmb_detalle_drogas option:eq(2)").prop('selected', true);
            $("#detalle_drogas").prop('disabled', false);
            $("#fecha_tratamiento_institucion").prop('disabled', false);
        @else
            $("#detalle_drogas").prop('disabled', true);
            $("#fecha_tratamiento_institucion").prop('disabled', true);
        @endif
        @if($oficio->PersonaDatosRelacionadoAusencia->tratamiento_institucion)
            $("#cmb_tratamiento_institucion option:eq(2)").prop('selected', true);
            $("#tratamiento_institucion").prop('disabled', false);
            $("#fecha_tratamiento_institucion").prop('disabled', false);
        @else
            $("#tratamiento_institucion").prop('disabled', true);
            $("#fecha_tratamiento_institucion").prop('disabled', true);
        @endif
        @if($oficio->PersonaDatosRelacionadoAusencia->padecimiento_mental)
            $("#cmb_padecimiento_mental option:eq(2)").prop('selected', true);
            $("#padecimiento_mental").prop('disabled', false);
            $("#cmb_padecimiento_institucion").prop('disabled', false);
            @if($oficio->PersonaDatosRelacionadoAusencia->padecimiento_institucion)
                $("#cmb_padecimiento_institucion option:eq(2)").prop('selected', true);
                $("#padecimiento_institucion").prop('disabled', false);
                $("#fecha_padecimiento").prop('disabled', false);
            @else
                $("#padecimiento_institucion").prop('disabled', true);
                $("#fecha_padecimiento").prop('disabled', true);
            @endif
        @else
            $("#padecimiento_mental").prop('disabled', true);
            $("#cmb_padecimiento_institucion").prop('disabled', true);
            $("#padecimiento_institucion").prop('disabled', true);
            $("#fecha_padecimiento").prop('disabled', true);
        @endif
        @if($oficio->PersonaDatosRelacionadoAusencia->especifique_enfermedad)
            $("#cmb_especifique_enfermedad option:eq(2)").prop('selected', true);
            $("#especifique_enfermedad").prop('disabled', false);
            $("#cmb_especifique_tratamiento_medico").prop('disabled', false);
            @if($oficio->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico)
                $("#cmb_especifique_tratamiento_medico option:eq(2)").prop('selected', true);
                $("#especifique_tratamiento_medico").prop('disabled', false);
                $("#cmb_especifique_alguna_medicacion").prop('disabled', false);
                @if($oficio->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico)
                    $("#cmb_especifique_alguna_medicacion option:eq(2)").prop('selected', true);
                    $("#especifique_alguna_medicacion").prop('disabled', false);
                @else
                    $("#especifique_alguna_medicacion").prop('disabled', true);
                @endif
            @else
                $("#especifique_tratamiento_medico").prop('disabled', true);
                $("#especifique_alguna_medicacion").prop('disabled', true);
            @endif
        @else
            $("#especifique_enfermedad").prop('disabled', true);
            $("#cmb_especifique_tratamiento_medico").prop('disabled', true);
            $("#especifique_tratamiento_medico").prop('disabled', true);
            $("#especifique_alguna_medicacion").prop('disabled', true);
        @endif
        @if($oficio->PersonaDatosRelacionadoAusencia->especifique_documentacion_consigo)
            $("#cmb_especifique_documentacion_consigo option:eq(2)").prop('selected', true);
            $("#especifique_documentacion_consigo").prop('disabled', false);
        @else
            $("#especifique_documentacion_consigo").prop('disabled', true);
        @endif
        @if($oficio->PersonaDatosRelacionadoAusencia->especifique_situacion_conflicto)
            $("#cmb_especifique_situacion_conflicto option:eq(2)").prop('selected', true);
            $("#especifique_situacion_conflicto").prop('disabled', false);
        @else
            $("#especifique_situacion_conflicto").prop('disabled', true);
        @endif
    @else

        $("#cmb_detalle_drogas").val("1");
        $("#adiccion_alcohol").val("1");
        $("#cmb_tratamiento_institucion").val("1");
        $("#cmb_padecimiento_mental").val("1");
        $("#id_institucion_momento_ausencia").val("1");
        $("#cmb_padecimiento_institucion").val("1");
        $("#situacion_calle").val("1");
        $("#cmb_especifique_enfermedad").val("1");
        $("#sustraccion_parental").val("1");
        $("#cmb_especifique_tratamiento_medico").val("1");
        $("#cmb_especifique_alguna_medicacion").val("1");
        $("#cmb_especifique_situacion_conflicto").val("1");
        $("#primera_vez_ausenta").val("1");
        $("#cmb_especifique_documentacion_consigo").val("1");

        $("#detalle_drogas").prop('disabled', true);
        $("#fecha_tratamiento_institucion").prop('disabled', true);
        $("#tratamiento_institucion").prop('disabled', true);
        $("#fecha_tratamiento_institucion").prop('disabled', true);
        $("#padecimiento_institucion").prop('disabled', true);
        $("#cmb_padecimiento_institucion").prop('disabled', true);
        $("#padecimiento_mental").prop('disabled', true);
        $("#fecha_padecimiento").prop('disabled', true);
        $("#especifique_tratamiento_medico").prop('disabled', true);
        $("#cmb_especifique_alguna_medicacion").prop('disabled', true);
        $("#cmb_especifique_tratamiento_medico").prop('disabled', true);
        $("#especifique_enfermedad").prop('disabled', true);
        $("#especifique_alguna_medicacion").prop('disabled', true);
        $("#especifique_documentacion_consigo").prop('disabled', true);
        $("#especifique_situacion_conflicto").prop('disabled', true);
    @endif




    $("#cmb_detalle_drogas").change(function(){
        var text = $("#cmb_detalle_drogas option:selected").text();
        $("#detalle_drogas").val("");
        if(text == "Si")
            $("#detalle_drogas").prop('disabled', false);
        else
            $("#detalle_drogas").prop('disabled', true);

    });

    $("#cmb_tratamiento_institucion").change(function(){
        var text = $("#cmb_tratamiento_institucion option:selected").text();
        $("#tratamiento_institucion").val("");
        $("#fecha_tratamiento_institucion").val("");
        if(text == "Si")
        {
            $("#tratamiento_institucion").prop('disabled', false);
            $("#fecha_tratamiento_institucion").prop('disabled', false);
        }
        else
        {
            $("#tratamiento_institucion").prop('disabled', true);
            $("#fecha_tratamiento_institucion").prop('disabled', true);

        }

    });

    $("#cmb_padecimiento_mental").change(function(){
        var text = $("#cmb_padecimiento_mental option:selected").text();
        $("#padecimiento_mental").val("");
        if(text == "Si")
        {
            $("#padecimiento_mental").prop('disabled', false);
            $("#cmb_padecimiento_institucion").prop('disabled', false);
        }
        else
        {
            $("#padecimiento_mental").prop('disabled', true);
            $("#cmb_padecimiento_institucion").prop('disabled', true);
        }

    });

    $("#cmb_padecimiento_institucion").change(function(){
        var text = $("#cmb_padecimiento_institucion option:selected").text();
        $("#padecimiento_institucion").val("");
        $("#fecha_padecimiento").val("");
        if(text == "Si")
        {
            $("#padecimiento_institucion").prop('disabled', false);
            $("#fecha_padecimiento").prop('disabled', false);
        }
        else
        {
            $("#padecimiento_mental").prop('disabled', true);
            $("#fecha_padecimiento").prop('disabled', true);
        }

    });


    $("#cmb_especifique_enfermedad").change(function(){
        var text = $("#cmb_especifique_enfermedad option:selected").text();
        $("#especifique_enfermedad").val("");
        $("#cmb_especifique_tratamiento_medico").val("");
        $("#cmb_especifique_alguna_medicacion").val("");
        $("#especifique_alguna_medicacion").val("");
        if(text == "Si")
        {
            $("#especifique_enfermedad").prop('disabled', false);
            $("#cmb_especifique_tratamiento_medico").prop('disabled', false);
        }
        else
        {
            $("#especifique_enfermedad").prop('disabled', true);
            $("#cmb_especifique_tratamiento_medico").prop('disabled',true);
            $('#cmb_especifique_tratamiento_medico > option[value="3"]').attr('selected', 'selected');
            $('#cmb_especifique_tratamiento_medico > option[value="3"]').attr('selected', 'selected');

        }

    });

    $("#cmb_especifique_tratamiento_medico").change(function(){
        var text = $("#cmb_especifique_tratamiento_medico option:selected").text();
        $("#especifique_tratamiento_medico").val("");
        if(text == "Si")
        {
            $("#especifique_tratamiento_medico").prop('disabled', false);
            $("#cmb_especifique_alguna_medicacion").prop('disabled', false);
        }
        else
        {
            $("#especifique_tratamiento_medico").prop('disabled', true);
            $("#cmb_especifique_alguna_medicacion").prop('disabled', true);
        }

    });

    $("#cmb_especifique_alguna_medicacion").change(function(){
        var text = $("#cmb_especifique_alguna_medicacion option:selected").text();
        $("#especifique_alguna_medicacion").val("");
        if(text == "Si")
            $("#especifique_alguna_medicacion").prop('disabled', false);
        else
            $("#especifique_alguna_medicacion").prop('disabled', true);

    });

    $("#cmb_especifique_situacion_conflicto").change(function(){
        var text = $("#cmb_especifique_situacion_conflicto option:selected").text();
        $("#especifique_situacion_conflicto").val("");
        if(text == "Si")
            $("#especifique_situacion_conflicto").prop('disabled', false);
        else
            $("#especifique_situacion_conflicto").prop('disabled', true);

    });

    $("#cmb_especifique_documentacion_consigo").change(function(){
        var text = $("#cmb_especifique_documentacion_consigo option:selected").text();
        if(text == "Si")
            $("#especifique_documentacion_consigo").prop('disabled', false);
        else
            $("#especifique_documentacion_consigo").prop('disabled', true);

    });

    $("#btnCalcularEdad").click(function(){
        if(existeFecha($("#fecha_nacimiento").val()))
        {
            $("input:text[name=edad]").val(getAge($("#fecha_nacimiento").val()));
        }
//        if()
//        console.log(getAge()));
    });

    $("#btnMostrarHistorico").click(function(){

        $("#contenido-modal").html("Buscando");
        $("#confirmacion").modal(function(){show:true});
        var id_persona = $("#frmPersonaBuscada :input[name='id_persona']").val();
        var datos = "id_oficio="+$("#id").val()+"&id_persona="+id_persona;
        var destino = "{{ Route('solicitud_paradero.obtenerHistorico') }}";
        $.ajax({
            type: "Post",
            url: destino,
            data: datos,
            async: true,
            cache: false,
            success: function(respuesta){
                $("#contenido-modal").html(respuesta);
                $("#confirmacion").modal(function(){show:true});
            }

        })
    });

    $("input:text[name=nro_caso]").mask("9999999999");


    $(".eliminar_foto").click(function(){
        $("#div_"+this.id).addClass("hidden");
        var name = this.id.split("_");
        if($("input:hidden[name=hidden_fotos_eliminadas]").val() == "")
            $("input:hidden[name=hidden_fotos_eliminadas]").val(name[1]);
        else
            $("input:hidden[name=hidden_fotos_eliminadas]").val($("input:hidden[name=hidden_fotos_eliminadas]").val()+","+name[1]);


        console.log($("input:hidden[name=hidden_fotos_eliminadas]").val());
    });



});
</script>
@stop