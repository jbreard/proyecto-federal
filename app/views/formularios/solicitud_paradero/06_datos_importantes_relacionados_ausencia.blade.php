<div class="form-group">
    {{ Form::model($oficio->PersonaDatosRelacionadoAusencia,['method' => 'POST', 'id' => 'frmDatosRelacionadoAusencia'],['role' => 'form']) }}
        <div class="col-md-4">
            {{ Form::label('fecha_ultima_vez', 'Fecha en la que fue vista por última vez')}}
            {{ Form::text( 'fecha_ultima_vez',null,['class' => 'form-control fecha']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('hora_ultima_vez', 'Hora en la que fue vista por última vez') }}
            {{ Form::text( 'hora_ultima_vez',null,['class' => 'form-control time']) }}
        </div>
        <div class="col-md-12">
            {{ Form::label('lugar_ultima_vez', 'Lugar en la que fue vista por última vez') }}
            {{ Form::textarea( 'lugar_ultima_vez',null,['placeholder' => 'Indique con detalles', 'class' => 'form-control','style'=>'height:8em']) }}
        </div>
        <div class="col-md-12">
            {{ Form::label('descripcion_vestimenta', 'Descripción de la vestimenta y elementos que llevaba consigo') }}
            {{ Form::textarea( 'descripcion_vestimenta',null,['class' => 'form-control','style'=>'height:8em']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('cmb_detalle_drogas', '¿Padece adicción a los estupefacientes?') }}
            {{ Form::select( 'cmb_detalle_drogas',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_detalle_drogas']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('detalle_drogas', 'Detalle Drogas: ') }}
            {{ Form::textarea( 'detalle_drogas',null, ['class' => 'form-control','style'=>'height:8em','id' => 'detalle_drogas']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('adiccion_alcohol', '¿Padece adicción al alcohol?') }}
            {{ Form::select( 'adiccion_alcohol',$comboSiNo,null,['class' => 'form-control']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('cmb_tratamiento_institucion', '¿Realizó o realiza tratamiento en alguna institucion?') }}
            {{ Form::select( 'cmb_tratamiento_institucion',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_tratamiento_institucion']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('tratamiento_institucion', 'Indique en cual') }}
            {{ Form::text( 'tratamiento_institucion',null,['class' => 'form-control','id' => 'tratamiento_institucion']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('fecha_tratamiento_institucion', 'Fecha que estuvo en la institucion')}}
            {{ Form::text( 'fecha_tratamiento_institucion',null,['class' => 'form-control','id' => 'fecha_tratamiento_institucion']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('cmb_padecimiento_mental', '¿Padecimiento mental?') }}
            {{ Form::select( 'cmb_padecimiento_mental',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_padecimiento_mental']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('padecimiento_mental', 'Indique en cual') }}
            {{ Form::text( 'padecimiento_mental',null,['class' => 'form-control','id' => 'padecimiento_mental']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('cmb_padecimiento_institucion', '¿Realizó o realiza tratamiento en alguna institucion?') }}
            {{ Form::select( 'cmb_padecimiento_institucion',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_padecimiento_institucion']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('padecimiento_institucion', 'Indique en cual') }}
            {{ Form::text( 'padecimiento_institucion',null,['class' => 'form-control','id' => 'padecimiento_institucion']) }}
        </div>
        <div class="col-md-2">
            {{ Form::label('fecha_tratamiento_padecimiento_mental', 'Fecha')}}
            {{ Form::text( 'fecha_tratamiento_padecimiento_mental',null,['class' => 'form-control','id' => 'fecha_padecimiento']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('id_institucion_momento_ausencia', '¿Se encontraba en el momento de la ausencia en alguna institucion?') }}
            {{ Form::select( 'id_institucion_momento_ausencia',$comboSiNo,null,['class' => 'form-control','id' => 'id_institucion_momento_ausencia']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('institucion_momento_ausencia', 'Indique en cual') }}
            {{ Form::text( 'institucion_momento_ausencia',null,['class' => 'form-control','id' => 'institucion_momento_ausencia']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('internacion_estadia_voluntaria', 'Acerca de la internación') }}
            {{ Form::select( 'internacion_estadia_voluntaria',['' => 'S/D','V' => 'Voluntaria','C' => 'Compulsiva'],null,['class' => 'form-control','id' => 'internacion_estadia_voluntaria']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('autoridad_ordeno_medida', 'Autoridad que ordenó la medida en su caso') }}
            {{ Form::text( 'autoridad_ordeno_medida',null,['class' => 'form-control']) }}
        </div>
        <div class="col-md-12" style="margin-top: 20px; margin-bottom: 40px;">
            <fieldset>
                <legend>Dirección de la Institución en la que se encontraba</legend>
                    <div class="col-md-3">
                        {{ Form::label('direccion', 'Dirección') }}
                        {{ Form::text( 'direccion',null,['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('nro', 'Número') }}
                        {{ Form::text( 'nro',null,['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('piso', 'Piso') }}
                        {{ Form::text( 'piso',null,['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('dpto', 'Departamento') }}
                        {{ Form::text( 'dpto',null,['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('codigo_postal', 'Codigo Postal') }}
                        {{ Form::text( 'codigo_postal',null,['class' => 'form-control','maxlength' => 10]) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('id_provincia', 'Provincia') }}
                        {{ Form::select( 'id_provincia',$comboProvincia,null,['class' => 'form-control provincia','data-target'=>'.partido_laboral']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('id_partido', 'Partido') }}
                        {{ Form::select( 'id_partido',[],null,['class' => 'form-control partido partido_laboral']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('localidad', 'Barrio - Localidad') }}
                        {{ Form::text( 'localidad',null,['class' => 'form-control localidad_laboral']) }}
                    </div>
            </fieldset>
        </div>

        <div class="col-md-6">
            {{ Form::label('situacion_calle', '¿Se encontraba en situacion de calle?')}}
            {{ Form::select( 'situacion_calle',$comboSiNo,null,['class' => 'form-control']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('lugares_frecuentaba', 'Lugares que la persona solia frecuentar') }}
            {{ Form::textarea( 'lugares_frecuentaba',null,['class' => 'form-control','style'=>'height:8em']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('cmb_especifique_enfermedad', '¿Posee alguna enfermedad?') }}
            {{ Form::select( 'cmb_especifique_enfermedad',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_especifique_enfermedad']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('especifique_enfermedad', 'Especifique') }}
            {{ Form::textarea( 'especifique_enfermedad',null,['class' => 'form-control','style'=>'height:8em', 'id' => 'especifique_enfermedad']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('cmb_especifique_tratamiento_medico', '¿Realiza algun tratamiento medico?') }}
            {{ Form::select( 'cmb_especifique_tratamiento_medico',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_especifique_tratamiento_medico']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('especifique_tratamiento_medico', 'Especifique') }}
            {{ Form::textarea( 'especifique_tratamiento_medico',null,['class' => 'form-control','style'=>'height:8em', 'id' => 'especifique_tratamiento_medico']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('cmb_especifique_alguna_medicacion', '¿Necesita de alguna medicacion?') }}
            {{ Form::select( 'cmb_especifique_alguna_medicacion',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_especifique_alguna_medicacion']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('especifique_alguna_medicacion', 'Especifique') }}
            {{ Form::textarea( 'especifique_alguna_medicacion',null,['class' => 'form-control','style'=>'height:8em', 'id' => 'especifique_alguna_medicacion']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('cmb_especifique_situacion_conflicto', '¿Se encontraba en situacion de conflicto en el ultimo tiempo?')}}
            {{ Form::select( 'cmb_especifique_situacion_conflicto',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_especifique_situacion_conflicto']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('especifique_situacion_conflicto', 'Especifique') }}
            {{ Form::textarea( 'especifique_situacion_conflicto',null,['placeholder' => 'Familiar, escolar, etc', 'class' => 'form-control','style'=>'height:8em', 'id' => 'especifique_situacion_conflicto']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('sustraccion_parental', '¿Se trata de una sustraccion parental?')}}
            {{ Form::select( 'sustraccion_parental',$comboSiNo,null,['class' => 'form-control']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('primera_vez_ausenta', '¿Es la primera vez que se ausenta?')}}
            {{ Form::select( 'primera_vez_ausenta',$comboSiNo,null,['class' => 'form-control']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('cmb_especifique_documentacion_consigo', '¿Llevaba documentacion consigo?')}}
            {{ Form::select( 'cmb_especifique_documentacion_consigo',$comboSiNo,null,['class' => 'form-control','id' => 'cmb_especifique_documentacion_consigo']) }}
        </div>
        <div class="col-md-6">
            {{ Form::label('especifique_documentacion_consigo', 'Especifique') }}
            {{ Form::textarea( 'especifique_documentacion_consigo',null,['class' => 'form-control','style'=>'height:8em', 'id' => 'especifique_documentacion_consigo']) }}
        </div>
        <div class="col-md-12">
            {{ Form::label('compania_alguien', '¿Podria encontrarse en compañía de alguien? Indique quien y aporte datos de identificación') }}
            {{ Form::textarea( 'compania_alguien',null,['class' => 'form-control','style'=>'height:8em']) }}
        </div>
        <div>
            <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosRelacionadoAusencia" style="display: block">Guardar datos relacionados con la ausencia</button>
        </div>
    {{ Form::close() }}
</div>