{{ Form::model($oficio->PersonaUltimoDomicilio,['method' => 'POST', 'id' => 'frmUltimoDomicilio'], array('role' => 'form')) }}
<div class="form-group">
    <div class="col-md-3">
        {{ Form::label('direccion', 'Direccion')}}
        {{ Form::text( 'direccion', null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('nro', 'N°') }}
        {{ Form::text('nro',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('piso', 'Piso') }}
        {{ Form::text( 'piso',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('dpto', 'Departamento') }}
        {{ Form::text( 'dpto',null,['class' => 'form-control','maxlength' => 5]) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('interseccion', 'Interseccion') }}
        {{ Form::text( 'interseccion',null, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-3">
        {{ Form::label('codigo_postal', 'Codigo Postal') }}
        {{ Form::text( 'codigo_postal',null,['class' => 'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_provincia', 'Provincia') }}
        {{ Form::select('id_provincia',$comboProvincia,null,['class' => 'form-control provincia','data-target'=>'.partido_desaparecido']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_partido', 'Partido') }}
        {{ Form::select( 'id_partido',[],null, ['class' => 'form-control partido partido_desaparecido']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('localidad', 'Localidad') }}
        {{ Form::text( 'localidad',null,['class' => 'form-control localidad']) }}
    </div>
    <div class="col-md-12">
        {{ Form::label('observaciones', 'Observaciones') }}
        {{ Form::textarea('observaciones',null,['class' => 'form-control']) }}
    </div>
</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosUltimoDomicilio" style="display: block">Guardar datos último domicilio</button>
</div>
{{ Form::close() }}