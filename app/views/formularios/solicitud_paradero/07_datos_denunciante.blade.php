{{ Form::model($oficio->PersonaDatosDenunciante,['method' => 'POST', 'id' => 'frmDatosDenunciante','enctype' => 'multipart/form-data', 'files'=>true],['role' => 'form']) }}
<input type="hidden" id="hidden_telefonos_denunciante" name="hidden_telefonos_denunciante"/>
<div class="form-group">
    {{ Form::hidden('id_persona_denunciante',null) }}
    <div class="col-md-2">
        {{ Form::label('apellido', 'Apellido')}}
        {{ Form::text( 'apellido',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('nombre', 'Nombre')}}
        {{ Form::text( 'nombre',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('id_tipo_documento', 'Tipo Documento')}}
        {{ Form::select( 'id_tipo_documento',$comboTipoDocumento,null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('nro_documento', 'N° de Documento')}}
        {{ Form::text( 'nro_documento',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-4">
        {{ Form::label('vinculo', 'Vinculo con la persona buscada')}}
        {{ Form::select( 'vinculo',$comboVinculos,null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('direccion', 'Dirección')}}
        {{ Form::text( 'direccion',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-1">
        {{ Form::label('nro', 'N°')}}
        {{ Form::text( 'nro',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-1">
        {{ Form::label('piso', 'Piso')}}
        {{ Form::text( 'piso',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-1">
        {{ Form::label('dpto', 'Departamento')}}
        {{ Form::text( 'dpto',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('codigo_postal', 'Codigo Postal')}}
        {{ Form::text( 'codigo_postal',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('id_provincia', 'Provincia')}}
        {{ Form::select( 'id_provincia',$comboProvincia,null,['class'=>'form-control provincia','data-target'=>'.partido_denunciante']) }}
    </div>
    <div class="col-md-2">
        {{ Form::label('id_partido', 'Partido')}}
        {{ Form::select( 'id_partido',[],null,['class'=>'form-control partido partido_denunciante']) }}
    </div>
    <div class="row">
        <div class="col-md-2">
            {{ Form::label('cod_area_telefono','Cod.Area') }}
            {{ Form::text( 'cod_area_telefono',null,['placeholder' => '011', 'class' => 'form-control']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('telefono', 'Telefono fijo') }}
            {{ Form::text( 'telefono',null,['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            {{ Form::label('cod_area_celular','Cod.Area') }}
            {{ Form::text( 'cod_area_celular',null,['placeholder' => '011', 'class' => 'form-control','id' => 'cod_area_celular_denunciante']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('celular_denunciante', 'N° de Celular') }}
            {{ Form::text( 'celular_denunciante',null,['class' => 'form-control','id' => 'celular_denunciante']) }}
        </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-success btn-sm" id="btnAgregarCelularDenunciante" style="margin-top: 23px">Agregar</button>
        </div>
    </div>

    <div class="col-md-12 margin-top" id="div_celulares_denunciante"></div>

    <div class="col-md-3">
        {{ Form::label('dependencia', 'Dependencia donde hizo la Denuncia: ')}}
        {{ Form::text( 'dependencia',null,['class'=>'form-control']) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('fecha_denuncia', 'Fecha') }}
        {{ Form::text( 'fecha_denuncia',null,['class' => 'form-control fecha']) }}
    </div>
    <div class="col-md-7">
        {{ Form::label('relato', 'Relato de los Hechos') }}
        {{ Form::textarea( 'relato',null,['class' => 'form-control','style'=>'height:8em']) }}
    </div>

    <div class="col-md-5 form-group">
        {{ Form::label('nota_adjunta', 'O puede obviarse adjuntando una nota') }}
        <input type="file" id="nota_adjunta" name="nota_adjunta">
        @if(is_object($oficio->PersonaDatosDenunciante))
            @if($oficio->PersonaDatosDenunciante->nota_adjunta != "")
                <a href="{{ Route('archivos.descargar')}}?q={{ $oficio->PersonaDatosDenunciante->nota_adjunta }}" target="_blank" >Descargar</a>
<!--                <a href="{{ Route('solicitud_paradero.mostrar_archivo',["archivos/".$oficio->id."/denuncias",$oficio->PersonaDatosDenunciante->nota_adjunta]) }}" target="_blank">Descargar</a>-->
            @endif
        @endif

    </div>
    <div class="col-md-12">
        {{ Form::label('intervencion_otros_organismos', '¿Se dio intervención a otros organismos o divisiones? Indique cual')}}
        {{ Form::text( 'intervencion_otros_organismos',null,['class'=>'form-control']) }}
    </div>


</div>
<div>
    <button type="submit" class="btn btn-primary btn-sm pull-right margin-top" id="btnGuardarDatosDenunciante" style="display: block">Guardar datos del denunciante</button>
</div>
{{ Form::close() }}