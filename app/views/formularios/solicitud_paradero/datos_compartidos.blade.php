<div class="col-md-3">
    {{ Form::label('apellido', 'Apellido') }}
    {{ Form::text( 'apellido',null, ['class' => 'form-control apellido','id'=>'apellido','maxlength' => 50]) }}
</div>
<div class="col-md-3">
    {{ Form::label('nombres', 'Nombres') }}
    {{ Form::text( 'nombres',null, ['class' => 'form-control nombre','id'=>'nombre','maxlength' => 50]) }}

</div>
<div class="col-md-2">
    {{ Form::label('fecha_nacimiento', 'Fecha Nacimiento') }}
    {{ Form::text( 'fecha_nacimiento',null, ['class' => 'form-control fecha','id' => 'fecha_nacimiento']) }}
</div>
<div class="col-md-1">
    <button type="button" class="btn btn-success btn-sm" id="btnCalcularEdad" style="margin-top: 23px">Calcular</button>
</div>
<div class="col-md-3">
    {{ Form::label('edad', 'Edad') }}
    {{ Form::text( 'edad',null, ['class' => 'form-control']) }}
</div>
