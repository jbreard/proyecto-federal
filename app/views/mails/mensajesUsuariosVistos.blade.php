<table class="table table-responsive">
    <thead>
        <th>Usuario</th>
        <th>Organismo</th>
        <th>Visto el</th>
    </thead>
    <tbody>
    @foreach($mensajesVistos as $mensajeVisto)
        <tr>
            <td>{{$mensajeVisto->usuario->nombre}} {{$mensajeVisto->usuario->apellido}}</td>
            <td>{{$mensajeVisto->usuario->getJurisdiccion->descripcion}}</td>
            <td>{{$mensajeVisto->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>