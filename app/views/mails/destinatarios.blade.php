<form method="post" id="formulario">
    <div class="row">
        <div class="col-md-5">
            <select name="origen[]" id="origen" multiple="multiple" size="8" class="form-control">
                @foreach($mailsDirecciones as $mail)
                    <option value="{{$mail->id}}">{{$mail->descripcion}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <div class="row" id="controloes">
                <input type="button" class="pasar izq btn btn-primary "  value="Pasar »">
                <input type="button" class="quitar der btn btn-primary"  value="« Quitar">
                <input type="button" class="pasartodos izq btn btn-primary" value="Todos »">
                <input type="button" class="quitartodos der btn btn-primary" value="« Todos">
            </div>
        </div>
        <div class="col-md-5">
            <select  class="form-control"  name="destino[]" id="destino" multiple="multiple" size="8"></select>
        </div>
    </div>
</form>