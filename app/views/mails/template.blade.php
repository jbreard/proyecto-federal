@extends('header')
{{ HTML::style('assets/css/jquery.tagsinput.css', array('media' => 'screen')) }}
{{ HTML::style('assets/css/jquery.cleditor.css', array('media' => 'screen')) }}
{{ HTML::style('assets/css/jquery.dataTables.css', array('media' => 'screen')) }}
{{ HTML::style('assets/datetimepicker/css/bootstrap-datetimepicker.min.css', array('media' => 'screen')) }}
@section('scripts')
   {{ HTML::script('assets/js/dragdrop.js') }}
   {{ HTML::script('assets/js/jquery.tagsinput.min.js') }}
   {{ HTML::script('assets/js/jquery.cleditor.js') }}
   {{ HTML::script('assets/js/jquery.dataTables.min.js') }}
   {{ HTML::script('assets/js/helper.js') }}
   {{ HTML::script('assets/js/jquery.jscroll.js') }}
   {{ HTML::script('assets/js/jquery.formulario.js') }}
   {{ HTML::script('assets/js/jquery.blockUI.js') }}
   {{ HTML::script('assets/moment/moment.js') }}
   {{ HTML::script('assets/moment/locale/es.js') }}
   {{ HTML::script('assets/datetimepicker/js/bootstrap-datetimepicker.min.js') }}


   <script>
   jQuery.validator.addMethod("checkTags", function(value) { //add custom method
           return ($("#mails_tagsinput").find(".tag").length > 0);
   },"Por Favor Seleccione un Destinatario");

    function confirmar(result, el, utils, ruta, id){
        if(result){
                    el.attr("disabled","disabled");
                    utils.ajax(null,'get',ruta,{},function(data){

                        if(data.success){
                            var blade = $("#"+id+"NoLeido");
                            blade.text('Leido');
                            blade.removeClass('label-danger');
                            blade.addClass('label-success');
                            }
                    },true)
                }else{
                    el.attr('checked', false);
        }
}
   $(function(){
        var page = 0;
       $(".clear_mails").on("click", function () {
           var el  = $(this);
           var $exampleMulti = getSelect(el);
           $exampleMulti.val(null).trigger("change");
       });
       function getSelect(el) {
           var id = el.data("id-listado");
           return  $(id);
      }

       $(".set_mails").on("click", function () {
           var el  = $(this);
           var $exampleMulti = getSelect(el);
           var options = $exampleMulti.children('option');
           var array = [];
           options.each(function()
           {
              array.push($(this).val());
           });
           console.log(array);
           $exampleMulti.val(array).trigger("change");
       });

        $(".grupos_correos").on("click", function (e) {
           e.preventDefault();
           var el  = $(this);
           var $exampleMulti = getSelect(el);
           
           var options = $exampleMulti.children('option');
           var idGrupo = el.attr("id-grupo");
           var array = [];
           var options = $exampleMulti.children('option');
           var array = $exampleMulti.select2("val");
           $.ajax({
              url:'/grupos_correos/gruposMiembros/'+idGrupo,
              success:function(data){
                $.each(data,function(index,value){
                    array.push(value.id_organismo);
                });
                $exampleMulti.val(array).trigger("change");     
              }
          });
          
       });


    var i = 1;
    var utils = new Helper();


   $("#showFilters").click(function(){
       return $("#modalFilters").modal();
   });

   $('.dateTime').datetimepicker({
       locale: 'es',
       format:'DD/MM/YYYY HH:mm:ss'
   });

    utils.modalClose("#myModal",function(){
        $('#mails').importTags();
    });



        $("#mensajes_leidos").change(function(){
            var valor = $(this).val();
            var url   = utils.basePath+"mensajes?leidos="+valor;
            window.location.href=url;

        });

        $("#form_mail").form({
            beforeSend:function(){
                $.blockUI({ message: 'Enviando...',baseZ: 2000 });
            },
            done:function(data){
                if(data.success){
                    $.unblockUI();
                    $("#message").html("<p style='color:red'>Mensaje Enviado</p>");
                    $("#form_mail").resetear();
                    $exampleMulti = $("#listadoMails");
                    $exampleMulti.val(null).trigger("change");
                    $('#mails').importTags();
                }
            }
        });




        $(".marcar_mail_leido").click(function(){
            var ruta = $(this).val();
            var id   = $(this).attr("data-id");
            var el   = $(this);

            bootbox.confirm({
                title: 'Atención!',
                message: 'Esta seguro de Marcar como Leido?',
                buttons: {
                    'cancel': {
                        label: 'No',
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: 'Si',
                        className: 'btn-danger'
                    }
                },
                callback: function(result) {
                    confirmar(result,el,utils,ruta,id);
                }
            });

        });


       $(".tagMails").select2();

       $("body").on('click','.info_mail',function(event){
           event.preventDefault();
           $("#mailsInfoBody").html("");
           var url = $(this).attr("href");
           utils.ajax(null,'get',url,{},function(html){
               $("#mailsInfoBody").html(html);
               $("#modalInfoMails").modal();

           });

       });



        $("#form_mail").validate(
            {rules: {
                dummy:"checkTags"
            }

        });


       $("#btnFilters").click(function(e){
           e.preventDefault();
           var form = $("#formFilter");
           var datoFrm = form.serialize();

           var data = {
               data:datoFrm,
               leido:$("#mensajes_leidos").val()
           };

           var url  = $("#url").val();
           var method = "post";
           $.ajax({
               url:url,
               data:datoFrm,
               type:method,
               success:function(data){
                   $("#ulRecibidos").html(data);


               }
           });
       });


       $("#btnEnviar").click(function(){
           if($("#form_mail").valid()) return $("#form_mail").submit();
       });


        $(document).scroll(function () {
            var winHeight = $(window).height();
            var docHeight = $(document).height();
            var scrollTop = $(window).scrollTop();
            var sideBar   = $("#menuLateral").height()+50;
            var diff =  docHeight-sideBar;
            var url  = $("#url").val();
            var leido = $("#mensajes_leidos").val();
            var q     = $("[name='q']").val() || null;
            var form = $("#formFilter");
            var dataFilter = form.serializeArray();




            //url  = url+'&leidos='+leido+'&q='+q;
//            console.log(scrollTop + "  docHeight: "+docHeight+" sideBar: "+sideBar+" diff "+ diff+" menuLaterl "+ menuLateral);
            $("#loadingGif").hide();

            if(scrollTop == diff)
            {
                i++;
                console.log(i);

                console.log(dataFilter);
                $("#loadingGif").show();

                $.ajax({
                    url:url+'&page='+i,
                    data:dataFilter,
                    type:'post',
                    success:function(html){
                        $("#loadingGif").hide();
                        $("#ulRecibidos").append(html);
                    }
                });


            }

        });

    });
   </script>

@stop
<style>
    #controloes > input {
        margin-top:5px;
    }
    #dummy{
        visibility: hidden!Important;
        height:1px;
        width:1px;
    }
    #padding{
        display: none;
    }
    #buscar:hover{
        cursor:pointer;
    }
</style>

@section('contentMailsHeader')


<div class="row row-merge">

    <div class="col-sm-2 bg-primary custom-grid menu-grid">
    <button type="button" class="navbar-toggle" id="inboxMenuToggle">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
					<div class="menu-header">
						Mensajes
						<a class="btn btn-sm pull-right" href="#newFolder" data-toggle="modal"><i class="fa fa-envelope fa-lg"></i></a>
					</div>
					<ul class="inbox-menu" id="inboxMenu">
						<li class="active">
							<a href="{{route('mensajes.index')}}">
								<i class="fa fa-inbox fa-lg"></i>
								<span class="m-left-xs">Recibidos</span>
							</a>
						</li>
						<li>
							<a href="{{route('mensajes.enviados')}}">
								<i class="fa fa-envelope fa-lg"></i>
								<span class="m-left-xs">Enviados</span>

							</a>
						</li>
						<li>
							<a  data-toggle="modal" data-target="#myModal">
								<i class="fa fa-pencil fa-lg"></i><span class="m-left-xs">Escribir Nuevo</span>
							</a>
						</li>


					</ul>
				</div><!-- /.col -->
				@include('errores')
                <div class="col-sm-9">
                    @yield('content_mail')

			    </div>
@stop

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Enviar Mensaje</h4><span id="message"></span>
      </div>
      <div class="modal-body">
        @include('mails.formulario',["labelDestino"=>"Destinatarios"]);
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnEnviar" >Enviar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDestinatarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Destinatarios</h4>
          </div>
          <div class="modal-body">
               @include('mails.destinatarios')
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="aceptarDestinos">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modalInfoMails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Reporte de Lectura</h4>
      </div>
      <div class="modal-body" id="mailsInfoBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>

      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="modalFilters" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Filtros <i class="fa fa-filter"></i> </h4>
            </div>
            <div class="modal-body">
                <form id="formFilter" action="{{route('mensajes.busqueda')}}" method="post">
                <div class="form-group">
                    <div class="row">
                    <div class="col-md-5">
                        {{ Form::label('mails', 'Fecha desde') }}
                        <input class="dateTime form-control"  name="data[fecha][desde]" id="fecha['>=']">
                    </div>
                    <div class="col-md-5">
                        {{ Form::label('mails', 'Fecha hasta') }}
                        <input class="dateTime form-control" name="data[fecha][hasta]" id="fecha['<=']">
                    </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        @include('mails/select_mails')

                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            {{ Form::label('asunto', 'Asunto') }}
                            {{ Form::text('data[asunto]', null, array('class' => 'form-control','required'=>'required')) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            {{ Form::label('contenido', 'Contenido') }}
                            {{ Form::textarea('data[contenido]', null, array('class' => 'form-control','required'=>'required')) }}
                        </div>
                    </div>
                </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnFilters">Buscar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>


@if(isset($tagLeidos)and $tagLeidos)
    <input id="url" type="hidden" value="{{ route('mensajes.cargar',['recibidos'=>true]) }}">
@else
    <input id="url" type="hidden" value="{{ route('mensajes.cargar',['recibidos'=>false]) }}">
@endif
