<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <strong>Asunto: {{$mail->asunto}} <span class="pull-right">Fecha: {{$mail->created_at}}</span></strong>
            </div></div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                Remitente:{{$mail->emisor->getJurisdiccion->descripcion}} {{$mail->emisor->nombre}} {{$mail->emisor->apellido}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                Destinatarios:
                @foreach($mail->destinatarios as $destinatario)
                    @if(isset($destinatario->getOrganismo->descripcion))
                        {{ $destinatario->getOrganismo->descripcion }}
                    @endif
                @endforeach
                </div>
        </div>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                {{$mail->contenido}}
            </div>
        </div>
        @if(count($mail->archivos)>0)
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <p>Archivos Adjuntos</p>

                @foreach($mail->archivos as $adjunto)
                    <ul><li><a href="{{ Route('archivos.descargar')}}?q={{ $adjunto->path }}" target="__blank">Descargar</a></li></ul>
                @endforeach
            </div>
        </div>
        @endif

        @if(count($mail->respuestas)>0)
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11">
                    <hr>
                    <h4>Respuestas:</h4>
                </div>
            </div>
            @foreach($mail->respuestas as $respuesta)
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11">
                    <b><div class="pull-right">Fecha: {{$respuesta->created_at}}</div>
                    <div class="pull-left">Usuario: {{$respuesta->usuario->nombre}} {{$respuesta->usuario->apellido}} {{$respuesta->usuario->jurisdiccion}} </div>
                    </b>
                </div></div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11">
                    {{$respuesta->respuesta}}
                        <hr>
                </div></div>

            @endforeach
        @endif




    </div>
</div>