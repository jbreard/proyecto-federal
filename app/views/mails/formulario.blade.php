{{ Form::open($form_data, array('role' => 'form')) }}
    <div class="form-group">
        <div class="row">
            @include('mails/select_mails',["id"=>"listadoMails","nameInput"=>"mails[]"])
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-10">
            {{ Form::label('asunto', 'Asunto') }}
            {{ Form::text('asunto', null, array('class' => 'form-control','required'=>'required')) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-10">
                {{ Form::label('contenido', 'Contenido') }}
                {{ Form::textarea('contenido', null, array('class' => 'form-control','required'=>'required')) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-10">
                {{ Form::label('adjuntos', 'Adjuntos') }}
                {{ Form::file('adjuntos[]', $attributes = array("class"=>"files","multiple"=>true)) }}
            </div>
        </div>
    </div>
{{Form::close()}}
<table id="destino_files">
    <tbody></tbody>
</table>



