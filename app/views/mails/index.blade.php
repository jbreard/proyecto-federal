@extends('mails.template')
@section('content_mail')
<div class="col-sm-12">

    <div class="panel panel-default inbox-panel">
       <div class="panel-heading">
        <!---<div class="input-group">
          <input type="text" class="form-control input-sm" placeholder="Buscar...">
            <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div><!-- /input-group -->
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <div class="pull-left">
                    <a  data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-danger">
                        <i class="fa fa-plus"></i> Escribir Nuevo
                    </a>
                </div>
            </div>
            <div class="col-md-7">
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" id="exampleInputAmount" placeholder="Buscar..." value="{{isset($q)?$q:null}}" id="q" name="q">--}}
                    {{--<div class="input-group-addon" id="buscar"><i class="fa fa-search"></i> </div>--}}
                {{--</div>--}}
                <button id="showFilters" class="btn btn-info btn-sm">Filtros Mensajes <i class="fa fa-filter"></i></button>
            </div>
            @if($opcionesMensajes)
                <div class="pull-right col-md-2">
                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" id="actualizar"><i class="fa fa-refresh"></i></a>
                    <div class="btn-group" id="inboxFilter">
                        {{Form::select("leido",$opcionesMensajes,$leidos,["id"=>"mensajes_leidos","class"=>"form-control"])}}
                    </div>
                </div>
            @endif
        </div>
    </div>




     <div id="scroll">
        <ul class="list-group" id="ulRecibidos">
            @include('mails.listadoMails')
        </ul>
        <div id="loadingGif" style="display:none"><center>Cargando....</center></div>
    </div>
    </div><!-- /panel -->
</div>


@stop