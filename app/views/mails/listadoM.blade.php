@extends ('header')
@section ('content')
<style>
    .row {
        margin-top: 5px !important;
    }
</style>
@section ('content')
<h2>Listados de Mails</h2>
<a href="{{ route('mails.create') }}"><button class="btn btn-info " >Cargar Mail <i class="fa fa-envelope"></i></button></a>
<table class="table table-responsive table-hover">
    <thead>
    <tr>
        <th>Direccion</th>
        <th>Organismos</th>
        <th>Descripcion</th>
        <th>Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($correos as $key => $value)
    <tr>
        <td>{{ $value->mail }}</td>
        <td>{{ $value->getOrganismo->descripcion }}</td>
        <td>{{ $value->descripcion }}</td>
        <!-- we will also add show, edit, and delete buttons -->
        <td>
            <a class="btn btn-small btn-info"   href="{{ route('mails.show',$value->id) }}">Editar</a>

            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
            <!-- we will add this later since its a little more complicated than the other two buttons -->

            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
            <!-- <a class="btn btn-small btn-info" href="{{ route('usuarios.show',$value->id) }}">Editar</a>  -->
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
{{ $correos->links() }}
@stop
