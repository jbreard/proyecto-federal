<div class="col-md-10" id="mails_destinatarios">
    <span>{{ Form::label('mails', $labelDestino) }}</span>
    <a href="#" class="clear_mails" data-id-listado="#{{$id or 'mailsFilters'}}">Limpiar</a>
    <a href="#" class="set_mails" data-id-listado="#{{$id or 'mailsFilters'}}">Todos</a>


<div class="dropdown pull-right">
  <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    Grupos
    <span class="caret"></span>
  </a>

  <ul class="dropdown-menu" aria-labelledby="dLabel">
    @foreach($grupos as $grupo)
  	<li>
  		<a class="grupos_correos" data-id-listado="#{{$id or 'mailsFilters'}}" id-grupo="{{$grupo->id}}" href="#">{{$grupo->descripcion}}</a>
  	</li>
    @endforeach
  </ul>
</div>


    <select class="form-control tagMails "   id="{{$id or 'mailsFilters'}}" name="{{$nameInput or 'data[mailsFilters][]' }}" multiple>
        @foreach($mailsDirecciones as $correos)
            <option value="{{$correos->id}}">{{$correos->descripcion}} {{$correos->mail}}</option>
        @endforeach
    </select>
</div>
