@extends ('header')
@section ('content')
<style>
input{
    text-transform: none !important; 
}
</style>
{{ Form::model(isset($correo) ? $correo : null ,$form_data, array('role' => 'form')) }}

<h1>{{ $accion }} Mail</h1>

<div class="row">
    <div class="form-group col-md-4">
        {{ Form::label('mail', 'Mail') }}
        {{ Form::email('mail', null, array('placeholder' => 'Ingrese la Direccion De Correo', 'class' => 'form-control')) }}
        {{ $errors->first('mail') }}
    </div>

    <div class="form-group col-md-4">
        {{ Form::label('descripcion', 'Descripcion') }}
        {{ Form::text('descripcion', null, array('placeholder' => 'Ingrese Descripcion', 'class' => 'form-control')) }}
        {{ $errors->first('descripcion') }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('id_jurisdiccion', 'Organismos') }}
        {{ Form::select('id_jurisdiccion',$combosOrganismos ,null, array('placeholder' => 'Ingrese Descripcion', 'class' => 'form-control')) }}
        {{ $errors->first('descripcion') }}
    </div>
</div>
<button class="btn btn-primary">Guardar »</button>
{{ Form::close() }}
@stop
