@extends('mails.template')
@section('content_mail')
@include('mails.respuestas_pdf')

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-lg-11" >
        <h4>Responder:</h4>
        {{ Form::open($form_data) }}

        <div class="col-lg-10">{{Form::textarea('respuesta'.null,null,["class"=>"form-control","required"=>"required"])}}</div>
        <div class="col-lg-10">{{Form::hidden('id_mensaje_respuesta',$mail->id,["class"=>"form-control","type"=>"hidden"])}}</div>
        <div class="col-lg-10"><button type=""  id="enviarRespuesta" class="btn btn-primary pull-left" style="margin-top:5px">Enviar</button> </div>
        {{ Form::close() }}
    </div>
</div>

@stop
