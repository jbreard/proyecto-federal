@include('include_head')
<style>
    #contenido{
        margin-top:25px;
    }
</style>
<div id="top-nav" class="skin-6">
    <!-- logo -->
    <figure>
        <img src="{{ URL::asset('bootstrap/images/logo-minseg.png')}}" id="logominseg">
    </figure>

    <div class="brand">
        <span>
            {{Config::get('nombre.siglas')}}
            {{Config::get('nombre.nombre')}}
        </span>
    </div>

</div>

<div id="contenido" class="main-container">
    <div class="col-md-2">
        Remitente:{{$remitente->nombre}} {{$remitente->apellido}}, Jurisdicción:{{$remitente->getJurisdiccion->descripcion}}.
        <br>

    </div>
    <div class="col-md-8">
        {{$contenido}}
    </div>
    <div class="col-md-2"></div>
    <br>
    <div class="col-md-8">
        Mensaje anterior:<br>
        {{$mensaje_anterior}}
    </div>


</div>

