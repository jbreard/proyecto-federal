@foreach($mails as $mail)
    <a href="{{route('mensajes.show',$mail->idM)}}" data-id="{{$mail->idM}}">
        <li class="list-group-item clearfix inbox-item

            @if(isset($mail->leido) and ($mail->leido))
                list-group-item-success
            @else
                list-group-item-danger
             @endif

        ">
        @if($tagLeidos)

            <label class="label-checkbox inline">

                <input type="checkbox" class="chk-item marcar_mail_leido" value="{{route('mensajes.marcar.leido',$mail->idM)}}" data-id="{{$mail->idM}}" data-bb="confirm"
                @if($mail->leido)
                    disabled
                @endif
                >
                <span class="custom-checkbox"></span>
            </label>
        @endif

            <span class="from">{{$mail->organismo}} {{$mail->emisor_nombre}} {{$mail->emisor_apellido}}</span>
            <span class="detail">
               {{$mail->asunto}}
            </span>
            <span class="inline-block pull-right">
                @if(isset($mail->adjunto) and ($mail->adjunto))
                <a href="#" ><span class="attachment"><i class="fa fa-paperclip fa-lg"></i></span></a>
                @endif
                <a href="{{route('mensajes.vistos',$mail->idM)}}" class="info_mail" ><span class="attachment"><i class="fa fa-info-circle fa-lg"></i></span></a>
                <a href="{{route('mensajes.pdf',$mail->idM)}}"><span class="attachment"><i class="fa fa-print fa-lg"></i></span></a>
                <a href=""><span class="">{{ Time::FormatearHoraFecha($mail->created_at)}}</span></a>
                @if($tagLeidos)
                    <input id="url" type="hidden" value="{{ route('mensajes.cargar',['recibidos'=>true]) }}">
                @endif

            </span>
        </li>
    </a>
@endforeach

