@if ($errors->any())
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Por favor corrige los siguentes errores:</strong>
    <ul style="list-style: none">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (Session::has('mensa_error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Por favor corrige los siguentes errores:</strong>
    <ul style="list-style: none">
            <li>{{ Session::get('mensa_error') }}</li>
    </ul>
</div>
@endif
@if (Session::has('mensa_ok'))
<div class="alert alert-success col-md-10" style="margin-top:10px;text-align:center">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{Session::get('mensa_ok')}}</strong>

</div>
@endif