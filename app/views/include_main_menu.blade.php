<!-- main-menu
================================================== -->
<aside class="fixed skin-6" id="menuLateral">

    <div class="sidebar-inner scrollable-sidebar">
        <div class="size-toggle">
            <a class="btn btn-sm" id="sizeToggle">
                <span class="icon-bar"></span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> </a>
            <a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm"> <i class="fa fa-power-off fa-lg"></i> </a>
        </div>
        <!-- /size-toggle -->
        <!-- user-block -->
        <!-- <div class="user-block clearfix"> <img< src="img/user.jpg" alt="User Avatar">
          <div class="detail">
          <strong>Usuario</strong>
          </div>
        </div> -->
        <!-- /user-block -->
        <div class="main-menu">
            <ul>
                <!--<li class="active openable open">-->
                <li class="openable open">
                    <a href="#"> <span class="menu-icon"> <i class="fa fa-user"></i></span>

                        <span class="text"> {{ Auth::user()->nombre }} {{ Auth::user()->apellido }} </span> <span class="menu-hover"></span> </a>
                    <ul class="submenu">
                        <li><a href="{{  route('usuarios.clave') }}"><span class="submenu-label">Modificar contraseña</span></a></li>
                        <li><a target="_blank" href="{{URL::to('documentos/resolucion.pdf')}}"><span class="submenu-label">Reglamento SIFCOP</span></a></li>
                        <li><a target="_blank" href="{{URL::to('documentos/Instructivo-de-Uso-10DIC2020IF-2020-85892078-APN-CBPYACPPC_MSG.pdf')}}"><span class="submenu-label">Instructivo de uso</span></a></li>
                        <li><a target="_blank" href="{{URL::to('documentos/circular.pdf')}}"><span class="submenu-label">Criterios para la carga</span></a></li>
                        <li><a target="_blank" href="{{URL::to('documentos/terminos_y_condiciones.pdf')}}"><span class="submenu-label">Términos y Condiciones</span></a></li>
                        <li><a  href="{{route('miembros.index')}}"><span class="submenu-label">Agenda de Miembros</span></a></li>
                    </ul>
                </li>
                <li class=" ">
                    <a href="{{ route('inicio') }}"> <span class="menu-icon"> <i class="fa fa-home "></i></span>
                        <span class="text"> Inicio </span> <span class="menu-hover"></span> </a>
                </li>
                @if(Auth::user()->id_perfil != 4)
                   @include('menu.notificaciones')
                @if( (Auth::user()->id_perfil==1) or (Auth::user()->id_perfil==3 or(Auth::user()->id_perfil == 5)) )
                    @include('menu.oficios')
                @endif
                @if(Auth::user()->id_perfil==5)
                    @include('menu.administrador')
                @endif
               <li class="">
                 <a href="{{ route('oficios.index') }}"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>
                    <span class="text">Consulta de  Requerimientos</span> <span class="menu-hover"></span> </a>
               </li>
               @endif
               @if(Auth::user()->permiso_interconsultas==1)
               <li class="">
                <a href="{{ route('interconsultas.index') }}"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>
                   <span class="text">Interconsultas</span> <span class="menu-hover"></span> </a>
              </li>
               @endif
                @if(Auth::user()->id_perfil ==1 || Auth::user()->id_perfil == 3 || Auth::user()->id_perfil == 5)
                    @include('menu.alertas_tempranas')
                @endif
                @if(Auth::user()->id_perfil != 4)
                <li class=" alertas_tempranas">
                    <a href="{{ route('oficios.index',['alerta_temp'=>1]) }}"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>
                        <span class="text">Consulta de  Alertas Tempranas</span> <span class="menu-hover"></span> </a>
                </li>
                @endif






                <li class="">
                {{--<a href="@{{route('mails.create')}}"> <span class="menu-icon"> <i class="fa fa-chevron-right"></i></span>--}}
                    {{--<span class="text">Mensajes</span> <span class="menu-hover"></span>--}}
                {{--</a>--}}
                </li>
            </ul>
        </div>
        <!-- /main-menu -->
    </div>
    <!-- /sidebar-inner -->
</aside>
<!-- ./main-menu -->
