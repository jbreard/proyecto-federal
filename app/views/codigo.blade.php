<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Control de Acceso</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap core CSS -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    {{ HTML::style('bootstrap/bootstrap/css/bootstrap.css', array('media' => 'screen')) }}

    <!-- Font Awesome -->
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->
    {{ HTML::style('bootstrap/css/font-awesome.min.css', array('media' => 'screen')) }}
    <!-- Endless -->
    <!-- <link href="css/endless.min.css" rel="stylesheet">-->
    {{ HTML::style('bootstrap/css/endless.min.css', array('media' => 'screen')) }}

    <style type="text/css">
        body {
            background: #1e5799; /* Old browsers */
            background: -webkit-gradient(linear, left top, left bottom, from(#4ca4d6), to(#0a4aa8)) fixed;
            background: -o-linear-gradient(#4ca4d6, #0a4aa8) fixed; /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#4ca4d6, #0a4aa8) fixed; /* For Firefox 3.6 to 15 */
            background: linear-gradient(#4ca4d6, #0a4aa8) fixed; /* Standard syntax (must be last) */



        }

        .text-success {
            color: #fff;
            font-size: 22px;
        }

        .pull-left {
            font-size: 20px;
        }

        .panel {
            width: 350px;
            padding: 10px 40px;
            margin-left: auto;
            margin-right: auto;
            border-radius: 7px;
            -webkit-box-shadow: 1px 0px 10px 10px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 1px 0px 10px 10px rgba(0, 0, 0, 0.1);
            box-shadow: 1px 0px 10px 10px rgba(0, 0, 0, 0.1);
        }

        .btn-sm, .btn-group-sm > .btn {
            padding: 5px 15px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px;
        }

        h2 {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        hr {
            margin: 10px 0;
        }

    </style>

</head>

<body>



<div class="login-wrapper">
    <div class="text-center">
        <img src="{{ URL::asset('bootstrap/images/login-logo_03.png')}}" class="fadeInUp animation-delay9">
    </div>
    <div class="text-center">
        <h2 class="fadeInUp animation-delay8" style="font-weight:bold">
            <span class="text-success">{{Config::get('nombre.siglas')}}</span>
            <br>
            <span class="text-success">{{Config::get('nombre.nombre')}}</span>
        </h2>
    </div>







    <div class="login-widget animation-delay1">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <i class="fa fa-lock fa-lg"></i> Codigo
                </div>
            </div>
            <div class="panel-body">


                {{ Form::open(array('url' => '/validate','class'=>'form-signin','role'=>'form','autocomplete'=>'off')) }}
                <div class="form-group">
                    <label>Ingrese el Código enviado</label>
                    {{ Form::text('codigo', null, array('placeholder' => '- - - - - -', 'class' =>
                    'form-control','required'=>'required','autocomplete'=>'off')) }}
                </div>





                <button class="btn btn-primary btn-sm bounceIn animation-delay5 pull-right"><i class="fa fa-sign-in"></i> Ingresar</button>
                <!-- @{{ Form::submit('Ingresar »',array("class"=>"btn btn-primary btn-sm bounceIn animation-delay5 login-link pull-right" )) }} -->

                {{ Form::close(); }}


            </div>
            @if(!empty($mensaje_correcto))
               <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                   <ul style="list-style: none">
                      <li><strong>{{ $mensaje_correcto }}</strong></li>
                   </ul-->
               </div>
            @endif
        </div>
        <!-- /panel -->
    </div>
    <!-- /login-widget -->
    <div class="row">

        <div class="text-center">
                <h2 class="fadeInUp animation-delay8" style="font-weight:bold">
                    <span class="text-success">Secretaría de Seguridad y Politica Criminal</span>
              </h2>
        </div>
    </div>

<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2" style=""><p>  {{ HTML::image('Isologotipo-SIFCOP2.png', '') }} </p></div>
    <div class="col-md-5"></div>
</div>
</div>

</body>
</html>
