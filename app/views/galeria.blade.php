@if( (Request::segment(2) != 'pdf') and ($fotos != null))
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $titulo }}</h3>
    </div>
    <div class="panel-body">
        <div class="your-class">
            <div class="row">
                @foreach($fotos as $foto)
                    @if(is_object($foto))
                        @if(substr($foto->path, -3) == "pdf")
                            <div class="col-md-2" style="cursor: zoom-in"><a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" >
                                    <img src="{{ Route('archivos.descargar')}}?q=imagen.jpg" width="150px" height="150px"></a>
                                </div>
                        @elseif(substr($foto->path, -3) == "doc" or substr($foto->path, -4) == "docx")
                        <div class="col-md-2" style="cursor: zoom-in"><a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" >
                                <img src="{{ Route('archivos.descargar')}}?q=doc.png" width="150px" height="150px"></a>
                        </div>
                        @elseif(substr($foto->path, -3) == "xls" or substr($foto->path, -4) == "xlsx")
                            <div class="col-md-2" style="cursor: zoom-in"><a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" >
                                    <img src="{{ Route('archivos.descargar')}}?q=excel.png" width="150px" height="150px"></a>
                            </div>
                        @elseif(substr($foto->path, -3) == "ppt" or substr($foto->path, -4) == "pptx")
                        <div class="col-md-2" style="cursor: zoom-in"><a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" >
                                <img src="{{ Route('archivos.descargar')}}?q=ppt.jpg" width="150px" height="150px"></a>
                        </div>
                        @else
                            <div class="col-md-2" style="cursor: zoom-in"><a href="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" target="_blank" ><img src="{{ Route('archivos.descargar')}}?q={{ $foto->path }}" width="150px" height="150px"></a></div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif