@extends ('header')
@section ('title') Listado de Usuarios @stop
{{ HTML::script('assets/js/bootbox.all.min.js') }}
@section ('content')
    <a href="{{ route('usuarios.create') }}"><button class="btn btn-primary" >Cargar Usuario <i class="glyphicon glyphicon-user"></i></button></a>

@if(Session::has('mensaje'))
    <div class="alert alert-success" role="alert" style="margin-top: 5px">{{Session::get('mensaje') }}</div>
@endif

    {{ Form::model(isset($busqueda)?$busqueda:null,array('method'=>'get')) }}
        <div class="row" style="margin-top:5px">
            <div class="form-group">
                <div class="col-md-3">
                    {{ Form::label('id_perfil','Perfil:') }}
                    {{ Form::select( 'id_perfil',[""=>""]+$perfiles,null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('username','Nombre de Usuario:') }}
                    {{ Form::text('username',null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','Organismo:') }}
                    {{ Form::select( 'id_jurisdiccion',[""=>""]+$combosOrganismos,null, array('placeholder' => '', 'class' =>'form-control  input-sm','id'=>'id_jurisdiccion')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','Sub Organismo:') }}
                    {{ Form::select( 'sub_organismo',[""=>""],null, array('placeholder' => '', 'class' =>'form-control  input-sm', 'id'=>'sub_organismo')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','Nombre:') }}
                    {{ Form::text( 'nombre',null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','Apellido:') }}
                    {{ Form::text( 'apellido',null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','DNI:') }}
                    {{ Form::text( 'dni',null,   array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('','Estado:') }}
                    {{ Form::select( 'habilitado',[""=>""]+["1"=>"Habilitado","0"=>"Bloqueado","2"=>"Inhabilitado"],null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:5px">
            <div class="col-md-3">
                <button class="btn btn-info"><i class="fa fa-search"></i></button>
            </div>
        </div>


    {{Form::close()}}











<h2>Listados de Usuarios</h2>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>DNI</th>
			<th>Nombre de Usuario</th>
			<th>Organismo</th>
			<th>Sub Organismo</th>
			<th>Perfil</th>
			<th>Estado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $key => $value)
		<tr>
			<td>{{ $value->nombre }}</td>
			<td>{{ $value->apellido }}</td>
			<td>{{ $value->dni }}</td>
			<td>{{ $value->username }}</td>
			<td>{{ $value->getJurisdiccion->descripcion }}</td>
			<td>{{ $value->getSubOrganismo->descripcion or '' }}</td>
			<td>{{ $value->getPerfil->perfil }}</td>
			<td>
                @if($value->habilitado == 1)
                    Habilitado
                @elseif($value->habilitado == 2)
                    Inhabilitado
                @elseif($value->habilitado == 0)
                    Bloqueado
                @endif


            </td>

			<!-- we will also add show, edit, and delete buttons -->
			<td>

				<!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
				<!-- we will add this later since its a little more complicated than the other two buttons -->

				<!-- show the nerd (uses the show method found at GET /nerds/{id} -->
				<a class="btn btn-small btn-info" href="{{ route('usuarios.show',$value->id) }}">Editar</a>
                <a class="btn btn-small btn-danger modal_clave" href="#" data-id = "{{ $value->id }}" data-toggle="modal" data-target="#myModal" >Reset Clave</a>

			</td>
		</tr>
	@endforeach
	</tbody>
</table>

    {{$users->appends(isset($busqueda)?$busqueda:null)->links()}}


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"> Restablecer Clave</h4>
                <div class="alert alert-info" role="alert">
                    <b>La clave debe contener:</b> <br>
                    Al menos 8 Caracteres. <br>
                    Al menos 1 Letra Minúscula. <br>
                    Al menos 1 Letra Mayúscula. <br>
                    Al menos 1 Número. <br>
                    Al menos 1 Caracter Especial. (/*_-)
                </div>
            </div>
            <div class="modal-body">

                <p>
                    {{ Form::open(array('route'=>'usuarios.restablecer','method'=>'POST','id'=>'form')) }}
                        {{ Form::label('clave','Cambiar Clave') }}
                        {{ Form::password('clave',array('class'=>'form-control')) }}
                        {{ Form::hidden('id',null,array('id'=>'id_usuario')) }}
                    {{ Form::close(); }}
                 <button id="seguridad" type="button" class="btn btn-success" onclick="CheckPassword(clave.value)">Comprobar Seguridad »</button>
                </p>
            </div>
            <div class="modal-footer">
                <button id="pass" type="button" class="btn btn-success enviar_clave" onclick="CheckPassword(clave.value)" disabled>Aceptar »</button>
                <button type="button" class="btn btn-default cancelar_clave" data-dismiss="modal" id="cancelar_clave">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            var helper = new Helper();
            $("#id_jurisdiccion").change(function(){
                var idOrganismo = $(this).val();
                var ruta        = "suborganismos/organismo/"+idOrganismo;
                var selected = '{{$busqueda['sub_organismo'] or ''}}';

                helper.ActualizarComboFirstOptionBlank("#sub_organismo",ruta,'get',selected);
            });
            $("#id_jurisdiccion").change();
        });
        function CheckPassword(inputtxt)
        {

            var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
            var button = document.getElementById('pass');
            var ok = document.getElementById('ok');
            if(inputtxt.match(decimal))
            {
                //alert('OK...');
                button.disabled = false;
                return true;
            }
            else
            {
                bootbox.alert({
                    message: '<h4 class="alert alert-info" style="margin: 2%;">\n' +
                        '  El password: '+'<b>'+inputtxt+'</b><br>'+' no cumple las condiciones' +
                        '</h4>',
                    size: 'small'
                });


                return false;
            }
        }
    </script>
@stop
@section('jquery')


            $('.modal_clave').click(function(event){
                event.preventDefault();

                var id = $(this).attr('data-id');
                $("#id_usuario").val(id);
            });
            $(".enviar_clave").click(function(event){

               event.preventDefault();
               clave = $("#clave").val();
                if(clave!=''){
                    $("#form").submit();
                }
                $("#mensaje_error").html("<b>La Clave No Puede Estar Vacía</b>");
                $("#mensaje_error_modal").show();
            });
            $(".cancelar_clave").click(function(event){
                event.preventDefault();
                $("#clave").val('');
                $("#id_usuario").val('');
                $("#mensaje_error").html("");
                $("#mensaje_error_modal").hide();
            });
@stop
