@extends ('header')
<style>
input{
    text-transform: none !important;
}
</style>
@section('scripts')
<script type="text/javascript">
	$(function(){
		var helper = new Helper();
		$("#id_jurisdiccion").change(function(){
			var idOrganismo = $(this).val();
			var ruta        = "suborganismos/organismo/"+idOrganismo;
			helper.ActualizarCombo("#sub_organismo",ruta,'get');
		});
	});
        function desabilitar() {
        document.getElementById("id_guardar").disabled = true;
        document.getElementById("usuarioForm").submit();
    }
</script>
@stop


<?php
    if ($user->exists):
        $form_data = array('route' => array('usuarios.update', $user->id), 'method' => 'PATCH','id' => 'usuarioForm');
        $user_create = $user->id_user_reator;
        $action    = 'Editar';
    else:
        $form_data = array('route' => 'usuarios.store', 'method' => 'POST','id' => 'usuarioForm');
        $action    = 'Crear';        
    endif;

?>

@section ('title') {{$action}} usuario @stop

@section ('content')
<h1>{{$action}} Usuario</h1>
<div class="alert alert-danger fade in" style="display:none" id="errores">
      
     
</div>

{{ Form::model($user,$form_data, array('role' => 'form')) }}

<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('id', 'Perfil') }}
		{{ Form::select('id_perfil',$perfiles ,null,array('class' => 'form-control','id'=>'id_perfil')) }}
	</div>
    <div class="form-group col-md-4">
        {{ Form::label('username', 'Nombre de Usuario') }}
        {{ Form::text('username', null, array('placeholder' => 'Ingrese el Nombre del Usuario', 'class' => 'form-control')) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {{ Form::label('id_jurisdiccion','Organismos') }}
        {{ Form::select('id_jurisdiccion',$combosOrganismos,null,array('class'=>'form-control','data-ruta'=>route('suborganismo.byorganismo')))}}
    </div>
    <div class="form-group col-md-4">
		{{ Form::label('sub_organismo', 'Sub Organismo') }}
		{{ Form::select('sub_organismo',isset($sub_organismo) ? $sub_organismo:[],null, array('placeholder' => '', 'class' => 'form-control')) }}
	</div>
</div>

<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('nombre', 'Nombre') }}
		{{ Form::text('nombre', null, array('placeholder' => 'Ingrese el Nombre del Usuario', 'class' => 'form-control')) }}
	</div>

    <div class="form-group col-md-4">
		{{ Form::label('apellido', 'Apellido') }}
		{{ Form::text('apellido', null, array('placeholder' => 'Ingrese el Apellido del Usuario ', 'class' => 'form-control')) }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('dni', 'Dni') }}
		{{ Form::text('dni', null, array('placeholder' => 'Ingrese el Dni del Usuario ', 'class' => 'form-control')) }}
	</div>
    <div class="form-group col-md-4">
        {{ Form::label('email', 'Correo electrónico') }}
        {{ Form::email('email',null,['class' => 'form-control class_find','placeholder' => 'Ingrese un email']) }}
    </div>

</div>
<div class="row">
    <div class="form-group col-md-4">
        {{ Form::label('permiso_pe', '¿Tiene permiso para ver búsqueda en Personas extraviadas?') }}
        {{ Form::select('permiso_pe',[0=>"No",1 => "Si"],$user->permiso_pe, array('class' => 'form-control')) }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('permiso_interconsultas', '¿Tiene permiso para ver interconsultas?') }}
        {{ Form::select('permiso_interconsultas',[0=>"No",1 => "Si"],$user->permiso_interconsultas, array('class' => 'form-control')) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        {{ Form::label('permiso_anses', '¿Tiene permiso para buscar en anses?') }}
        {{ Form::select('permiso_anses',[0=>"No",1 => "Si"],$user->permiso_anses, array('class' => 'form-control')) }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('habilitado', 'Estado') }}
        {{ Form::select('habilitado',["1"=>"Habilitado","2"=>"Inhabilitado","0"=>"Bloqueado"],null, array('placeholder' => 'Ingrese el Dni del Usuario ', 'class' => 'form-control')) }}
    </div>
</div>
@if($action == "Editar")
<div class="row">
    <div class="form-group col-md-4">
<h4 class=" text-light bg-grey text-center">Fecha de cambio de Password</h4>
    </div>
    <div class="form-group col-md-4">
<h4 class=" text-light bg-grey text-center">Fecha de aceptación de Términos y Condiciones</h4>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        <div class="alert alert-success"><h4 class="text-center">{{date('d-m-Y', strtotime($user->pass_created))}}</h4></div>
    </div>
    <div class="form-group col-md-4">
        <div class="alert alert-success"><h4 class="text-center">@if($user->term_cond<>null){{date('d-m-Y', strtotime($user->term_cond))}}@else <p>No aceptado aún</p>@endif</h4></div>
    </div>
    
</div>
    @if($user_creator <> "")
        <div class="row">



            <div class="form-group col-md-4">
                   <h4 class=" text-light bg-grey text-center">Usuario creado por:</h4>
                <div class="alert alert-warning">
                    <h4 class="text-left">Nombre: {{ $user_creator['nombre'] }}</h4><p>
                    <h4 class="text-left">Apellido: {{ $user_creator['apellido'] }}</h4><p>
                    <h4 class="text-left">Username: {{ $user_creator['username'] }}</h4><p>    
                 </div>
            </div>
        </div>
    @endif
@endif
<div class="row">
	
	<div class="form-group col-md-4">
		
		{{ Form::button('Guardar', array('type' => 'submit' ,'class' => 'btn btn-primary','id'=>'id_guardar','onclick'=>'desabilitar()')) }}
	</div>
</div>
{{ Form::close() }}
@stop


