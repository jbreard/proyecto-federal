@extends ('header')
{{ HTML::script('assets/js/bootbox.all.min.js') }}
@section ('title') Cambiar Clave @stop
<style type="text/css">
	#email {
		text-transform: lowercase;
	}


</style>
@section ('content')
	<script>

        function CheckPassword(inputtxt)
        {

            var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
            var button = document.getElementById('cambia');
            if(inputtxt.match(decimal))
            {
                //alert('OK...');
                button.disabled = false;
                return true;
            }
            else
            {

                bootbox.alert({
                    message: '<h4 class="alert alert-info" style="margin: 2%;">\n' +
                        '  El password: '+'<b>'+inputtxt+'</b><br>'+' no cumple las condiciones' +
                        '</h4>',
                    size: 'small'
                });

                return false;
            }
        }

	</script>

<h3>Cambiar Clave</h3>
{{ Form::model($clave,array('id'=>'form1','url'=>'usuarios/clave', 'method' => 'POST'), array('role' => 'form')) }}
	<div class="row">

		<div class="form-group col-md-3">
			<div class="alert alert-info" role="alert">
				<b>La clave debe contener:</b> <br>
				Al menos 8 Caracteres. <br>
				Al menos 1 Letra Minúscula. <br>
				Al menos 1 Letra Mayúscula. <br>
				Al menos 1 Número. <br>
				Al menos 1 Caracter Especial. (/*_-)
			</div>
		</div>
	</div>
@if($email)
<div class="row">

	<div class="form-group col-md-3">

		{{ Form::hidden('email',$email,['value' => $email]) }}

	</div>
</div>
@else
	<div class="row">

		<div class="form-group col-md-3">

			{{ Form::label('email', 'Correo') }}
			{{ Form::email('email',null,['class' => 'form-control class_find','placeholder' => 'Ingrese un Email']) }}

		</div>
	</div>
@endif

<div class="row">

	<div class="form-group col-md-3">
		{{ Form::label('clave', 'Clave Anterior') }}
		{{ Form::password('clave_old',array('class' => 'form-control')) }}

	</div>
</div>
<div class="row">
	<div class="form-group col-md-3">
		{{ Form::label('clave_nueva', 'Clave Nueva') }}
		{{ Form::password('clave_nueva',array('class' => 'form-control')); }}

	</div>
</div>
<div class="row">
	<div class="form-group col-md-3">
		{{ Form::label('clave_confirma', 'Confirme Clave') }}
		{{ Form::password('clave_confirma',array('class' => 'form-control','type'=>'number','onfocus'=>'CheckPassword(clave_nueva.value)')); }}

	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::button(' Cambiar »	 ', array('id'=>'cambia','type' => 'submit', 'class' => 'btn btn-primary','disabled')) }}
	</div>
</div>

{{ Form::close() }}
@stop