@extends ('header')
@section('scripts')

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=es&amp;libraries=places"></script>
{{ HTML::script('maps/jquery.geocomplete.js') }}
{{ HTML::script('maps/map.js') }}
{{ HTML::script('assets/js/datepicker_espanish.js') }}
{{ HTML::style('maps/css/styles.css', array('media' => 'screen')) }}
<script>
	window.onload = function() {
		DibujarMapa({{ $datosLatLon }});
	};
	
</script>
<style>
	#map {
		margin-left:2%;
		margin-top: 5%;
		width: 90%;
		height:500px !important;
		border: 5px solid black;
		border-radius: 1px;
	}
</style>
@stop

@section ('title') Mapa @stop

@section ('content')

<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-11">
		<div id="map"></div>
	</div>
</div>

@stop