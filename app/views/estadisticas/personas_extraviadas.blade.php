@extends ('header')

@section('scripts')

    <script>

        $(document).ready(function(){

            $("#sin_resultados").addClass("hidden");
            $("#con_resultados").addClass("hidden");

            $("#formBusqueda").on("submit", function(e){

                $("#btnSearch").attr("disabled",true);
                e.preventDefault();
                var formData = new FormData(document.getElementById("formBusqueda"));
                var destino = "{{ Route('solicitud_paradero.filtro_estadisticas') }}";
                $.ajax({
                    type: "Post",
                    url: destino,
                    data: formData,
                    assync: true,
                    dataType: "html",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respuesta) {
                        $("#span_res").text(respuesta);
                        $("#btnSearch").attr("disabled", false);
                        if (respuesta > 0) {
                            $("#con_resultados").removeClass("hidden");
                            $("#sin_resultados").addClass("hidden");
                        }
                        else
                        {
                            $("#con_resultados").addClass("hidden");
                            $("#sin_resultados").removeClass("hidden");
                        }
                    }


                });
            });

        });


    </script>

@endsection

@section ('content')
    <h2>Filtro de estadísticas con cantidades</h2>
    {{ Form::model(isset($busqueda)?$busqueda:null,array('method'=>'get','id'=>'formBusqueda')) }}

    <div class="row">
        {{ Form::model(isset($busqueda)?$busqueda:null,array('method'=>'get','id'=>'formBusqueda')) }}
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('id_provincia', 'Provincia') }}
                {{ Form::select('id_provincia',['' => 'Todos'] + $comboProvincia,null,['class' => 'form-control provincia','data-target'=>'.partido_desaparecido']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('id_partido', 'Partido') }}
                {{ Form::select( 'id_partido',[],null, ['class' => 'form-control partido partido_desaparecido']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('sexo', 'Género') }}
                {{ Form::select('sexo',['' => 'Todos'] + $comboSexo,null, ['class' => 'form-control']) }}
            </div>

            <div class="col-md-3">
                {{ Form::label('vigentes', '¿Vigentes o habidos?') }}
                {{ Form::select('vigentes',['' => '',1 => 'Vigentes',2 => 'Habidos'],null, ['class' => 'form-control']) }}
            </div>
        </div>
    </div>
    <div class="row">
        {{ Form::model(isset($busqueda)?$busqueda:null,array('method'=>'get','id'=>'formBusqueda')) }}
        <div class="form-group">
            <div class="col-md-3">
                {{ Form::label('fecha_desde_carga', 'Fecha desde de carga') }}
                {{ Form::text( 'fecha_desde_carga',null, ['class' => 'form-control fecha','id' => 'fecha_desde_carga']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('fecha_hasta_carga', 'Fecha hasta de carga') }}
                {{ Form::text( 'fecha_hasta_carga',null, ['class' => 'form-control fecha','id' => 'fecha_hasta_carga']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('edad_desde', 'Edad desde') }}
                {{ Form::text( 'edad_desde',null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('edad_hasta', 'Edad hasta') }}
                {{ Form::text( 'edad_hasta',null, ['class' => 'form-control']) }}
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:1.6em!important" id="divBtns">
        <div class="col-md-2">
            <button class="btn btn-success" id="btnSearch"><i class="fa fa-search"></i> </button>
        </div>
    </div>

    <h3 id="con_resultados">
        Resultado: <span id="span_res"></span>
    </h3>

    <h3 id="sin_resultados">
        No se encontraron resultados
    </h3>

    {{ Form::close() }}

@stop


@section('jquery')


@stop