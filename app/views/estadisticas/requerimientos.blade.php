@extends ('header')
@section('scripts')

<!-- Polyfill Web Components support for older browsers -->

{{ HTML::script('assets/polymer/webcomponentsjs/webcomponents-lite.min.js') }}

<!-- Import element -->
<link rel="import" href="{{ asset('assets/polymer/google-chart/google-chart.html')}}">

<!--eliminar-->
<!--eliminar--><script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.14/vue.min.js"></script><!--eliminar-->
<!--eliminar-->

<style>
    google-chart {
        height: 300px;
        width: 100%;
    }
    .separacion{
        margin-left: 5px;
    }
</style>

<script>

$(function(){
    //var data ='[["MES", "Prohibicion","Solicitud"],["Enero", 100,200],["Febrero", 50,70]]';

    var demo = new Vue({
        el: '#estadisticas',
        data: {
            title: 'todos',
            cols:'[{"label":"Mes", "type":"string"}, {"label":"Cantidad", "type":"number"}]',
            cols2:'[{"label":"Organismo", "type":"string"}, {"label":"Cantidad", "type":"number"}]',
            rows:null,
            meses:{"1":"Enero","2":"Febrero","3":"Marzo","4":"Abril","5":"Mayo","6":"Junio","7":"julio","8":"Agosto","9":"Septiembre","10":"Octubre","11":"Noviembre","12":"Diciembre"},
            options:'{"bar":{"groupWidth":"60%"},"animation":{"startup":"true","duration":"1000"}}',
            options2:'{"explorer":{"axis":"horizontal","keepInBounds": "true"},"bar":{"groupWidth":"60%"},"animation":{"startup":"true","duration":"1000"}}',
            type:'column',
            data:null,
            tiposOficios:null,
            organismos:null,
            dataMes:null,
            dataOrganismo:null,
            anio:2015
        },
        ready:function(){
            //console.log(this.data);
            this.getTiposOficios();
            this.getOrganismos();

        },
        methods:{
            getData:function(anio){

                var el = this;
                var estados = [1,2];
                var mesesAux = null;
                var anio = el.anio_req_mes || el.anio;
                $.ajax({
//                   url:'/desarrollo/sifcop/public/estadisticas/getRequerimientos/mes',
                   url:'{{ Request::root() }}/estadisticas/getRequerimientos/mes',
                   data:{'anio':anio},
                   success:function(data){
                       //var rowsA = '[["Jan", 30],["Feb", 25],["Mar", 11],["May", 29]]';
                       var cabecera = '["MES","Vigente","No Vigente"],';
                       $.each(el.meses,function(index,value){//recorro los meses
                           var cantidad;
                           var mesesAuxFinal = '';
                           mesesAux += '["'+value+'",';
                           $.each(estados,function(index2,value2){//recorro los tipos de estados
                               cantidad = 0;
                               var total = 0;
                               $.each(data,function(index3,value3){//recorro los datos
                                   total = 0;
                                   //console.log(index);
                                   if((index == value3.mes) && (value2 == value3.estado )){
                                       cantidad = value3.cantidad;
                                       total += parseInt(value3.cantidad);
                                   }
                               });
                               mesesAuxFinal += cantidad+",";
                               mesesAuxFinal  = mesesAuxFinal.replace('null','');
                           });
                           mesesAuxFinal =  mesesAuxFinal.substring(0, mesesAuxFinal.length - 1);
                           mesesAux += mesesAuxFinal+"],";
                       });
                       mesesAux = mesesAux.substring(0,mesesAux.length - 1);
                       mesesAux = mesesAux.replace('null','');
                       el.dataMes = "["+cabecera+mesesAux+"]";
                   }
                });

            },

            getTiposOficios:function(){
                var el = this;
                $.ajax({
                    url:'{{ Request::root() }}/estadisticas/getTiposReq',
                    async:false,
                    success:function(data){
                        //var rowsA = '[["Jan", 30],["Feb", 25],["Mar", 11],["May", 29]]';
                        el.tiposOficios = data;
                    }
                });
                el.getData(this.anio);  // requerimientos por mes
            },


            getOrganismos:function(){
                var el = this;
                $.ajax({
                    url:'{{ Request::root() }}/estadisticas/getOrganismos',
                    async:false,
                    success:function(data){
                        el.organismos = data;
                    }
                });
                //el.getOrg(this.anio);  // requerimientos por organismos
            },


            getInfo:function(anio){
                var el = this
                var tiposAux = null;
                var estados = [1,2];
                //console.log(el);
                var anio  = el.anio_req_tipo || el.anio ;
                $.ajax({
                   // url:'/desarrollo/sifcop/public/estadisticas/getRequerimientosPorTipo',
                   url:'{{ Request::root() }}/estadisticas/getRequerimientosPorTipo',
                    data:{'anio':anio},
                    success:function(data){
                        //ARMAR CABECERA
                        var info = '[["REQUERIMIENTOS","Vigente","No Vigente"],';
//                        $.each(el.tiposOficios,function(index,value){
//                            info +=   '"'+value.descripcion+'",';
//                        });
//                        info = info.substring(0, info.length - 1)+"],";
                        $.each(el.tiposOficios,function(index,value){//recorro los tipos
                            //console.log(value);
                            var cantidad;
                            var tiposAuxFinal = '';
                            tiposAux += '["'+value.descripcion+'",';
                            $.each(estados,function(index2,value2){//recorro los estados
                                cantidad = 0;
                                var total = 0;
                                $.each(data,function(index3,value3){//recorro los datos
                                    total = 0;
                                    if((value.descripcion == value3.tipo) && (value2 == value3.estado)){
                                       cantidad = value3.cantidad;
                                       total += parseInt(value3.cantidad);
                                    }
                                });
                                tiposAuxFinal += cantidad+",";
                                tiposAuxFinal  = tiposAuxFinal.replace('null','');
                            });
                            tiposAuxFinal =  tiposAuxFinal.substring(0, tiposAuxFinal.length - 1);
                           // console.log(tiposAuxFinal);
                            tiposAux += tiposAuxFinal+"],";
                        });
                        tiposAux = tiposAux.substring(0,tiposAux.length - 1);
                        tiposAux = tiposAux.replace('null','');
                        el.data = info+tiposAux+"]";
//                        rows = rows.substring(0, rows.length - 1);
                     //   el.rows = "[contenido]".replace('contenido',rows);
                    }
                });
            },




            getOrg:function(anio){
                var el = this;
                var estados = [1,2];
                var organismoAux = null;
                //var anio = anio ;
                var anio  = el.anio_req_org || el.anio ;
                $.ajax({
                    //url:'/desarrollo/sifcop/public/estadisticas/getReqPorOrganismos',
                    url:'{{ Request::root() }}/estadisticas/getReqPorOrganismos',
                    data:{'anio':anio},
                    success:function(data){
                        var cabecera = '["ORGANISMO","Vigente","No Vigente"],'
//                        var rows = '';
//                        $.each(data,function(index,value){
//                            rows += '["'+value.organismo+'",'+value.cantidad+'],'
//                        });
//                        rows = rows.substring(0, rows.length - 1);
//                        el.rows = "[contenido]".replace('contenido',rows);
//                        var array = new Array();
//                        $.each(data,function(index,value){ //recorro los organismos
//                            array.push(value.organismo);
//                        });
//                        array = jQuery.unique( array );
                        $.each(el.organismos,function(index,value){
                            var cantidad;
                            var organismoAuxFinal = '';
                             organismoAux += '["'+value.organismo+'",';
                            $.each(estados,function(index2,value2){//recorro los tipos de estados
                                cantidad = 0;
                                var total = 0;
                                $.each(data,function(index3,value3){//recorro los datos
                                    total = 0;
                                    if((value.organismo == value3.organismo) && (value2 == value3.estado )){
                                        cantidad = value3.cantidad;
                                        total += parseInt(value3.cantidad);
                                    }
                                });
                                organismoAuxFinal += cantidad+",";
                                organismoAuxFinal  = organismoAuxFinal.replace('null','');
                            });
                            organismoAuxFinal =  organismoAuxFinal.substring(0, organismoAuxFinal.length - 1);
                            //console.log(organismoAuxFinal);
                            organismoAux += organismoAuxFinal+"],";
                        });
                        //console.log(organismoAux);
                        organismoAux = organismoAux.substring(0,organismoAux.length - 1);
                        organismoAux = organismoAux.replace('null','');
                        el.dataOrganismo = "["+cabecera+organismoAux+"]";
                        //console.log(el.dataOrganismo);
                    }
                });
            },

            changeAnioReqMes:function(){
                this.getData(this.anio_req_mes);
            },
            changeAnioReqTipo:function(){
                this.getInfo(this.anio_req_tipo);
            },
            changeAnioReqOrg:function(){
                this.getOrg(this.anio_req_org);
            }
        }

    })



});


</script>


@stop
@section ('content')
<div id="estadisticas">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#requerimientos_mes" data-toggle="tab">Requerimientos por Mes</a>
        </li>
        <li v-on="click:getInfo()">
            <a href="#requerimientos_tipo" data-toggle="tab">Requerimientos Por Tipo</a>
        </li>
        <li v-on="click:getOrg()">
            <a href="#requerimientos_organismo"  data-toggle="tab">Requerimientos Por Organismo</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="requerimientos_mes">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <span class="pull-left"><h4>Requerimientos por Mes</h4></span>
                    <ul class="tool-bar">
<!--                        <li>-->
<!--                            <select v-model="type" class="form-control">-->
<!--                                <option value="column">Columnas</option>-->
<!--                                <option value="pie">Torta</option>-->
<!--                            </select>-->
<!--                        </li>-->
                        <li><select v-model="anio_req_mes" v-on="change:changeAnioReqMes" class="form-control separacion">
                                <option value="2015" selected="selected">2015</option>
                                <option value="2014">2014</option>
                        </select></li>
                    </ul>
                </div>
                <div class="panel-body no-padding collapse in" id="collapseWidget">
                    <div class="padding-md">
                        <div class="col-md-9">
                            <google-chart
                                type=@{{type}}
                                options=@{{options}}
                                data=@{{dataMes}}>
                            </google-chart>
                        </div>
                        <div class="col-md-3">
                            <google-chart
                                type="table"
                                options=@{{options}}
                                data=@{{dataMes}}>
                            </google-chart>
                        </div>
                    </div>
                </div>
                <div class="loading-overlay">
                    <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                </div>
            </div>
        </div>



        <div class="tab-pane" id="requerimientos_tipo">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <span class="pull-left"><h4>Requerimientos por Tipo</h4></span>
                    <ul class="tool-bar">
                        <li>
<!--                            <select v-model="type" class="form-control">-->
<!--                                <option value="column">Columnas</option>-->
<!--                                <option value="pie">Torta</option>-->
<!--                            </select>-->
                        </li>
                        <li><select v-model="anio_req_tipo" v-on="change:changeAnioReqTipo" class="form-control separacion">
                                <option value="2015" selected="selected">2015</option>
                                <option value="2014">2014</option>
                            </select></li>
                    </ul>
                </div>
                <div class="panel-body no-padding collapse in" id="collapseWidget">
                    <div class="padding-md">
                        <div class="row">
                            <div class="col-md-12">
                                <google-chart
                                    type='column'
                                    options=@{{options}}
                                    data=@{{data}}
                                </google-chart>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <google-chart
                                    type="table"
                                    options=@{{options}}
                                    data=@{{data}}
                                 </google-chart>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-overlay">
                    <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                </div>
            </div>
        </div>




        <div class="tab-pane" id="requerimientos_organismo">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <span class="pull-left"><h4>Requerimientos por Organismo</h4></span>
                    <ul class="tool-bar">
                        <li>
<!--                            <select v-model="type" class="form-control">-->
<!--                                <option value="column">Columnas</option>-->
<!--                                <option value="pie">Torta</option>-->
<!--                            </select>-->
                        </li>
                        <li><select v-model="anio_req_org" v-on="change:changeAnioReqOrg" class="form-control separacion">
                                <option value="2015" selected="selected">2015</option>
                                <option value="2014">2014</option>
                            </select></li>
                    </ul>
                </div>
                <div class="panel-body no-padding collapse in" id="collapseWidget">
                    <div class="padding-md">
                        <div class="row">
                            <div class="col-md-12">
                                <google-chart
                                    type='column'
                                    options=@{{options2}}
                                    data=@{{dataOrganismo}}
                                </google-chart>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <google-chart
                                    type="table"
                                    options=@{{options2}}
                                    data=@{{dataOrganismo}}
                                </google-chart>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-overlay">
                    <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                </div>
            </div>
        </div>

    </div>
</div>


@stop