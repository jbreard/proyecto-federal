<style>
    .row{
        padding:1px;
        margin:1px;
    }
</style>

@if (isset($oficios) and ($oficios->estado==1) )
    <div class="alert alert-success">
        <div class="row"> <b>Estado:Vigente</b></div>
        <div class="row">
            <p class="pull-left"> Usuario que Cargo el Requerimiento: {{ $oficios->user->nombre }} {{ $oficios->user->apellido }} </p>
            <p class="pull-right"> Organismo: {{ $oficios->user->getJurisdiccion->descripcion }}</p>
        </div>
        <div class="row">
            <p>Sub Organismo: {{ $oficios->user->subOrganismo->descripcion or ''}}</p>
        </div>
        <div class="row">
            <p class="pull-left">Fecha de Carga: {{ $oficios->created_at }}</p>
            <p class="pull-right">Nro. De Registro:  {{ $oficios->id }}</p>

        </div>

    </div>
@elseif (isset($oficios) and ($oficios->estado==2) and ($oficios->oficioCesado))
    <div class="alert alert-danger">

        <div class="row"> <b> Estado: Sin Efecto</b></div>

        <div class="row">
            <p>Usuario que Cargo el Requerimiento de Sin Efecto: {{ $oficios->oficioCesado->user->nombre }} {{ $oficios->oficioCesado->user->apellido }}
                <span class="pull-right"><b>Nro. de Registro:  {{ $oficios->id }}</b></span>
            </p>
            <p>Organismo: {{$oficios->oficioCesado->user->getJurisdiccion->descripcion}}</p>
            <p>Fecha de carga de Sin Efecto:  {{ $oficios->oficioCesado->fecha_cese }}</p>
        </div>
        <hr>
        <div class="row">
            <p>
                Fecha de carga del Requerimiento:  {{ $oficios->created_at }}
                <span class="pull-right"> Usuario que Cargo el Requerimiento: {{ $oficios->user->nombre }} {{ $oficios->user->apellido }}</span>
            </p>
            <p>Organismo que Cargo el Requerimiento: {{ $oficios->user->getJurisdiccion->descripcion }}</p>

        </div>

    </div>
@elseif (isset($oficios) and ($oficios->estado==2) and empty($oficios->oficioCesado))
    <div class="alert alert-danger">
        <div class="row"> <b> Estado: Sin Efecto</b></div>

        <div class="row">
            <p>Usuario que Cargo el Requerimiento de Sin Efecto: Baja automatica por Vencimiento.
                <span class="pull-right"><b>Nro. de Registro:  {{ $oficios->id }}</b></span>
            </p>
            <p>Organismo: Minseg</p>
            <p>Fecha de carga de Sin Efecto: Automatica</p>
        </div>
        <hr>
        <div class="row">
            <p>
                Fecha de carga del Requerimiento:  {{ $oficios->created_at }}
                <span class="pull-right"> Usuario que Cargo el Requerimiento: {{ $oficios->user->nombre }} {{ $oficios->user->apellido }}</span>
            </p>
            <p>Organismo que Cargo el Requerimiento: {{ $oficios->user->getJurisdiccion->descripcion }}</p>

        </div>

    </div>
@endif