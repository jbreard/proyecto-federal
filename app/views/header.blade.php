<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title',Config::get("nombre.nombre"))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @include("include_head")
</head>

<body class="overflow-hidden">
<!-- Overlay Div -->
<div id="overlay" class="transparent"></div>
<!-- wrapper -->
<div id="wrapper" class="preload">
    <!-- top-nav-->
    @include("include_top_nav")
    <!-- /top-nav-->
    <!-- menu-lateral -->
    @include("include_main_menu")
    <!-- /menu-lateral -->
    <!-- main-container -->

    <div id="main-container">
        <!-- breadcrumb -->

        <!-- breadcrumb -->

        <div class="padding-md" id="padding">
        @include("errores")
        @if ( Session::has("id") )
            <div class="alert alert-animated alert-success">
                Requerimiento Cargado Correctamente
            </div>
        @endif
            @yield('content')

        </div>
        <!-- </div>-->
        @yield('contentMailsHeader')
    </div>

    <!-- /main-container -->


</div>
<!-- /wrapper -->
@include("logout")
<!-- include("include_script.php"); -->
@include("include_script")
</body>
</html>