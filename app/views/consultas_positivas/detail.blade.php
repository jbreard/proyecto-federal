@extends ('header')
@section('scripts')
{{ HTML::script('assets/js/busqueda.js') }}

<script>
	$(function(){
		$("#formBusqueda :input").prop("disabled",true);
		$("#formBusqueda select").prop("disabled",true);
		$("#divBtns").hide();
		var params = jQuery.parseJSON($("#parametrosBusqueda").val());

		$.each(params,function(index,value){
			if(typeof value =='object'){
				$.each(value,function(index2,value2){
					var name = index+"["+index2+"]";
				 	$("#formBusqueda :input[name='"+name+"']").val(value2);	
		 	    	$("#buscar_por").trigger("change");
    				$("#tipo_elemento").trigger("change");
    				$("#tipo_medida").trigger("change");
				});  	
			}else{
		    	$("#formBusqueda :input[name='"+index+"']").val(value);
			}
		});
	})
</script>
@stop
@section ('content')
<div class="row" style="display:none">
	<textarea class="form-control" style="height:400px" id="parametrosBusqueda">{{$detalles->parametros_busqueda}}</textarea>
</div>
	@include('listados.formulario_busqueda')
	<div class="row">
		<table class="table table-bordered">
			<thead>
				  <td>N° Requerimiento</td>
        		  <td>Tipo de Medida</td>
        		  <td>Fecha de Carga del Req</td>
        		  <td>Estado</td>
        		  <td>Alerta Temprana</td>
                  <td>Acciones</td>
			</thead>
			<tbody>
		@foreach ($oficios as $oficio)


    <tr class="{{ ($oficio->oficios->estado==2) ? 'danger' : 'info' }} {{ ($oficio->oficios->plazo_autorizado == 1) ? 'warning': '' }}">
        <td>{{ $oficio->oficios->id }}</td>
        <td>{{ $oficio->oficios->tipo->descripcion }} @if(($oficio->oficios->tipo->id == 9) or ($oficio->oficios->tipo->id == 5 )) ({{$oficio->oficios->medida_restrictiva}}) @endif</td>
        <td>{{ $oficio->oficios->created_at }}</td>
        <td>
            @if($oficio->oficios->tipo_oficio==3)
                @if($oficio->oficios->estadoOficio->id == 1 && $oficio->oficios->plazo_autorizado == 1)
                    Autorizado
                @else
                    {{ $oficio->oficios->estadoOficio->descripcion }}
                @endif
            @else
                {{ $oficio->oficios->estadoOficio->descripcion }}
            @endif
        </td>
        <td>@if($oficio->oficios->alerta_temprana ==1)
            <span class="label label-danger">SI</span> @else <span class="label label-success">NO</span>@endif
        </td>
        <td>
            <a href='{{ route($oficio->oficios->tipo->ruta.".show",$oficio->oficios->id) }}' class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Ver Reporte"><i class="fa fa-eye"></i></a>
            <a href='{{ route($oficio->oficios->tipo->ruta.".pdf",$oficio->oficios->id) }}' target="_blank" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Descargar Reporte"><i class="fa fa-download"></i></a>
        </td>
        
    </tr>
    @endforeach
			</tbody>
		</table>	
	</div>
	
	

{{ $oficios->links() }}




@stop
