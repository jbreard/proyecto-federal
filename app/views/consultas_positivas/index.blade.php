@extends ('header')
@section('scripts')
	<script type="text/javascript">
	$(function(){

	});
	</script>
@stop
@section ('content')
	<table class="table table-bordered">
		<thead>
			<th>Usuario</th>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Cantidad de Requerimientos</th>
			<th>Acciones</th>
		</thead>
		<tbody>
			@foreach($consultasPositivas as $consulta)
			<tr>
				<td>{{$consulta->consultas->user->nombre}} {{$consulta->consultas->user->apellido}} {{$consulta->consultas->user->getJurisdiccion->descripcion}}</td>
				<td>{{$consulta->consultas->getFecha()}}</td>
				<td>{{$consulta->consultas->getHora()}}</td>
				<td>{{$consulta->consultas->oficios->count()}}</td>
				<td><a href="{{route('consulta_positiva.show',$consulta->id_consulta_positiva)}}" class="btn btn-info">Ver »</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop
