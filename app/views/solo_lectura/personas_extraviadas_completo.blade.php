@extends('header')
@section('content')
@include('mensaje_activo_cesado',$datos)
<div class="span5">
<h4>Datos de Persona Buscada</h4>
@if(is_object($datos['oficios']->PersonaBuscada))
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaBuscada->apellido != "")
        <tr>
            <td>Apellido: {{$datos['oficios']->PersonaBuscada->apellido or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->nombres != "")
        <tr>
            <td>Nombre: {{$datos['oficios']->PersonaBuscada->nombres or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->fecha_nacimiento != "")
        <tr>
            <td>Fecha de nacimiento: {{$datos['oficios']->PersonaBuscada->fecha_nacimiento or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->edad != "")
        <tr>
            <td>Edad: {{$datos['oficios']->PersonaBuscada->edad or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->sobrenombre != "")
        <tr>
            <td>Sombrenombre: {{$datos['oficios']->PersonaBuscada->sobrenombre or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->id_nacionalidad != 0)
        <tr>
            <td>Nacionalidad: {{$datos['oficios']->PersonaBuscada->nacionalidad->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->sexo != 0)
        <tr>
            <td>Sexo: {{$datos['oficios']->PersonaBuscada->objSexo->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->id_estado_civil != 0)
        <tr>
            <td>Estado civil: {{$datos['oficios']->PersonaBuscada->estadoCivil->estado_civil or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->nombre_conyuge != "")
        <tr>
            <td>Nombre cónyuge: {{$datos['oficios']->PersonaBuscada->nombre_conyuge or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->apellido_nombre_paterno != "")
        <tr>
            <td>Apellido y nombre paterno: {{$datos['oficios']->PersonaBuscada->apellido_nombre_paterno or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->apellido_nombre_materno != "")
        <tr>
            <td>Apellido y nombre materno: {{$datos['oficios']->PersonaBuscada->apellido_nombre_materno or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->id_tipo_documento != 0)
        <tr>
            <td>Tipo de documento: {{$datos['oficios']->PersonaBuscada->tipoDocumento->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->nro_documento != "")
        <tr>
            <td>Nro de documento: {{$datos['oficios']->PersonaBuscada->nro_documento or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->autoridad_expide != "")
        <tr>
            <td>Autoridad que lo expide: {{$datos['oficios']->PersonaBuscada->autoridad_expide or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->cod_area_telefono != "")
        <tr>
            <td>Cod de area: {{$datos['oficios']->PersonaBuscada->cod_area_telefono or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->id_compania_telefono != 0)
        <tr>
            <td>Compañia teléfono: {{$datos['oficios']->PersonaBuscada->companiaTelefono->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->facebook != "")
        <tr>
            <td>Facebook: {{$datos['oficios']->PersonaBuscada->facebook or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->twitter != "")
        <tr>
            <td>Twitter: {{$datos['oficios']->PersonaBuscada->twitter or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->badoo != "")
        <tr>
            <td>Badoo: {{$datos['oficios']->PersonaBuscada->badoo or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->instagram != "")
        <tr>
            <td>Instagram: {{$datos['oficios']->PersonaBuscada->instagram or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->linkedin != "")
        <tr>
            <td>linkedin: {{$datos['oficios']->PersonaBuscada->linkedin or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->snapchat != "")
        <tr>
            <td>Snapchat: {{$datos['oficios']->PersonaBuscada->snapchat or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaBuscada->otro != "")
        <tr>
            <td>Otro: {{$datos['oficios']->PersonaBuscada->otro or ''}}</td>
        </tr>
        @endif
    </table>
</div>
@endif

@if(is_object($datos['oficios']->fotos->first()))
    @include('galeria',array("titulo"=>"Fotos cargadas","fotos"=>$datos['oficios']->fotos))
    @if(is_object($datos['oficios']->autorizacion))
    <tr>
        <td>Observaciones: {{$datos['oficios']->autorizacion->observaciones or ''}}</td>
    </tr>
    @endif
@endif

@if(is_object($datos['oficios']->PersonaUltimoDomicilio))
@if($datos['oficios']->PersonaUltimoDomicilio->direccion != '' || $datos['oficios']->PersonaUltimoDomicilio->nro != '' || $datos['oficios']->PersonaUltimoDomicilio->piso != '' || $datos['oficios']->PersonaUltimoDomicilio->dpto != '' || $datos['oficios']->PersonaUltimoDomicilio->interseccion != '' || $datos['oficios']->PersonaUltimoDomicilio->codigo_postal != '' || $datos['oficios']->PersonaUltimoDomicilio->id_provincia != 25 || $datos['oficios']->PersonaUltimoDomicilio->id_partido != 0 || $datos['oficios']->PersonaUltimoDomicilio->localidad != '' || $datos['oficios']->PersonaUltimoDomicilio->observaciones != '')
<h4>Último domicilio</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaUltimoDomicilio->direccion != "")
        <tr>
            <td>Dirección: {{$datos['oficios']->PersonaUltimoDomicilio->direccion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->nro != "")
        <tr>
            <td>Nro: {{$datos['oficios']->PersonaUltimoDomicilio->nro or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->piso != "")
        <tr>
            <td>Piso: {{$datos['oficios']->PersonaUltimoDomicilio->piso or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->dpto != "")
        <tr>
            <td>Dpto: {{$datos['oficios']->PersonaUltimoDomicilio->dpto or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->interseccion != "")
        <tr>
            <td>Intersección: {{$datos['oficios']->PersonaUltimoDomicilio->interseccion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->codigo_postal != "")
        <tr>
            <td>Código postal: {{$datos['oficios']->PersonaUltimoDomicilio->codigo_postal or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->provincia != "" && $datos['oficios']->PersonaUltimoDomicilio->provincia->id != 25)
        <tr>
            <td>Provincia: {{$datos['oficios']->PersonaUltimoDomicilio->provincia->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->partido != "")
        <tr>
            <td>Partido: {{$datos['oficios']->PersonaUltimoDomicilio->partido->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->localidad != "")
        <tr>
            <td>Localidad: {{$datos['oficios']->PersonaUltimoDomicilio->localidad or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaUltimoDomicilio->observaciones != "")
        <tr>
            <td>Observaciones: {{$datos['oficios']->PersonaUltimoDomicilio->observaciones or ''}}</td>
        </tr>
        @endif
    </table>
</div>
@endif
@endif
@if(is_object($datos['oficios']->PersonaDescripcionFisica))
@if($datos['oficios']->PersonaDescripcionFisica->id_contextura_fisica != 4 || $datos['oficios']->PersonaDescripcionFisica->altura != '' || $datos['oficios']->PersonaDescripcionFisica->peso != '' || $datos['oficios']->PersonaDescripcionFisica->id_tipo_cabello != 6 || $datos['oficios']->PersonaDescripcionFisica->largo_cabello != 1 || $datos['oficios']->PersonaDescripcionFisica->id_color_cabello != 7 || $datos['oficios']->PersonaDescripcionFisica->id_color_ojos != 8 || $datos['oficios']->PersonaDescripcionFisica->id_tez != 5 || $datos['oficios']->PersonaDescripcionFisica->id_senias_particulares != '' || $datos['oficios']->PersonaDescripcionFisica->detalle_senias != '' || $datos['oficios']->PersonaDescripcionFisica->observaciones != '')
<h4>Descripción física</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaDescripcionFisica->contexturaFisica->contextura_fisica != "S/D")
        <tr>
            <td>Contextura física: {{$datos['oficios']->PersonaDescripcionFisica->contexturaFisica->contextura_fisica or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->altura != "")
        <tr>
            <td>Altura: {{$datos['oficios']->PersonaDescripcionFisica->altura or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->peso != "")
        <tr>
            <td>Peso: {{$datos['oficios']->PersonaDescripcionFisica->peso or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->tipoCabello != "" && $datos['oficios']->PersonaDescripcionFisica->tipoCabello->tipo_cabello != "S/D")
        <tr>
            <td>Tipo de cabello: {{$datos['oficios']->PersonaDescripcionFisica->tipoCabello->tipo_cabello or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->largoCabello->largo_cabello != "S/D")
        <tr>
            <td>Largo del cabello: {{$datos['oficios']->PersonaDescripcionFisica->largoCabello->largo_cabello or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->colorCabello->color_cabello != "S/D")
        <tr>
            <td>Color del cabello: {{$datos['oficios']->PersonaDescripcionFisica->colorCabello->color_cabello or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->colorOjos->color_ojos != "S/D")
        <tr>
            <td>Color de ojos: {{$datos['oficios']->PersonaDescripcionFisica->colorOjos->color_ojos or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->tez->tez != "S/D")
        <tr>
            <td>Tez: {{$datos['oficios']->PersonaDescripcionFisica->tez->tez or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->id_senias_particulares != "")
        <tr>
            <td>Señas particulares: {{$datos['oficios']->PersonaDescripcionFisica->id_senias_particulares or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->detalle_senias != "")
        <tr>
            <td>Detalle señas: {{$datos['oficios']->PersonaDescripcionFisica->detalle_senias or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDescripcionFisica->observaciones != "")
        <tr>
            <td>Observaciones: {{$datos['oficios']->PersonaDescripcionFisica->observaciones or ''}}</td>
        </tr>
        @endif
    </table>
</div>
@endif
@endif
@if(is_object($datos['oficios']->PersonaDatosLaborales))
@if($datos['oficios']->PersonaDatosLaborales->profesion_ocupacion != '' || $datos['oficios']->PersonaDatosLaborales->empleador != '' || $datos['oficios']->PersonaDatosLaborales->telefono != '' || $datos['oficios']->PersonaDatosLaborales->direccion != '' || $datos['oficios']->PersonaDatosLaborales->nro != '' || $datos['oficios']->PersonaDatosLaborales->piso != '' || $datos['oficios']->PersonaDatosLaborales->dpto != '' || $datos['oficios']->PersonaDatosLaborales->codigo_postal != '' || $datos['oficios']->PersonaDatosLaborales->id_provincia != 25 || $datos['oficios']->PersonaDatosLaborales->id_partido != 0 || $datos['oficios']->PersonaDatosLaborales->localidad != '')
<h4>Datos laborales</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaDatosLaborales->profesion_ocupacion != "")
        <tr>
            <td>Profesión/Ocupación: {{$datos['oficios']->PersonaDatosLaborales->profesion_ocupacion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->empleador != "")
        <tr>
            <td>Empleador: {{$datos['oficios']->PersonaDatosLaborales->empleador or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->telefono != "")
        <tr>
            <td>Teléfono: {{$datos['oficios']->PersonaDatosLaborales->telefono or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->direccion != "")
        <tr>
            <td>Dirección: {{$datos['oficios']->PersonaDatosLaborales->direccion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->nro != "")
        <tr>
            <td>Número: {{$datos['oficios']->PersonaDatosLaborales->nro or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->piso != "")
        <tr>
            <td>Piso: {{$datos['oficios']->PersonaDatosLaborales->piso or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->dpto != "")
        <tr>
            <td>Departamento: {{$datos['oficios']->PersonaDatosLaborales->dpto or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->codigo_postal != "")
        <tr>
            <td>Codigo Postal: {{$datos['oficios']->PersonaDatosLaborales->codigo_postal or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->provincia != "" && $datos['oficios']->PersonaDatosLaborales->provincia->id != 25)
        <tr>
            <td>Provincia: {{$datos['oficios']->PersonaDatosLaborales->provincia->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->partido != "")
        <tr>
            <td>Partido: {{$datos['oficios']->PersonaDatosLaborales->partido->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosLaborales->localidad != "")
        <tr>
            <td>Barrio - Localidad: {{$datos['oficios']->PersonaDatosLaborales->localidad or ''}}</td>
        </tr>
        @endif
    </table>
</div>
@endif
@endif
@if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia))
@if(($datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_ultima_vez != null &&  $datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_ultima_vez != "") || ($datos['oficios']->PersonaDatosRelacionadoAusencia->hora_ultima_vez != null && $datos['oficios']->PersonaDatosRelacionadoAusencia->hora_ultima_vez != "" )|| $datos['oficios']->PersonaDatosRelacionadoAusencia->lugar_ultima_vez != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->descripcion_vestimenta != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->detalle_drogas != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->adiccion_alcohol != '1' || $datos['oficios']->PersonaDatosRelacionadoAusencia->tratamiento_institucion != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_institucion != ''|| $datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_mental != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_institucion != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_padecimiento_mental != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->id_institucion_momento_ausencia != 1 || $datos['oficios']->PersonaDatosRelacionadoAusencia->institucion_momento_ausencia != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->autoridad_ordeno_medida != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->direccion != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->nro != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->piso != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->dpto != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->codigo_postal != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->id_provincia != 25 || $datos['oficios']->PersonaDatosRelacionadoAusencia->id_partido != 0 || $datos['oficios']->PersonaDatosRelacionadoAusencia->localidad != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->situacion_calle != '1' || $datos['oficios']->PersonaDatosRelacionadoAusencia->lugares_frecuentaba != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_enfermedad != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_alguna_medicacion != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_situacion_conflicto != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->sustraccion_parental != '1' || $datos['oficios']->PersonaDatosRelacionadoAusencia->primera_vez_ausenta != '1' || $datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_documentacion_consigo != '' || $datos['oficios']->PersonaDatosRelacionadoAusencia->compania_alguien != '')
<h4>Datos relacionados con la ausencia</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_ultima_vez != "")
        <tr>
            <td>Fecha en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_ultima_vez or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->hora_ultima_vez != "")
        <tr>
            <td>Hora en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->hora_ultima_vez or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->lugar_ultima_vez != "")
        <tr>
            <td>Lugar en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->lugar_ultima_vez or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->descripcion_vestimenta != "")
        <tr>
            <td>Descripcion de la vestimenta: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->descripcion_vestimenta or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->detalle_drogas != "")
        <tr>
            <td>Detalle de las drogas: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->detalle_drogas or ''}}</td>
        </tr>
        @endif
        @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia->adiccionAlcohol))
        <tr>
            <td>¿Padece adicción al alcohol?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->adiccionAlcohol->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->tratamiento_institucion != "")
        <tr>
            <td>¿Realizó o realiza tratamiento en alguna institucion? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->tratamiento_institucion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_institucion != "")
        <tr>
            <td>Fecha de tratamiento {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_institucion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_mental != "")
        <tr>
            <td>¿Padecimiento mental? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_mental or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_institucion != "")
        <tr>
            <td>¿Realizó o realiza tratamiento en alguna institucion? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_institucion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_padecimiento_mental != "")
        <tr>
            <td>Fecha de tratamiento por padecimiento mental: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_padecimiento_mental or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->institucion != "")
        <tr>
            <td>¿Se encontraba en el momento de la ausencia en alguna institucion?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->institucion->institucion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->institucion_momento_ausencia != "")
        <tr>
            <td>Indique en cual: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->institucion_momento_ausencia or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria != "")
        <tr>
            @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia))
            @if($datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria == 'V')
            <td>Acerca de la internación: Voluntaria</td>
            @elseif($datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria == 'C')
            <td>Acerca de la internación: Compulsiva</td>
            @else
            <td>Acerca de la internación: </td>
            @endif
            @endif
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->autoridad_ordeno_medida != "")
        <tr>
            <td>Autoridad que ordenó la medida en su caso: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->autoridad_ordeno_medida or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->direccion != "")
        <tr>
            <td>Direccion: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->direccion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->nro != "")
        <tr>
            <td>Nro: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->nro or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->piso != "")
        <tr>
            <td>Piso: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->piso or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->dpto != "")
        <tr>
            <td>Depto: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->dpto or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->codigo_postal != "")
        <tr>
            <td>Código postal: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->codigo_postal or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->provincia != "" && $datos['oficios']->PersonaDatosRelacionadoAusencia->provincia->id != 25)
        <tr>
            <td>Provincia: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->provincia->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->partido != "")
        <tr>
            <td>Partido: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->partido->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->localidad != "")
        <tr>
            <td>Barrio - Localidad: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->localidad or ''}}</td>
        </tr>
        @endif
        @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia->situacionCalle))
        <tr>
            <td>¿Se encontraba en situacion de calle?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->situacionCalle->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->lugares_frecuentaba != "")
        <tr>
            <td>Lugares que la persona solia frecuentar: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->lugares_frecuentaba or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_enfermedad != "")
        <tr>
            <td>¿Posee alguna enfermedad? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_enfermedad or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico != "")
        <tr>
            <td>¿Realiza algun tratamiento medico? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_alguna_medicacion != "")
        <tr>
            <td>¿Necesita de alguna medicacion? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_alguna_medicacion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_situacion_conflicto != "")
        <tr>
            <td>¿Se encontraba en situacion de conflicto en el ultimo tiempo? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_situacion_conflicto or ''}}</td>
        </tr>
        @endif
        @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia->sustraccionParental))
        <tr>
            <td>¿Se trata de una sustraccion parental?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->sustraccionParental->descripcion or ''}}</td>
        </tr>
        @endif
        @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia->primeraVezAusenta))
        <tr>
            <td>¿Es la primera vez que se ausenta?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->primeraVezAusenta->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_documentacion_consigo != "")
        <tr>
            <td>¿Llevaba documentacion consigo? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_documentacion_consigo or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosRelacionadoAusencia->compania_alguien != "")
        <tr>
            <td>¿Podria encontrarse en compañía de alguien? Indique quien y aporte datos de identificación: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->compania_alguien or ''}}</td>
        </tr>
        @endif

    </table>
</div>
@endif
@endif
@if(is_object($datos['oficios']->PersonaDatosDenunciante))
<h4>Datos del denunciante</h4>
<div class="table-responsive">
    <table class="table table-bordered">
        @if($datos['oficios']->PersonaDatosDenunciante->apellido != "")
        <tr>
            <td>Apellido: {{$datos['oficios']->PersonaDatosDenunciante->apellido or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->nombre != "")
        <tr>
            <td>Nombre: {{$datos['oficios']->PersonaDatosDenunciante->nombre or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->tipoDocumento != "")
        <tr>
            <td>Tipo de documento: {{$datos['oficios']->PersonaDatosDenunciante->tipoDocumento->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->nro_documento != "")
        <tr>
            <td>Nro de documento: {{$datos['oficios']->PersonaDatosDenunciante->nro_documento or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->objVinculo != "")
        <tr>
            <td>Vínculo: {{$datos['oficios']->PersonaDatosDenunciante->objVinculo->vinculo or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->direccion != "")
        <tr>
            <td>Direccion: {{$datos['oficios']->PersonaDatosDenunciante->direccion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->nro != "")
        <tr>
            <td>Nro: {{$datos['oficios']->PersonaDatosDenunciante->nro or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->piso != "")
        <tr>
            <td>Piso: {{$datos['oficios']->PersonaDatosDenunciante->piso or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->dpto!= "")
        <tr>
            <td>Depto: {{$datos['oficios']->PersonaDatosDenunciante->dpto or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->codigo != "")
        <tr>
            <td>Código postal: {{$datos['oficios']->PersonaDatosDenunciante->codigo_postal or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->provincia != "")
        <tr>
            <td>Provincia: {{$datos['oficios']->PersonaDatosDenunciante->provincia->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->partido != "")
        <tr>
            <td>Partido: {{$datos['oficios']->PersonaDatosDenunciante->partido->descripcion or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->cod_area_telefono != "")
        <tr>
            <td>Cod de area: {{$datos['oficios']->PersonaDatosDenunciante->cod_area_telefono or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->telefono != "")
        <tr>
            <td>Telefono: {{$datos['oficios']->PersonaDatosDenunciante->telefono or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->dependencia != "")
        <tr>
            <td>Dependencia: {{$datos['oficios']->PersonaDatosDenunciante->dependencia or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->fecha_denuncia != "")
        <tr>
            <td>fecha de la denuncia: {{$datos['oficios']->PersonaDatosDenunciante->fecha_denuncia or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->relato != "")
        <tr>
            <td>Relato: {{$datos['oficios']->PersonaDatosDenunciante->relato or ''}}</td>
        </tr>
        @endif
        @if($datos['oficios']->PersonaDatosDenunciante->intervencion_otros_organismos != "")
        <tr>
            <td>Intervención de otros organismos: {{$datos['oficios']->PersonaDatosDenunciante->intervencion_otros_organismos or ''}}</td>
        </tr>
        @endif
    </table>
</div>
@endif
@include('solo_lectura.autoridad_judicial')
</div>
@stop