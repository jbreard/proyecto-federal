<div class="span5">
    <h4>Autorización de salida del País</h4>
    <table class="table table-bordered table-hover">
        <tr>
            <th>Inicio</th>
            <th>Final</th>
            <th>Destino</th>
            <th>Observaciones</th>
            <th>Archivos</th>
        </tr>
        @foreach($datos['plazos'] as $plazo)
            <tr>
                <td>{{$plazo->fecha_inicio}}</td>
                <td>{{$plazo->fecha_final}}</td>
                <td>{{$plazo->destino}}</td>
                <td>{{$plazo->observaciones}}</td>
                <td>
                    @if(Request::segment(2) !="pdf")
                    @foreach ($plazo->archivos as $archivo)
                        <a href="{{ Route('archivos.descargar')}}?q={{ $archivo->path }}">{{ $archivo->created_at }} <i class="fa fa-download"></i></a>
                    @endforeach
                    @endif
                </td>
            </tr>
        @endforeach
    </table>

</div>
