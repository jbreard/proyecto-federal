<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 7/22/20
 * Time: 1:03 p.m.
 */
if($imgDatos['ext']=='pdf'){
    header("Content-type: application/pdf");
    header('Content-disposition: attachment; filename="thing.pdf"');
    header("Content-Length: " . strlen($imgDatos['content']));
    print $imgDatos['content'];
}elseif($imgDatos['ext']=='doc'){
    //no anda
    header("Content-type: word/document.xml");
    header('Content-disposition: attachment; filename="Documento.docx"');
    header("Content-Length: " . strlen($imgDatos['content']));
    print $imgDatos['content'];
}
else {
    echo '<img src="data:image/jpeg;base64,'.base64_encode($imgDatos['content']) .' "/>';
}