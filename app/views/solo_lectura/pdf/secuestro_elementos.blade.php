@include('mensaje_activo_cesado',$datos)
<h3>  Secuestro de Elementos @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h3>

<div class="span11">
    <div class="span5">
        <h4>
            Datos del Elemento
        </h4>

        <table class="table table-bordered table-hover">
            <tr><td>Tipo de Elemento: {{ $datos['secuestro_elementos']->tipo_elemento }}</td></tr>
            <tr><td>Identificacion/Descripcion: {{ $datos['secuestro_elementos']->identificacion }}</td></tr>
            <tr><td>Datos Adicionales: {{ $datos['secuestro_elementos']->marca }}</td></tr>
        </table>
    </div>
    <div class="span5">
        <h4>
            Datos del Titular
        </h4>

        <table class="table table-bordered table-hover">

            <tr>
                <td>Nombre:  {{ $datos['titular']->nombre }}  </td>
            </tr>
            <tr>
                <td>Apellido:  {{ $datos['titular']->apellido }} </td>
            </tr>
            <tr>
                <td>Apellido Materno:  {{ $datos['titular']->apellido_materno }} </td>
            </tr>
            <tr>
                <td>Sexo:  {{ $datos['titular']->sexo->descripcion }} </td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['titular']->genero->descripcion)){{$datos['titular']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento:  {{ $datos['titular']->edad }}  </td>
            </tr>
            <tr>
                <td>Tipo de Documento:  {{ $datos['titular']->tipoDocumento->descripcion }}  </td>
            </tr>
            <tr>
                <td>N&deg; de Documento:  {{ $datos['titular']->nro_documento }}  </td>
            </tr>
            <tr>
                <td>Nacionalidad:  {{ $datos['titular']->nacionalidad->descripcion }}  </td>
            </tr>
            <tr>
                <td>Direccion:  {{ $datos['titular']->ultimo_calle }}  </td>
            </tr>
            <tr>
                <td>Interseccion:  {{ $datos['titular']->interseccion }}  </td>
            </tr>
            <tr>
                <td>N&deg;:  {{ $datos['titular']->ultimo_numero }} </td>
            </tr>
            <tr>
                <td>Piso:  {{ $datos['titular']->ultimo_piso }} </td>
            </tr>
            <tr>
                <td>Departamento:  {{ $datos['titular']->ultimo_departamento }} </td>
            </tr>
            <tr>
                <td>Provincia:  {{ $datos['titular']->ultimaProvincia->descripcion }} </td>
            </tr>
            <tr>
                <td>Partido:  {{ $datos['titular']->ultimoPartido->descripcion }} </td>
            </tr>
            <tr>
                <td>Localidad:  {{ $datos['titular']->ultima_localidad }} </td>
            </tr>
            <tr>
                <td>Observaciones:  {{ $datos['titular']->observaciones_persona }} </td>
            </tr>


        </table>
    </div>
</div>
@include('solo_lectura.autoridad_judicial')
