@include('mensaje_activo_cesado',$datos)
<h3> {{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h3>
<div class="span11">
    <div class="span5">
        <h4>
            Datos de la Persona
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre:  {{ $datos['buscado']->nombre }}  </td>
            </tr>
            <tr>
                <td>Apellido:  {{ $datos['buscado']->apellido }} </td>
            </tr>
            <tr>
                <td>Apellido Materno:  {{ $datos['buscado']->apellido_materno }} </td>
            </tr>
            <tr>
                <td>Sobrenombre o Apodo:  {{ $datos['buscado']->apodos }} </td>
            </tr>
            <tr>
                <td>Sexo:  {{ $datos['buscado']->sexo->descripcion }} </td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['buscado']->genero->descripcion)){{$datos['buscado']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento:  {{ $datos['buscado']->edad }}  </td>
            </tr>
            <tr>
                <td>Tipo de Documento:  {{ $datos['buscado']->tipoDocumento->descripcion }}  </td>
            </tr>
            <tr>
                <td>N&deg; de Documento:  {{ $datos['buscado']->nro_documento }}  </td>
            </tr>
            <tr>
                <td>Nacionalidad:  {{ $datos['buscado']->nacionalidad->descripcion }}  </td>
            </tr>
            <tr>
                <td>Observaciones:  {{ $datos['buscado']->observaciones_persona }}  </td>
            </tr>

        </table>

    </div>

    <div class="span5">

        <h4>
            &Uacute;ltimo Domicilio Conocido
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Direccion:  {{ $datos['buscado']->ultimo_calle }}  </td>
            </tr>
            <tr>
                <td>N&deg;:  {{ $datos['buscado']->ultimo_numero }} </td>
            </tr>
            <tr>
                <td>Interseccion:  {{ $datos['buscado']->interseccion }}  </td>
            </tr>
            <tr>
                <td>Piso:  {{ $datos['buscado']->ultimo_piso }} </td>
            </tr>
            <tr>
                <td>Departamento:  {{ $datos['buscado']->ultimo_departamento }} </td>
            </tr>
            <tr>
                <td>CP:  {{ $datos['buscado']->ultimo_codigo_postal }}</td>
            </tr>
            <tr>
                <td>Provincia:  {{ $datos['buscado']->ultimaProvincia->descripcion }} </td>
            </tr>
            <tr>
                <td>Partido:  {{ $datos['buscado']->ultimoPartido->descripcion }} </td>
            </tr>
            <tr>
                <td>Localidad:  {{ $datos['buscado']->ultima_localidad }} </td>
            </tr>
            <tr>
                <td>Observaciones:  {{ $datos['oficios']->observaciones }} </td>
            </tr>
        </table>
    </div>
    @include('galeria',["fotos"=>$datos['buscado']->getFotos,"titulo"=>"Fotos"])
    @include('solo_lectura.autoridad_judicial')
    @include('solo_lectura.plazos')



    <!-- <fieldset>
        <h4>
            Fotografias de la Persona
        </h4>
        <div class="span10 hero-unit " id="box-foto">
            <div  class="span10">
               @{{ macro.galeriaFotos(fotos) }}
            </div>
        </div>
    </fieldset>-->
</div>