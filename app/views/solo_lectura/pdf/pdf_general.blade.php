<!DOCTYPE html>
<html>
    <head>
        <title>Sistema Integrado de Requerimientos Judiciales</title>
        <meta charset="UTF-8">
        <style>
            table {
                border-spacing: 0;
                border-collapse: collapse;
                width: 100%;
            }
            td,
            th {
                padding: 0;
            }
            .table-bordered th,
            .table-bordered td {
                border: 1px solid #ddd !important;
            }
            .alert{
                margin-top: 10px;
                border: solid 1px #000000;
                padding: 10px;
            }
            @page { margin: 180px 50px; }
                header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; text-align: center; }
                /*header #info {width:100%; margin: 0;}*/
                /*header #info #hora{text-align: left;}*/
                /*header #info .pagenro{text-align: right;}*/

                header .page:after { content: counter(page); }
                footer { left: 0px; bottom: -180px; right: 0px; height: 150px;}
                footer p {text-align: center;}
                

                




        </style>
    </head>
    <body>
        <header>

            <table>
                <tr><td>{{ date("d/m/Y H:i:s") }}</td><td><span class="pagenro">Nº:</span><span class="page pagenro"></span></td></tr>
            </table>


            <center><img src="{{ URL::asset('bootstrap/images/logo-minseg2.png')}}" /></center>
            <br>
            <center> Secretaria de Seguridad y Politica Criminal</center>
            <hr>
            {{Config::get('nombre.nombre')}}
        </header>
            {{ $html }}

        <footer>

            <p>Incorporado a la Base de Datos del {{Config::get('nombre.siglas')}}</p>
        </footer>
    </body>
</html>
