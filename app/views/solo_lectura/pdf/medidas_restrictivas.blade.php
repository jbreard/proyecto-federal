@include('mensaje_activo_cesado',$datos)
<h2>Medidas Restrictivas @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif</h2>
<div class="span11">
    <div class="span5">
        <h4>
            Datos del Denunciado
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Medida Solicitada: {{ $datos['oficios']->medida_restrictiva }}</td>
            </tr>
            <tr>
                <td>Nombre: {{ $datos['responsable']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['responsable']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno: {{ $datos['responsable']->apellido_materno }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['responsable']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['responsable']->genero->descripcion)){{$datos['responsable']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['responsable']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['responsable']->tipoDocumento->descripcion }}</td>
            </tr>
            <tr>
                <td>N&deg; de Documento: {{ $datos['responsable']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['responsable']->nacionalidad->descripcion }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['responsable']->observaciones_persona }}</td>
            </tr>



        </table>

    </div>
    <div class="span5">

        <h4>
            &Uacute;ltimo Domicilio Conocido
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Direccion: {{ $datos['responsable']->ultimo_calle }}</td>
            </tr>
            <tr>
                <td>N&deg;: {{ $datos['responsable']->ultimo_numero }}</td>
            </tr>
            <tr>
                <td>Piso: {{ $datos['responsable']->ultimo_piso }}</td>
            </tr>
            <tr>
                <td>Interseccion: {{ $datos['responsable']->interseccion }}</td>
            </tr>
            <tr>
                <td>Departamento: {{ $datos['responsable']->ultimo_departamento }}</td>
            </tr>
            <tr>
                <td>Provincia: {{ $datos['responsable']->ultimaProvincia->descripcion }}</td>
            </tr>
            <tr>
                <td>Partido: {{ $datos['responsable']->ultimoPartido->descripcion }}</td>
            </tr>
            <tr>
                <td>Localidad: {{ $datos['responsable']->ultima_localidad }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['responsable']->observaciones }}</td>
            </tr>
        </table>
    </div>
</div>
    @include("galeria",["fotos"=>$datos['responsable']->getFotos,"titulo"=>'Fotos del Denunciado'])
<div class="span11">
    <div class="span5">
        <h4>
            Datos de la V&iacute;ctima
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre: {{ $datos['victima']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['victima']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno: {{ $datos['victima']->apellido_materno }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['victima']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['victima']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['victima']->tipoDocumento->descripcion }}</td>
            </tr>
            <tr>
                <td>N&deg; de Documento: {{ $datos['victima']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['victima']->nacionalidad->descripcion }}</td>
            </tr>
            <tr>
                <td>Dirección: {{ $datos['victima']->ultimo_calle }}</td>
            </tr>
            <tr>
                <td>N°: {{ $datos['victima']->ultimo_numero }}</td>
            </tr>
            <tr>
                <td>Piso: {{ $datos['victima']->ultimo_piso }}</td>
            </tr>
            <tr>
                <td>Interseccion: {{ $datos['victima']->interseccion }}</td>
            </tr>
            <tr>
                <td>Departamento: {{ $datos['victima']->ultimo_departamento }}</td>
            </tr>
            <tr>
                <td>Provincia: {{ $datos['victima']->ultimaProvincia->ultima_provincia }}</td>
            </tr>
            <tr>
                <td>Localidad: {{ $datos['victima']->ultima_localidad }}</td>
            </tr>
            <tr>
                <td>Datos Adicionales: {{ $datos['victima']->observaciones }}</td>
            </tr>
        </table>
    </div>
    @include("galeria",["fotos"=>$datos['victima']->getFotos,"titulo"=>'Fotos de la Victima'])
   @include('solo_lectura.autoridad_judicial')
</div>
