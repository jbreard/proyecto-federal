<header style="margin-top: 50px">
        <section class="imgLogo">
            <img src="{{ URL::asset('bootstrap/images/logo_secretaria.png')}}" width="250" height="125" />
        </section>
    <section style="display: inline-block; margin-top: 100px">
        <section class="ref1">
            Ref: CUDAP:
        </section>
        </hr>
        <section class="ref2">
            <strong>Req: Sifcop: {{ $oficio->id }}</strong>
        </section>
        </hr>
        <section class="notaNro">
            Nota N°&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/SCPJMPyL/2015
        </section>
        </hr>
        <section class="lugar">
            BUENOS AIRES,
        </section>
    </section>
</header>
<section class="contenido">
    <p class="mayuscula">{{ $destinatario }}</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--        Tengo el agrado de dirigirme a Usted, con relación al Expediente de referencia, librado en el marco de la Causa {{$oficio->nro_causa}} - {{$oficio->caratula}}, en trámite por ante {{ $asucargo }} a su cargo, mediante el cual se ordena {{ $oficio->tipo->descripcion_oficio }} {{ $responsable->nombre }} {{ $responsable->apellido }}, DNI: {{ $responsable->nro_documento }}.<p>-->
        Tengo el agrado de dirigirme a Usted, con relación al Expediente de referencia, por el que tramita el Oficio Judicial de fecha {{ $fecha_oficio[0] }} de {{ $fecha_oficio[1] }} de {{ $fecha_oficio[2] }}, librado en el marco de la Causa N° {{$oficio->nro_causa}} , caratulada: “{{ strtoupper($oficio->caratula) }}", en trámite por ante el Juzgado a su cargo, mediante el cual se ordena @if($oficio->cesado == 1) DEJAR SIN EFECTO @endif {{ $oficio->tipo->descripcion_oficio }} de {{ strtoupper($responsable->nombre) }} {{ strtoupper($responsable->apellido) }}, DNI N° {{ $responsable->nro_documento }}.<p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Al respecto, le informo que con fecha {{ $fecha[0] }} de {{ $fecha[1] }} de {{ $fecha[2] }}, se efectuó la retransmisión correspondiente a través del Registro N° {{ $oficio->id }}, por medio del Sistema Federal de Comunicaciones Policiales –SIFCOP- del MINISTERIO DE SEGURIDAD, dando así cumplimiento a lo solicitado en la mencionada Manda Judicial.</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Por último, dejo a su disposición los datos de la Oficina Central del SIFCOP, a los efectos de tomar contacto en cuanto entienda pueda ser útil para diligenciar los correspondientes Requerimientos Judiciales:  Te. (011) 5278-9800 int. 525 / Fax. (011) 5278-9728 / correo electrónico: ofcentral.sifcop@minseg.gob.ar</p>


    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Saludo a Ud atentamente.</p>

</section>
<footer>
    <p class="mayuscula">
        {{ $dar_senor_senora }}
    </p>
    <p class="mayuscula">
        {{ $nombre_fiscalia_juzgado }}
    </p>
    <p class="mayuscula">
        {{ $nombre_juez_fiscal }}
    </p>
    <p class="mayuscula">
        S.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D.
    </p>
</footer>