@include('mensaje_activo_cesado',$datos)
<div class="span11">
    <h2> {{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h2>

    <div class="span5">
        <h4 class="">
            Datos de La Persona Buscada
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre: {{ $datos['buscado']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['buscado']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno:  {{ $datos['buscado']->apellido_materno }} </td>
            </tr>
            <tr>
                <td>Sobrenombre: {{ $datos['buscado']->apodos }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['buscado']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Genero: {{$datos['buscado']->genero->descripcion or ''}}</td>

            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['buscado']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['buscado']->tipoDocumento->descripcion}}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['buscado']->nacionalidad->descripcion}}</td>
            </tr>
            <tr>
                <td>N° de Documento: {{ $datos['buscado']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Codigo De Area: {{ $datos['buscado']->codArea_celular }}</td>
            </tr>
            <tr>
                <td>N°de Celular: {{ $datos['buscado']->numero_celular }}</td>
            </tr>
            <tr>
                <td>Compañia de Celular: {{ $datos['buscado']->companiaCelular->descripcion }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['buscado']->observaciones_persona }}</td>
            </tr>
        </table>
    </div>
    @include('galeria',array("titulo"=>"Fotos de la Persona Buscada","fotos"=>$datos['buscado']->getFotos))
    <div class="span5">
        <h4>
            &Uacute;ltimo Domicilio Conocido
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Direccion: {{ $datos['buscado']->ultimo_calle }}</td>
            </tr>
            <tr>
                <td>Interseccion: {{ $datos['buscado']->interseccion }}</td>
            </tr>

            <tr>
                <td>N&deg;: {{ $datos['buscado']->ultimo_numero }}</td>
            </tr>
            <tr>
                <td>Piso: {{ $datos['buscado']->ultimo_piso }}</td>
            </tr>
            <tr>
                <td>Departamento: {{ $datos['buscado']->ultimo_departamento }}</td>
            </tr>
            <tr>
                <td>CP: {{ $datos['buscado']->ultimo_codigo_postal }}</td>
            </tr>

            <tr>
                <td>Provincia: {{ (isset($datos['buscado']->ultimaProvincia->descripcion)) ? $datos['buscado']->ultimaProvincia->descripcion : '' }}</td>
            </tr>
            <tr>
                <td>Partido: {{ (isset($datos['buscado']->ultimoPartido->descripcion)) ? $datos['buscado']->ultimoPartido->descripcion : ''  }}</td>
            </tr>
            <tr>
                <td>Localidad: {{ $datos['buscado']->ultima_localidad }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['buscado']->observaciones_domicilio }}</td>
            </tr>

        </table>
    </div>
    <div class="span5">
        <h4>
            Tarjetas de Credito
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>N° Tarjeta de Crédito: {{ $datos['buscado']->nro_tarjeta_credito }}</td>
            </tr>
            <tr>
                <td>Banco Emisor: {{ $datos['buscado']->bco_tarjeta_credito }}</td>
            </tr>

        </table>
    </div>
</div>
<div class="span11">

    <div class="span5">
        <h4>
            Datos Desaparición
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Fecha en la que fue vista por última vez: {{ $datos['solicitud_paradero']->fecha_visto }}</td>
            </tr>
            <tr>
                <td>Hora en la que fue vista por última vez: {{ $datos['solicitud_paradero']->hora_visto }}</td>
            </tr>
            <tr>
                <td>Lugar en la que fue vista por última vez: {{ $datos['solicitud_paradero']->lugar_visto }}</td>
            </tr>
            <tr>
                <td>¿ Es la primera vez que se extravía ? : {{
                    $datos['solicitud_paradero']->esPrimeraDesaparece->descripcion }}
                </td>
            </tr>
            <tr>
                <td>¿ Posee alguna enfermedad ?: {{ $datos['solicitud_paradero']->esEnfermedad->descripcion}}</td>
            </tr>
            <tr>
                <td>¿Es consumidor de estupefacientes/alcohol?: {{ $datos['solicitud_paradero']->esDrogadiccion->descripcion
                    }}
                </td>
            </tr>
            <tr>
                <td>¿ Se encontraba en alguna institución ?: {{
                    $datos['solicitud_paradero']->esInternadoInstitucion->descripcion }}
                </td>
            </tr>
            <tr>
                <td>Núcleo de referencia o pertenencia: {{ $datos['solicitud_paradero']->pertenencia_referencia }}</td>
            </tr>
            <tr>
                <td>Lugares de concurrencia frecuente: {{ $datos['solicitud_paradero']->concurrencia }}</td>
            </tr>
            <tr>
                <td>Descripción de la vestimenta que poseía la última vez que fue vista: {{ $datos['solicitud_paradero']->vestimenta }}
                </td>
            </tr>
            <tr>
                <td>Descripción fisica de la persona: {{ $datos['solicitud_paradero']->descripcion_fisica }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['solicitud_paradero']->observaciones }}</td>
            </tr>
        </table>
    </div>

    <div class="span5">
        <h4>
            Datos del Denunciante
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Apellido: {{ $datos['denunciante']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno: {{ $datos['denunciante']->apellido_materno }}</td>
            </tr>
            <tr>
                <td>Nombre: {{ $datos['denunciante']->nombre }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['denunciante']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['denunciante']->edad }}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['denunciante']->nacionalidad->descripcion }}</td>
            </tr>
            <tr>
                <td>Provincia: {{ $datos['denunciante']->provincia->descripcion }}</td>
            </tr>
            <tr>
                <td>Tipo Documento: {{ $datos['denunciante']->tipoDocumento->descripcion }}</td>
            </tr>
            <tr>
                <td>N° de documento: {{ $datos['denunciante']->nro_documento }}</td>
            </tr>

            <tr>
                <td>Calle: {{ $datos['denunciante']->ultimo_calle }}</td>
            </tr>

            <tr>
                <td>N°: {{ $datos['denunciante']->ultimo_numero }}</td>
            </tr>

            <tr>
                <td>Piso: {{ $datos['denunciante']->ultimo_piso }}</td>
            </tr>

            <tr>
                <td>Departamento: {{ $datos['denunciante']->ultimo_departamento }}</td>
            </tr>

            <tr>
                <td>CP: {{ $datos['denunciante']->ultimo_codigo_postal }}</td>
            </tr>
        </table>
    </div>
<!--	
    @include('solo_lectura.autoridad_judicial')
-->    <!--
        <fieldset>
            <h4>
                Fotografias de la Persona
            </h4>
            <div class="span10 hero-unit " id="box-foto">
                <div  class="span10">
                </div>
            </div>
        </fieldset>
    -->
</div>
