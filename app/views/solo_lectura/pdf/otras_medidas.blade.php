@include('mensaje_activo_cesado',$datos)
<h2>Otras Medidas @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif</h2>
<div class="span11">
        <h4>
            Datos del la Medida:
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Medida Solicitada: {{ $datos['oficios']->medida_restrictiva }}</td>
            </tr>
        </table>

@if(!empty($datos["responsable"]))
    <div class="span5">
        <h4>
            Datos de la Persona
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Medida Solicitada: {{ $datos['oficios']->medida_restrictiva }}</td>
            </tr>
            <tr>
                <td>Nombre: {{ $datos['responsable']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['responsable']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno: {{ $datos['responsable']->apellido_materno }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['responsable']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['responsable']->genero->descripcion)){{$datos['responsable']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['responsable']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['responsable']->tipoDocumento->descripcion }}</td>
            </tr>
            <tr>
                <td>N&deg; de Documento: {{ $datos['responsable']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['responsable']->nacionalidad->descripcion }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['responsable']->observaciones_persona }}</td>
            </tr>



        </table>

    </div>
@endif

            <h4>
                Descripcion:
            </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Descripcion: {{ $datos['responsable']->observaciones_persona }}</td>
            </tr>
        </table>

    </div>
    @include('solo_lectura.autoridad_judicial')
</div>


