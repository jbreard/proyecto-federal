 @include('mensaje_activo_cesado',$datos)
<h3>  {{ $datos[0] }} @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h3>
<div class="span11">

        <div class="span5">
            <h4>
                Datos del Veh&iacute;culo
            </h4>

            <table class="table table-bordered table-hover">
                <tr><td>Dominio: {{ $datos['secuestro_vehicular']->dominio }}</td></tr>
                <tr><td>Nro. Chasis: {{ $datos['secuestro_vehicular']->nro_chasis }}</td></tr>
                <tr><td>Nro. Motor: {{ $datos['secuestro_vehicular']->nro_motor }}</td></tr>
                <tr><td>Año: {{ $datos['secuestro_vehicular']->anio_vehiculo }}</td></tr>
                <tr><td>Lugar de radicación del Vehículo: {{ $datos['secuestro_vehicular']->radicacion }}</td></tr>
                <tr><td>Tipo Vehiculo: {{ $datos['secuestro_vehicular']->getTipoVehiculo->descripcion}}</td></tr>
                @if(!empty($datos['secuestro_vehicular']->otro_tipo_vehiculo))
                    <tr><td>Otro Tipo Vehiculo: {{ $datos['secuestro_vehicular']->otro_tipo_vehiculo }}</td></tr>
                @endif

                    <tr><td>Marca: {{ $datos['secuestro_vehicular']->getMarca->marca}}</td></tr>
                @if(!empty($datos['secuestro_vehicular']->otra_marca))
                    <tr><td>Otra Marca: {{$datos['secuestro_vehicular']->otra_marca}}</td></tr>
                @endif
                <tr><td>Modelo Vehiculo: {{ $datos['secuestro_vehicular']->getModelo->modelo}}</td></tr>

                <tr><td>Otro Modelo: {{$datos['secuestro_vehicular']->otro_modelo}}</td></tr>

		

                <tr><td>Marca Motor: {{ $datos['secuestro_vehicular']->marca_motor}}</td></tr>
                <tr><td>Color: {{ $datos['secuestro_vehicular']->color}}</td></tr>


            </table>
        </div>
    @if(isset($datos['titular']))
        <div class="span5">
            <h4>
                Datos del Titular
            </h4>

            <table class="table table-bordered table-hover">

                <tr>
                    <td>Nombre:  {{ $datos['titular']->nombre }}  </td>
                </tr>
                <tr>
                    <td>Apellido:  {{ $datos['titular']->apellido }} </td>
                </tr>
                <tr>
                    <td>Apellido Materno:  {{ $datos['titular']->apellido_materno }} </td>
                </tr>
                <tr>
                    <td>Sexo:  {{ $datos['titular']->sexo->descripcion }} </td>
                </tr>
                <tr>
                    <td>Genero: @if(!empty($datos['titular']->genero->descripcion)){{$datos['titular']->genero->descripcion}}@endif</td>
                </tr>
                <tr>
                    <td>Fecha de Nacimiento:  {{ $datos['titular']->edad }}  </td>
                </tr>
                <tr>
                    <td>Tipo de Documento:  {{ $datos['titular']->tipoDocumento->descripcion }}  </td>
                </tr>
                <tr>
                    <td>N&deg; de Documento:  {{ $datos['titular']->nro_documento }}  </td>
                </tr>
                <tr>
                    <td>Nacionalidad:  {{ $datos['titular']->nacionalidad->descripcion }}  </td>
                </tr>
                <tr>
                    <td>Direccion:  {{ $datos['titular']->ultimo_calle }}  </td>
                </tr>
                <tr>
                    <td>Interseccion:  {{ $datos['titular']->interseccion }}  </td>
                </tr>
                <tr>
                    <td>N&deg;:  {{ $datos['titular']->ultimo_numero }} </td>
                </tr>
                <tr>
                    <td>Piso:  {{ $datos['titular']->ultimo_piso }} </td>
                </tr>
                <tr>
                    <td>Departamento:  {{ $datos['titular']->ultimo_departamento }} </td>
                </tr>
                <tr>
                    <td>CP:  {{ $datos['titular']->ultimo_codigo_postal }}</td>
                </tr>
                <tr>
                    <td>Provincia:  {{ $datos['titular']->ultimaProvincia->descripcion }} </td>
                </tr>
                <tr>
                    <td>Partido:  {{ $datos['titular']->ultimoPartido->descripcion }} </td>
                </tr>
                <tr>
                    <td>Localidad:  {{ $datos['titular']->ultima_localidad }} </td>
                </tr>
                <tr>
                    <td>Observaciones:  {{ $datos['titular']->observaciones_persona }} </td>
                </tr>


            </table>
        </div>
    @endif
    @if(!empty($datos["responsable"]))

    <div class="span5">
        <h4>
            Datos del Denunciante
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre: {{ $datos['responsable']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['responsable']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno: {{ $datos['responsable']->apellido_materno }}</td>
            </tr>
	    <tr>
		<td>Sobrenombre: {{ $datos['responsable']->apodos }}</td>
	    </tr>
            <tr>
                <td>Sexo: {{ $datos['responsable']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['responsable']->genero->descripcion)){{$datos['responsable']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['responsable']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['responsable']->tipoDocumento->descripcion }}</td>
            </tr>
            <tr>
                <td>N&deg; de Documento: {{ $datos['responsable']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['responsable']->nacionalidad->descripcion }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['responsable']->observaciones_persona }}</td>
            </tr>

        </table>

    </div>
@endif

    {{--@if(!isset($robo_automotor))--}}
    @if(count($datos['secuestro_vehicular']->getFotos))
        @include('galeria',array("titulo"=>"Fotografias del Vehiculo","fotos"=>$datos['secuestro_vehicular']->getFotos))
    @endif
    {{--@endif--}}
    @include('solo_lectura.autoridad_judicial')
</div>
