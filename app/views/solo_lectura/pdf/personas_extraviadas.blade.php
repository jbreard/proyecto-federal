@include('mensaje_activo_cesado',$datos)
<div class="span5">
<h2> Búsqueda de Personas Extraviadas @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h2>
    <h4>Datos de Persona Buscada</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Apellido: {{$datos['oficios']->PersonaBuscada->apellido or ''}}</td>
            </tr>
            <tr>
                <td>Nombre: {{$datos['oficios']->PersonaBuscada->nombres or ''}}</td>
            </tr>
            <tr>
                <td>Fecha de nacimiento: {{$datos['oficios']->PersonaBuscada->fecha_nacimiento or ''}}</td>
            </tr>
            <tr>
                <td>Edad: {{$datos['oficios']->PersonaBuscada->edad or ''}}</td>
            </tr>
            <tr>
                <td>Sombrenombre: {{$datos['oficios']->PersonaBuscada->sobrenombre or ''}}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{$datos['oficios']->PersonaBuscada->nacionalidad->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Sexo: {{$datos['oficios']->PersonaBuscada->objSexo->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Genero: {{$datos['oficios']->PersonaBuscada->objGenero->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Estado civil: {{$datos['oficios']->PersonaBuscada->estadoCivil->estado_civil or ''}}</td>
            </tr>
            <tr>
                <td>Nombre cónyuge: {{$datos['oficios']->PersonaBuscada->nombre_conyuge or ''}}</td>
            </tr>
            <tr>
                <td>Apellido y nombre paterno: {{$datos['oficios']->PersonaBuscada->apellido_nombre_paterno or ''}}</td>
            </tr>
            <tr>
                <td>Apellido y nombre materno: {{$datos['oficios']->PersonaBuscada->apellido_nombre_materno or ''}}</td>
            </tr>
            <tr>
                <td>Tipo de documento: {{$datos['oficios']->PersonaBuscada->tipoDocumento->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Nro de documento: {{$datos['oficios']->PersonaBuscada->nro_documento or ''}}</td>
            </tr>
            <tr>
                <td>Autoridad que lo expide: {{$datos['oficios']->PersonaBuscada->autoridad_expide or ''}}</td>
            </tr>
            <tr>
                <td>Cod de area: {{$datos['oficios']->PersonaBuscada->cod_area_telefono or ''}}</td>
            </tr>
            <tr>
                <td>Compañia teléfono: {{$datos['oficios']->PersonaBuscada->companiaTelefono->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Facebook: {{$datos['oficios']->PersonaBuscada->facebook or ''}}</td>
            </tr>
            <tr>
                <td>Twitter: {{$datos['oficios']->PersonaBuscada->twitter or ''}}</td>
            </tr>
            <tr>
                <td>Badoo: {{$datos['oficios']->PersonaBuscada->badoo or ''}}</td>
            </tr>
            <tr>
                <td>Instagram: {{$datos['oficios']->PersonaBuscada->instagram or ''}}</td>
            </tr>
            <tr>
                <td>linkedin: {{$datos['oficios']->PersonaBuscada->linkedin or ''}}</td>
            </tr>
            <tr>
                <td>Snapchat: {{$datos['oficios']->PersonaBuscada->snapchat or ''}}</td>
            </tr>
            <tr>
                <td>Otro: {{$datos['oficios']->PersonaBuscada->otro or ''}}</td>
            </tr>
        </table>
    </div>
    <h4>Último domicilio</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Dirección: {{$datos['oficios']->PersonaUltimoDomicilio->direccion or ''}}</td>
            </tr>
            <tr>
                <td>Nro: {{$datos['oficios']->PersonaUltimoDomicilio->nro or ''}}</td>
            </tr>
            <tr>
                <td>Piso: {{$datos['oficios']->PersonaUltimoDomicilio->piso or ''}}</td>
            </tr>
            <tr>
                <td>Dpto: {{$datos['oficios']->PersonaUltimoDomicilio->dpto or ''}}</td>
            </tr>
            <tr>
                <td>Intersección: {{$datos['oficios']->PersonaUltimoDomicilio->interseccion or ''}}</td>
            </tr>
            <tr>
                <td>Código postal: {{$datos['oficios']->PersonaUltimoDomicilio->codigo_postal or ''}}</td>
            </tr>
            <tr>
                <td>Provincia: {{$datos['oficios']->PersonaUltimoDomicilio->provincia->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Partido: {{$datos['oficios']->PersonaUltimoDomicilio->partido->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Localidad: {{$datos['oficios']->PersonaUltimoDomicilio->localidad or ''}}</td>
            </tr>
            <tr>
                <td>Observaciones: {{$datos['oficios']->PersonaUltimoDomicilio->observaciones or ''}}</td>
            </tr>
        </table>
    </div>

    @if(is_object($datos['oficios']->fotos->first()))
        @include('galeria',array("titulo"=>"Fotos cargadas","fotos"=>$datos['oficios']->fotos))
    @endif

   @if(is_object($datos['oficios']->autorizacion))
   <div class="table-responsive">
       <table class="table table-bordered">
           <tr>
               <td>Observaciones (Fotos): {{$datos['oficios']->autorizacion->observaciones or ''}}</td>
           </tr>
       </table>
   </div>
   @endif


    <h4>Descripción física</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Contextura física: {{$datos['oficios']->PersonaDescripcionFisica->contexturaFisica->contextura_fisica or ''}}</td>
            </tr>
            <tr>
                <td>Altura: {{$datos['oficios']->PersonaDescripcionFisica->altura or ''}}</td>
            </tr>
            <tr>
                <td>Peso: {{$datos['oficios']->PersonaDescripcionFisica->peso or ''}}</td>
            </tr>
            <tr>
                <td>Tipo de cabello: {{$datos['oficios']->PersonaDescripcionFisica->tipoCabello->tipo_cabello or ''}}</td>
            </tr>
            <tr>
                <td>Largo del cabello: {{$datos['oficios']->PersonaDescripcionFisica->largoCabello->largo_cabello or ''}}</td>
            </tr>
            <tr>
                <td>Color del cabello: {{$datos['oficios']->PersonaDescripcionFisica->colorCabello->color_cabello or ''}}</td>
            </tr>
            <tr>
                <td>Color de ojos: {{$datos['oficios']->PersonaDescripcionFisica->colorOjos->color_ojos or ''}}</td>
            </tr>
            <tr>
                <td>Tez: {{$datos['oficios']->PersonaDescripcionFisica->tez->tez or ''}}</td>
            </tr>
            <tr>
                <td>Señas particulares: {{$datos['oficios']->PersonaDescripcionFisica->id_senias_particulares or ''}}</td>
            </tr>
            <tr>
                <td>Detalle señas: {{$datos['oficios']->PersonaDescripcionFisica->detalle_senias or ''}}</td>
            </tr>
            <tr>
                <td>Observaciones: {{$datos['oficios']->PersonaDescripcionFisica->observaciones or ''}}</td>
            </tr>
        </table>
    </div>
    <h4>Datos laborales</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Profesión/Ocupación: {{$datos['oficios']->PersonaDatosLaborales->profesion_ocupacion or ''}}</td>
            </tr>
            <tr>
                <td>Empleador: {{$datos['oficios']->PersonaDatosLaborales->empleador or ''}}</td>
            </tr>
            <tr>
                <td>Teléfono: {{$datos['oficios']->PersonaDatosLaborales->telefono or ''}}</td>
            </tr>
            <tr>
                <td>Dirección: {{$datos['oficios']->PersonaDatosLaborales->direccion or ''}}</td>
            </tr>
            <tr>
                <td>Número: {{$datos['oficios']->PersonaDatosLaborales->nro or ''}}</td>
            </tr>
            <tr>
                <td>Piso: {{$datos['oficios']->PersonaDatosLaborales->piso or ''}}</td>
            </tr>
            <tr>
                <td>Departamento: {{$datos['oficios']->PersonaDatosLaborales->dpto or ''}}</td>
            </tr>
            <tr>
                <td>Codigo Postal: {{$datos['oficios']->PersonaDatosLaborales->codigo_postal or ''}}</td>
            </tr>
            <tr>
                <td>Provincia: {{$datos['oficios']->PersonaDatosLaborales->provincia->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Partido: {{$datos['oficios']->PersonaDatosLaborales->partido->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Barrio - Localidad: {{$datos['oficios']->PersonaDatosLaborales->localidad or ''}}</td>
            </tr>
        </table>
    </div>
    <h4>Datos relacionados con la ausencia</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Fecha en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_ultima_vez or ''}}</td>
            </tr>
            <tr>
                <td>Hora en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->hora_ultima_vez or ''}}</td>
            </tr>
            <tr>
                <td>Lugar en la que fue vista por última vez: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->lugar_ultima_vez or ''}}</td>
            </tr>
            <tr>
                <td>Descripcion de la vestimenta: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->descripcion_vestimenta or ''}}</td>
            </tr>
            <tr>
                <td>Detalle de las drogas: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->detalle_drogas or ''}}</td>
            </tr>
            <tr>
                <td>¿Padece adicción al alcohol?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->adiccionAlcohol->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>¿Realizó o realiza tratamiento en alguna institucion? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->tratamiento_institucion or ''}}</td>
            </tr>
            <tr>
                <td>Fecha de tratamiento {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_institucion or ''}}</td>
            </tr>
            <tr>
                <td>¿Padecimiento mental? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_mental or ''}}</td>
            </tr>
            <tr>
                <td>¿Realizó o realiza tratamiento en alguna institucion? ¿Cual?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->padecimiento_institucion or ''}}</td>
            </tr>
            <tr>
                <td>Fecha de tratamiento por padecimiento mental: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->fecha_tratamiento_padecimiento_mental or ''}}</td>
            </tr>
            <tr>
                <td>¿Se encontraba en el momento de la ausencia en alguna institucion?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->institucion->institucion or ''}}</td>
            </tr>
            <tr>
                <td>Indique en cual: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->institucion_momento_ausencia or ''}}</td>
            </tr>
            <tr>
                @if(is_object($datos['oficios']->PersonaDatosRelacionadoAusencia))
                    @if($datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria == 'V')
                       <td>Acerca de la internación: Voluntaria</td>
                    @elseif($datos['oficios']->PersonaDatosRelacionadoAusencia->internacion_estadia_voluntaria == 'C')
                        <td>Acerca de la internación: Compulsiva</td>
                    @else
                        <td>Acerca de la internación: </td>
                    @endif
                @else
                    <td>Acerca de la internación: </td>
                @endif
            </tr>
            <tr>
                <td>Autoridad que ordenó la medida en su caso: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->autoridad_ordeno_medida or ''}}</td>
            </tr>
            <tr>
                <td>Direccion: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->direccion or ''}}</td>
            </tr>
            <tr>
                <td>Nro: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->nro or ''}}</td>
            </tr>
            <tr>
                <td>Piso: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->piso or ''}}</td>
            </tr>
            <tr>
                <td>Depto: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->dpto or ''}}</td>
            </tr>
            <tr>
                <td>Código postal: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->codigo_postal or ''}}</td>
            </tr>
            <tr>
                <td>Provincia: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->provincia->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Partido: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->partido->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Barrio - Localidad: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->localidad or ''}}</td>
            </tr>
            <tr>
                <td>¿Se encontraba en situacion de calle?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->situacionCalle->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Lugares que la persona solia frecuentar: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->lugares_frecuentaba or ''}}</td>
            </tr>
            <tr>
                <td>¿Posee alguna enfermedad? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_enfermedad or ''}}</td>
            </tr>
            <tr>
                <td>¿Realiza algun tratamiento medico? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_tratamiento_medico or ''}}</td>
            </tr>
            <tr>
                <td>¿Necesita de alguna medicacion? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_alguna_medicacion or ''}}</td>
            </tr>
            <tr>
                <td>¿Se encontraba en situacion de conflicto en el ultimo tiempo? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_situacion_conflicto or ''}}</td>
            </tr>
            <tr>
                <td>¿Se trata de una sustraccion parental?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->sustraccionParental->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>¿Es la primera vez que se ausenta?: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->primeraVezAusenta->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>¿Llevaba documentacion consigo? Especifique: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->especifique_documentacion_consigo or ''}}</td>
            </tr>
            <tr>
                <td>¿Podria encontrarse en compañía de alguien? Indique quien y aporte datos de identificación: {{$datos['oficios']->PersonaDatosRelacionadoAusencia->compania_alguien or ''}}</td>
            </tr>

        </table>
    </div>
    <h4>Datos del denunciante</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>Apellido: {{$datos['oficios']->PersonaDatosDenunciante->apellido or ''}}</td>
            </tr>
            <tr>
                <td>Nombre: {{$datos['oficios']->PersonaDatosDenunciante->nombre or ''}}</td>
            </tr>
            <tr>
                <td>Tipo de documento: {{$datos['oficios']->PersonaDatosDenunciante->tipoDocumento->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Nro de documento: {{$datos['oficios']->PersonaDatosDenunciante->nro_documento or ''}}</td>
            </tr>
            <tr>
                <td>Vínculo: {{$datos['oficios']->PersonaDatosDenunciante->objVinculo->vinculo or ''}}</td>
            </tr>
            <tr>
                <td>Direccion: {{$datos['oficios']->PersonaDatosDenunciante->direccion or ''}}</td>
            </tr>
            <tr>
                <td>Nro: {{$datos['oficios']->PersonaDatosDenunciante->nro or ''}}</td>
            </tr>
            <tr>
                <td>Piso: {{$datos['oficios']->PersonaDatosDenunciante->piso or ''}}</td>
            </tr>
            <tr>
                <td>Depto: {{$datos['oficios']->PersonaDatosDenunciante->dpto or ''}}</td>
            </tr>
            <tr>
                <td>Código postal: {{$datos['oficios']->PersonaDatosDenunciante->codigo_postal or ''}}</td>
            </tr>
            <tr>
                <td>Provincia: {{$datos['oficios']->PersonaDatosDenunciante->provincia->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Partido: {{$datos['oficios']->PersonaDatosDenunciante->partido->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Cod de area: {{$datos['oficios']->PersonaDatosDenunciante->cod_area_telefono or ''}}</td>
            </tr>
            <tr>
                <td>Telefono: {{$datos['oficios']->PersonaDatosDenunciante->telefono or ''}}</td>
            </tr>
            <tr>
                <td>Dependencia: {{$datos['oficios']->PersonaDatosDenunciante->dependencia or ''}}</td>
            </tr>
            <tr>
                <td>fecha de la denuncia: {{$datos['oficios']->PersonaDatosDenunciante->fecha_denuncia or ''}}</td>
            </tr>
            <tr>
                <td>Relato: {{$datos['oficios']->PersonaDatosDenunciante->relato or ''}}</td>
            </tr>
            <tr>
                <td>Intervención de otros organismos: {{$datos['oficios']->PersonaDatosDenunciante->intervencion_otros_organismos or ''}}</td>
            </tr>
        </table>
    </div>
    @if($permiso_pe)
    <h4>Datos Búsqueda</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr><td>Nro Caso: {{$datos['oficios']->PersonaDatosBusqueda->nro_caso or ''}}</td></tr>
            <tr><td>Año: {{$datos['oficios']->PersonaDatosBusqueda->anio or ''}}</td></tr>
            <tr><td>Fecha de Aparición: {{$datos['oficios']->PersonaDatosBusqueda->fecha_aparicion or ''}}</td></tr>
            <tr><td>Procedencia: {{$datos['oficios']->PersonaDatosBusqueda->procedencia->descripcion or ''}}</td></tr>
            <tr><td>Estado Actual: {{$datos['oficios']->PersonaDatosBusqueda->estado_actual->descripcion or ''}}</td></tr>
            <tr><td>Circunstancias de aparición: {{$datos['oficios']->PersonaDatosBusqueda->circunstancias_aparicion or ''}}</td></tr>
        </table>
    </div>
    <h4>Avances</h4>
    <div class="table-responsive">
        @if(count($datos['oficios']->PersonaDatosBusquedaXAvance) > 0)
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Detalle</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                </tr>
                <tr>
                @foreach ($datos['oficios']->PersonaDatosBusquedaXAvance as $avance)
                    <tr>
                        <td>{{ $avance->detalle }}</td>
                        @if($avance->usuario)
                            <td>{{ $avance->usuario->username }}</td>
                        @else
                            <td></td>
                        @endif
                        <td width="150px">{{ $avance->created_at }}</td>
                    </tr>
                @endforeach
            </table>
        @endif
    </div>
    @if(is_object($datos['oficios']->PersonaDatosBusqueda))
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr><td>{{$radioButtons[$datos['oficios']->PersonaDatosBusqueda->menor] or ''}}</td><td>Menor</td></tr>
            <tr><td>{{$radioButtons[$datos['oficios']->PersonaDatosBusqueda->difusion_foto] or ''}}</td><td>Difusión de foto</td></tr>
            <tr><td>{{$radioButtons[$datos['oficios']->PersonaDatosBusqueda->victima_ilicito] or ''}}<br></td><td>Víctima de ilícito {{$datos['oficios']->PersonaDatosBusqueda->obj_victima_ilicito->descripcion or ''}}</td></tr>
        </table>
    </div>
    @endif
    <div class="table-responsive">
        <table class="table table-bordered">
            <tr><td>observaciones</td></tr>
            <tr><td>{{$datos['oficios']->PersonaDatosBusqueda->observaciones or ''}}</td></tr>
        </table>
    </div>
    @endif
    @include('solo_lectura.autoridad_judicial')

</div>
