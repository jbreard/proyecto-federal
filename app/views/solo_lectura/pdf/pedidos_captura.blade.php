@include('mensaje_activo_cesado',$datos)
<h3> {{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h3>
<div class="span11">
    <div class="span5">
        <h4>
            Datos de la Persona
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre:  {{ $datos['responsable']->nombre or ''}}  </td>
            </tr>
            <tr>
                <td>Apellido:  {{ $datos['responsable']->apellido or '' }} </td>
            </tr>
            <tr>
                <td>Apellido Materno:  {{ $datos['responsable']->apellido_materno or '' }} </td>
            </tr>
            <tr>
                <td>Sobrenombre:  {{ $datos['responsable']->apodos or '' }} </td>
            </tr>
            <tr>
                <td>Sexo:  {{ $datos['responsable']->sexo->descripcion or '' }} </td>
            </tr>
            <tr>
                <td>Genero: @if(!empty($datos['responsable']->genero->descripcion)){{$datos['responsable']->genero->descripcion}}@endif</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento:  {{ $datos['responsable']->edad or '' }}  </td>
            </tr>
            <tr>
                <td>Tipo de Documento:  {{ $datos['responsable']->tipoDocumento->descripcion or '' }}  </td>
            </tr>
            <tr>
                <td>N&deg; de Documento:  {{ $datos['responsable']->nro_documento or '' }}  </td>
            </tr>
            <tr>
                <td>Nacionalidad:  {{ $datos['responsable']->nacionalidad->descripcion or '' }}  </td>
            </tr>
            <tr>
                <td>Descripci&oacute;n F&iacute;sica:  {{ $datos['responsable']->descripcion_persona or '' }}  </td>
            </tr>
            <tr>
                <td>Observaciones:  {{ $datos['responsable']->observaciones_persona or '' }}  </td>
            </tr>
        </table>

    </div>
    @if(isset($datos['responsable']))
        @include('galeria',["titulo"=>"Fotos","fotos"=>$datos['responsable']->getFotos])
    @endif
    <div class="span5">
        <h4>
            &Uacute;ltimo Domicilio Conocido
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Direccion:  {{ $datos['responsable']->ultimo_calle or '' }}  </td>
            </tr>
            <tr>
                <td>Interseccion:  {{ $datos['responsable']->interseccion or '' }} </td>
            </tr>
            <tr>
                <td>N&deg;:  {{ $datos['responsable']->ultimo_numero or '' }} </td>
            </tr>
            <tr>
                <td>Piso:  {{ $datos['responsable']->ultimo_piso or '' }} </td>
            </tr>
            <tr>
                <td>Departamento:  {{ $datos['responsable']->ultimo_departamento or '' }} </td>
            </tr>
            <tr>
                <td>CP:  {{ $datos['responsable']->ultimo_codigo_postal or '' }}</td>
            </tr>
            <tr>
                <td>Provincia:  {{ $datos['responsable']->ultimaProvincia->descripcion or '' }} </td>
            </tr>
            <tr>
                <td>Partido:  {{ $datos['responsable']->ultimoPartido->descripcion or '' }} </td>
            </tr>
            <tr>
                <td>Localidad:  {{ $datos['responsable']->ultima_localidad or '' }} </td>
            </tr>
            <tr>
                <td>Observaciones:  {{ $datos['oficios']->observaciones or '' }} </td>
            </tr>
        </table>

    </div>
    @include('solo_lectura.autoridad_judicial')

</div>