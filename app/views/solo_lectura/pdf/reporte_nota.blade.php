<!DOCTYPE html>
<html>
<head>
    <title>Sistema Integrado de Requerimientos Judiciales</title>
    <meta charset="UTF-8">

    <style>
        body{
            height: 900px;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
            width: 100%;
        }
        td,
        th {
            padding: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ddd !important;
        }
        .alert{
            margin-top: 10px;
            border: solid 1px #000000;
            padding: 10px;
        }
        @page { margin: 180px 50px; }
        header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; text-align: center; }
        /*header #info {width:100%; margin: 0;}*/
        /*header #info #hora{text-align: left;}*/
        /*header #info .pagenro{text-align: right;}*/

        header .page:after { content: counter(page); }
        footer {
            position: fixed;
            margin-left: 50px;
            left: -45px;
            bottom: -10px;
            right: -45px;
            height: 50px;
            padding-right: 10%;
        }
        footer p {
            height: 5px;
        }


        .imgLogo{
            display: inline-block;
            width: 256px;
            float: left;
        }

        .tituloAnio{
            display: inline-block;
            width: 512px;
            float: right;
        }

        .ref1{
            text-align: left;
            margin-left: 200px !important;
            display: block;
        }
        .ref2{
            text-align: left;
            margin-left: 200px !important;
            display: block;
        }
        .notaNro{
            text-align: left;
            margin-left: 200px !important;
            display: block;
        }
        .lugar{
            text-align: left;
            margin-top: 20px;
            margin-left: 200px !important;
            display: block;
        }

        .contenido{
            margin-top: 40px;
            font-size: 19px;
            margin-left: 30px;
            text-align:justify;
        }

        .sangria{
            margin-left: 120px;
            margin-top: -200px;
        }

        .pie{
            width: 700px;
            display: inline-block;
            float: right;
        }
        .mayuscula{
            text-transform: uppercase;
        }
    </style>
</head>
<body style="display: block">


{{ $html }}


</body>
</html>