@include('mensaje_activo_cesado',$datos)
<div class="span11">
    <h2> {{ $titulo_oficio }} @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1))(Alerta Temprana)@endif </h2>

    <div class="span5">
        <h4 class="">
            Datos de La Persona Buscada
        </h4>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Nombre: {{ $datos['buscado']->nombre }}</td>
            </tr>
            <tr>
                <td>Apellido: {{ $datos['buscado']->apellido }}</td>
            </tr>
            <tr>
                <td>Apellido Materno:  {{ $datos['buscado']->apellido_materno }} </td>
            </tr>
            <tr>
                <td>Sobrenombre: {{ $datos['buscado']->apodos }}</td>
            </tr>
            <tr>
                <td>Sexo: {{ $datos['buscado']->sexo->descripcion }}</td>
            </tr>
            <tr>
                <td>Genero: {{$datos['buscado']->genero->descripcion or ''}}</td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento: {{ $datos['buscado']->edad }}</td>
            </tr>
            <tr>
                <td>Tipo de Documento: {{ $datos['buscado']->tipoDocumento->descripcion}}</td>
            </tr>
            <tr>
                <td>Nacionalidad: {{ $datos['buscado']->nacionalidad->descripcion}}</td>
            </tr>
            <tr>
                <td>N° de Documento: {{ $datos['buscado']->nro_documento }}</td>
            </tr>
            <tr>
                <td>Codigo De Area: {{ $datos['buscado']->codArea_celular }}</td>
            </tr>
            <tr>
                <td>N°de Celular: {{ $datos['buscado']->numero_celular }}</td>
            </tr>
            <tr>
                <td>Compañia de Celular: {{ $datos['buscado']->companiaCelular->descripcion }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['buscado']->observaciones_persona }}</td>
            </tr>
        </table>
    </div>
    @include('galeria',array("titulo"=>"Fotos de la Persona Buscada","fotos"=>$datos['buscado']->getFotos))
    <div class="span5">
        <h4>
            &Uacute;ltimo Domicilio Conocido
        </h4>

        <table class="table table-bordered table-hover">
            <tr>
                <td>Direccion: {{ $datos['buscado']->ultimo_calle }}</td>
            </tr>
            <tr>
                <td>Interseccion: {{ $datos['buscado']->interseccion }}</td>
            </tr>
            <tr>
                <td>N&deg;: {{ $datos['buscado']->ultimo_numero }}</td>
            </tr>
            <tr>
                <td>Piso: {{ $datos['buscado']->ultimo_piso }}</td>
            </tr>
            <tr>
                <td>Departamento: {{ $datos['buscado']->ultimo_departamento }}</td>
            </tr>
            <tr>
                <td>CP: {{ $datos['buscado']->ultimo_codigo_postal }}</td>
            </tr>

            <tr>
                <td>Provincia: {{ (isset($datos['buscado']->ultimaProvincia->descripcion)) ? $datos['buscado']->ultimaProvincia->descripcion : '' }}</td>
            </tr>
            <tr>
                <td>Partido: {{ (isset($datos['buscado']->ultimoPartido->descripcion)) ? $datos['buscado']->ultimoPartido->descripcion : ''  }}</td>
            </tr>
            <tr>
                <td>Localidad: {{ $datos['buscado']->ultima_localidad }}</td>
            </tr>
            <tr>
                <td>Observaciones: {{ $datos['buscado']->observaciones_domicilio }}</td>
            </tr>

        </table>
    </div>

</div>
    @include('solo_lectura.autoridad_judicial')
    <!--
        <fieldset>
            <h4>
                Fotografias de la Persona
            </h4>
            <div class="span10 hero-unit " id="box-foto">
                <div  class="span10">
                </div>
            </div>
        </fieldset>
    -->
</div>