@if(!isset($robo_automotor) && !isset($profugo_evadido))
<div class="span5">
    <h4>
        Autoridad Judicial Interviniente
    </h4>
    <table class="table table-bordered ">
        <tr>
            <td>Fecha del Oficio:  {{ $datos['oficios']->fecha_oficio }}  </td>
        </tr>
        <tr>
            <td>N° del Oficio:  {{ $datos['oficios']->nro_oficio }} </td>
        </tr>
        <tr>
            <td>Carátula de la Causa:  {{ $datos['oficios']->caratula }}  </td>
        </tr>
        <tr>
            <td>Juzgado Interviniente:  {{ $datos['oficios']->juzgados->juzgado }}  </td>
        </tr>
        <tr>
            <td>Otro Juzgado Interviniente :  {{ $datos['oficios']->juzgado_otro }}  </td>
        </tr>
        <tr>
            <td>Secretaría:  {{ $datos['oficios']->secretaria }} </td>
        </tr>
        <tr>
            <td>N° de la Causa:  {{ $datos['oficios']->nro_causa }} </td>
        </tr>
        <tr>
            <td>Fiscalía Interviniente:  {{ $datos['oficios']->fiscalias->fiscalias }}</td>
        </tr>
        <tr>
            <td>Otra Fiscalía Interviniente :  {{ $datos['oficios']->otra_fiscalia }}  </td>
        </tr>
        <tr>
            <td>N° de la Causa:  {{ $datos['oficios']->nro_causa_expediente }} </td>
        </tr>

        <tr>
            <td>Observaciones:  {{ $datos['oficios']->observaciones_judicial }} </td>
        </tr>

@if($datos['oficios']->fecha_oficio2 !=  "")
        <table class="table table-bordered ">

            <h4>
                Autoridad Judicial Interviniente Añadida
            </h4>

            <tr>
                <td>Fecha del Oficio:  {{ $datos['oficios']->fecha_oficio2 }}  </td>
            </tr>
            <tr>
                <td>N° del Oficio:  {{ $datos['oficios']->nro_oficio2 }} </td>
            </tr>
            <tr>
                <td>Carátula de la Causa:  {{ $datos['oficios']->caratula2 }}  </td>
            </tr>
            <tr>
                <td>Juzgado Interviniente:  {{ $datos['oficios']->juzgadosAniadida->juzgado }}  </td>
            </tr>
            <tr>
                <td>Otro Juzgado Interviniente :  {{ $datos['oficios']->juzgado_otro_2 }}  </td>
            </tr>
            <tr>
                <td>Secretaría:  {{ $datos['oficios']->secretaria2 }} </td>
            </tr>
            <tr>
                <td>N° de la Causa:  {{ $datos['oficios']->nro_causa2 }} </td>
            </tr>
            <tr>
                <td>Fiscalía Interviniente:  {{ $datos['oficios']->fiscaliasAniadida->fiscalias }}</td>
            </tr>
            <tr>
                <td>Otra Fiscalía Interviniente :  {{ $datos['oficios']->otra_fiscalia_2 }}  </td>
            </tr>

            <tr>
                <td>N° de la Causa:  {{ $datos['oficios']->nro_causa_expediente2 }} </td>
            </tr>

            <tr>
                <td>Observaciones:  {{ $datos['oficios']->observaciones_judicial2 }} </td>
            </tr>


@endif

        @if ($datos['oficios']->plazo_fecha != null)
            {{ Session::forget("plazo_fecha") }}
            <tr>
                <td>Plazo:  {{ $datos['oficios']->plazo_fecha}}  </td>
            </tr>
        @endif
        @if ($datos['oficios']->plazo_hr != null)
            {{ Session::forget("plazo_hr") }}
            <tr>
                <td>Plazo:  {{ $datos['oficios']->plazo_hr}} </td>
            </tr>
        @endif
        @if(isset($datos['oficios']->oficioCesado))
        <tr>
            <td>Observaciones de Sin Efecto:
                {{ $datos['oficios']->oficioCesado->observaciones  }}
            </td>
        </tr>
        @endif
    </table>

</div>
@endif

<div class="span5">



@if( (Request::segment(2) != 'pdf'))

    <table class="table table-bordered table-hover">

        <h4>
            Archivos Adjuntados
        </h4>

        <thead>
            <th>
                Archivo
            </th>
            <th>
                Fecha de Carga
            </th>
            <th>
                Descargar
            </th>
        </thead>
        @foreach ($datos['oficios']->archivos as $archivo)
            <tr>
              <td>Oficio de la Medida Solicitada</td>
              <td>{{ $archivo->created_at }}</td>
              <td><a href="{{ Route('archivos.descargar')}}?q={{$archivo->path}} "><i class="fa fa-download"></i></a></td>
            </tr>
        @endforeach

        @if (!empty($datos['oficios']->oficioCesado->path ))
            @if(Request::segment(2) !="pdf")
                <td>Oficio de Sin Efecto de la medida</td>
                <td>{{ $datos['oficios']->oficioCesado->created_at }}</td>
                <td><a href="{{ Route('archivos.descargar')}}?q={{ $datos['oficios']->oficioCesado->path}}" target="__blank"><i class="fa fa-download"></i></a></td>
            @endif
        @endif
    </table>

@if(Auth::user()->id_perfil!=2)
    <div class="span10">
        @if($datos['oficios']->confirmado != 1)
        {{ Form::open(['route'=>['oficios.observaciones',$datos['oficios']->id],'method'=>'POST','files'=>true], array('role' => 'form','files'=>'true')) }}
        <div class="col-md-12">
            <div class="row">
             <h4>
                Observaciones:
            </h4>
            {{ Form::textarea('observaciones',isset($oficios) ? $oficios->getObservaciones->observaciones:null, array('placeholder'=> '', 'class' => 'form-control')) }}
            {{ Form::label('   ', '   ') }}
            </div>
            @for ($i = 0; $i < 4; $i++)
            <div class="row">
                <div class="col-md-4">
                    {{Form::file('adjunto[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('adjunto[]')}}
                </div>
                <div class="col-md-4">
                    {{Form::file('adjunto[]')}}
                </div>
            </div>
            @endfor
            <div class="row">
                <button class="btn btn-primary" id="" type="submit">Guardar »</button>
            </div>
        </div>
        {{Form::close()}}
        @endif
    </div>
@endif
    <div class="span10">
    </div>

</div>
@endif
<div class="row">
    <h4>
        Observaciones Añadidas:
    </h4>
<table class="table table-bordered">

    <tr>
        <th>Fecha</th>
        <th>Observacion</th>
        <th>Usuario</th>
        <th>Adjuntos</th>
        <th>Organismo</th>
    </tr>

    @foreach($datos['oficios']->getObservaciones as $observacion)
    <tr>
        <td>{{ $observacion['created_at'] }}</td>
        <td>{{ $observacion['observaciones'] }}</td>
        <td>{{ $observacion->user->nombre }} {{ $observacion->user->apellido }}</td>
        <td>
            <ul>
                @foreach($observacion->archivos as $archivo)
                <li><a href="{{ Route('archivos.descargar') }}?q={{ $archivo->path }}" target="_blank">Descargar</a></li>
                @endforeach
            </ul>


        </td>
        <td>{{ $observacion->user->getJurisdiccion->descripcion }}</td>
    </tr>
    @endforeach
</table>
</div>


@if ($datos['oficios']->confirmado == 1)
    <a href="{{ route('oficios.confirmar',$datos['oficios']->id) }}" class="btn btn-primary" id="btn_confirmar" onclick="confirmar()">Confirmar


        @if(Session::has("alerta_temprana") or  (isset( $datos['oficios']->alerta_temprana) and  $datos['oficios']->alerta_temprana==1)) Alerta Temprana
        @else
            Requerimiento
        @endif



    </a>
    <a href="{{ route($datos['oficios']->tipo->ruta.'.edit',$datos['oficios']->id) }}" class="btn btn-danger" onclick="confirmar()">Volver</a>
    <script>
       var save = false;
       function confirmar(){
           save = true;
       }
       window.onbeforeunload = function(e) {
            if(!save) return "estas seguro de salir?"
       }
    </script>
@endif


@if(Session::has("agregar_otro"))

<a href="{{ route('oficios.cargar_otro',$datos['oficios']->id) }}" class="btn btn-primary" >Cargar Otro </a>
@endif
