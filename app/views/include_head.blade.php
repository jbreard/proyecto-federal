<!-- Head
================================================== -->
<!-- <link rel="shortcut icon" href="favicon.png"> -->
<!-- Bootstrap core CSS -->
{{ HTML::style('bootstrap/bootstrap/css/bootstrap.css',array('media' => 'screen')) }}
<!-- Font Awesome -->
{{ HTML::style('bootstrap/css/font-awesome.min.css', array('media' => 'screen')) }}
<!-- Pace -->
<!-- <link type="text/css" href="css/pace.css" rel="stylesheet"> -->
{{ HTML::style('bootstrap/css/pace.css', array('media' => 'screen')) }}
<!-- Endless -->
{{ HTML::style('bootstrap/css/endless.min.css', array('media' => 'screen')) }}
<!-- <link type="text/css" href="css/endless-skin.css" rel="stylesheet">-->
{{ HTML::style('bootstrap/css/endless-skin.css', array('media' => 'screen')) }}
<!-- minseg -->
<!-- <link type="text/css" href="css/style-minseg.css" rel="stylesheet"/> -->
{{ HTML::style('bootstrap/css/style-minseg.css', array('media' => 'screen')) }}
<!-- ./Head -->

<!-- Datepicker -->
<!-- <link href="css/datepicker.css" rel="stylesheet"/>-->

{{ HTML::style('bootstrap/css/datepicker.css', array('media' => 'screen')) }}

<!-- Timepicker -->
{{ HTML::style('bootstrap/css/bootstrap-timepicker.min.css', array('media' => 'screen')) }}

{{ HTML::style('bootstrap/css/gritter/jquery.gritter.css', array('media' => 'screen')) }}
{{ HTML::style('bootstrap/css/jquery.tagsinput.css', array('media' => 'screen')) }}

{{ HTML::style('bootstrap/css/jquery-ui.min.css', array('media' => 'screen')) }}

{{ HTML::style('bootstrap/css/jquery-ui.theme.min.css', array('media' => 'screen')) }}
{{ HTML::style('assets/select2/select2.css', array('media' => 'screen')) }}
{{ HTML::style('assets/select2/select2-bootstrap.css', array('media' => 'screen')) }}
{{ HTML::style('assets/sweetalert/dist/sweetalert.css', array('media' => 'screen')) }}
{{ HTML::style('assets/bootstrap3-editable/bootstrap3-editable/css/bootstrap-editable.css', array('media' => 'screen')) }}

<style>
input{
    text-transform: uppercase;
}
table tbody  td{
    text-transform: uppercase;
}

[data-notify="danger"] {
	margin-bottom: 0px;
	position: absolute;
	bottom: 0px;
	left: 0px;
	width: 100%;
	height: 5px;
	z-index: 10000;
	
}

</style>

<!-- Font Awesome -->
<!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->

<!-- Endless -->
<!-- <link href="css/endless.min.css" rel="stylesheet">-->
