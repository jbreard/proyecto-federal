@extends ('header')
@section('scripts')
<style>
    .row {
        margin-top: 5px !important;
    }
</style>
@stop
@section ('content')
<div class="main-header clearfix">
    <div class="page-title">
        <h3 class="no-margin">{{Config::get('nombre.siglas')}}</h3>
        <span>{{ Auth::user()->nombre }} {{ Auth::user()->apellido }}</span>
    </div>
    <!-- /page-title -->

    <ul class="page-stats">





    </ul>
    <!-- /page-stats -->
</div>
<div class="grey-container shortcut-wrapper">
    <h3>Bienvenido/a al {{Config::get('nombre.nombre')}}
</h3>
                @if( Session::has('mensaje_pass'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul style="list-style: none">
                        <li>{{ Session::get('mensaje_pass') }}</li>
                    </ul>
                </div>
                @endif
    <span>Los  siguientes enlaces le ayudarán a navegar con el Sistema.</span>
</div>
<div class="row">
    @if(Auth::user()->id_perfil == 4)
    <a href="{{ route('mensajes.enviados') }}" class="btn btn-default block">Mensajes</a>
    @endif
    @if(Auth::user()->id_perfil != 4 && Auth::user()->id_perfil != 2 )
    <div class="col-md-4">
        <h4 class="headline">
            Cargar Requerimiento
            <span class="line"></span>
        </h4>
        <div class="panel bg-success fadeInDown animation-delay4">

            <div class="seperator"></div>
            <div class="panel-body">

                @include('menu.oficios_inicio')

            </div>

        </div>
    </div>
    @endif
    @if(Auth::user()->id_perfil <> 4)
    <div class="col-md-4">
        <h4 class="headline">
             Base de Datos
             <span class="line"></span>
        </h4>
        <div class="panel bg-success fadeInDown animation-delay5">

            <div class="seperator"></div>
            <div class="panel-body">
                <a href="{{ route('oficios.index') }}" class="btn btn-default block">Registro de Requerimientos</a>

            </div>

        </div>
    </div>
    @endif
    @if(Auth::user()->id_perfil == 1 || Auth::user()->id_perfil == 5 )
    <div class="col-md-4">
        <h4 class="headline">
            Administración
            <span class="line"></span>
        </h4>
        <div class="panel bg-success fadeInDown animation-delay6">

            <div class="seperator"></div>
            <div class="panel-body">
                @if(Auth::user()->id_perfil == 5)
                <a href="{{ route('usuarios.index') }}" class="btn btn-default block">Listado Usuarios</a>
                <div class="seperator"></div>
                <a href="{{ route('usuarios.create') }}" class="btn btn-default block">Crear Usuario</a>
                <div class="seperator"></div>
                @endif
                <a href="{{ route('mails.create') }}" class="btn btn-default block">Crear Mail</a>
                <div class="seperator"></div>
                <a href="{{ route('mails.index') }}" class="btn btn-default block">Listado Mails</a>

            </div>

        </div>
     

    </div>
    @endif


</div>
@if($term == null)         
@include('terminos_condiciones')       
@endif

@stop
