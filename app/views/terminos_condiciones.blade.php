{{ HTML::script('assets/js/jquery.js') }}

<script>
$(document).ready(function() {
   
$('#terminos').modal({
    backdrop: 'static',
    keyboard: false
})
    
    $("form#myform").submit(function(event) {
        event.preventDefault();
        var aceptar = $("#aceptar").val();

        $.ajax({
            type: "POST",
            url: "{{ route('usuarios.aceptar') }}",
            data: aceptar,
            cache: false,
            processData: false,
            contentType: false,
          success: function(result) { //we got the response
             //alert(result);
             $('#terminos').modal('hide')
         },
         error: function(xhr, ajaxOptions, thrownError) {
             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
         }
        });

    });
});
</script>


<div class="modal fade" id="terminos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5>
       <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b><u><span
           style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>TÉRMINOS
           Y CONDICIONES DE USO DEL <a name="OLE_LINK1"></a><a name="OLE_LINK2"></a></span></u></b></p>
           <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b><u><span
          style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>SISTEMA
          FEDERAL DE COMUNICACIONES POLICIALES -SIFCOP</span></u></b><b><u><span
          style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>-</span></u></b></p>

         </h5>

      </div>
      <div class="modal-body">
        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><u><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>A.
        DEFINICIONES</span></u></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>1.
        Términos y Condiciones (en adelante, “TyC”): son aquellos que rigen la relación
        entre el Organismo Implementador del SISTEMA FEDERAL DE COMUNICACIONES
        POLICIALES - SIFCOP (en adelante el SIFCOP) y los/as usuarios/as del sistema.</span></p>

        <p class=MsoNormal style='margin-bottom:0cm;text-align:justify;line-height:
        150%;text-autospace:none'><span style='font-size:12.0pt;line-height:150%;
        font-family:"Arial",sans-serif'>2.  Organismo Implementador: es el MINISTERIO
        DE SEGURIDAD DE LA NACIÓN.</span></p>

        <p class=MsoNormal style='margin-bottom:0cm;text-align:justify;line-height:
        150%;text-autospace:none'><span style='font-size:12.0pt;line-height:150%;
        font-family:"Arial",sans-serif'>&nbsp;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>3. Administrador
        del sistema: es la<a name="OLE_LINK5"> UNIDAD DEL SISTEMA FEDERAL DE
        COMUNICACIONES POLICIALES –SIFCOP</a>, dependiente de la SUBSECRETARÍA DE
        INVESTIGACIÓN CRIMINAL Y COOPERACIÓN JUDICIAL de la SECRETARÍA DE SEGURIDAD Y
        POLÍTICA CRIMINAL.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>4.
        El/la Usuario/a: comprende a todas las personas que hayan logrado la habilitación
        para el acceso a la plataforma en todas sus modalidades, pertenecientes a
        Fuerzas Federales de Seguridad, Cuerpos Policiales Provinciales, Poderes
        Judiciales, Ministerios Públicos y distintos organismos de orden Nacional,
        Provincial y de la Ciudad Autónoma de Buenos Aires.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>5.
        Información Confidencial: todo  requerimiento,  información, documentos o datos
        a los que acceda el/la usuario/a para el desarrollo de sus actividades en las
        funciones y/o tareas asignadas por los organismos  a los que pertenece. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>En
        particular, se considerará Información Confidencial a:</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(i) La
        información, documentación o datos que no sean de público conocimiento y
        resulten conocidos por el/la usuario/a con motivo o en ocasión del desarrollo
        de sus funciones;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(ii)
        Cualquier tipo de información, documentación, oficios judiciales, denuncias
        policiales, actos administrativos, y/o cualquier otra documentación relacionada
        con los requerimientos, actividades y funciones presentes y/o futuras y/o
        relacionada con la plataforma y con sus módulos específicos, de la cual el/la
        usuario/a tenga conocimiento y/o a la que tenga acceso por cualquier medio o
        circunstancia en virtud del cumplimiento de sus funciones.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(iii)
        Cualquier información, datos, documentos, oficios judiciales, actos
        administrativos, denuncias policiales, y/o requerimientos que los Organismos hayan
        tomado conocimiento a través del el/la usuario/a, en representación del este.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(iv<a
        name="OLE_LINK8"></a><a name="OLE_LINK9">) Cualquier información a la que pueda
        acceder el/la usuario/a a través de la plataforma y/o cualquiera de sus
        módulos, sobre datos </a>personales referida a personas humanas de conformidad
        a lo establecido en el artículo 2 inciso 1) de la <a name="OLE_LINK6"></a><a
        name="OLE_LINK7">Ley N° 25.326 </a>y sus modificatorias, especialmente Nombres,
        Apellidos, CUIT/CUIL, números de documento, fotografías, domicilios, números de
        trámites, fechas de nacimiento y/o fallecimiento, fecha de renovación, etc. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><a
        name="OLE_LINK10"><span style='font-size:12.0pt;line-height:150%;font-family:
        "Arial",sans-serif'>(v) Cualquier información a la que pueda acceder el/la
        usuario/a a través de la plataforma y/o cualquiera de sus módulos</span></a><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>,
        sobre datos de vehículos de cualquier tipo referente a datos registrales, números
        de dominio, números de chasis, números de motor, cedulas de habilitación,
        radicación de legajos, titulares y sus datos personales, etc. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(vi)
        Cualquier información a la que pueda acceder el/la usuario/a a través de la
        plataforma y/o cualquiera de sus módulos referente a medidas judiciales, carátulas
        y números de expedientes, organismos solicitantes, etc. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>6. Uso
        permitido del sistema: la utilización del SIFCOP debe ser al solo efecto del cumplimiento
        de las funciones propias asignadas <a name="OLE_LINK3"></a><a name="OLE_LINK4">al/la
        usuario/a</a>. En consecuencia, la utilización del SIFCOP es de uso exclusivo
        para asuntos relacionados con las tareas y los organismos para los cuales al/la
        usuario/a fue habilitado. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>7.
        “Auditores” o “Supervisores”: comprende a todas las personas humanas que se
        desempeñen en la UNIDAD DEL SISTEMA FEDERAL DE COMUNICACIONES POLICIALES
        –SIFCOP- asignadas a dichas tareas y/o aquellas a la que expresamente se las
        designe para cumplirlas, en forma permanente o transitoria, que tengan acceso y
        utilicen el SIFCOP con motivo de las funciones desarrolladas en ese Organismo
        Implementador.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><u><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'><span
         style='text-decoration:none'>&nbsp;</span></span></u></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><u><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>B.
        CONDICIONES GENERALES</span></u></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>1. DEL
        SIFCOP</span></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>1.1 El
        ingreso de los/as Usuarios/as al SIFCOP implica la aceptación inmediata y sin
        reserva de los presentes TyC. En consecuencia, los/as Usuarios/as manifiestan
        haber leído y aceptado en su totalidad los mismos; siendo dicha aceptación
        indispensable para su utilización.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>1.2
        Para operar en el SIFCOP se deberá contar con nombre de usuario y contraseña,
        otorgados por el administrador del sistema.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>1.3 El
        organismo implementador y/o el administrador del sistema se reserva el derecho
        de suspender y/o eliminar unilateral y automáticamente de la utilización del
        SIFCOP a los/as usuarios/as que incumplan los presentes TyC y asimismo se
        reservan el derecho a suspender el acceso al sistema y/o a determinado
        contenido y/o módulos por motivos de mantenimiento o de seguridad, en cualquier
        momento.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2. DE
        LOS/AS USUARIOS/AS</span></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.1
        Los/las usuarios/as que ingresen al SIFCOP se obligan a utilizarlo en forma
        lícita, de acuerdo a los presentes TyC y respetando la normativa nacional
        vigente, las buenas costumbres, el orden público y la moral, comprometiéndose
        en todo momento a no causar daños. En especial deberán atenerse a lo prescripto
        en las Leyes Nº 25.326 y Nº 11.179 y sus modificatorias.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.2
        Los/as usuarios/as autorizados/as a utilizar el sistema son los siguientes:</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(i<a
        name="OLE_LINK13">)</a> Usuarios/as habilitados para modalidad “SUPERVISOR”.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(ii) <a
        name="OLE_LINK14">Usuarios/as habilitados para modalidad “CARGA”</a>/”TRANSMISIÓN
        DE DATOS”</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><a
        name="OLE_LINK15"></a><a name="OLE_LINK16"><span style='font-size:12.0pt;
        line-height:150%;font-family:"Arial",sans-serif'>(iii) Usuarios/as habilitados
        para modalidad “CONSULTA”.</span></a></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(iii)
        Usuarios/as habilitados para el módulo “INTERCONSULTAS”.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.3 El
        <a name="OLE_LINK17">nombre de usuario y contraseña </a>de usuario/a otorgados
        por el administrador del sistema habilitará a los/las usuarios/as a operar el mismo.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.4 El
        nombre de usuario y contraseña de usuario/a otorgados por el administrador del
        sistema serán de la exclusiva responsabilidad del/la usuario/a.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.5 En
        su primer inicio de sesión en el SIFCOP, el/la usuario/a deberá modificar la contraseña
        suministrada, siendo responsable de su resguardo </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.6
        Los/as usuarios/as del SIFCOP no pueden transferir, prestar, ceder y/o divulgar
        a cualquier título su nombre de usuario ni contraseña los que son personales,
        secretos e intransferibles. El/la usuario/a será responsable exclusivo en caso
        de su divulgación y de los perjuicios que ello pudiese ocasionar a terceros o
        al SIFCOP, siendo pasible de ser sancionado, sin perjuicio de las acciones
        civiles, penales y/o de otra naturaleza que le pudiesen corresponder.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.7
        Las transmisiones de requerimientos y/o consultas de cualquier índole
        realizadas por el/la usuario/a utilizando su respectivo nombre de usuario y
        contraseña se consideran realizadas por él/ella en representación del organismo
        al que pertenece y/o preste servicio. A tal efecto, se considera que las mismas
        son válidas, legítimas y auténticas sin necesidad de realizar o tomar ningún otro
        resguardo, de cualquier índole.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.8 Se
        recomienda que el/la usuario/a modifique su contraseña periódicamente.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.9
        El/la usuario/a es responsable por el uso indebido o inadecuado del sistema y
        de los daños y perjuicios que puedan resultar a causa de cualquier presentación
        falsa o incorrecta efectuada al usar el SIFCOP.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>En tal
        sentido, sin perjuicio de lo establecido en el punto 2.17 de los presentes TyC,
        queda prohibido el uso del SIFCOP por parte de los/as usuarios/as a los fines
        de influir de manera negativa en el desempeño, imagen, funciones del organismo implementador,
        el administrador del sistema y/o generar responsabilidades para la Nación.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.10
        Los/as usuarios/as se comprometen a notificar inmediatamente al administrador
        del sistema respecto de cualquier pérdida de su contraseña o acceso no
        autorizado por parte de terceros a su cuenta. Además, se compromete a enviar
        información sobre las vulnerabilidades detectadas en el sistema a la dirección
        de correo electrónico coordinación.sifcop@minseg.gob.ar. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.11
        El/la usuario/a tiene el deber de respetar la confidencialidad y la integridad
        de cualquier documento, información o dato al que tenga acceso, y es
        personalmente responsable de proteger y salvaguardar estos recursos.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.12
        La información personal solicitada por el administrador del sistema a el/la usuario/a
        para habilitarlo/a en el SIFCOP contendrá campos obligatorios, entre ellos: nombres,
        apellidos, DNI, <a name="OLE_LINK18">dirección de correo electrónic</a>o personal
        (institucional), organismo (por ej., “Policía Federal”) suborganismo (por ej.
        “División Antecedentes”), cargo del usuario, dirección / ubicación, dirección
        de correo electrónico de la dependencia donde presta servicios (institucional),
        provincia / jurisdicción, y modalidad solicitada, todo ello mediante nota de
        estilo con firma y sello de autoridad competente. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>2.13 Al utilizar el sistema, el/la usuario/a reconoce y acepta que
        los administradores del sistema y los auditores / supervisores del mismo podrán
        acceder a los datos que previamente otorgaron para la generación del acceso al
        SIFCOP, preservarlos y/o divulgarlos en caso de ser necesario por razones
        estrictamente legales o, si existiese razones fundadas que permitan suponer que
        el acceso, conservación o divulgación de dicha información es necesario para:</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(i) Cumplir con leyes, regulaciones, procesos legales o
        solicitudes administrativas efectuadas por autoridad competente o judiciales
        exigibles;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(ii) Aplicar estos TyC, incluida la investigación de posibles
        infracciones a los mismos;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(iii) Detectar, prevenir y/o abordar, de cualquier modo, los casos
        o situaciones relativas a la seguridad del SIFCOP o de sus usuarios/as;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(iv) Proteger contra todo daño inminente los derechos, propiedad o
        seguridad del SIFCOP y de sus usuarios/as en la manera prevista o permitida por
        la Ley, especialmente de conformidad a lo previsto en el artículo 11 de la Nº <a
        name="OLE_LINK19">Ley 25.326 </a>y sus modificatorios;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(v) Utilizarla para responder a los pedidos de acceso a la
        información pública, en el marco de lo establecido en la Ley Nº 27275, sus
        modificatorias y complementarias;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif;
        color:black'>(vi) Optimizar las respuestas de pedidos de acceso a la
        información pública, en los términos señalados en el punto anterior;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.14
        La información de identificación que el/la usuario/a provea para acceder y operar
        dentro del SIFCOP se mantendrá en servidores ubicados en un ambiente controlado
        y seguro, protegidos del acceso, uso o divulgación no autorizados.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.15
        Es responsabilidad del/la usuario/a mantener sus datos personales actualizados
        en todo momento.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.16
        Si el/la usuario/a proporcionara datos falsos, inexactos o incompletos o, los administradores
        del sistema o el organismo implementador tuvieren motivos fundados para
        sospechar tal conducta, la cuenta del/la usuario/a podrá ser cancelada por el administrador
        del sistema o por el organismo implementador, denegándole el acceso y uso del SIFCOP.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.17
        Queda expresamente prohibido por parte del/la usuario/a:</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(i)
        Modificar, alterar y/o borrar, sin las autorizaciones correspondientes, la
        información, requerimientos o datos, que integren la base de datos del SIFCOP.  </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(ii)
        Interferir, sin autorización, el acceso de otros/as usuarios/as al SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(iii)
        Transgredir o eludir las verificaciones de identidad establecidas en el SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(iv)
        Realizar actos maliciosos o que atenten contra el SIFCOP, que impacten
        directamente en el funcionamiento adecuado del mismo, o que de alguna forma
        puedan llegar a dañar, inutilizar, sobrecargar, deteriorar, limitar o
        inutilizar el normal funcionamiento del sistema o la utilización de todas o
        algunas de las funcionalidades del mismo, entre ellos: cargar archivos que
        contengan malware, archivos dañados o cualquier otro software o programas
        similares o introducir al SIFCOP cualquier tipo de virus, gusano, o programa de
        computación cuya intención sea hostil, destructiva y que impidan, inutilicen,
        puedan dañar o efectivamente causen daños al SIFCOP y/o a sus usuarios/as y/o
        inferir cualquier otro daño a los equipos o a la información, las
        configuraciones del sistema operativo o los aplicativos que se encuentren
        instalados en el Sistema;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(v)
        Efectuar acciones tendientes a perturbar o alterar la propiedad intelectual o
        los derechos de autor del Estado Nacional sobre el sistema;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(vi)
        Realizar acciones que impongan una carga irrazonable o desproporcionadamente
        grande sobre la infraestructura del SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(vii)
        Intentar acceder, mediante el SIFCOP, a datos restringidos sin la debida
        autorización y/o transgredir las barreras de seguridad del sistema para llegar
        a ellos;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(vii)
        Realizar búsquedas de vulnerabilidades o explotación de las mismas, a través
        del SIFCOP, para cualquier fin, sin la debida autorización efectuada por el administrador
        del sistema;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(ix)
        Divulgar información acerca de la detección de vulnerabilidades encontradas en
        el SIFCOP a terceros no autorizados;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(x)
        Difundir, publicar, distribuir o difundir dentro del SIFCOP, material o
        información difamatorio, transgresor, obsceno, indecente o ilegal;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xi)
        Cargar al SIFCOP archivos que contengan software u otro material protegido por
        derechos de propiedad intelectual, o que transgreda el derecho a la privacidad,
        a menos que el Organismo Implementador o el/la Usuario/a sea el/la
        propietario/a o controle dichos derechos, o haya recibido las autorizaciones
        necesarias;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xii)
        Difundir, a través del SIFCOP, ideas políticas, lograr la adhesión a campañas o
        movilizaciones, captar el voto, enviar o reenviar comunicaciones masivas o
        realizar cualquier otro tipo de práctica de políticas partidarias;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xiii)
        Enviar cualquier transmisión de datos en forma fraudulenta a través del SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xiv)
        Distribuir, permutar o intercambiar con fines comerciales la información
        contenida dentro del SIFCOP.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xv)
        Modificar, copiar, transmitir, vender, ceder, distribuir, exhibir, publicar,
        licenciar, crear trabajos derivados, o usar, en general, el contenido
        disponible en o a través del SIFCOP para fines comerciales y otros sin la
        previa autorización del organismo Implementador, entre otros:</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>xvi)
        copiar, modificar, adaptar, traducir, realizar ingeniería inversa, descompilar
        o desensamblar cualquier parte del contenido y/o del SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xvii)
        reproducir y/o comunicar por cualquier medio el contenido con fines prohibidos;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xviii)
        interferir o interrumpir el funcionamiento del SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xix)
        vender, ceder, licenciar o explotar el contenido y/o cualquier tipo de acceso
        y/o uso del SIFCOP;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>xx) utilizar
        el SIFCOP con fines ilícitos o inmorales; e</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>(xxi)
        infringir de cualquier modo los presentes TyC.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.18 El/la
        usuario/a se compromete a cerrar su cuenta al final de cada sesión utilizando
        el enlace “SALIR” o “CERRAR SESIÓN”. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>2.19
        Los/as usuarios/as del SIFCOP declaran conocer y aceptar la circunstancia
        relativa a que el administrador del sistema puede, en cualquier momento,
        modificar en todo o en parte los presentes TyC.</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>&nbsp;</span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>3.
        CONSIDERACIONES LEGALES</span></b></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>3.1 A
        todos los efectos legales, en relación a estos TyC, será aplicable la
        legislación vigente en la República Argentina, especialmente la relacionadas a
        las Leyes 25.326 y 11.179 y sus modificatorias. </span></p>

        <p class=MsoNormal style='text-align:justify;line-height:150%'><span
        style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>3.2
        Cualquier controversia que se suscite entre el <a name="OLE_LINK12">organismo implementador
        </a>y/o el administrador del sistema y los/las usuarios/as habilitados en forma
        personal y/o en representación de sus respectivos organismos, con relación a
        estos TyC, su existencia, validez, calificación, interpretación, alcance,
        cumplimiento o resolución, se resolverá definitiva y exclusivamente por los
        Tribunales en lo Contencioso Administrativo Federal, con asiento en la Ciudad
        Autónoma de Buenos Aires. Asimismo, el organismo implementador se reserva el
        derecho de prorrogar la jurisdicción a la del lugar de la comisión del delito. </span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:12.0pt;line-height:150%;font-family:"Arial",sans-serif'>4.3
Toda comunicación derivada del cumplimiento de los presentes Términos y
Condiciones, deberá ser efectuada a la casilla de correo
coordinación.sifcop@minseg.gob.ar.</span></p>
      </div>
      <div class="modal-footer">
    <form id="myform"  method="POST"  class="form_statusinput">
    <input type="hidden"  name="aceptar" id="aceptar" value="1">
    <div id="button_block">
    <input type="submit" id="button" class="btn btn-primary" value="Aceptar">    

      </div>
    </div>
  </div>
</div>

