<div style="display: none">
    {{ isset($oficios->archivos->nombre) ? $oficios->archivos : null }}
</div>
@if (isset($oficios->archivos))
<div class="panel panel-info" id="listado_archivos" data-bool="existe">
    <div class="panel-heading">Archivos del Oficio</div>
    <div class="panel-body">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        Fecha de Carga
                    </th>
                    <th>
                        Ver
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($oficios->archivos as $archivo)
                <tr id="tr_archivo_{{$archivo->id}}">
                    <td>{{ $archivo->created_at }}</td>
                    <td><a href="{{ Route('archivos.descargar',['q'=>$archivo->path]) }}">Descargar</a> <a href="{{route('files.delete',[$archivo->id,$oficios->id])}}" class="eliminar_archivo" data-id-archivo="{{$archivo->id}}" data-id-oficio="{{$oficios->id}}">Eliminar</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif