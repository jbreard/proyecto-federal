<div class="row">
    {{ Form::model(isset($busqueda)?$busqueda:null,array('method'=>'get','id'=>'formBusqueda')) }}
    <div class="form-group">

             @if( Session::has('mensaje_pass'))
             <div class="container shortcut-wrapper">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul style="list-style: none">
                        <li>{{ Session::get('mensaje_pass') }}</li>
                    </ul>
                </div>
            </div>     
                @endif

        <div class="col-md-3">
            {{ Form::label('id','Nro de Requerimiento:') }}
            {{ Form::text( 'id',null, array('placeholder' => '', 'class' =>'form-control  input-sm')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('alerta_temp','Alertas Tempranas:') }}
            {{ Form::select('alerta_temp',[""=>"",'1'=>'Si','2'=>'No'],null,array('class'=>'form-control input-sm') ) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('buscar_por','Buscar Por:') }}
            {{ Form::select('buscar_por',[""=>"",'personas'=>'Personas','elementos'=>'Elementos'],null,array('class'=>'form-control input-sm') ) }}
        </div>

        <div class="col-md-3">
            {{ Form::label('fecha_desde', 'Fecha Desde:') }}
            {{ Form::text( 'fecha_desde',null, array('placeholder' => '', 'class' =>'form-control fecha input-sm')) }}
        </div>

        <div class="col-md-3">
            {{ Form::label('fecha_hasta', 'Fecha Hasta:') }}
            {{ Form::text( 'fecha_hasta',null, array('placeholder' => '', 'class' =>'form-control fecha input-sm')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('organismos', 'Organismos:') }}
            {{ Form::select( 'organismos',["0"=>""]+$combosOrganismos,null, array('placeholder' => '', 'class' =>'form-control input-sm','id'=>'id_jurisdiccion')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('','Sub Organismo:') }}
            {{ Form::select( 'sub_organismo',["0"=>""],null, array('placeholder' => '', 'class' =>'form-control  input-sm', 'id'=>'sub_organismo')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('tipo_medida', 'Tipo Medida:') }}
            {{ Form::select( 'tipo_medida',["0"=>""]+$comboTipoOficios,null , array('placeholder' => '', 'class'=>'form-control input-sm')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('req_pendientes', 'Requerimientos Pendientes:') }}
            {{ Form::select('req_pendientes',["0"=>""]+ array_except($comboSiNo,1) ,null , array('placeholder' => '', 'class'=>'form-control input-sm')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('req_mod_pendientes', 'Requerimientos Modificados:') }}
            {{ Form::select('req_mod_pendientes',["0"=>""]+array_except($comboSiNo,1),null , array('placeholder' => '', 'class'=>'form-control input-sm')) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('vigente', 'Requerimiento Vigente:') }}
            {{ Form::select( 'vigente',["0"=>"","1"=>'Si',"2"=>'No'],null,array('placeholder' => '', 'class' =>'form-control input-sm')) }}
        </div>

        <!-- Datos Persona -->
            <div class="persona">
                <div class="col-md-3">
                    {{ Form::label('nombre', 'Nombre:') }}
                    {{ Form::text( 'personas[nombre]',null , array('placeholder' => '', 'class' =>'form-control input-sm no-validate')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('apellido', 'Apellido:') }}
                    {{ Form::text( 'personas[apellido]',null, array('placeholder' => '', 'class' =>'form-control input-sm no-validate')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('nro_documento', 'N° Documento:') }}
                    {{ Form::text( 'personas[nro_documento]',null,array('placeholder' => '', 'class' =>'form-control input-sm')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('Fecha Nacimiento','Fecha Nacimiento: ') }}
                    {{ Form::text( 'personas[edad]',null, array('placeholder' => '', 'class' =>'form-control fecha input-sm')) }}
                </div>
            </div>
            <!-- datos elementos -->
            <div class="elementos">
                <div class="col-md-3">
                    {{ Form::label('tipo_elemento','Tipo de Elemento:') }}
                    {{ Form::select('tipo_elemento',[""=>"",'vehiculo'=>'Vehiculos','armas'=>'Armas',"otros"=>"Otros"],null,array('class'=>'form-control input-sm') ) }}
                </div>
            </div>

            <!-- Datos Vehiculo -->


            <div class="vehiculo tipo_elemento sub-elemento" style="display: none">

                <div class="form-group">
                    <div class="col-md-3">
                        {{ Form::label('dominio', 'Dominio') }}
                        {{ Form::text( 'vehiculo[dominio]', null, array('placeholder' => '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('anio_vehiculo', 'Año') }}
                        {{ Form::text( 'vehiculo[anio_vehiculo]',null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('marca_vehiculo', 'Marca') }}
                        {{ Form::select('vehiculo[marca_vehiculo]',$comboVehiculosMarcas ,null, array('placeholder'=> '', 'class' => 'form-control vehiculo','data-target'=>'.marca','id'=>'marca_vehiculo')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('modelo_vehiculo', 'Modelo Vehículo') }}
                        {{ Form::select('vehiculo[modelo_vehiculo]',[],null, array('placeholder'=> '', 'class' => 'form-control vehiculo','data-target'=>'.marca','id'=>'modelo_vehiculo')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('nro_chasis', 'N° de Motor') }}
                        {{ Form::text( 'vehiculo[nro_motor]',  null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('nro_chasis', 'N° de Chasis') }}
                        {{ Form::text( 'vehiculo[nro_chasis]',  null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}

                    </div>
                    

                </div>

            </div>
            <!-- Armas Tipo Elementos-->
            <div class="armas tipo_elemento sub-elemento" style="display: none">
                <div class="form-group">
                    <div class="col-md-3">
                        {{ Form::label('tipo_arma', 'Tipo De Arma') }}
                        {{ Form::text( 'armas[tipo_arma]',null, array('placeholder' => '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('marca', 'Marca') }}
                        {{ Form::text( 'armas[marca]', null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('modelo', 'Modelo') }}
                        {{ Form::text( 'armas[modelo]', null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('calibre', 'Calibre') }}
                        {{ Form::text('armas[calibre]',null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('armas[matricula]', 'Matrícula Nro.') }}
                        {{ Form::text('armas[matricula]',null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                </div>
            </div>
            <!-- Prohibicion salida del country -->
            <div class="plazo_autorizado" style="display:none">
                <div class="col-md-3">
                    {{ Form::label('autorizado', 'Requerimiento Autorizado:') }}
                    {{ Form::select( 'autorizado',["2"=>"","1"=>'Si',"0"=>'No'],null,array('placeholder' => '', 'class' =>'form-control input-sm')) }}
                </div>
            </div>
            <!-- Otros Elementos-->
            <div class="otros tipo_elemento sub-elemento" style="display: none">
                <div class="form-group">
                    <div class="col-md-3">

                        {{ Form::label('tipo_elemento_otro', 'Tipo De Elemento') }}
                        {{ Form::text( 'otros[tipo_elemento]', null, array('placeholder' => '', 'class' => 'form-control input-sm')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label('identificacion', 'Identificacion/Descripcion') }}
                        {{ Form::text( 'otros[identificacion]', null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}

                    </div>

                    <div class="col-md-3">
                        {{ Form::label('datos adicionales', 'Datos Adicionales') }}
                        {{ Form::text( 'otros[modelo]', null, array('placeholder'=> '', 'class' => 'form-control input-sm')) }}
                    </div>
                </div>
            </div>
    </div>
</div>

<div class="row" style="margin-top:1.6em!important" id="divBtns">
    <div class="col-md-2">
        <button class="btn btn-success"><i class="fa fa-search"></i> </button>
    </div>

    
    {{ Form::close() }}
</div>
@if(!$term)         
@include('terminos_condiciones')       
@endif


