@extends ('header')
@section('scripts')
<style>
    .persona {
        display: none;
    }
    .vehiculo {
        display: none;
    }
</style>
{{ HTML::script('assets/js/empty.js') }}
{{ HTML::script('assets/js/cesar_oficio.js') }}
{{ HTML::script('assets/js/busqueda.js') }}
{{ HTML::script('assets/js/autoridad_judicial.js') }}
{{ HTML::script('assets/js/plazos.js') }}


<script type="text/javascript">
    $().ready(function () {

        var global_href = "";

        $(".exportarNota").click(function(){
                    $("#pregunta").modal(function(){show:true});
                    var href = $(this).data("href");
                    global_href = href;
                    $("#exportar").attr("href",href);
        });

        $("#exportar").click(function(){
            var href = global_href;
            //alert(href);
            var dirigido = $("#dirigido").val();
            var sexo = $("#sexo_dirigido").val();
            var nombre_dirigido = $("#nombre_dirigido").val();
            var nombre_juez_fiscal = $("#nombre_juez_fiscal").val();

            href = href+"?dirigido="+dirigido+"&sexo="+sexo+"&nombre_dirigido="+nombre_dirigido+"&nombre_juez_fiscal="+nombre_juez_fiscal;
            $("#exportar").attr("href",href);
            $("#pregunta").modal('hide');
        });

        $("#pregunta").on('hidden.bs.modal',function(){
            $("#dirigido").val("F");
            $("#sexo_dirigido").val("F");
            $("#nombre_dirigido").val("");
            $("#nombre_juez_fiscal").val("");
        });

        var helper = new Helper();
        $("#id_jurisdiccion").change(function(){
            var idOrganismo = $(this).val();
            var ruta        = "suborganismos/organismo/"+idOrganismo;
            var selected = '{{$busqueda['sub_organismo'] or ''}}';

            helper.ActualizarComboFirstOptionBlank("#sub_organismo",ruta,'get',selected);
        });
        $("#id_jurisdiccion").change();
    });

</script>

@stop
@section('breadcrumb')
<li>Listados</li>
@stop
@section ('content')
<h3>Registro de Requerimientos</h3>
@include('listados.formulario_busqueda')
@if($ofi==1)
<table class="table responsive table-bordered table-hover table-striped" id="tablaOficios" style="margin-top: 1em !important">
    <thead>
    <tr>
        <td>N° Requerimiento</td>
        <td>Tipo de Medida</td>
        <td>Fecha de Carga del Req</td>
        <td>Estado</td>
        <td>Alerta Temprana</td>
        <td>Acciones</td>
    </tr>
    </thead>
    <tbody id="bodyOficios">
    @foreach ($oficios as $oficio)
    <tr class="{{ ($oficio->estado==2) ? 'danger' : 'info' }} {{ ($oficio->plazo_autorizado == 1) ? 'warning':'' }}">
        <td>{{ $oficio->id }}</td>
        <td>{{ $oficio->tipo->descripcion }} @if(($oficio->tipo->id == 9) or ($oficio->tipo->id == 5 )) ({{$oficio->medida_restrictiva}}) @endif</td>
        <td>{{ $oficio->created_at }}  </td>
        <td>
        @if($oficio->tipo_oficio==3)
               @if($oficio->estadoOficio->id == 1 && $oficio->plazo_autorizado == 1)
                   Autorizado
               @else
                   {{ $oficio->estadoOficio->descripcion or '' }} {{$oficio->oficioCesado->created_at or ''}}
               @endif
           @else
               {{ $oficio->estadoOficio->descripcion or '' }} <br>
               @if($oficio->estado == 2)
                   {{$oficio->oficioCesado->created_at or ''}}
              @endif
         @endif
	</td>
        <td>@if($oficio->alerta_temprana ==1)
            <span class="label label-danger">SI</span> @else <span class="label label-success">NO</span>@endif
        </td>
        <td>
            <a href='{{ route($oficio->tipo->ruta.".show",$oficio->id) }}' class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Ver Reporte"><i class="fa fa-eye"></i></a>
            @if ( (Auth::user()->id_perfil == 1) or (Auth::user()->id_perfil == 5) )
                <a href='{{ route("oficios.log",$oficio->id) }}' class=" tooltip-test"  data-toggle="tooltip" data-placement="top"   title="Seguimiento del Oficio"><i class="fa fa-bookmark"></i></a>
                <a href='#' data-href='{{ route("reporte_nota.pdf",$oficio->id) }}' class="tooltip-test exportarNota" id="{{$oficio->id}}" title="Generar Nota" data-toggle="tooltip" data-placement="top"><i class="fa fa-arrow-circle-o-down"></i></a>

                @if((($oficio->tipo->id == 3) or ($oficio->tipo->id == 5)) and $oficio->vencimiento_confirmado == 0)
                    <a href='{{ route("oficios.confirmar.vencimiento",$oficio->id) }}' data-id="{{$oficio->id}}" class="tooltip-test vencimientoOficio"  data-fecha="{{$oficio->plazo_fecha}}"  data-toggle="tooltip" data-placement="top" title="Vencimiento del Oficio">
                    <i class="fa fa-calendar" data-toggle="modal" data-target="#myModalVencimiento"></i></a>
                @endif
            @endif
            <a href='{{ route($oficio->tipo->ruta.".pdf",$oficio->id) }}' target="_blank" class=" tooltip-test"  data-toggle="tooltip" data-placement="top" title="Descargar Reporte"><i class="fa fa-download"></i></a>
            @if ( Auth::user()->id_perfil != 2 )
                @if ( $oficio->estado == 1 )
                <a href='#'  data-id='{{$oficio->id }}' class="abrir_modal tooltip-test"   data-toggle="tooltip" data-placement="top"  title="Dejar Sin Efecto">
                <i class="fa fa-minus-circle" data-toggle="modal"data-target="#myModal"></i></a>
                @else
                    <a href='#'  data-id='{{$oficio->id }}' class="abrir_modal tooltip-test"   data-toggle="tooltip" data-placement="top"  title="Dejar Vigente">
                        <i class="fa fa-undo" data-toggle="modal"data-target="#dejar_vigente"></i></a>
                @endif

                @if($oficio->user->getJurisdiccion->id == Auth::user()->getJurisdiccion->id or Auth::user()->id_perfil == 1 or Auth::user()->id_perfil == 5)
                    <a href='{{ route($oficio->tipo->ruta.".edit",$oficio->id) }}' class=" tooltip-test"  data-toggle="tooltip" data-placement="top"   title="Editar Requerimiento"><i class="fa fa-pencil"></i></a>
                @endif
                @if($oficio->tipo->id == 3)
                    <a href='#' data-idoficio="{{$oficio->id }}" data-toggle="modal" data-target="#modalPlazo" class="tooltip-test openModalPlazo"  data-toggle="tooltip" data-placement="top"  title="Plazo Autorizacion"><i class="fa fa-plane"></i></a>
                @endif

            @endif
            @if(Auth::user()->id_perfil==1 || Auth::user()->id_perfil==5)
            {{Form::open(['route'=>['oficios.destroy',$oficio->id],'method'=>'delete','id'=>'form_'.$oficio->id])}}
                <a  href="#" class="tooltip-test eliminar_oficio" data-id="{{$oficio->id}}"  data-toggle="tooltip" data-placement="top"  title="Eliminar Oficio"><i class="fa fa-trash-o"></i></a>
            {{Form::close()}}
            @endif
        </td>

    </tr>

    @endforeach


    </tbody>

</table>


    <div class="alert alert-danger" style="display:none; margin-top:5px" id="registrosMensaje" role="alert">No se encontraron registros para esta busqueda</div>


{{ $oficios->appends($busqueda)->links() }}
<div class="modal fade" id="pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Generar nota</h4>
            </div>
            <div class="modal-body" id="contenido-pregunta">
                <label>Dirigido a un</label>
                <select name="dirigido" id="dirigido" class="form-control input-sm">
                    <option VALUE="F">Fiscal</option>
                    <option VALUE="J">Juez</option>
                </select>
                <label>Sexo</label>
                <select name="sexo_dirigido" id="sexo_dirigido" class="form-control input-sm">
                    <option VALUE="F">Femenino</option>
                    <option VALUE="M">Masculino</option>
                </select>
                <label>Nombre del juzgado/fiscalia</label>
                <input type="text" name="nombre_dirigido" id="nombre_dirigido" class="form-control no-validate" />
                <label>Nombre del juez/fiscal</label>
                <input type="text" name="nombre_juez_fiscal" id="nombre_juez_fiscal" class="form-control no-validate"/>
            </div>
            <div class="modal-footer">
                <button id="cancelar" data-dismiss="modal" class="btn btn-success btn-sm">Cancelar</button>
                <a target="_blank" id="exportar" href="" class="btn btn-primary btn-sm">Genera Nota</a>
            </div>
        </div>
    </div>
</div>
@endif
@include('listados.modales')

@stop
