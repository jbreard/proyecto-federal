<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Dejar Sin Efecto </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="modal-error"  style="display: none;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Por favor corrige los siguentes errores:</strong>
                    <ul style="list-style: none">

                        <li id="mensaje_error" ></li>

                    </ul>
                </div>

                {{ Form::open(array('name'=>'frm_cesar','id'=>'frm_cesar','route'=>'oficios.cesar','file'=>'true')); }}
                {{ Form::label('fecha_cese','Fecha de Sin Efecto:') }}
                {{ Form::text('fecha_cese',null,array('placeholder' => '', 'class' =>'form-control fecha')) }}
                {{ Form::label('escaneo_cese','Oficio de Sin Efecto:') }}
                {{ Form::file('escaneo_cese',null,array('placeholder' => '', 'class' =>'form-control')) }}
                {{ Form::label('observaciones','Observaciones:') }}
                {{ Form::textarea('observaciones',null,array('placeholder' => '', 'class' =>'form-control')) }}
                {{ Form::hidden('id_oficio',null,array('id'=>'id_oficio','placeholder' => '', 'class' =>'form-control')) }}

            </div>
            <div class="modal-footer">
                <button type="button" id="enviar_cese" class="btn btn-primary">Guardar</button>
                {{ Form::close(); }}
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->

<div class="modal fade" id="dejar_vigente" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Dejar Vigente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Dejar Vigente el Oficio</p>
                <div id="nro_oficio"></div>
            </div>
            {{ Form::open(array('name'=>'frm_vigente','id'=>'frm_vigente','route'=>'oficios.vigente','file'=>'true')); }}
            {{ Form::hidden('id_oficio2',null,array('id'=>'id_oficio2','placeholder' => '', 'class' =>'form-control')) }}
            <div class="modal-footer">
                <button type="button" id="enviar_vigente" class="btn btn-primary">Guardar</button>
                {{ Form::close(); }}
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalVencimiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar Fecha</h4>
            </div>

            <div class="modal-body">
                <label>Fecha de Vencimiento</label>
                <input type="text" class="fecha form-control" id="fecha_vencimiento" name="plazo_fecha">
            </div>

            <div class="modal-footer">
                <button type="button" id="btnConfirmarVto" class="btn btn-primary">Confirmar »</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAutoridad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Editar Autoridad Judicial</h4>
            </div>



        <div class="modal-body" id="bodyAutoridad">

        </div>

        <div class="modal-footer">
            <button type="button" id="guardarAutoridad" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modalPlazo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Plazo</h4>
            </div>

        <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Recuerde que por este plazo, la persona va a poder salir del pais
        </div>

        <div class="modal-body">

            {{ Form::open(array('name'=>'frm-plazo','id'=>'frm-plazo','route'=>'plazos.store','files'=>'true')); }}
            {{ Form::label('fecha_inicio','Fecha de Inicio:') }}
            {{ Form::text('fecha_inicio',null,array('placeholder' => '', 'class' =>'form-control fecha')) }}
            {{ Form::label('fecha_final','Fecha de Final:') }}
            {{ Form::text('fecha_final',null,array('placeholder' => '', 'class' =>'form-control fecha')) }}
            {{ Form::label('escaneo_plazo','Oficio que Autoriza el plazo:') }}
            {{ Form::file('escaneo_plazo[]',null,array('placeholder' => '', 'class' =>'form-control')) }}
            {{ Form::label('destino','Destino:') }}
            {{ Form::text('destino',null,array('placeholder' => '', 'class' =>'form-control')) }}
            {{ Form::label('observaciones','Observaciones:') }}
            {{ Form::textarea('observaciones',null,array('placeholder' => '', 'class' =>'form-control')) }}
            {{ Form::hidden('id_oficio',null,array('id'=>'id_oficio_plazo','placeholder' => '', 'class' =>'form-control')) }}
            {{ Form::close(); }}
        </div>

        <div class="modal-footer"  id="div-plazos">
            <button type="button" id="" class="btn btn-primary btn-guardar-plazo">Guardar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPlazoAlerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Alerta</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <div  id="div-plazo-mensaje">Recuerde que por este plazo, la persona va a poder salir del pais</div>
                </div>
            </div>

            <div class="modal-footer"  id="div-plazos">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>