@extends ('header')
@section ('content')
@if(Session::get('message') )
	<?php $path = Session::get('path');?>
	<script>
        setTimeout(function(){
            window.location.href = '<?php echo 'http://sifcop-docker.minseg.gob.ar/oficio/'.$path?>';
        }, 3000);
	</script>
@endif

	<div class="row">
		<div class="form-group col-md-8">
			@if(Session::get('message') )
				<div class= "alert alert-success " role="alert">
					{{ Session::get('message')  }}
				</div>
			@else
				<div class="alert alert-warning" role="alert">
				Está por dejar Sin efecto el Oficio <b>{{ $id_oficio }}</b>!
				</div>
			@endif
		</div>
		{{ Form::open(array('route'=>'oficios.cesar','file'=>'true','method'=> 'post', 'enctype' => "multipart/form-data")) }}

		<div class="form-group col-md-8">

					{{ Form::label('fecha_cese','Fecha de Sin Efecto:') }}
					{{ Form::text('fecha_cese',null,array('placeholder' => '', 'class' =>'form-control fecha','required'=>'required' )) }}
		</div>
		<div class="form-group col-md-8">
					{{ Form::label('escaneo_cese','Oficio de Sin Efecto:') }}
					{{ Form::file('escaneo_cese',array('class' =>'form-control','required'=>'required' )) }}
		</div>
		<div class="form-group col-md-8">
					{{ Form::label('observaciones','Observaciones:') }}
					{{ Form::textarea('observaciones',null,array('placeholder' => '', 'class' =>'form-control')) }}
		</div>
					{{ Form::hidden('id_oficio',$id_oficio,array('id'=> 'id_oficio')) }}
					{{ Form::hidden('link','1',array('id'=> 'link')) }}


		<div class="form-group col-md-8">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}

			{{ Form::close() }}
		</div>
	</div>




@stop


