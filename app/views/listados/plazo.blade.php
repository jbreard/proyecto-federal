@extends ('header')
@section ('content')
	@if(Session::get('message') )
        <?php $path = Session::get('path');?>
		<script>
            setTimeout(function(){
                window.location.href = '<?php echo 'http://sifcop-docker.minseg.gob.ar/oficio/'.$path?>';
            }, 3000);
		</script>
	@endif

	<div class="row">
		<div class="form-group col-md-8">
			@if(Session::get('message') )
				<div class= "alert alert-success " role="alert">
					{{ Session::get('message')  }}
				</div>
			@else
				<div class="alert alert-info" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					Recuerde que por este plazo, la persona va a poder salir del pais <br>Oficio Nro: {{ $id_oficio }}
				</div>
			@endif
		</div>
		{{ Form::open(array('name'=>'frm-plazo','id'=>'frm-plazo','route'=>'plazos.store','files'=>'true','method'=> 'post', 'enctype' => "multipart/form-data")) }}

		<div class="form-group col-md-8">

			{{ Form::label('fecha_inicio','Fecha de Inicio:') }}
			{{ Form::text('fecha_inicio',null,array('placeholder' => '', 'class' =>'form-control fecha')) }}
		</div>
		<div class="form-group col-md-8">
			{{ Form::label('fecha_final','Fecha de Final:') }}
			{{ Form::text('fecha_final',null,array('placeholder' => '', 'class' =>'form-control fecha')) }}
		</div>
		<div class="form-group col-md-8">
			{{ Form::label('escaneo_plazo','Oficio que Autoriza el plazo:') }}
			{{ Form::file('escaneo_plazo[]',null,array('placeholder' => '', 'class' =>'form-control')) }}
		</div>
		<div class="form-group col-md-8">
			{{ Form::label('destino','Destino:') }}
			{{ Form::text('destino',null,array('placeholder' => '', 'class' =>'form-control')) }}
		</div>
		<div class="form-group col-md-8">
			{{ Form::label('observaciones','Observaciones:') }}
			{{ Form::textarea('observaciones',null,array('placeholder' => '', 'class' =>'form-control')) }}
		</div>
		{{ Form::hidden('id_oficio',$id_oficio,array('id'=>'id_oficio_plazo','placeholder' => '', 'class' =>'form-control')) }}

		{{ Form::hidden('link','1',array('id'=> 'link')) }}


		<div class="form-group col-md-8">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}

			{{ Form::close() }}
		</div>
	</div>




@stop


