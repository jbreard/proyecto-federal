@extends ('header')
@section ('content')
<h3>Seguimiento del Requermimento:<b>  </b></h3>
<hr>
<div class="row">
    <div class="col-md-2">
        <label>Generar Reporte de la Fecha: </label>
    </div>
    <div class="col-md-4">
        <select class="form-control">
            <option>--SELECCIONE FECHA--</option>
            <option></option>
            <option></option>
            <option></option>
        </select>
    </div>
    <div class="col-md-4">
        <button class="btn btn-info">Generar Reporte</button>
    </div>
</div>

<div class="row" style="margin-top:5px">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <th>Nombre Usuario</th>
                <th>Organismo</th>
                <th>Nombre del Campo</th>
                <th>Valor Antiguo</th>
                <th>Valor Al Que Se Cambio</th>
                <th>Fecha de Modificacion</th>
            </thead>
            <tbody>
            @foreach($oficio->revisionHistory as $history )

                @if($history->oldValue() !=$history->newValue())
                    <tr>
                        <td>{{ $history->userResponsible()->nombre }} {{ $history->userResponsible()->apellido }}</td>
                        <td>{{ $history->userResponsible()->getJurisdiccion->descripcion }}</td>
                        <td>{{ $history->fieldName() }}</td>
                        <td>{{ $history->oldValue() }}</td>
                        <td>{{ $history->newValue() }}</td>
                        <td>{{ $history->created_at }}</td>
                    </tr>
                @endif

            @endforeach
            </tbody>
        </table>
    </div>


</div>
@stop