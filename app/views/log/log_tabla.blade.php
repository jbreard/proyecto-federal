<h4>{{ $titulo }}</h4>
<table class="table table-responsive">
    <thead>
        <th>Campo Modificado</th>
        <th>Valor anterior</th>
        <th>Fecha de Modificación</th>
        <th>Usuario que Realizo la Modificación</th>
    </thead>
    @foreach($datos as $dato)
        <tr>
            <td>{{ $dato->field }}</td>
            <td>{{ $dato->value }}</td>
            <td>{{ $dato->created_at }}</td>
            <td>{{ $dato->getUsuario->nombre }} {{ $dato->getUsuario->apellido }} </td>
        </tr>
    @endforeach
</table>