@extends ('header')
@section ('content')
<style>
input{
    text-transform: none !important;
}
</style>

{{ Form::model(isset($subOrganismo) ? $subOrganismo: null ,$form_data, array('role' => 'form')) }}


<h1>Cargar Sub-Organismo</h1>


<div class="row">

    <div class="form-group col-md-4">
        {{ Form::label('id_organismo', 'Organismos') }}
        {{ Form::select('id_organismo',$combosOrganismos ,null, array('placeholder' => 'Ingrese Descripcion', 'class' => 'form-control')) }}
        {{ $errors->first('descripcion') }}
    </div>

    <div class="form-group col-md-4">
        {{ Form::label('descripcion', 'Nombre') }}
        {{ Form::text('descripcion', null, array('placeholder' => 'Ingrese Nombre', 'class' => 'form-control')) }}
        {{ $errors->first('descripcion') }}
    </div>

</div>
<button class="btn btn-primary">Guardar »</button>
{{ Form::close() }}
@stop
