<?php
namespace dmz;

class OficiosSoloLectura extends \Eloquent
{
    protected $table = 'oficios';



    public function getOficios(){

        $oficios = self::all();

        return $oficios;
    }


    public function user()
    {

       return $this->hasOne('User', 'id', 'id_usuario');

    }
    public function personas()
    {
        return $this->hasMany('Personas','id_oficio','id_oficio');
    }
    public function tipo()
    {
        return $this->hasOne('TiposOficios','id','tipo_oficio');
    }


}
?>