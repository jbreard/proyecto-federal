<?php namespace Usuarios\Entities;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Oficios\ClasesAbstractas\BusquedaAbstracta;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use subOrganismos\Entities\subOrganismos;


class User extends BusquedaAbstracta implements UserInterface, RemindableInterface {

    use SoftDeletingTrait;


    protected $fillable = array('id',
                                'nombre',
                                'username', 
                                'apellido',
                                'email',
                                'id_perfil',
                                'dni', 
                                'password',
                                'habilitado',
                                'id_jurisdiccion',
                                'contador_fallas',
                                'sub_organismo',
                                'permiso_pe',
                                'permiso_interconsultas',
                                'permiso_anses',
                                'term_cond',
                                'id_user_creator',
                                'pass_created',
                                'codigo_login',
                                'flag_login',
                                'cont_cod_login');
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this -> getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword() {
		return $this -> password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail() {
		return $this -> email;
	}

	public function getRememberToken() {
		return $this -> remember_token;
	}

	public function setRememberToken($value) {
		$this -> remember_token = $value;
	}

	public function getRememberTokenName() {
		return 'remember_token';
	}

	public $errors;

	public function isValid($data) {
		$rules = array( 'id_perfil'=>'exists:perfiles,id',
						'nombre'   => 'required|max:40',
						'username' => 'required|max:40|unique:usuarios,username,'.$this->id,
						'apellido' => 'required|max:40',
                        'email'    => 'required|email',
						'dni'      => 'required|numeric|unique:usuarios,dni,'.$this->id);


		$validator = \Validator::make($data, $rules);

		if ($validator -> passes()) {
			return true;
		}

		$this -> errors = $validator -> errors();

		return false;
	}
	public function isValidClave($data){

		$rules = array(
            'email'    => 'required|email',
			"clave_old"=>"required",
			"clave_nueva"=>"required|min:8|different:clave_old",
			"clave_confirma"=>"same:clave_nueva"
		);

        $messages =  array(
            'email'                   => 'Ingrese un Correo Electrónico Valido',
            'clave_old.required'      => 'La Clave Anterior Es Obligatoria',
            'clave_nueva.required'    => 'La Clave Nueva Es Obligatoria',
            'clave_nueva.min'         => 'La Clave Nueva Debe Tener 8 caracteres',
            'clave_nueva.different'   => 'La Clave Nueva Debe Ser Diferente a la Anterior',
            'clave_confirma.same'     => 'La Confirmación de La Clave debe Ser Igual A la Clave',
        );


		$validator = \Validator::make($data, $rules,$messages);

		if ($validator -> passes()) {
			return true;
		}

		$this -> errors = $validator -> errors();

		return false;

	}
    public function getNombreAttribute($value){
        return ucfirst($value);
    }
    public function getApellidoAttribute($value){
        return ucfirst($value);
    }
    public function getIdPerfilAttribute($value){
        //if(\Config::get('config.dmz')) return 2;
        return $value;
    }

    public function getJurisdiccion(){
        return $this->hasOne('Combos\Entities\CombosOrganismos','id','id_jurisdiccion');
    }

    public function getSubOrganismo(){
        return $this->hasOne('SubOrganismos\Entities\SubOrganismos','id','sub_organismo');
    }


    public function getMail(){

        return $this->hasOne('Correos\Entities\Correo','id_jurisdiccion','id_jurisdiccion');
    }




    public function getPerfil(){

        return $this->hasOne('Usuarios\Entities\Perfiles','id','id_perfil');
    }



    public function subOrganismo(){

        return $this->hasOne('SubOrganismos\Entities\SubOrganismos','id','sub_organismo');
    }









    function getFields()
    {
        return $this->fillable;
    }

    function relations()
    {
        return ['getJurisdiccion','getSubOrganismo'];
    }

    function relationsPivot()
    {
        // TODO: Implement relationsPivot() method.
        return [];
    }

    public function buscar($datos){
        $model = $this;
        foreach($datos as $key=>$value){
            if(($value<>"") and ($key != 'page')){ 
            $model = $model->where($key,'=',$value);
            }
        }

        
        return $model->paginate(15);
    }

    public function getIdUsers()
    {
        return $this->where('habilitado','=',1)->get(['id']);
    }

	public function verificarExistenciaUsuario($username)
	{
		return $this->where('username','=',$username)->first();
	}
        
	public function sumarIntentoFallido($id_usuario)
	{
	    // Si los usuarios son supervisor, desarrollo o infraestructura, no cuento los errores para que no se bloquee el usuario.
        if(!in_array($id_usuario,[1,11029,11030]))
    		$this->where('id',$id_usuario)->update(array('contador_fallas' => \DB::raw('contador_fallas + 1')));
	}

	public function deshabilitarUsuario($id)
	{
		$this->where('id',$id)->update(array('habilitado' => 0));
	}

	public function limpiarContadorErrores($id)
	{
		$this->where('id',$id)->update(array('contador_fallas' => 0));
	}


}
