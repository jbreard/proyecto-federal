<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	//'debug' =>true,
	'debug' =>true,

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

    //url' => 'https://sifcop-consultas.minseg.gob.ar',
   // 	'url' => 'https://sifcop.minseg.gob.ar',
       // 'url'=>'https://sifcop.minseg.gob.ar',
//    'url' => 'http://sifcop',
    'url' => 'http://localhost:9999',
    //'url' => 'http://192.168.126.128',


	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'America/Argentina/Buenos_Aires',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'es',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => 'I51aiTTvpj1byUwpRxRNLYMApa3PfIWX',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => array(

		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Session\CommandsServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Html\HtmlServiceProvider',
		'Illuminate\Log\LogServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Database\MigrationServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Remote\RemoteServiceProvider',
		'Illuminate\Auth\Reminders\ReminderServiceProvider',
		'Illuminate\Database\SeedServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Workbench\WorkbenchServiceProvider',
        'Oficios\Composers\ComposerServiceProvider',
        'Combos\Composers\ComposerServiceProvider',
        'Barryvdh\Debugbar\ServiceProvider',
        'Thujohn\Pdf\PdfServiceProvider',
        'Oficios\Avisos\AvisosServiceProvider',
        'Indatus\Dispatcher\ServiceProvider',
        'YOzaz\LaravelSwiftmailer\ServiceProvider',
        'Oficios\Providers\TransformersProvider',
        'Tymon\JWTAuth\Providers\JWTAuthServiceProvider'



        //php artisan generate:migration add_departamento_laboral_to_personas_table --field="departamento_laboral=string"


    ),

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => storage_path().'/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => array(

		'App'             => 'Illuminate\Support\Facades\App',
		'Artisan'         => 'Illuminate\Support\Facades\Artisan',
		'Auth'            => 'Illuminate\Support\Facades\Auth',
		'Blade'           => 'Illuminate\Support\Facades\Blade',
		'Cache'           => 'Illuminate\Support\Facades\Cache',
		'ClassLoader'     => 'Illuminate\Support\ClassLoader',
		'Config'          => 'Illuminate\Support\Facades\Config',
		'Controller'      => 'Illuminate\Routing\Controller',
		'Cookie'          => 'Illuminate\Support\Facades\Cookie',
		'Crypt'           => 'Illuminate\Support\Facades\Crypt',
		'DB'              => 'Illuminate\Support\Facades\DB',
		'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
		'Event'           => 'Illuminate\Support\Facades\Event',
		'File'            => 'Illuminate\Support\Facades\File',
		'Form'            => 'Illuminate\Support\Facades\Form',
		'Hash'            => 'Illuminate\Support\Facades\Hash',
		'HTML'            => 'Illuminate\Support\Facades\HTML',
		'Input'           => 'Illuminate\Support\Facades\Input',
		'Lang'            => 'Illuminate\Support\Facades\Lang',
		'Log'             => 'Illuminate\Support\Facades\Log',
		'Mail'            => 'YOzaz\LaravelSwiftmailer\Facade',
		'Paginator'       => 'Illuminate\Support\Facades\Paginator',
		'Password'        => 'Illuminate\Support\Facades\Password',
		'Queue'           => 'Illuminate\Support\Facades\Queue',
		'Redirect'        => 'Illuminate\Support\Facades\Redirect',
		'Redis'           => 'Illuminate\Support\Facades\Redis',
		'Request'         => 'Illuminate\Support\Facades\Request',
		'Response'        => 'Illuminate\Support\Facades\Response',
		'Route'           => 'Illuminate\Support\Facades\Route',
		'Schema'          => 'Illuminate\Support\Facades\Schema',
		'Seeder'          => 'Illuminate\Database\Seeder',
		'Session'         => 'Illuminate\Support\Facades\Session',
		'SSH'             => 'Illuminate\Support\Facades\SSH',
		'Str'             => 'Illuminate\Support\Str',
		'URL'             => 'Illuminate\Support\Facades\URL',
		'Validator'       => 'Illuminate\Support\Facades\Validator',
		'View'            => 'Illuminate\Support\Facades\View',
		'Carbon'		  => 'Carbon\Carbon',
    	'PDF'             => 'Thujohn\Pdf\PdfFacade',
    	'Notification'    => 'Oficios\Avisos\AvisosFacade',
    	'Transformer'     => 'Oficios\Facades\TransformersFacade',
    	'JWTAuth'         => 'Tymon\JWTAuth\Facades\JWTAuth',
    	'JWTFactory' => 'Tymon\JWTAuth\Facades\JWTFactory'



    ),
		'modulos' => array(
			'mensajes' => false
		),
		'apis' => array(
			'base_incremental' => array(
				'active' => true,
				#'url' => 'http://10.100.62.10:8888/sisen/',
				'url' => 'https://sisen-p.minseg.gob.ar/sisen/',
				'origen' => 'sifcop',
				'id_origen' => 5,
				'api_key' => '0ee7863c32008fce3108f929cd7fede8d7246ce3',
				'oficios_tipo_dato' => array(
					1 => 'personas',
					2 => 'vehiculos',
					3 => 'personas',
					4 => 'personas',
					5 => 'personas',
					6 => 'personas',
					7 => 'personas',
					8 => '',
					9 => '',
					10 => '',
				),
				'codigos_tipo_dato' => array(
					'personas' => 81,
					'vehiculos' => 82
				),
				'settings' => array(
					'flow' => array(

						/**
						 * Validations :
						 * Contiene switches (si/no) por 'tipo de persona'
						 * para enviarle si corresponde( -> true) los datos a la API BASE INCREMENTAL
						 * utilizado en \Oficios\Managers\Flujos\FlujoFilterManager
						 *
						 * tipo_oficio => [ tipo_persona => ( true / false ) ]
						 */

						'validations' => array(

							1 => array(//Tipo de Oficio: Busqueda de Personas Extraviadas
								1 => false,//Tipo de Persona: Victima
								2 => true,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							2 => array(//Tipo de Oficio: Pedido de Secuestro Vehicular
								1 => false,//Tipo de Persona: Victima
								2 => false,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							3 => array(//Tipo de Oficio: Prohibición Salida del País / Provincia
								1 => false,//Tipo de Persona: Victima
								2 => true,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							4 => array(//Tipo de Oficio: Pedidos de Captura / Detención
								1 => false,//Tipo de Persona: Victima
								2 => true,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							5 => array(//Tipo de Oficio: Medidas Restrictivas
								1 => false,//Tipo de Persona: Victima
								2 => true,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							6 => array(//Tipo de Oficio: Solicitud de Paradero por Comparendo
								1 => false,//Tipo de Persona: Victima
								2 => false,//Tipo de Persona: Responsable
								3 => true,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							7 => array(//Tipo de Oficio: Habeas Corpus
								1 => false,//Tipo de Persona: Victima
								2 => true,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							8 => array(//Tipo de Oficio: Pedido de Secuestro de Elementos
								1 => false,//Tipo de Persona: Victima
								2 => false,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							9 => array(//Tipo de Oficio: Otras Medidas
								1 => false,//Tipo de Persona: Victima
								2 => false,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
							10 => array(//Tipo de Oficio: Pedido de Secuestro Armas
								1 => false,//Tipo de Persona: Victima
								2 => false,//Tipo de Persona: Responsable
								3 => false,//Tipo de Persona: Buscado
								4 => false,//Tipo de Persona: Solicitante
								5 => false,//Tipo de Persona: Titular
							),
						)
					)
				)
			)
		),


);
