<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuborganismoAUsuarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('usuarios', function(Blueprint $table)
        {
            $table->integer('sub_organismo');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('usuarios', function(Blueprint $table)
        {
            $table->dropColumn("sub_organismo");
        });
	}

}
