<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personas', function(Blueprint $table)
    {
            $table->engine = 'MyISAM';
			$table->increments('id');
			$table->integer('id_oficio')->unsigned()->index('id_oficio');
			$table->integer('id_sexo')->unsigned();
			$table->integer('id_nacionalidad')->unsigned();
			$table->integer('id_tipo_documento')->unsigned();
			$table->integer('id_tipo_persona')->unsigned();
			$table->integer('id_ultimo_partido')->unsigned()->default(1);
			$table->integer('id_provincia_laboral')->unsigned()->default(1);
			$table->integer('id_partido_laboral')->unsigned()->default(1);
			$table->string('id_localidad_laboral', 100);
			$table->integer('id_compania_celular')->unsigned();
			$table->string('codArea_celular');
			$table->integer('numero_celular');
			$table->text('nombre', 65535);
			$table->text('apellido', 65535);
			$table->text('apellido_materno', 65535);
			$table->text('descripcion_persona', 65535);
			$table->date('edad');
			$table->string('nro_documento', 22);
			$table->string('apodos');
			$table->string('ultimo_calle', 25);
			$table->string('ultimo_numero', 25);
			$table->string('ultimo_piso', 25);
			$table->string('ultimo_departamento', 25);
			$table->string('nro_tarjeta_credito', 25);
			$table->string('bco_tarjeta_credito', 25);
			$table->string('ultima_localidad', 25);
			$table->string('ultimo_codigo_postal', 25);
			$table->string('ultima_provincia', 25)->default('1');
			$table->string('ocupacion', 25);
			$table->string('empleador', 25);
			$table->string('cuit', 50);
			$table->string('domicilio_laboral', 25);
			$table->integer('telefono_laboral');
			$table->string('calle_laboral', 25);
			$table->integer('numero_laboral');
			$table->integer('piso_laboral');
			$table->integer('codigo_postal_laboral');
			$table->text('observaciones', 65535);
			$table->integer('telefono');
			$table->text('observaciones_persona', 65535);
			$table->string('vinculo_victima', 25);
			$table->timestamps();
			$table->integer('departamento_laboral');
			$table->string('interseccion');
			$table->string('observaciones_domicilio');
			$table->index(['edad']);
		});

		\DB::statement('ALTER TABLE personas ADD FULLTEXT search(nombre,apellido,nro_documento)');
//		\DB::statement('ALTER TABLE personas ADD date search(edad)');

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personas');
	}

}
