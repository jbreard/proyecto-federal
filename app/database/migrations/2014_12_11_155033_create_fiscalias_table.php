<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiscaliasTable extends Migration {

    protected $table = "fiscalias";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {

            Schema::create('fiscalias', function($table) {
                $table -> engine = 'InnoDB';
                $table -> increments('id');
                $table -> string('fiscalias');
                $table -> timestamps();
            });
        }
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('fiscalias', function(Blueprint $table)
        {

        });
	}

}
