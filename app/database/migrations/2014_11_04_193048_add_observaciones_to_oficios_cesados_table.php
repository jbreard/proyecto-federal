<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddObservacionesToOficiosCesadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oficios_cesados', function(Blueprint $table)
		{
            $table->text('observaciones');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios_cesados', function(Blueprint $table)
		{
            $table->dropColumn('observaciones');
        });
	}

}
