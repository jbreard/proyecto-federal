<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeDescripcionFisica extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_descripciones_fisicas', function(Blueprint $table)
        {
            $table->integer('id_oficio');
            $table->integer('id_contextura_fisica');
            $table->string('altura',20);
            $table->string('peso',20);
            $table->integer('id_tipo_cabello');
            $table->string('largo_cabello',50);
            $table->integer('id_color_cabello');
            $table->integer('id_color_ojos');
            $table->integer('id_tez');
            $table->integer('id_senias_particulares',50);
            $table->string('detalle_senias',50);
            $table->string('observaciones',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_descripciones_fisicas');
	}

}
