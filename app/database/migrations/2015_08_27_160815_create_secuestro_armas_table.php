<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSecuestroArmasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secuestro_armas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_persona')->unsigned();
			$table->string('tipo_arma')->index('tipo_arma');
			$table->string('marca')->index('marca');
			$table->string('modelo')->index('modelo');
			$table->string('calibre')->index('calibre');
			$table->string('matricula')->index('matricula');
			$table->timestamps();
			$table->text('observaciones_armas', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secuestro_armas');
	}

}
