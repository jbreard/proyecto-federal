<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarcasTable extends Migration {

    protected $table = "vehiculos_marcas";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable($this->table)) {

            Schema::create('vehiculos_marcas', function($table) {
                $table -> engine = 'InnoDB';
                $table -> increments('id');
                $table -> string('marca');
                $table -> timestamps();
            });
        }
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('vehiculos_marcas', function(Blueprint $table)
        {

        });
	}

}
