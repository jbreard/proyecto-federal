<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlazosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plazos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_oficio');
			$table->date('fecha_inicio');
			$table->date('fecha_final');
			$table->string('destino');
			$table->text('observaciones', 65535);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plazos');
	}

}
