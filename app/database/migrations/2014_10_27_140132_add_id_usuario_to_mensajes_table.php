<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioToMensajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mensajes', function(Blueprint $table)
		{
			$table->integer('usuario_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mensajes', function(Blueprint $table)
		{
            $table->dropColumn('usuario_id');

        });
	}

}
