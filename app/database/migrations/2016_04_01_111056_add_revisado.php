<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRevisado extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
			//
			$table->boolean('revisado');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
			//
		    $table->dropColumn('revisado');

		});
	}

}
