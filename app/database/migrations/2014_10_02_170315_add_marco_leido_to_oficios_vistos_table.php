<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMarcoLeidoToOficiosVistosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oficios_vistos', function(Blueprint $table)
		{
			$table->boolean('marco_leido');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios_vistos', function(Blueprint $table)
		{
			$table->dropColumn(['marco_leido']);
		});
	}

}
