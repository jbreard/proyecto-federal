<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableGrupoOrganismosMiembros extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grupo_organismos_miembros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_grupo');
			$table->integer('id_organismo');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grupo_organismos_miembros');
	}

}
