<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oficios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_usuario')->unsigned();
			$table->integer('tipo_oficio')->unsigned()->index('tipo_oficio');
			$table->integer('nro_oficio')->unsigned();
			$table->string('nro_causa');
			$table->string('juzgado');
			$table->string('secretaria');
			$table->string('nro_causa_expediente');
			$table->string('causa_medida');
			$table->integer('estado')->default(1)->index('estado');
			$table->date('fecha');
			$table->text('observaciones', 65535);
			$table->boolean('cesado');
			$table->boolean('hora_denuncia');
			$table->string('nombre_denunciante');
			$table->string('fiscalia');
			$table->string('caratula');
			$table->date('fecha_denuncia');
			$table->string('juzgado_secretaria');
			$table->string('medida_restrictiva');
			$table->string('tipo_medida');
			$table->date('fecha_oficio');
			$table->date('plazo_fecha');
			$table->string('plazo_hr');
			$table->text('observaciones_judicial', 65535);
			$table->timestamps();
			$table->integer('alerta_temprana')->nullable()->index('alerta_temprana');
			$table->integer('confirmado')->default(1)->index('index2');
			$table->boolean('vencimiento_confirmado');
			$table->integer('nro_oficio2')->unsigned();
			$table->integer('nro_causa2')->unsigned();
			$table->string('nro_causa_expediente2');
			$table->date('fecha_oficio2');
			$table->string('juzgado2');
			$table->string('secretaria2');
			$table->text('observaciones_judicial2', 65535);
			$table->string('fiscalia2');
			$table->string('caratula2');
			$table->string('otra_fiscalia');
			$table->string('otra_fiscalia_2');
			$table->string('juzgado_otro_2');
			$table->string('juzgado_otro');
			$table->string('deleted_at', 45)->nullable();
			$table->boolean('plazo_autorizado')->default(0);
			$table->integer('notificado');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oficios');
	}

}
