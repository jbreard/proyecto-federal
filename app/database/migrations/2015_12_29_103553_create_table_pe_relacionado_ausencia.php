<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeRelacionadoAusencia extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_datos_relacionado_ausencia', function(Blueprint $table)
        {
            $table->integer('id_oficio');
            $table->date('fecha_ultima_vez');
            $table->time('hora_ultima_vez');
            $table->string('lugar_ultima_vez',255);
            $table->string('descripcion_vestimenta',255);
            $table->string('detalle_drogas',255);
            $table->string('adiccion_alcohol',1);
            $table->string('tratamiento_institucion',100);
            $table->date('fecha_tratamiento_institucion');
            $table->string('padecimiento_mental');
            $table->string('padecimiento_institucion');
            $table->date('fecha_tratamiento_padecimiento_mental');
            $table->integer('id_institucion_momento_ausencia');
            $table->string('institucion_momento_ausencia',100);
            $table->string('internacion_estadia_voluntaria',1);
            $table->string('autoridad_ordeno_medida',100);
            $table->string('direccion',50);
            $table->string('nro',10);
            $table->string('piso',5);
            $table->string('dpto',5);
            $table->string('codigo_postal',10);
            $table->integer('id_provincia');
            $table->integer('id_partido');
            $table->string('localidad',50);
            $table->string('situacion_calle',1);
            $table->string('lugares_frecuentaba',255);
            $table->string('especifique_enfermedad',255);
            $table->string('especifique_tratamiento_medico',255);
            $table->string('especifique_alguna_medicacion',255);
            $table->string('especifique_situacion_conflicto',255);
            $table->string('sustraccion_parental',1);
            $table->string('primera_vez_ausenta',1);
            $table->string('especifique_documentacion_consigo',255);
            $table->string('compania_alguien',255);
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_datos_relacionados_ausencia');
	}

}
