<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdMensajeRespuestaToMensajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mensajes', function(Blueprint $table)
		{
			$table->integer("id_mensaje_respuesta");
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mensajes', function(Blueprint $table)
		{
            $table->dropColumn("id_mensaje_respuesta");
			
		});
	}

}
