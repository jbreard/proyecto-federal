<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOficiosLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oficios_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_oficio')->unsigned()->index('oficios_log_id_oficio_foreign');
			$table->string('field');
			$table->string('value')->nullable();
			$table->integer('id_usuario');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oficios_log');
	}

}
