<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCumplioAnioOficios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('oficios', function(Blueprint $table)
        {
            $table->integer('cumplio_anio');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('oficios', function(Blueprint $table)
        {
            $table->dropColumn("cumplio_anio");
        });
	}

}
