<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecuestroArmasTable extends Migration {

    protected $table = "secuestro_armas";

    public function up() {
        if (!Schema::hasTable($this->table)) {

            Schema::create($this->table, function($table) {
                $table -> engine = 'InnoDB';

                $table -> increments('id');
                $table ->integer('id_persona')->unsigned();

                $table -> string('tipo_arma');
                $table -> string('marca');
                $table -> string('modelo');
                $table -> string('calibre');
                $table -> string('matricula');

                $table -> timestamps();
            });
        }
    }
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
