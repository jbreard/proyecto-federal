<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuborganismosTable extends Migration {

public function up()
	{
		Schema::create('suborganismos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_organismo');
			$table->text("descripcion");
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suborganismos');
	}

}

