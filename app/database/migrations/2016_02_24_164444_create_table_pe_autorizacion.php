<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeAutorizacion extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_autorizacion', function(Blueprint $table)
        {
            $table->integer('id_autorizacion')->unsigned();
            $table->integer('id_oficio');
            $table->string('path',255);
            $table->string('observaciones',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_fotos');
	}

}
