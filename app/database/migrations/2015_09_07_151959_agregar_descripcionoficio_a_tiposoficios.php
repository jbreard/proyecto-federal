<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarDescripcionoficioATiposoficios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tipos_oficios', function(Blueprint $table)
        {
            $table->string('descripcion_oficio');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('tipos_oficios', function(Blueprint $table)
        {
            $table->dropColumn("descripcion_oficio");
        });
	}

}
