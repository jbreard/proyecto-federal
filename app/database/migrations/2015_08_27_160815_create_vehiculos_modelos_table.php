<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiculosModelosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehiculos_modelos', function(Blueprint $table)
		{
			$table->integer('id')->default(0)->unique('id');
			$table->integer('id_marca')->nullable();
			$table->string('modelo', 42)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehiculos_modelos');
	}

}
