<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudParaderoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solicitud_paradero', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_persona')->unsigned();
			$table->boolean('primera_desaparece');
			$table->boolean('enfermedad');
			$table->boolean('drogadiccion');
			$table->boolean('internado_institucion');
			$table->text('descripcion_caso', 65535);
			$table->text('novedades', 65535);
			$table->text('enfermedades', 65535);
			$table->text('concurrencia', 65535);
			$table->text('descipcion_persona', 65535);
			$table->text('lugar_visto', 65535);
			$table->date('fecha_visto');
			$table->time('hora_visto');
			$table->string('medicacion');
			$table->string('vinculo_desaparecido');
			$table->string('pertenencia_referencia');
			$table->string('vestimenta');
			$table->string('descripcion_fisica');
			$table->text('observaciones', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitud_paradero');
	}

}
