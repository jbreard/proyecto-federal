<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOficiosCesadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oficios_cesados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_oficio')->unsigned();
			$table->date('fecha_cese');
			$table->string('tipo_medida', 40);
			$table->string('persona_auto', 1);
			$table->string('valor');
			$table->string('path')->nullable();
			$table->integer('id_usuario')->unsigned();
			$table->string('descripcion');
			$table->timestamps();
			$table->text('observaciones', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oficios_cesados');
	}

}
