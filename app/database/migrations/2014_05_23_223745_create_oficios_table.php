<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficiosTable extends Migration {
    protected  $table = 'oficios';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create($this->table, function($table) {
            $table -> engine = 'InnoDB';

            $table -> increments('id');

            $table -> integer('id_usuario')->unsigned();

            $table -> integer('tipo_oficio')->unsigned();

            $table -> integer('nro_oficio')->unsigned();

            $table -> integer('nro_causa')->unsigned();

            $table -> string('juzgado',255);

            $table -> string('secretaria',255);

            $table -> string('nro_causa_expediente',255);

            $table -> string('causa_medida',255);

            $table -> integer('estado')->default('1');

            $table -> date('fecha');

            $table -> text('observaciones');

            $table -> boolean('cesado');

            $table -> boolean('hora_denuncia');

            $table -> string('nombre_denunciante');

            $table -> string('fiscalia',255);

            $table -> string('caratula',255);

            $table -> date('fecha_denuncia',40);

            $table -> string('juzgado_secretaria',255);

            $table -> string('medida_restrictiva',255);

            $table -> string('tipo_medida',255);

            $table -> date('fecha_oficio');

            $table -> date('plazo_fecha');

            $table -> string('plazo_hr');

            $table -> text('observaciones_judicial');

            $table -> timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop($this->table);
	}

}
