<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSecuestroVehicularTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secuestro_vehicular', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_oficio')->unsigned();
			$table->integer('id_persona')->unsigned();
			$table->string('dominio')->index('dominio');
			$table->string('marca_vehiculo', 25)->index('marca_vehiculo');
			$table->string('modelo_vehiculo', 25)->index('modelo_vehiculo');
			$table->string('nro_chasis')->index('nro_chasis');
			$table->string('nro_motor');
			$table->string('radicacion', 25);
			$table->string('juzgado_secretaria');
			$table->integer('anio_vehiculo')->index('anio_vehiculo');
			$table->timestamps();
			$table->string('marca_motor')->nullable();
			$table->string('color')->nullable();
			$table->integer('id_tipo_vehiculo')->nullable()->default(1);
			$table->string('otra_marca')->nullable();
			$table->string('otro_tipo_vehiculo', 250);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secuestro_vehicular');
	}

}
