<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMiembrosxoperadoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('miembrosxoperadores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_miembro');
			$table->integer('id_operador');
			$table->string('nombre_operador', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('miembrosxoperadores');
	}

}
