<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMensajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mensajes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('asunto', 65535);
			$table->text('contenido', 65535);
			$table->text('adjuntos', 65535);
			$table->timestamps();
			$table->integer('usuario_id');
			$table->integer('id_mensaje_respuesta');
			$table->integer('id_organismo')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mensajes');
	}

}
