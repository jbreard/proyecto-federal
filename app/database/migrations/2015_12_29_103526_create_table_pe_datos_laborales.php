<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeDatosLaborales extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_datos_laborales', function(Blueprint $table)
        {
            $table->integer('id_oficio');
            $table->string('profesion_ocupacion',50);
            $table->string('empleador',50);
            $table->string('telefono',50);
            $table->string('direccion',50);
            $table->string('nro',20);
            $table->string('piso',10);
            $table->string('dpto');
            $table->string('codigo_postal',10);
            $table->integer('id_provincia');
            $table->integer('id_partido');
            $table->string('localidad',50);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_datos_laborales');
	}

}
