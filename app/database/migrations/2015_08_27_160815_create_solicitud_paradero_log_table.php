<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudParaderoLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solicitud_paradero_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_solicitud_paradero')->unsigned()->index('solicitud_paradero_log_id_solicitud_paradero_foreign');
			$table->string('field');
			$table->string('value');
			$table->integer('id_usuario');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitud_paradero_log');
	}

}
