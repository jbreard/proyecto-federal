<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModificacionesVistasTable extends Migration {

    protected $table = "modificaciones_vistas";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable($this->table)) {

            Schema::create($this->table, function($table) {
                $table -> engine = 'InnoDB';
                $table -> increments("id");
                $table -> integer("id_oficio")->unsigned();
                $table -> boolean("visto");
                $table -> timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasTable($this->table)) {
            Schema::drop($this->table);
        }

    }

}
