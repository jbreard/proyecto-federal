<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration {

    protected $table = "vehiculos_modelos";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {

            Schema::create('vehiculos_modelos', function($table) {
                $table -> engine = 'InnoDB';
                $table -> increments('id');
                $table -> integer('id_marca');
                $table -> string('modelo');
                $table -> timestamps();
            });
        }
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('vehiculos_modelos', function(Blueprint $table)
        {

        });
	}

}
