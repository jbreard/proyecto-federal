<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeDenunciante extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_datos_denunciante', function(Blueprint $table)
        {
            // scram - itil
            $table->integer('id_oficio');
            $table->string('apellido',100);
            $table->string('nombre',100);
            $table->integer('id_tipo_documento');
            $table->string('nro_documento',20);
            $table->string('vinculo',50);
            $table->string('direccion',100);
            $table->string('nro',10);
            $table->string('piso',5);
            $table->string('dpto',5);
            $table->string('codigo_postal',10);
            $table->integer('id_provincia');
            $table->integer('id_partido');
            $table->string('cod_area_telefono',10);
            $table->string('telefono',50);
            $table->integer('id_compania_telefono');
            $table->string('dependencia',100);
            $table->date('fecha_denuncia');
            $table->string('relato',255);
            $table->string('nota_adjunta',100);
            $table->string('intervencion_otros_organismos',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_datos_denunciante');
	}

}
