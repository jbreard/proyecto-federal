<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSecuestroVehicularLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secuestro_vehicular_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_secuestro_vehicular')->unsigned()->index('secuestro_vehicular_log_id_secuestro_vehicular_foreign');
			$table->string('field');
			$table->string('value');
			$table->integer('id_usuario');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secuestro_vehicular_log');
	}

}
