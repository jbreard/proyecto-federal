<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeNrosCelulares extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_nros_celulares', function(Blueprint $table)
        {
            $table->increments('id_celular')->unsigned();
            $table->integer('id_oficio');
            $table->string('cod_area',10);
            $table->string('telefono',30);
            $table->integer('id_compania');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_nros_celulares');
	}

}
