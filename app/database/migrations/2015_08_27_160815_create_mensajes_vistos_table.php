<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMensajesVistosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mensajes_vistos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_mensaje')->index('id_mensaje');
			$table->integer('id_usuario')->index('id_usuario');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mensajes_vistos');
	}

}
