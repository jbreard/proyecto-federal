<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContadorNotificaciones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	protected $tableName = 'contador_notificaciones';

	public function up()
	{
		Schema::create($this->tableName, function(Blueprint $table)
		{
			$table->integer('id')->default(0)->unique('id');
			$table->integer('id_organismo')->index();
			$table->integer('mensajes_nuevos');
			$table->integer('oficios_nuevos');
			$table->integer('oficios_modificados');
			$table->timestamps();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->tableName);
	}

}
