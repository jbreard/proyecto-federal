<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMiembrosxresponsablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('miembrosxresponsables', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_miembro');
			$table->integer('id_responsable');
			$table->string('nombre_responsable', 50)->nullable();
			$table->string('email_responsable', 50)->nullable();
			$table->string('telefono_responsable', 50)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('miembrosxresponsables');
	}

}
