<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposOficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipos_oficios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descripcion');
			$table->string('ruta');
			$table->timestamps();
			$table->integer('orden');
			$table->string('alerta_temprana', 45)->nullable();
			$table->string('titulo_alerta_temprana', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipos_oficios');
	}

}
