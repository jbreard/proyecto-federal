<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeFotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_fotos', function(Blueprint $table)
        {
            $table->increments('id_foto')->unsigned();
            $table->integer('id_oficio');
            $table->integer('id_usuario');
            $table->string('path',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_fotos');
	}

}
