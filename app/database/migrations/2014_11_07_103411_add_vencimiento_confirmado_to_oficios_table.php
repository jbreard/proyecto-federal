<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddVencimientoConfirmadoToOficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
			$table->boolean("vencimiento_confirmado");
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
			$table->dropColumn("vencimiento_confirmado");
		});
	}

}
