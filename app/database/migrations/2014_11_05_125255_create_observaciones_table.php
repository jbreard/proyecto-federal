<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservacionesTable extends Migration {
    protected $table = 'observaciones';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::create($this->table, function($table) {

            $table -> engine = 'InnoDB';

            $table -> increments('id');

            $table -> integer('id_usuario')->unsigned();

            $table -> integer('id_oficio')->unsigned();

            $table -> text('observaciones');

            $table -> timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop($this->table);
    }

}
