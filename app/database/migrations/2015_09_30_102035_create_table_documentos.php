<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('documentos', function(Blueprint $table)
        {
            $table->increments('id_documento');
            $table->integer('id_tipo_documento');
            $table->integer('nro_documento');
            $table->integer('id_persona');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('documentos');
	}

}
