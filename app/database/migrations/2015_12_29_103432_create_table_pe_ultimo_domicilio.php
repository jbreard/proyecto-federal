<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeUltimoDomicilio extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_ultimos_domicilios', function(Blueprint $table)
        {
            $table->integer('id_oficio');
            $table->string('direccion',50);
            $table->string('nro',10);
            $table->string('piso',5);
            $table->string('dpto',5);
            $table->string('interseccion',50);
            $table->string('codigo_postal',10);
            $table->integer('id_provincia');
            $table->integer('id_partido');
            $table->string('localidad',50);
            $table->string('observaciones',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_ultimos_domicilios');
	}

}
