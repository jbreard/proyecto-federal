<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableConsultasPositivasOficios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consultas_positivas_oficios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_consulta');
			$table->integer('id_oficio');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consultas_positivas_oficios');
	}

}
