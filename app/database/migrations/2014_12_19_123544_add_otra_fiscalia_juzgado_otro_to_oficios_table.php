<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOtraFiscaliaJuzgadoOtroToOficiosTable extends Migration {

    protected $table = "oficios";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
        Schema::table('oficios', function(Blueprint $table){

                    $table -> string('otra_fiscalia');

                    $table -> string('juzgado_otro');

                    $table -> string('otra_fiscalia_2');

                    $table -> string('juzgado_otro_2');
                    });
        }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
			
		});
	}

}
