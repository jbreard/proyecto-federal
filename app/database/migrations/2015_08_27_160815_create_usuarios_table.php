<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sistema');
			$table->string('nombre', 25);
			$table->string('apellido', 25);
			$table->string('email', 50);
			$table->string('password');
			$table->integer('dni');
			$table->boolean('habilitado')->default(1);
			$table->boolean('cambio_clave')->default(0);
			$table->integer('id_perfil')->unsigned()->index('usuarios_id_perfil_foreign');
			$table->integer('id_jurisdiccion')->unsigned()->default(1);
			$table->string('username', 25);
			$table->string('remember_token', 100);
			$table->boolean('contador_fallas')->default(0);
			$table->timestamps();
			$table->date('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
