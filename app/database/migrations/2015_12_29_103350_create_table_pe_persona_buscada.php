<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePePersonaBuscada extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pe_personas_buscadas', function(Blueprint $table)
        {
            $table->integer('id_oficio');
            $table->string('apellido',50);
            $table->string('nombres',50);
            $table->string('sobrenombre',50);
            $table->integer('id_nacionalidad');
            $table->date('fecha_nacimiento');
            $table->integer('sexo');
            $table->integer('id_estado_civil');
            $table->string('nombre_conyuge',50);
            $table->string('apellido_nombre_paterno',100);
            $table->string('apellido_nombre_materno',100);
            $table->integer('id_tipo_documento');
            $table->string('nro_documento',15);
            $table->string('autoridad_expide',100);
            $table->string('cod_area_telefono',10);
            $table->string('telefono',20);
            $table->integer('id_compania_telefono');
            $table->string('facebook',50);
            $table->string('twitter',50);
            $table->string('badoo',50);
            $table->string('instagram',50);
            $table->string('linkedin',50);
            $table->string('snapchat',50);
            $table->string('otro',255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pe_personas_buscadas');
	}

}
