<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCamposJudicialToOficiosTable extends Migration {


    protected $table = "oficios";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oficios', function(Blueprint $table)
		{
            if (Schema::hasTable($this->table)) {

                Schema::table($this->table, function($table) {

                    $table -> integer('nro_oficio2')->unsigned();

                    $table -> integer('nro_causa2')->unsigned();

                    $table -> string('nro_causa_expediente2',255);

                    $table -> date('fecha_oficio2');

                    $table -> string('juzgado2',255);

                    $table -> string('secretaria2',255);

                    $table -> text('observaciones_judicial2');

                    $table -> string('fiscalia2',255);

                    $table -> string('caratula2',255);


                });

            }
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oficios', function(Blueprint $table)
		{

		});
	}

}
