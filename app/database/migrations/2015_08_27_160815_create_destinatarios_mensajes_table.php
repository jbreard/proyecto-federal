<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDestinatariosMensajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destinatarios_mensajes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_mensaje')->index('id_mensaje');
			$table->integer('id_correo')->index('id_correo');
			$table->timestamps();
			$table->integer('id_organismo')->nullable();
			$table->unique(['id_mensaje','id_organismo'], 'id_mensaje_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destinatarios_mensajes');
	}

}
