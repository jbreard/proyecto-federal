<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableDestinatariosConsultasPositivas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destinatarios_consultas_positivas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_consulta_positiva');
			$table->integer('id_organismo');
			$table->boolean('leido');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destinatarios_consultas_positivas');
	}

}
