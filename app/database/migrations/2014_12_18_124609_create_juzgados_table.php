<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJuzgadosTable extends Migration {

    protected $table = "juzgados";
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable($this->table)) {

            Schema::create('juzgados', function($table) {
                $table -> engine = 'InnoDB';
                $table -> increments('id');
                $table -> string('juzgado');
                $table -> timestamps();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
