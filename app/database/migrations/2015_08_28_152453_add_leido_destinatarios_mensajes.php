<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeidoDestinatariosMensajes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	protected $tableName = 'destinatarios_mensajes';

	public function up()
	{
		
		Schema::table($this->tableName, function($table){
    		$table->boolean('leido')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table($this->tableName, function($table){
    		$table->dropColumn('leido');
		});
	}

}
