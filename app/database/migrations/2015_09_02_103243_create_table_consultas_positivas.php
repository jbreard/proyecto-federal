<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableConsultasPositivas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consultas_positivas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_usuario');
			$table->text('parametros_busqueda');
			$table->text('resultados');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consultas_positivas');
	}

}
