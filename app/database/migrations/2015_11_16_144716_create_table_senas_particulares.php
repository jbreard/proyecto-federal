<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSenasParticulares extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {

        Schema::create('senas_particulares', function(Blueprint $table)
        {
            $table->increments('id_senas_particulares');
            $table->string('senas_particulares');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('senas_particulares');
    }

}
