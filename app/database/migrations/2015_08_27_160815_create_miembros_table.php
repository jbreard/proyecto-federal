<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMiembrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('miembros', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_organismo');
			$table->string('nombre_area', 256)->nullable();
			$table->string('direccion_area', 100)->nullable();
			$table->string('telefono_area', 50)->nullable();
			$table->string('email_area', 50)->nullable();
			$table->boolean('habilitado');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('miembros');
	}

}
