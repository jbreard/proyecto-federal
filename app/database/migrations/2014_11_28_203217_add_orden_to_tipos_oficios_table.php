<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrdenToTiposOficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tipos_oficios', function(Blueprint $table)
		{
			$table->integer("orden");
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tipos_oficios', function(Blueprint $table)
		{
			$table->dropColumn("orden");
		});
	}

}
