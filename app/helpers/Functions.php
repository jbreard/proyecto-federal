<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 31/08/16
 * Time: 18:01
 */

class Functions
{
    public static function darComa($value)
    {
        if($value == "")
            return "";
        else
            return ",";
    }

    public static function darComaConEspacio($value)
    {
        if($value == "")
            return "";
        else
            return ", ";
    }
}