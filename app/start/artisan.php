<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/
Artisan::add(new OficiosSinEfectoCommand);
Artisan::add(new OficiosPlazosAutorizadosCommands);
Artisan::add(new EliminarPlazosAutorizadosCommands);
Artisan::add(new WebSocket);
Artisan::add(new AlertasTempranas);
Artisan::add(new WebSocket);
Artisan::add(new MigracionLeidos);
Artisan::add(new TransaccionPrimariaSISEN);
Artisan::add(new TransaccionPrimariaSISENInitProcess);

