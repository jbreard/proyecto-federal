<?php
use Oficios\Managers\OficiosVistoManager;
use Oficios\Repositories\ModificacionesRepo;
use Oficios\Managers\ModificacionesManager;
use Oficios\Repositories\ModificacionesVistasRepo;

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/



App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/



Route::filter('auth', function()
{
    

    try{
        if(!JWTAuth::parseToken()->authenticate()) return Redirect::guest('login');
    } catch (Exception $e) {
            #return Auth::user();
    }


    

	if (Auth::guest()) return Redirect::guest('login');

    if(Auth::user()->last_sessid != \Session::getId() ) return Redirect::to('logout');

});

Route::filter('oficioVisto',function($ruta){

   if($ruta->getParameter("id")){

       $id                  = $ruta->getParameter("id");

       $oficiosVistoManager = new OficiosVistoManager();
       $oficiosVistoManager->save($id);
   }

});


Route::filter('alerta_temprana',function($ruta){
    $param = Input::get('alerta_temp',false);
    if($param){
        Session::put('alerta_temprana',1);
    }else{
        if(Session::has('alerta_temprana')){
            Session::forget('alerta_temprana');
        }

    }

});


Route::filter('forget_alerta_temprana',function($ruta){
    if(Session::has('alerta_temprana')){
        Session::forget('alerta_temprana');
    }
});




Route::filter("mail",function($ruta){

    if($ruta->getParameter("id")){
        $id                  = $ruta->getParameter("id");


    }


});


Route::filter('forgot',function($ruta){

    Session::forget('oficio_id');


});



Route::filter('oficiosModificacion',function($ruta){




    if($idOficio = $ruta->getParameter("id")){
        $repo   = new ModificacionesRepo();
        $model = $repo->consultaModel($idOficio);


        if($model){
            $idMod = $model->id;
            $repoVistas = new ModificacionesVistasRepo();
            $existeModVista = $repoVistas->existe($idMod);

             if(count($existeModVista)<=0){
                 $repoVistas = new ModificacionesVistasRepo();
                 $entity = $repoVistas->newModVistas($idMod);

                 $repoManager = new \Oficios\Managers\ModificacionesVistasManager($entity,[]);
                 $repoManager->save();

             }
        }
    }
});


//controlamos todos los roles de usuarios desde aquí
Route::filter('role',  function($ruta,$peticion,$roles,$redirect)
{

    $roles = explode("-", $roles);
    if(!in_array(Auth::user()->id_perfil,$roles))
        return Redirect::to($redirect);
        
});

Route::filter('cambio_clave',function($ruta='usuarios.cambiar_clave'){

    if (Auth::guest()) return Redirect::guest('login');

    if(Auth::user()->cambio_clave == 0  ){

        return Redirect::route('usuarios.clave');
    }

});









Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Route::filter('search_personas_extraviadas', function()
{
    if(Auth::user()->permiso_pe != 1){
        return Redirect::to('inicio');
    }
});