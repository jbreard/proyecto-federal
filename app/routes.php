<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Oficios\Entities\Oficios;




if(\Config::get('config.dmz')) {
    Route::get('oficios/indexJson','OficiosController@indexJson');
    Route::get('oficios/listanegravehiculos','OficiosController@getSecuestrosVehicular');
}





Route::get('test',function(){
    $oficios = new Oficios();
    $oficios = $oficios->where('created_at','<=',Carbon::now()->subYears(1))->get();
    dd($oficios->toArray());



});


Route::group(array('before' => 'auth'), function () {

    Route::get('ip',function(){
    });
});




Route::get('login', 'AuthController@showLogin');
Route::get('validate', 'AuthController@codigo');
Route::post('validate', 'AuthController@codigo');

Route::get('protection', 'ProtectionController@generateProtection');
Route::get('refresh-protection', 'ProtectionController@generateProtection');

// Validamos los datos de inicio de sesión.
Route::post('login', 'AuthController@postLogin');


Route::group(array('before' => 'auth'), function () {
    
    Route::get('usuarios/changePass/{id}',['as' => 'usuarios.changePass',"uses"=>"usuariosController@cambiarClave"]);


    Route::get("usuarios/clave", array('as' => 'usuarios.clave', 'uses' => 'UsuariosController@getCambiarClave'));

    Route::post("usuarios/clave", array('as' => 'usuarios.clave', 'uses' => 'UsuariosController@postCambiarClave'));
    
    Route::get("estadisticas/requerimientos", array('as' => 'estadistica.requerimientos', 'uses' => 'EstadisticasController@requerimientos'));
    Route::get("estadisticas/getRequerimientos/mes", array('as' => 'estadistica.getRequerimientos', 'uses' => 'EstadisticasController@getReqMes'));
    Route::get("estadisticas/getRequerimientosPorTipo", array('as' => 'estadistica.getRequerimientosTipo', 'uses' => 'EstadisticasController@getReqTipo'));
    Route::get("estadisticas/getTiposReq", array('as' => 'estadistica.getTiposOficios', 'uses' => 'EstadisticasController@getTiposOficios'));
    Route::get("estadisticas/getOrganismos", array('as' => 'estadistica.getOrganismos', 'uses' => 'EstadisticasController@getOrganismos'));
    Route::get("estadisticas/getReqPorOrganismos", array('as' => 'estadistica.getReqPorOrganismos', 'uses' => 'EstadisticasController@getReqPorOrganismos'));


});


Route::post("pedidos_captura/agregarDocumento", array("as" => 'pedidos_captura.agregarDocumento', 'uses' => 'PedidosCapturaController@agregarDocumento'));



Route::when('archivos/*','auth');

Route::group(array('before'=>'cambio_clave'),function() {
    Route::group(array('before' => 'auth'), function () {

            Route::post("usuarios/aceptar",array("as" => "usuarios.aceptar", "uses" => "UsuariosController@aceptar"));
        Route::get('protection', 'ProtectionController@generateProtection');
        Route::get('refresh-protection', 'ProtectionController@generateProtection'); 
        
        Route::resource('oficios','OficiosController');
        Route::get('oficios/busqueda',array('as'=>'oficios.buscar','uses'=>'OficiosController@index'));
        Route::get('files/descargar',array('as'=>'archivos.descargar','uses'=>function(){
            $path = Input::get('q');
            return Response::download($path);
        }));


        //Permisos para el usuario superadmin (5), solo el puede editar y para el accesso a la administracion del sistema
            Route::group(array('before' => 'role:1-5,inicio','after'=>'oficiosModificacion'), function () {
            Route::get("admin/oficios_anio",["as"=>"oficios_anio.index","uses"=>"OficiosController@adminOficiosByAnio"]);
            Route::resource("admin", "AdminController");
            Route::get('consultas_positivas',["as"=>"consultas_positivas.index","uses"=>"ConsultaPositivaController@index"]);
            Route::get('consultas_positivas/{idConsulta}',["as"=>"consulta_positiva.show","uses"=>"ConsultaPositivaController@show"]);

            Route::get("oficios_anio",["as"=>"oficios_anio.get","uses"=>"OficiosController@getOficiosByAnio"]);


            Route::post("miembros/validarDatos",array('as' => 'miembros.validarDatos','uses'  => 'MiembrosController@validarDatos'));
            Route::post("miembros/{id}/actualizar",array('as' => 'miembros.actualizar','uses'  => 'MiembrosController@actualizar'));
            Route::post("miembros/eliminarOperador",array('as' => 'miembros.eliminarOperador','uses'  => 'MiembrosController@eliminarOperador'));
            Route::post("miembros/agregarOperador",array('as' => 'miembros.agregarOperador','uses'  => 'MiembrosController@agregarOperador'));
            Route::post("miembros/listadoOperadores",array('as' => 'miembros.listadoOperadores','uses'  => 'MiembrosController@listadoOperadores'));
            Route::post("miembros/listadoResponsables",array('as' => 'miembros.listadoResponsables','uses'  => 'MiembrosController@listadoResponsables'));
            Route::post("miembros/eliminarResponsable",array('as' => 'miembros.eliminarResponsable','uses'  => 'MiembrosController@eliminarResponsable'));
            Route::post("miembros/agregarResponsable",array('as' => 'miembros.agregarResponsable','uses'  => 'MiembrosController@agregarResponsable'));

            Route::get("suborganismos/organismo/{idOrganismo}",array('as' => 'suborganismo.byorganismo','uses'  => 'SubOrganismoController@getSubOrganismoByIdOrg'));

            Route::resource("miembros", "MiembrosController",array('except' => array('show','index')));


            Route::resource("usuarios", "UsuariosController");
            Route::resource("panel","AdminController");
            Route::resource("mails","CorreosController");
            Route::resource("subOrganismos","SubOrganismoController");
            Route::post('usuarios/restablecer/clave',array("as"=>"usuarios.restablecer","uses"=>"UsuariosController@RestablecerClave"));
//            Route::resource("solicitud_paradero", "OficioSolicitudParaderoController",array('except' => array('show','create','update','edit')));
            Route::resource("solicitud_paradero", "PersonaExtraviadaController",array('except' => array('show','create','update','edit')));
            Route::post("solicitud_paradero/agregarCelular",array('as' => 'solicitud_paradero.agregarCelular','uses'  => 'PersonaExtraviadaController@agregarCelular'));
            Route::post("solicitud_paradero/eliminarCelular",array('as' => 'solicitud_paradero.eliminarCelular','uses'  => 'PersonaExtraviadaController@eliminarCelular'));
//            Route::post("solicitud_paradero/agregarCelular",array('as' => 'solicitud_paradero.agregarCelular','uses'  => 'OficioSolicitudParaderoController@agregarCelular'));
//            Route::post("solicitud_paradero/eliminarCelular",array('as' => 'solicitud_paradero.eliminarCelular','uses'  => 'OficioSolicitudParaderoController@eliminarCelular'));
            Route::get("solicitud_paradero/{rute}/{name}/mostrar_archivo",['as' => 'solicitud_paradero.mostrar_archivo','uses' => 'PersonaExtraviadaController@show']);
            Route::get('solicitud_paradero/show/{id}',["as"=>'solicitud_paradero.show',"uses"=>"PersonaExtraviadaController@showReporte"]);
            Route::resource("secuestro_elementos", "SecuestroElementosController",array('except' => array('show','create','update','edit')));
            Route::resource("secuestro_armas", "SecuestroArmasController",array('except' => array('show','create','update','edit')));
            Route::resource("secuestro_vehicular", "PedidosSecuestroVehicularController",array('except' => array('show','create','update','edit')));
            Route::resource("prohibicion_salida", "ProhibicionSalidaPaisController",array('except' => array('show','create','update','edit')));
            Route::resource("pedidos_captura", "PedidosCapturaController",array('except' => array('show','create','update','edit')));
            Route::resource("medidas_restrictivas", "MedidasRestrictivasController",array('except' => array('show','create','update','edit')));
            Route::resource("solicitud_paradero2", "SolicitudParaderoReqJudController",array('except' => array('show','create','update','edit')));
            Route::resource("habeas_corpus", "HabeasCorpusController",array('except' => array('show','create','update','edit')));
            Route::resource("otras_medidas", "OtrasMedidasController",array('except' => array('show','create','update','edit')));
            Route::get("oficios/vencimiento/{id}",["as"=>"oficios.confirmar.vencimiento","uses"=>"OficiosController@confirmarVencimiento"]);
            Route::get('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@getAutoridad"]);
            Route::post('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@setAutoridad"]);
            Route::get('grupos_correos',["as"=>"grupo_correos.index","uses"=>"GruposCorreosController@index"]);
            Route::get('grupos_correos/getGrupos',["as"=>"grupo_correos.getGrupos","uses"=>"GruposCorreosController@getGrupos"]);
            Route::post('grupos_correos/grupos/create',["as"=>"grupo_correos.create","uses"=>"GruposCorreosController@createGrupo"]);
            Route::post('grupos_correos/grupos/miembros/add',["as"=>"grupo_correos.miembros.add","uses"=>"GruposCorreosController@createMiembroOrganismo"]);
            Route::post('grupos_correos/grupos/miembros/remove',["as"=>"grupo_correos.miembros.remove","uses"=>"GruposCorreosController@removeMiembroOrganismo"]);
            Route::get('grupos_correos/get/organismos',["as"=>"grupo_correos.getOrganismos","uses"=>"GruposCorreosController@getOrganismos"]);
            Route::get('/grupos_correos/gruposMiembros/{idGrupo}',["as"=>"grupo_correos.gruposMiembros","uses"=>"GruposCorreosController@getMiembrosByGrupo"]);

            Route::get('consultas_positivas',["as"=>"consultas_positivas.index","uses"=>"ConsultaPositivaController@index"]);
            Route::get('consultas_positivas/{idConsulta}',["as"=>"consulta_positiva.show","uses"=>"ConsultaPositivaController@show"]);



        });

        //Permisos para el usuario supervisor y de carga, permisos para create (formulario de carga) y store (guardar el formulario en la bbdd)

        Route::get('/', 'OficiosController@inicio');
        Route::get('inicio',array("as"=>"inicio","uses"=>'OficiosController@inicio'));


        Route::group(array('before'=>'role:1-3-5,inicio|oficioVisto|oficiosModificacion'),function(){
            Route::get('solicitud_paradero/{id}/edit',["as"=>'solicitud_paradero.edit',"uses"=>"PersonaExtraviadaController@edit"]);
            Route::get('solicitud_paradero/{id}/showComplete',["as"=>'solicitud_paradero.showComplete',"uses"=>"PersonaExtraviadaController@showComplete"]);
//            Route::get('solicitud_paradero/{id}/edit',["as"=>'solicitud_paradero.edit',"uses"=>"OficioSolicitudParaderoController@edit"]);
            Route::get('secuestro_elementos/{id}/edit',["as"=>'secuestro_elementos.edit',"uses"=>"SecuestroElementosController@edit"]);
            Route::get('secuestro_armas/{id}/edit',["as"=>'secuestro_armas.edit',"uses"=>"SecuestroArmasController@edit"]);
            Route::get('secuestro_vehicular/{id}/edit',["as"=>'secuestro_vehicular.edit',"uses"=>"PedidosSecuestroVehicularController@edit"]);
            Route::get('prohibicion_salida/{id}/edit',["as"=>'prohibicion_salida.edit',"uses"=>"ProhibicionSalidaPaisController@edit"]);
            Route::get('pedidos_captura/{id}/edit',["as"=>'pedidos_captura.edit',"uses"=>"PedidosCapturaController@edit"]);
            Route::get('medidas_restrictivas/{id}/edit',["as"=>'medidas_restrictivas.edit',"uses"=>"MedidasRestrictivasController@edit"]);
            Route::get('solicitud_paradero2/{id}/edit',["as"=>'solicitud_paradero2.edit',"uses"=>"SolicitudParaderoReqJudController@edit"]);
            Route::get('otras_medidas/{id}/edit',["as"=>'otras_medidas.edit',"uses"=>"OtrasMedidasController@edit"]);
            Route::get('habeas_corpus/{id}/edit',["as"=>'habeas_corpus.edit',"uses"=>"HabeasCorpusController@edit"]);
            Route::get('prohibicion_salida/{id}/edit',["as"=>'prohibicion_salida.edit',"uses"=>"ProhibicionSalidaPaisController@edit"]);
            Route::get('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@getAutoridad"]);

	    Route::get("suborganismos/organismo/{idOrganismo}",array('as' => 'suborganismo.byorganismo','uses'  => 'SubOrganismoController@getSubOrganismoByIdOrg'));


        });
        Route::group(array('before' => 'role:1-2-3-4-5,inicio|alerta_temprana'), function () {
            Route::get("suborganismos/organismo/{idOrganismo}",array('as' => 'suborganismo.byorganismo','uses'  => 'SubOrganismoController@getSubOrganismoByIdOrg'));
            Route::post("oficios/cesar",array('as'=>'oficios.cesar','uses'=>'OficiosController@cesar'));
            Route::post("oficios/vigente",array('as'=>'oficios.vigente','uses'=>'OficiosController@vigente'));

            Route::get("oficios/cease/{id}", array("as" => "oficios.cease", "uses" => "OficiosController@cesar_link"));
            Route::get("oficios/plazo/{id}", array("as" => "oficios.plazo", "uses" => "OficiosController@plazo_link"));

//            Route::resource("solicitud_paradero", "OficioSolicitudParaderoController",array('only' => array('create','store','update')));
            Route::resource("solicitud_paradero", "PersonaExtraviadaController",array('only' => array('create','store','update')));
            Route::resource("secuestro_elementos", "SecuestroElementosController",array('only' => array('create','store','update')));
            Route::resource("secuestro_armas", "SecuestroArmasController",array('only' => array('create','store','update')));
            Route::resource("secuestro_vehicular", "PedidosSecuestroVehicularController",array('only' => array('create','store','update')));
            Route::resource("prohibicion_salida", "ProhibicionSalidaPaisController",array('only' => array('create','store','update')));
            Route::resource("pedidos_captura", "PedidosCapturaController",array('only' => array('create','store','update')));
            Route::resource("medidas_restrictivas", "MedidasRestrictivasController",array('only' => array('create','store','update')));
            Route::resource("solicitud_paradero2", "SolicitudParaderoReqJudController",array('only' => array('create','store','update')));
            Route::resource("otras_medidas", "OtrasMedidasController",array('only' => array('store','create','update')));
            Route::resource("habeas_corpus", "HabeasCorpusController",array('only' => array('create','store','update')));
            Route::post("oficios/localidad",array("as"=>"combo.localidad","uses"=>"CombosController@getLocalidad"));
            Route::post("oficios/partido",array("as"=>"combo.partido","uses"=>"CombosController@getPartido"));
            
            Route::get("oficios/confirmar/{id}", array("as" => "oficios.confirmar", "uses" => "OficiosController@ConfirmarOficio"));

            Route::resource("robo_automotor", "RoboAutomotorController");
            Route::post('robo_automotor/validarRoboAutomotor',['as'=>'robo_automotor.validarRoboAutomotor','uses'=>'RoboAutomotorController@validarRoboAutomotor']);
            Route::post('robo_automotor/update',['as'=>'robo_automotor.update','uses'=>'RoboAutomotorController@update']);

            Route::resource("profugo_evadido", "ProfugoEvadidoController");
            Route::post('profugo_evadido/validarProfugoEvadido',['as'=>'profugo_evadido.validarProfugoEvadido','uses'=>'ProfugoEvadidoController@validarProfugoEvadido']);
            Route::post('profugo_evadido/update',['as'=>'profugo_evadido.update','uses'=>'ProfugoEvadidoController@update']);



            Route::get("seguimiento/{id}",array("as"=>"oficios.log","uses"=>"OficiosController@getLog"));
            Route::post("oficios/buscar_dni",array("as"=>"oficios.buscar.dni","uses"=>"OficiosController@tieneOficio"));
            Route::post("oficios/buscar_patente",array("as"=>"oficios.buscar.patente","uses"=>"PedidosSecuestroVehicularController@tienePatente"));

            Route::group(array('before' => 'role:1-2-3-4-5,inicio','after'=>'forgot'), function () {

                Route::post("oficios/llenar_campos", array("as" => "oficios.llenar_campos", "uses" => "OficiosController@llenarCampos"));
            });


            Route::get("oficios/cargar/otro/{id}",array("as"=>"oficios.cargar_otro","uses"=>"OficiosController@cargarOtro"));
            Route::get('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@getAutoridad"]);
            Route::post('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@setAutoridad"]);
            Route::get('modelos/{idMarca}',['as'=>'vehiculo.modelo','uses'=>"VehiculosController@modelosByMarca"]);
            Route::get('emails/lista','EmailController@listaMails');
            Route::post('mensajes/enviar',['as'=>'mails.send','uses'=>'EmailController@sendMail']);
            Route::get('mensajes/enviar',['as'=>'mails.enviar','uses'=>'EmailController@formMail']);
//            Route::get('mensajes',['as'=>'mensajes.create','uses'=>'EmailController@index']);
            Route::get('files/{idArchivo}/{idOficio}',['as'=>'files.delete','uses'=>'ArchivosController@delete']);
            Route::get("correos/paginate", ["uses"=>"EmailController@getMailsPaginate","as"=>"correos.paginate"]);
            Route::get('emails/lista','EmailController@listaMails');
            Route::post('mensajes/enviar',['as'=>'mensajes.send','uses'=>'EmailController@sendMail']);
            Route::get('mensajes/enviar',['as'=>'mensajes.enviar','uses'=>'EmailController@formMail']);
            Route::get('mensajes',['as'=>'mensajes.index','uses'=>'EmailController@index']);
            Route::get('marcar/mensajes/{id}/leidos',['as'=>'mensajes.marcar.leido','uses'=>'EmailController@marcarLeido']);
            Route::get('mensajes/pdf/{id}',['as'=>'mensajes.pdf','uses'=>'EmailController@pdf']);
            Route::get('mensajes/{id}',['as'=>'mensajes.show','uses'=>'EmailController@show']);
            Route::get('mensajes/vistos/{id}',['as'=>'mensajes.vistos','uses'=>'EmailController@mensajesVistos']);
            Route::post('mensajes/cargar/antiguos',['as'=>'mensajes.cargar','uses'=>'EmailController@getMailsPaginate']);
            Route::get('mensajes/cargar/nuevos',['as'=>'mensajes.nuevos','uses'=>'EmailController@actualizarMails']);
            Route::post('mensajes/responder/{idRespuesta}',['as'=>'mensajes.respuesta','uses'=>'EmailController@saveRespuesta']);
            Route::get('mensajes/ver/enviados',['as'=>'mensajes.enviados','uses'=>'EmailController@getEnviadosPaginate']);
            Route::get('mensajes/cantidad/noleidos',['as'=>'mensajes.cantnoleidos','uses'=>'EmailController@cantNoLeidos']);
            Route::post('mensajes/busqueda',['as'=>'mensajes.busqueda','uses'=>'EmailController@mensajesBusqueda']);

            Route::post('solicitud_paradero/guardarPEPersonaBuscada',['as'=>'solicitud_paradero.guardarPEPersonaBuscada','uses'=>'PersonaExtraviadaController@guardarPEPersonaBuscada']);
            Route::post('solicitud_paradero/guardarPEUltimoDomicilio',['as'=>'solicitud_paradero.guardarPEUltimoDomicilio','uses'=>'PersonaExtraviadaController@guardarPEUltimoDomicilio']);
            Route::post('solicitud_paradero/guardarPEFotos',['as'=>'solicitud_paradero.guardarPEFotos','uses'=>'PersonaExtraviadaController@guardarPEFotos']);
            Route::post('solicitud_paradero/guardarPEDescripcionFisica',['as'=>'solicitud_paradero.guardarPEDescripcionFisica','uses'=>'PersonaExtraviadaController@guardarPEDescripcionFisica']);
            Route::post('solicitud_paradero/guardarPEDatosLaborales',['as'=>'solicitud_paradero.guardarPEDatosLaborales','uses'=>'PersonaExtraviadaController@guardarPEDatosLaborales']);
            Route::post('solicitud_paradero/guardarPEDatosRelacionadoAusencia',['as'=>'solicitud_paradero.guardarPEDatosRelacionadoAusencia','uses'=>'PersonaExtraviadaController@guardarPEDatosRelacionadoAusencia']);
            Route::post('solicitud_paradero/guardarPEDatosDenunciante',['as'=>'solicitud_paradero.guardarPEDatosDenunciante','uses'=>'PersonaExtraviadaController@guardarPEDatosDenunciante']);
            Route::post('solicitud_paradero/guardarPEDatosBusqueda',['as'=>'solicitud_paradero.guardarPEDatosBusqueda','uses'=>'PersonaExtraviadaController@guardarPEDatosBusqueda']);
            Route::post('solicitud_paradero/guardarPEAutoridadJudicial',['as'=>'solicitud_paradero.guardarPEAutoridadJudicial','uses'=>'PersonaExtraviadaController@guardarPEAutoridadJudicial']);
            Route::post('solicitud_paradero/obtenerHistorico',['as'=>'solicitud_paradero.obtenerHistorico','uses'=>'PersonaExtraviadaController@obtenerHistorico']);
            Route::post('solicitud_paradero/filtro_estadisticas',['as'=>'solicitud_paradero.filtro_estadisticas','uses'=>'PersonaExtraviadaController@filtroEstadisticas']);
            Route::get('solicitud_paradero/estadisticas',['as' => 'solicitud_paradero.estadisticas','uses' => 'PersonaExtraviadaController@estadisticas']);
            Route::get('solicitud_paradero/edit/{id}',['as' => 'solicitud_paradero.edit','uses' => 'PersonaExtraviadaController@edit']);


            Route::get("interconsultas/personas/{documento}", array('as' => 'interconsultas.personas', 'uses' => 'InterconsultaController@personas'));
            Route::post("interconsultas/buscar", array('as' => 'interconsultas.buscar', 'uses' => 'InterconsultaController@buscar'));
            Route::post("interconsultas/renaper", array('as' => 'interconsultas.renaper', 'uses' => 'InterconsultaController@renaper'));
            Route::post("interconsultas/migraciones", array('as' => 'interconsultas.migraciones', 'uses' => 'InterconsultaController@migraciones'));
            Route::post("interconsultas/drnpa", array('as' => 'interconsultas.drnpa', 'uses' => 'InterconsultaController@drnpa'));
            Route::post("interconsultas/anses", array('as' => 'interconsultas.anses', 'uses' => 'InterconsultaController@anses'));
            Route::get("interconsultas", array('as' => 'interconsultas.index', 'uses' => 'InterconsultaController@index'));

        });

        Route::group(array('before' => 'role:1-2-3-5,inicio'), function () {
            Route::post('oficios/cantMod',array('as'=>'oficios.cantidad_mod','uses'=>'OficiosController@cantMod'));
            Route::post('oficios/cantMod/detalle',array('as'=>'oficios.cantidad_mod_detalle','uses'=>'OficiosController@cantModDetalle'));
            Route::get('no/leidos',array('as'=>'oficios.no_leidos','uses'=>'OficiosController@getCantidadOficiosNoVistos'));
            Route::get('no/leidos/detalle',array('as'=>'oficios.no_leidos_detalle','uses'=>'OficiosController@getCantidadOficiosNoVistosDetalle'));
        });


        //Permisos para el usuario supervisor , de carga y de consulta,permisos para show(ver el oficio en la web) y pdf para descargar el pdf del mismo
        Route::group(array('before' => 'role:1-2-3-5,inicio','after'=>'oficioVisto'), function () {
            Route::get("solicitud_paradero/{rute}/{name}/mostrar_archivo",['as' => 'solicitud_paradero.mostrar_archivo','uses' => 'PersonaExtraviadaController@show']);
            Route::get('solicitud_paradero/show/{id}',["as"=>'solicitud_paradero.show',"uses"=>"PersonaExtraviadaController@showReporte"]);
            Route::get('secuestro_elementos/{id}',["as"=>'secuestro_elementos.show',"uses"=>"SecuestroElementosController@show"]);
            Route::get('secuestro_armas/{id}',["as"=>'secuestro_armas.show',"uses"=>"SecuestroArmasController@show"]);
            Route::get('secuestro_vehicular/{id}',["as"=>'secuestro_vehicular.show',"uses"=>"PedidosSecuestroVehicularController@show"]);
            Route::get('prohibicion_salida/{id}',["as"=>'prohibicion_salida.show',"uses"=>"ProhibicionSalidaPaisController@show"]);
            Route::get('pedidos_captura/{id}',["as"=>'pedidos_captura.show',"uses"=>"PedidosCapturaController@show"]);
            Route::get('medidas_restrictivas/{id}',["as"=>'medidas_restrictivas.show',"uses"=>"MedidasRestrictivasController@show"]);
            Route::get('solicitud_paradero2/{id}',["as"=>'solicitud_paradero2.show',"uses"=>"SolicitudParaderoReqJudController@show"]);
            Route::get('otras_medidas/{id}',["as"=>'otras_medidas.show',"uses"=>"OtrasMedidasController@show"]);
            Route::get('habeas_corpus/{id}',["as"=>'habeas_corpus.show',"uses"=>"HabeasCorpusController@show"]);
            //Route::get('prohibicion_salida/{id}',["as"=>'prohibicion_salida.show',"uses"=>"ProhibicionSalidaPaisController@show"]);
            Route::get('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@getAutoridad"]);
            Route::post('oficios/autoridad/{idOficio}',["as"=>"oficios.autoridad","uses"=>"OficiosController@setAutoridad"]);



            Route::post('oficios',array('as'=>'oficios.buscar','uses'=>'OficiosController@filtrar'));

            Route::get('marcar_leidos',['as'=>'oficios.marcar_leidos','uses'=>'OficiosController@oficiosNoLeidos']);




            Route::resource("consultas", "OficiosConsultaController");

            Route::post('oficios/observaciones/{id}',['as'=>'oficios.observaciones','uses'=>'ObservacionesOficiosController@store']);


            Route::resource("miembros", "MiembrosController",array('except' => array('update','store')));



        });


        Route::get('medidas_restrictivas/pdf/{id}',array('as'=>'medidas_restrictivas.pdf','uses'=>'MedidasRestrictivasController@getPdf'));

        Route::get('otras_medidas/pdf/{id}',array('as'=>'otras_medidas.pdf','uses'=>'OtrasMedidasController@getPdf'));

        Route::get('pedidos_captura/pdf/{id}',array('as'=>'pedidos_captura.pdf','uses'=>'PedidosCapturaController@getPdf'));

        Route::get('reporte_nota/pdf/{id}',array('as'=>'reporte_nota.pdf','uses'=>'PedidosCapturaController@getPdfReporteNota'));

        Route::get('habeas_corpus/pdf/{id}',array('as'=>'habeas_corpus.pdf','uses'=>'HabeasCorpusController@getPdf'));

//        Route::get('solicitud_paradero/pdf/{id}',array('as'=>'solicitud_paradero.pdf','uses'=>'OficioSolicitudParaderoController@getPdf'));
        Route::get('solicitud_paradero/pdf/{id}',array('as'=>'solicitud_paradero.pdf','uses'=>'PersonaExtraviadaController@getPdf'));

        Route::get('solicitud_paradero2/pdf/{id}',array('as'=>'solicitud_paradero2.pdf','uses'=>'SolicitudParaderoReqJudController@getPdf'));

        Route::get('prohibicion_salida/pdf/{id}',array('as'=>'prohibicion_salida.pdf','uses'=>'ProhibicionSalidaPaisController@getPdf'));

        Route::get('secuestro_elementos/pdf/{id}',array('as'=>'secuestro_elementos.pdf','uses'=>'SecuestroElementosController@getPdf'));

        Route::get('secuestro_armas/pdf/{id}',array('as'=>'secuestro_armas.pdf','uses'=>'SecuestroArmasController@getPdf'));

        Route::get('secuestro_vehicular/pdf/{id}',array('as'=>'secuestro_vehicular.pdf','uses'=>'PedidosSecuestroVehicularController@getPdf'));

        Route::get('profugo_evadido/pdf/{id}',array('as'=>'profugo_evadido.pdf','uses'=>'ProfugoEvadidoController@getPdf'));

        Route::get('robo_automotor/pdf/{id}',array('as'=>'robo_automotor.pdf','uses'=>'RoboAutomotorController@getPdf'));

        Route::resource("consultas", "OficiosConsultaController");



        Route::post('plazos/store', array('as'=>'plazos.store','uses'=>'PlazosController@store'));
        


    });

});


        Route::group(array('before'=>'role:5,inicio|oficioVisto|oficiosModificacion'),function(){
            Route::resource("usuarios", "UsuariosController");
            });

Event::listen('404', function () {
    return Response::error('404');
});

Route::get('profile', array('as'=>'profile','uses'=>'AuthController@profile'));
Route::get('logout', array('as'=>'logout','uses'=>'AuthController@logOut'));





//Route::get('usuarios/dni', array('uses' => 'UsuariosController@validarDni'));
