<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Endless Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include("include_head.php"); ?>
  </head>

  <body>
	<!-- Overlay Div -->
	<div id="overlay" class="transparent"></div>
	<!-- wrapper -->
	<div id="wrapper" class="preload">
		<!-- top-nav-->
		<?php include("include_top-nav.php"); ?>
		<!-- /top-nav-->
		<!-- menu-lateral -->
		<?php include("include_main-menu.php"); ?>
		<!-- /menu-lateral -->
		<!-- main-container -->

		<div id="main-container">
			<!-- breadcrumb -->
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="index.html"> Inicio</a></li>
					 <li class="active">Blank page</li>	 
				</ul>
			</div>
			<!-- breadcrumb -->
			<div class="contenedor">
            <section class="encabezado">
			<h1>Titulo</h1>
			<hr />
            </section>
			<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
			</div>
		</div>
		<!-- /main-container -->

	<?php include("include_footer.php"); ?>	
	</div>
	<!-- /wrapper -->
	<?php include("include_logout.php"); ?>
	<?php include("include_script.php"); ?>
      
  </body>
</html>