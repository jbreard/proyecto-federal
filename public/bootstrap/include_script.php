<!-- javascript
================================================== -->
<!-- Jquery -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Bootstrap -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Modernizr -->
<script src='js/modernizr.min.js'></script>
<!-- Pace -->
<script src='js/pace.min.js'></script>
<!-- Popup Overlay -->
<script src='js/jquery.popupoverlay.min.js'></script>
<!-- Slimscroll -->
<script src='js/jquery.slimscroll.min.js'></script>
<!-- Cookie -->
<script src='js/jquery.cookie.min.js'></script>
<!-- Endless -->
<script src="js/endless/endless.js"></script>
<!-- javascript -->