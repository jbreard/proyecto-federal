<!-- Head
================================================== -->
<link rel="shortcut icon" href="favicon.png">
<!-- Bootstrap core CSS -->
<link type="text/css" href="bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Font Awesome -->
<link type="text/css" href="css/font-awesome.min.css" rel="stylesheet">
<!-- Pace -->
<link type="text/css" href="css/pace.css" rel="stylesheet">
<!-- Endless -->
<link type="text/css" href="css/endless.min.css" rel="stylesheet">
<link type="text/css" href="css/endless-skin.css" rel="stylesheet">
<!-- minseg -->
<link type="text/css" href="css/style-minseg.css" rel="stylesheet"/>
<!-- ./Head -->
