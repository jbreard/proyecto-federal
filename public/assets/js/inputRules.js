(function(){

    function alphaNumeric(node){
        return node.val(node.val().replace(/[^a-zñÑA-Z 0-9 ]/g,'') );
    }

    function alphaNumericBarra(node){
        return node.val(node.val().replace(/[^a-zñÑA-Z 0-9 -]/g,'') );
    }

    function alpha(node){
        if(node.hasClass('no-validate')) return
        return node.val(node.val().replace(/[^a-zñÑA-Z ]/g,'') );
    }

    $('.alphaonly').bind('keyup blur',function(){
           return alpha($(this));
    });

    $('.alphanumericonly').bind('keyup blur',function(){
            return alphaNumeric($(this));
    });
    $('.buscar_dni').bind('keyup blur',function(){
            var tipo_documento = $("#id_tipo_documento").val();
            if(tipo_documento == 7) return alphaNumericBarra($(this));
            return alphaNumeric($(this));
    });


    $('.buscarPatente').bind('keyup blur',function(){
        return alphaNumericBarra($(this));
    });

    $('input[name*="nombre"]').bind('keyup blur',function(e){

       return alpha($(this));


    });
    $('input[name*="apellido"]').bind('keyup blur',function(){
        return alpha($(this))
    });
    $('input[name*="apodos"]').bind('keyup blur',function(){
        return alpha($(this));
    });




})();
