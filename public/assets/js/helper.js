var Helper = function (){
    var basePath =  window.location.protocol + "//" + window.location.host +"/";
    this.basePath = basePath;

    this.getUrl    =  function(url){

        return basePath+url;

    }
    this.resetForm =  function (formulario) {
        $(formulario).each(function(){
            this.reset();
        });

    }

    var ajax = function(token,method,url,data,callback){

        $.ajax({
            type:method,
            url:url,
            data:data,
            success:function(response){
                callback(response);
            }
        });

    }
    this.ajax  = function (token,method,url,data,callback){
        ajax(token,method,url,data,callback);
    }

    this.sendForm = function(idFrm,callback,extraData){
        $(idFrm).on('submit',function(e){
            e.preventDefault();
            var form   = $(this);
            var method = form.find('input[name="_method"]').val() || "POST";
            var url    = form.prop('action');
            var token  = form.find('input[name="_token"]').val();
            var values = form.serializeArray();
            extraData  = extraData || false;
            if(extraData){
                values.push(extraData);
            }
            var data = $.param(values);
            //var data   = form.serialize();
            ajax(token,method,url,data,callback);


        });
    }

    this.ErrorMsg = function(rta,divShow,delay){
        var stemplate = $("#erroresTpl").html();
        var tmpl = Handlebars.compile(stemplate);
        var html = tmpl(rta);
        $(divShow).html(html).fadeIn(3000).delay(delay||1500).slideUp(3000);
        return false;
    }
    this.SuccessMsg = function(rta,divShow){
        var stemplate = $("#successTpl").html();
        var tmpl = Handlebars.compile(stemplate);
        var html = tmpl(rta);
        $(divShow).html(html).fadeIn(3000).delay(1500).slideUp(3000);
        return false;
    }

    this.ActualizarCombo = function (select,ruta,metodo){
        var url    = basePath+ruta;
        var method = metodo || "POST";
        var combo  = $(select);
        var option = $(select+" option");
        option.remove();
        ajax(null,method,url,null,function(data){
            var html  = '';
           
            $.each(data,function(index,value){
                html+='<option value="'+value.id+'">'+value.descripcion+'</option>'
            });
            combo.append(html);
        });

    }

    this.ActualizarComboFirstOptionBlank = function (select,ruta,metodo,optionSelected){
        var url    = basePath+ruta;
        var optionSelected = parseInt(optionSelected) || '';
        var method = metodo || "POST";
        var combo  = $(select);
        var option = $(select+" option");
        option.remove();
        ajax(null,method,url,null,function(data){
            var html  = '<option value=""></option>';

            $.each(data,function(index,value){
                html+='<option value="'+value.id+'">'+value.descripcion+'</option>';
            });
            combo.append(html).val(optionSelected);
        });

    }







    var onOpenModal = function(divModal){
        divModal.modal();
    }
    /*
     * Abre una ventana modal (bootstrap)
     * @param idModal    -- String -- selector del div para la ventana modal
     * @param idLauncher -- String -- id del trigger, para abirr el modal
     * @param callback   -- Function -- funcion a ejecutar con la respusta del modal
     * */
    this.modalOpen = function (idLauncher,idModal,callback){
        var divModal =  $(idModal);
        $("body").on("click",idLauncher,function (e) {
            e.preventDefault;
            el   = $(this);
            onOpenModal(divModal);
            callback(el);
        });
    }


    /*
     * Escucha el evento onclose de una ventana modal (bootstrap)
     * @param idModal    -- String -- selector del div de la ventana modal
     * @param callback   -- Function -- funcion a ejecutar con la respusta del modal
     * */
    this.modalClose = function(idModal,callback){
        $(idModal).on('hidden.bs.modal', function () {
            // Destroy Select2 and reset inputs
            callback();
        });
    }


}
