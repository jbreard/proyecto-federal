(function(){
    var util = new Helper();
    function append(sOptions,target){
        $(target).children("option").remove();
        $.each(sOptions, function(key, val) {

            $(target).append('<option value="'+key+'">' + val + '</option>');

        });
    }

    function setPartido(el) {
        var target = $(el).attr("data-target");
        var valor_partido  = $(target).attr("data-val");
        var id_provincia = $(el).val();
        var url = util.getUrl('oficios/partido');
        var data  = {'id_provincia':id_provincia};

        util.ajax(null,'POST',url,data,
            function (sOptions) {
                append(sOptions,target);
                $(target).val(valor_partido);
                var subTarget   = $(target).attr('data-target');
                $(subTarget).children("option").remove();
            }
        );


    }
    function setLocalidad(el,idSelected) {
        var target = $(el).attr("data-target");
        var id_partido = $(el).attr('data-value');
        var url = util.getUrl('oficios/partido');
        var data  =  {"id_partido":id_partido};
        util.ajax(null,'POST',url,data,
            function (sOptions) {
                append(sOptions,target);
                $(target).val($(target).attr('data-value'));
            }
        );

    }

    $(".provincia")
        .change(function(){setPartido(this)})
        .each(function(){
            setPartido(this);
    });

})();
