var util  = new Helper();
(function(){

    $('.fecha').datepicker({
        "language":'es'
    }).mask("99/99/9999");

    $('.time').timepicker({
        minuteStep: 5,
        showInputs: false,
        disableFocus: true,
        defaultTime: false,
        use24hours: true,
        format: 'HH:mm',
        showMeridian: false
    });

    $(".eliminar_archivo").click(function(event){
        event.preventDefault();
        var el = $(this);
        var url = el.attr("href");
        var idArchivo = el.attr("data-id-archivo");
        util.ajax(null,'get',url,{},function(respuesta){
            if(respuesta.success){
                $("#tr_archivo_"+idArchivo).remove();
            }
            else
            {
                var myModal = $("#myModal");
                var modalMensaje = $("#mensaje");
                modalMensaje.html("No puede eliminar todos los archivos, debe quedar al menos uno");
                myModal.modal('show');
            }
        });
    });



})();



/*
 * Busca y llena los datos del oficio en caso que quiera cargar otro igual
 *
 * @param {int} idOficio - el Id del Oficio
 *
 * @param {string} ruta  - ruta donde va a buscar los datos del oficio
 *
 *
 * */
function llenarCampos(idOficio,ruta){
    var data = {id:idOficio};
    util.ajax(null,'POST',ruta,data,function(data){
        data = jQuery.parseJSON(data);
        $.each(data,function(index,key){
            var node = $("#"+index);
            node.attr("readonly","readonly");
            if(node.hasClass('select')){
                node.select2('val',key);
            }else{
                node.val(key);
            }


        });

    })

}
