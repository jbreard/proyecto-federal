(function(){
    var util = new Helper();
    $("#marca_vehiculo").change(function(event){
        event.preventDefault();
        var valor = $(this).val();
        var ruta  = "modelos/"+valor;
        util.ActualizarCombo("#modelo_vehiculo",ruta,"GET");
    });
})();
