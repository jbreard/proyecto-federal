/**
 * Created by juan on 26/08/14.
 */
$("#guardar_form").click(function(event){
    event.preventDefault();
    var alerta_temprana = $("[name='alerta_temprana']").val() || false;
    if(alerta_temprana != 1){
        var escan = $("#archivo1").val();
        
        var escan2 = $("#archivo2").val();
        
        var fecha = $("#fecha_oficio");
        
        var nombre = $("#nombre");
        
        var nro_documento = $("#nro_documento");
        
        var validateNombre = nombre.data('novalidate') || false;
        
        var apellido = $("#apellido");
        
        var validateApellido = apellido.data('novalidate') || false;
        
        var nro_chasis = $("#nro_chasis").val();
        
        var nro_motor = $("#nro_motor").val();
        
        var dominio = $("#dominio").val();
        

        var medida_restrictiva = $("#medida_restrictiva").val();
        
        var tipo_elemento = $("#tipo_elemento");
        
        var tabla = $("#listado_archivos").attr("data-bool");
        
        var extensions = ["pdf"];
        
        var tipoOficio = parseInt($(this).attr('data-tipo_oficio')) || false;
        
        var modalMensaje = $("#mensaje");
        
        var myModal = $("#myModal");

        if(fecha.val() == '')
        {
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo Fecha de Requerimiento");
            //close(fecha);
        }
        else if(nro_documento.val() == ''){
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo Documento");
            //close(nombre);
        }
        else if(nombre.val() == '' && !validateNombre){
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo Nombre");
            //close(nombre);
        }
        else if(tipo_elemento.val() == ''){
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo Tipo Elemento");
            //close(tipo_elemento);
        }
        else if(apellido.val() == '' && !validateApellido){
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo Apellido");
            //close(apellido);
        }
        else if(escan == '' && tabla!='existe'){
            myModal.modal('show');
            modalMensaje.html("Por Favor, adjunte el oficio  correspondiente");
        }
        else if(medida_restrictiva == ''){
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete el campo de Tipo de Medida");
        }
        else if(tipoOficio === 2 && validarCamposPedidoSecuestroVehicular() === false)
        {/* Oficios del tipo Pedida Secuestro Vehicular */
            myModal.modal('show');
            modalMensaje.html("Por Favor, complete alguno de los siguientes campos: Dominio, Nro de Chasis ó Nro de Motor");
        }
        else if (escan !=''){

            var ext = escan.split('.').pop().toLowerCase();

            
            var extensiones = $.inArray(ext,extensions);
            if(extensiones == -1){

                var mensaje = "";

                myModal.modal('show');

                $.each(extensions,function(index,value){
                    mensaje+=value+",";
                });

                modalMensaje.html("Por Favor, ingrese un escaneo con una extension: <br>"+mensaje);
            }else if(escan2 !=''){

            var ext = escan2.split('.').pop().toLowerCase();

            
            var extensiones = $.inArray(ext,extensions);
            if(extensiones == -1){

                var mensaje = "";

                myModal.modal('show');

                $.each(extensions,function(index,value){
                    mensaje+=value+",";
                });

                modalMensaje.html("Por Favor, ingrese un escaneo con una extension: <br>"+mensaje);
            }else{
                enviar();
            }
        }
            else{
                enviar();
            }
        }
        else{
            enviar();
        }
    }else{
          enviar();  
    }
});
function trimClear(tag){}
        var campos = $(tag);

        campos.each(function(index, campo){
            if(this.data("trim") == "true"){
                var valor = $(campo).val().trim();
                $(campo).val(valor);
            }
        });
    
function enviar(){
    $("#guardar_form").attr("disabled","disabled");
    $("#guardar_form").html("Guardando...");
     trimClear("#nombre");

    $('#form').submit();
    console.log($("#form").attr("action"));
}



$("#ocultar").click(function(event){
    event.preventDefault();
    var div = document.getElementById("newpost");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }else{
        div.style.display = "block";
        $("#ocultar").hide();
    }



});



var CantDocumento = 0;

$("#agregar_documento").click(function(event){
    var url = $('#sarasa').val();
    event.preventDefault();
    CantDocumento++;
    $.ajax({
        type: "post",
        url: url,
        data: 'i=' + CantDocumento,
        success: function (html) {
            $(".documentos").append(html);
        }
    });

});





