(function(){
    'use strict'
    var files = new Array();//Variable global para almacenar la data de archivos

    function prepareUpload(event) {
        files = event.target.files;
    }


    $(".abrir_modal").click(function(){
        var valor = $(this).attr('data-id');
        $("#id_oficio").val(valor);
        $("#id_oficio2").val(valor);
        document.getElementById('nro_oficio').innerHTML= valor;

        $("#modal-error").hide();
    });

    $('input[type=file]').change(function(event){
        files = event.target.files;
    });
    $("#enviar_vigente").click(function (event) {
        var ruta     = $("#frm_vigente").attr('action');

            var data     = new FormData();
            var sIdFrm   = "#frm_vigente";
            var datosFrm = $(sIdFrm).serializeArray();
            var id       = $("#id_oficio2").val();

            $.each(datosFrm, function (key, object) {
                data.append(object.name, object.value);
            });

            $.ajax({
                url: ruta,
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                success: function (oJson) {
                //    alert(data);
                    window.location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
    });
    $("#enviar_cese").click(function (event) {
        var file     = $("#escaneo_cese").val();
        var fecha    = $("#fecha_cese").val();
        var ruta     = $("#frm_cesar").attr('action');
        if(file !='' && fecha !=''){

            var data     = new FormData();
            var sIdFrm   = "#frm_cesar";
            var datosFrm = $(sIdFrm).serializeArray();
            var id       = $("#id_oficio").val();


            $.each(files, function (key, value) {
                console.log(key);
                data.append("escaneo_cese", value);
            });
            $.each(datosFrm, function (key, object) {
                data.append(object.name, object.value);
            });

            $.ajax({
                url: ruta,
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                success: function (oJson) {
                    window.location.reload();
                }

            });
        }else{

            $("#modal-error").show();
            if(file == '' && fecha == ''){
                $("#mensaje_error").html("Por favor, Cargue el Oficio Correspondiente <br> Por favor complete el campo de fecha");
            }

            if(file == '' ){

                $("#mensaje_error").html("Por favor, Cargue el Oficio Correspondiente");

            }

            if (fecha == ''){

                $("#mensaje_error").html("Por favor complete el campo de fecha");

            }
        }
    });


})();



