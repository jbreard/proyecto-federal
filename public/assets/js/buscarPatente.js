(function(){
    var util         = new Helper();
    $('#buscarPatente').focusout(function(){
        var patente         = $(this).val();

        if (dominio !=''){
            var url  = util.getUrl('oficios/buscarPatente');
            var data = {'dominio':patente};
            util.ajax(null,'POST',url,data,
                function(data){
                    if(data.respuesta == "true"){
                        var mensaje = 'Este dominio ya tiene un pedido de secuestro cargado, ingrese <a target="_blank" style="color:red;text-decoration:underline" href="'+data.url+'">aqui</a>'+" para verlo";
                        //$("#guardar_form").attr("disabled","disabled");
                        $("#mensaje").html(mensaje);
                        $("#myModal").modal("show");


                    }else if(data.respuesta=="false"){
                        $("#guardar_form").removeAttr("disabled");
                    }
                }
            );
        }
    });
})();
