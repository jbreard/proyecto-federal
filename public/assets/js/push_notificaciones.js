
    var util = new Helper();
   // var socket = io('https://173.10.0.99:3000');

    function notificacionWeb(mensaje){
        $.gritter.add({
            title: '<i class="fa fa-info-circle"></i> Nuevo Requerimiento Judicial',
            text: mensaje,
            sticky: false,
            time: '',
            class_name: 'gritter-info'
        });

        return false;
    }

    function cantMod(){
        var url  = util.getUrl('oficios/cantMod');
        util.ajax(null,'POST',url,null,function(data){
            var data = jQuery.parseJSON(data);
            $(".req_mod").html(data);
        });
    }


    function cantModDetalle(){
        var url  = util.getUrl('oficios/cantMod/detalle');
        util.ajax(null,'GET',url,null,function(cantidad){
            console.log(cantidad);
            var cantidad = jQuery.parseJSON(cantidad);
            $.each(cantidad, function( index, value ) {
                $("#notificacion_modificados_"+value.tipo_oficio).html(value.cantidad);
            });
        });

    }

    function getCantidadOficios(){
        var url  = util.getUrl('no/leidos');
        util.ajax(null,'GET',url,null,function(cantidad){
            var cantidad = jQuery.parseJSON(cantidad);
            $(".cantidad_no_leidos").html(cantidad[1].cantidad_oficios);
        });
    }

    function getDetalleNoLeidos(){
        var url  = util.getUrl('no/leidos/detalle');
        util.ajax(null,'GET',url,null,function(cantidad){
            var cantidad = jQuery.parseJSON(cantidad);
            $.each(cantidad[1], function( index, value ) {
                    $("#notificacion_"+value.tipo_oficio).html(value.cantidad_oficios);
            });
        });
    }
    $("#notificaciones_no_vistas").click(function(){
        var submenu = $("#no_vistos_detalle");
        if(!submenu.is(':visible')){
            getDetalleNoLeidos();
        }
    });
    $("#notificaciones_modificaciones_no_vistas").click(function(){
        var submenu = $("#modificados_detalle");
        if(!submenu.is(':visible')){
            cantModDetalle();
        }
    });
    socket.on('oficio_nuevo', function (data) {
        var jdata = {mensaje:"Un Oficio Nuevo fue cargado",ruta:data.id_oficio};
        notificaciones(jdata);
    });
    socket.on('oficio_cesado', function (data) {
        var jdata = {mensaje:"Oficio Sin Efecto",ruta:data.id_oficio};
        notificaciones(jdata);
    });

    getCantidadOficios();
    cantMod();
    getDetalleNoLeidos();
    getMailsNoLeido();
