(function(){
    'use strict';
    var urlFechaVencimiento;
    var fechaVencimiento;
    var utils = new Helper();
    var selectorOficio;

    function countOficios(){
        
        
        var row = $("#bodyOficios tr").length;
        var tablaOficios = $("#tablaOficios");
        var alertOficios = $("#registrosMensaje")
        
	if(row == 0){
           tablaOficios.hide();
          return alertOficios.show();  
        } 
        alertOficios.hide();
        tablaOficios.show();

        return

    }


    countOficios();


    function eliminar(id){
      swal({
        title: "Esta seguro de eliminar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar!",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
      },
        function(){
            $("#form_"+id).submit();
        });
    }

    $('.eliminar_oficio').click(function(e){
        e.preventDefault();
        var id = $(this).data("id");
        eliminar(id);
    });











    function cambiarBusqueda() {
        var valor = $("#buscar_por").val();

        if (valor == "elementos") {
            $(".persona").hide();
            $(".persona :input").attr("disabled", "disabled");
            $(".elementos").show();
            $(".elementos :input").removeAttr("disabled");
        } else if (valor == 0) {
            $(".persona").hide();
            $(".elementos").hide();
            $(".persona :input").attr("disabled", "disabled");
            $(".elementos :input").attr("disabled", "disabled");
        }else if (valor == "personas"){
            $(".elementos").hide();
            $(".sub-elemento").hide();
            $(".elementos :input").attr("disabled", "disabled");
            $(".persona").show();
            $(".persona :input").removeAttr("disabled");
        }
    }

    function cambiarTipoElem(){

        var valor  =  $("#tipo_elemento").val();
        
        if(valor.length != 0){
            $("."+valor).show();
        }else{
            $(".tipo_elemento").hide();
            $(".tipo_elemento :input").val("");
        }
    }




    $("#buscar_por").change(function () {
        cambiarBusqueda();
    });

    $("#tipo_elemento").change(function(){

        cambiarTipoElem();
    });

    $(".vencimientoOficio").click(function(e){
        e.preventDefault();
        fechaVencimiento = $(this).attr("data-fecha");
        urlFechaVencimiento = $(this).attr("href");
        selectorOficio      = $(this);
        $("#fecha_vencimiento").val(fechaVencimiento);
    });


    $("#btnConfirmarVto").click(function(e){
        e.preventDefault();
        var fecha =  $("#fecha_vencimiento").val();
        utils.ajax(null,'GET',urlFechaVencimiento,{plazo_fecha:fecha},function(data){
            $('#myModalVencimiento').modal('hide')
            alert("Fecha de Vencimiento Confirmada");
            selectorOficio.remove();
        });
    });
    function plazo() {
        var valor = $("#tipo_medida").val();
        var div = $(".plazo_autorizado");
        if (valor == 3) {
            div.show();
        } else {
            div.hide();
            div.children("input").val("");
        }
    }

    $("#tipo_medida").change(function(e){
        plazo($(this));
    });



    $("#buscar_por").trigger("change");
    $("#tipo_elemento").trigger("change");
    $("#tipo_medida").trigger("change");

    //plazo();
    //cambiarBusqueda();
    //cambiarTipoElem();


})();
