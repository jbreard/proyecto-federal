function validarCamposPedidoSecuestroVehicular()
{
    var dominio = $("#dominio"),
        nro_chasis = $("#nro_chasis"),
        nro_motor = $("#nro_motor"),
        algunCampoLleno = !empty(dominio.val()) || !empty(nro_motor.val()) || !empty(nro_chasis.val());

    if(algunCampoLleno) return true;

    return false;
}
