/**
 * Created by david on 13/05/15.
 */

function parseDMY(value) {
    var date = value.split("/");
    var d = parseInt(date[0], 10),
        m = parseInt(date[1], 10),
        y = parseInt(date[2], 10);
    return new Date(y, m - 1, d);
}

$(function(){


    $(".openModalPlazo").click(function(e){
        var id = $(this).data('idoficio');
        $("#id_oficio_plazo").val(id);

    });

    $("#modalPlazo").on('click', '.btn-guardar-plazo', function(){

        var fechaInicio = $("#fecha_inicio");
        var fechaFinal = $("#fecha_final");
        var destino = $("#destino");
        var archivo = $('input[name="escaneo_plazo[]"]');
        var modalPlazoAlerta = $("#modalPlazoAlerta");
        var modalPlazoMensaje = $("#div-plazo-mensaje");

        if(empty(fechaInicio.val())) {
            modalPlazoAlerta.modal('show');
            modalPlazoMensaje.html("Debe completar la fecha inicio");
            return false;
        }

        if(empty(fechaFinal.val())) {
            modalPlazoAlerta.modal('show');
            modalPlazoMensaje.html("Debe completar la fecha final");
            return false;
        }else{
            var startDate = parseDMY($('#fecha_inicio').val());
            var endDate = parseDMY($('#fecha_final').val());

            if (startDate.getTime()  > endDate.getTime()){
                modalPlazoAlerta.modal('show');
                modalPlazoMensaje.html("La fecha final debe ser mayor a la fecha inicio");
                return false;
            }
        }

        if(empty(archivo.val())) {
            modalPlazoAlerta.modal('show');
            modalPlazoMensaje.html("Debe seleccionar 1 archivo");
            return false;
        }

        //if(empty(destino.val())) {
        //    modalPlazoAlerta.modal('show');
        //    modalPlazoMensaje.html("Debe completar el destino");
        //    return false;
        //}

        var form = $("#frm-plazo");
        form.submit();
    });
});
