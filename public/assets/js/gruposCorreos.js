var combosOrganismos = Vue.extend({
	template:'#combos',
	props:['idOrganismo'],
	data:function(){
		return {
			select:0,
		}
	},
	methods:{
		managerOrganismos:function(){
			var idGrupo = this.$parent.idGrupo;
			if(this.select == 0) return this.removeOrganismo(this.idOrganismo,idGrupo);
			return this.addOrganismo(this.idOrganismo,idGrupo);
		},
		removeOrganismo:function(idOrganismo,idGrupo){
			var el = this;
			$.ajax({
				url:'/grupos_correos/grupos/miembros/remove',
				data:{idGrupo:idGrupo,idOrganismo:idOrganismo},
				type:'post',
				success:function(data){
					$.notify({
						icon: 'fa fa-check-circle',
						message: "Organismo eliminado del grupo",
						z_index: 2000,
					});
				}
			});
		},
		addOrganismo:function(idOrganismo,idGrupo){
			var el = this;
			$.ajax({
				url:'/grupos_correos/grupos/miembros/add',
				data:{idGrupo:idGrupo,idOrganismo:idOrganismo},
				type:'post',
				success:function(data){
					$.notify({
						icon: 'fa fa-check-circle',
						message: "Organismo agregado al grupo"
					},{
						type:"info",
						z_index: 2000,

					});
				}
			});
		}

	}
});
var demo = new Vue({
	el:"#grupoCorreos",
	data:{
		title:null,
		idGrupo:null,
		organismos:[],
		grupos:[],
		filter:null,
		formCreate:{
			descripcion:null
		},

	},
	components:{
		"combos-organismos":combosOrganismos
	},
	ready:function(){
		var el = this;
		$(".organismo_si_no").on("change",function(){
			var valor = $(this).val();
			var idOrganismo = $(this).attr("id-organismo");
			if(valor == 0) return el.removeOrganismo(idOrganismo);
			return el.addOrganismo(idOrganismo);
		});
		this.getGrupos();
		this.getOrganismos();
		$('#descripcion_grupo').editable();

	},
	methods:{
		getOrganismos:function(){
			var el = this;

			$.ajax({
				url:'/grupos_correos/get/organismos',
				type:'get',
				success:function(data){	
					el.organismos = data;
					$.each(data,function(index,value){
						console.log(value.descripcion);
					})
				}
			});
		},
		
		
		getGrupos:function(){
			var el = this;
			$.ajax({
				url:'/grupos_correos/getGrupos',
				success:function(data){
					el.grupos = data;
				}
			});
		},

		crearGrupo:function(){
			$("#createGrupo").modal();
		},
		agregarOrganismo:function(id,title){
			
			this.title = title;
			this.idGrupo = id;
			$(".organismo_si_no").each(function(index,value){
				$(this).val(0);
			});
			$.ajax({
				url:'/grupos_correos/gruposMiembros/'+this.idGrupo,
				success:function(data){
					$(data).each(function(index,value){
						$("#organismo_combo_"+value.id_organismo).val(1);
					});
				}
			});

			$("#addOrganismo").modal();
		},
		listadoOrganismo:function(){},
		eliminar:function(){},
		guardarGrupo:function(){
			var el = this;
			$.ajax({
				url:'/grupos_correos/grupos/create',
				type:'post',
				data:el.formCreate,
				success:function(data){
					el.grupos.push(data);
					$.notify({
						icon: 'fa fa-check-circle',
						message: "Grupo creado correctamente"
					});
					$("#createGrupo").modal('hide');
				}
			});
			
		},
		guardaraddOrganismo:function(){
			
		}
	}
});