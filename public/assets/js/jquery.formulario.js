$.fn.form = function(options) {
    var form = this;
    var files = [];
    var settings = $.extend({},options)
    var data;
    var dataForm;
    var method = form.find('input[name="_method"]').val() ||  form.prop('method');;
    var url    = form.prop('action');
    var i      = 0;


    var appendDataFile = function () {
        if(files) {
            for (var i = 0; i < files.length; i++) {
                console.log(files);
                var file = files[i];

                data.append('adjuntos[]', file, file.name);
            }
        }
    }
    var appendDataForm = function () {
        console.log(dataForm);
        $.each(dataForm, function (key, value) {
            data.append(value.name, value.value);
        });
    }


    $('.files').on('change', prepareUpload);
    form.on('submit',function(e){
        e.preventDefault();
        e.stopPropagation();
        data = new FormData();
        dataForm   = form.serializeArray();
        appendDataFile();
        appendDataForm();
        $.ajax({
            url:url,
            method:method,
            data:data,
            dataType: options.dataType ||'json',
            processData: options.processData || false, // Don't process the files
            contentType: options.contentType || false, // Set content type to false as jQuery will tell the server its a query string request,
            beforeSend:options.beforeSend
        }).fail(options.fail).done(options.done);

    });

    function prepareUpload(event)
    {
        files = event.target.files;
        //files = event.target.files;
        i++;
        console.log(files);
    }

    return this;
}

$.fn.resetear = function () {
    this.each(function() { this.reset(); });
}
