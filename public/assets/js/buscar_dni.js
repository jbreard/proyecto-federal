(function(){
    var util         = new Helper();
    $('.buscar_dni').focusout(function(){
        var dni          = $(this).val();
        var tipo_oficio  = $(this).attr("data-tipo_oficio");
        if (dni!=''){
            var url  = util.getUrl('oficios/buscar_dni');
            var data = {'dni':dni,'tipo_oficio':tipo_oficio};
            util.ajax(null,'POST',url,data,
                function(data){
                    if(data.respuesta == "true"){
                        var mensaje = 'Esta persona ya tiene un requerimiento cargado, ingrese <a target="_blank" style="color:red;text-decoration:underline" href="'+data.url+'">aqui</a>'+" para verlo";
                        //$("#guardar_form").attr("disabled","disabled");
                        if($("input:text[name=nro_documento]").attr("data-tipo") == undefined)
                        {
                            $("#mensaje").html(mensaje);
                            $("#myModal").modal("show");
                        }
                        else
                        {
                            $("#contenido-modal").html(mensaje);
                            $("#confirmacion").modal("show");
                        }


                    }else if(data.respuesta=="false"){
                        $("#guardar_form").removeAttr("disabled");
                    }
                }
            );
        }
    });
})();