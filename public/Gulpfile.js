var gulp    = require('gulp'),
    gutil    = require('gulp-util'),
    uglify  = require('gulp-uglify'),
    concat  = require('gulp-concat'),
    combiner = require('stream-combiner2'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css');







gulp.task('compress', function() {
    var combined = combiner.obj([
        gulp.src('./assets/**/*.js'),
        sourcemaps.init(),
        uglify(),
        concat('all.js'),
        sourcemaps.write(),
        gulp.dest('./dist')
    ]);

    // any errors in the above streams will get caught
    // by this listener, instead of being thrown:
    combined.on('error', console.error.bind(console));

    return combined;
});

gulp.task('compressCss', function() {
    var combined = combiner.obj([
        gulp.src('./assets/**/*.css'),
        sourcemaps.init(),
        minifyCss({compatibility: 'ie8'}),
        concat('all.css'),
        sourcemaps.write(),
        gulp.dest('./dist')
    ]);

    // any errors in the above streams will get caught
    // by this listener, instead of being thrown:
    combined.on('error', console.error.bind(console));

    return combined;
});